cmake_minimum_required(VERSION 3.0.0)
project(CuteGL)

########################### Misc. Configs ##############################
message(STATUS "=============================================================")
message(STATUS "=================== Configuring ${PROJECT_NAME} ======================")

# Add custom cmake files folder
set (PROJECT_CMAKE_DIR ${PROJECT_SOURCE_DIR}/cmake)
set (CMAKE_MODULE_PATH "${PROJECT_CMAKE_DIR}" "${CMAKE_MODULE_PATH}")

# Add custom Compile settings and flags
include(CompileSettings)

# Add custom Install settings
include(InstallSettings)

#include some custom CMake util functions
include(CommonCMakeUtils)

# Use solution folders.
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

message(STATUS "=============================================================")
message(STATUS "================= Look for Dependencies =====================")

# Find OpenGL Dependencies
find_package(OpenGL REQUIRED)

# Find QT Dependencies
find_package(Qt5OpenGL 5.4 REQUIRED)

# Find Eigen dependencies
find_package(Eigen REQUIRED)

option (CUTEGL_ASSIMP_SUPPORT "Add Assimp Support" ON)
if(CUTEGL_ASSIMP_SUPPORT)
  find_package(Assimp)
  if(NOT ASSIMP_FOUND)
    set(CUTEGL_ASSIMP_SUPPORT OFF)
    message(STATUS "Assimp Not Found. Disabling CUTEGL_ASSIMP_SUPPORT") 
  endif()
endif()

option (CUTEGL_HDF5_SUPPORT "Add HDF5 Support" ON)
if(CUTEGL_HDF5_SUPPORT)
  find_package(HDF5 1.8.12
    COMPONENTS
      CXX
    )
  if(NOT HDF5_FOUND)
    set(CUTEGL_HDF5_SUPPORT OFF)
    message(STATUS "HDF5 Not Found. Disabling CUTEGL_HDF5_SUPPORT") 
  endif()
endif()

option (CUDA_SUPPORT "Add Nvidia CUDA Support" ON)
if(CUDA_SUPPORT)
  find_package(CUDA)
  if(NOT CUDA_FOUND)
    set(CUDA_SUPPORT OFF)
    message(STATUS "Nvidia CUDA Not Found. Disabling CUDA_SUPPORT")
  else()
    cuda_include_directories("${CMAKE_SOURCE_DIR}" "${CUDA_TOOLKIT_ROOT_DIR}/include")
    
    # Make sure to compile without the -pedantic, -Wundef, -Wnon-virtual-dtor
    # and -fno-check-new flags since they trigger thousands of compilation warnings
    # in the CUDA runtime
    # Also remove -ansi that is incompatible with std=c++11.
    string(REPLACE "-pedantic" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    string(REPLACE "-Wundef" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    string(REPLACE "-Wnon-virtual-dtor" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    string(REPLACE "-fno-check-new" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    string(REPLACE "-ansi" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

    include(CudaHelper) # Add custom CUDA helper functions
    select_nvcc_arch_flags(CUDA_NVCC_FLAGS)
    
    list(APPEND CUDA_NVCC_FLAGS "-std=c++11")
    list(APPEND CUDA_NVCC_FLAGS "--expt-relaxed-constexpr")
    list(APPEND CUDA_NVCC_FLAGS "-Xcudafe \"--display_error_number\"")
    
    
    message(STATUS "Found Cuda. Using CUDA_NVCC_FLAGS=${CUDA_NVCC_FLAGS}")
  endif()
endif()

message(STATUS "=============================================================")
message(STATUS "============== Generating CuteGL Config.h ===================")

option (CUTEGL_DEBUG_CUDA "Checks CUDA calls for error" ON)
option (CUTEGL_DEBUG_GL "Checks OpenGL calls for error" ON)

# setup Config.h for build tree
set(CUTEGL_SHADER_FOLDER \"${CMAKE_SOURCE_DIR}/CuteGL/Shaders\")
set(CUTEGL_ASSETS_FOLDER \"${CMAKE_SOURCE_DIR}/CuteGL/Assets\")
configure_file( ${CMAKE_SOURCE_DIR}/cmake/Config.h.cmake ${CMAKE_SOURCE_DIR}/CuteGL/Core/Config.h )
# setup Config.h for install tree
set(CUTEGL_SHADER_FOLDER \"${INSTALL_INCLUDE_DIR}/CuteGL/Shaders\")
set(CUTEGL_ASSETS_FOLDER \"${INSTALL_INCLUDE_DIR}/CuteGL/Assets\")
configure_file( ${CMAKE_SOURCE_DIR}/cmake/Config.h.cmake ${CMAKE_BINARY_DIR}/InstallConfig.h )

set(TESTS_DATA_FOLDER_PATH \"${CMAKE_SOURCE_DIR}/tests/data\")
configure_file( ${CMAKE_SOURCE_DIR}/cmake/TestsConfig.h.cmake ${CMAKE_SOURCE_DIR}/tests/TestsConfig.h )


message(STATUS "=============================================================")
message(STATUS "================= Adding CuteGL Library =====================")

add_subdirectory(CuteGL)

list(APPEND CuteGL_LIBS Core)
list(APPEND CuteGL_LIBS Utils)
list(APPEND CuteGL_LIBS Geometry)
list(APPEND CuteGL_LIBS IO)
list(APPEND CuteGL_LIBS Drawers)
list(APPEND CuteGL_LIBS Renderer)
list(APPEND CuteGL_LIBS Shaders)
list(APPEND CuteGL_LIBS Surface)

# Builds the examples by default set to true
option (BUILD_APPS "Build Apps(Executables)" ON)

if(BUILD_APPS)
  message(STATUS "=============================================================")
  message(STATUS "======================= Adding Apps =========================")
  add_subdirectory(apps) 
endif()

############################### Add Unit Tests #################################
# Builds the Unit tests. By default set to true
option (BUILD_TESTS "Build Unit Tests" ON)
if(BUILD_TESTS)
  message (STATUS "================  Configuring Unit Tests   =================")
  # Find required Unit test Dependency
  find_package(Boost COMPONENTS
    unit_test_framework
    )

  if(${Boost_UNIT_TEST_FRAMEWORK_FOUND})    
    enable_testing() # Enable CMake CTest
    add_subdirectory(tests)
  else()
    message(STATUS "Cannot build Unit Tests: Boost Unit Test not found.")
    set(BUILD_TESTS OFF)
  endif()
  message(STATUS "=============================================================")
endif()

####################### Add Docs target ################################
message(STATUS "==============  Configuring Documentation ===================")
# Add an option to toggle the generation of the API documentation
option(BUILD_DOCUMENTATION "Use Doxygen to create the HTML based API documentation" ON)
if(BUILD_DOCUMENTATION)
  find_package(Doxygen)
  if (DOXYGEN_FOUND)
    # Configure the Template Doxyfile for our specific project
    configure_file( ${CMAKE_SOURCE_DIR}/doc/Doxyfile.in
                            ${PROJECT_BINARY_DIR}/Doxyfile
                            @ONLY IMMEDIATE )
    # Add a custom target to run Doxygen when ever the project is built
    add_custom_target( doc
                     COMMAND ${DOXYGEN_EXECUTABLE} ${PROJECT_BINARY_DIR}/Doxyfile
                     SOURCES ${PROJECT_BINARY_DIR}/Doxyfile )
    message(STATUS "Doxygen Found. Run 'make doc' to build Documentation.")
  else()
    message(STATUS "Doxygen Not Found. Documentation will not be built.")
    set(BUILD_DOCUMENTATION OFF)
  endif()
endif()


######################### Installation Stuff ###########################
# Most of Installation happens in nested CMakeLists.txt files
include(InstallProjectConfig)

###################### Add uninstall target ############################
add_custom_target(uninstall
    COMMAND ${CMAKE_COMMAND} -P ${PROJECT_CMAKE_DIR}/cmake_uninstall.cmake)


message(STATUS "=============================================================")
message(STATUS "================  Configuration Summary  ====================")
message(STATUS "Project Name:       ${PROJECT_NAME}")
MESSAGE(STATUS "C++ Compiler:       ${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION}")
MESSAGE(STATUS "C++ Standard:       C++ ${CMAKE_CXX_STANDARD}")
message(STATUS "Build type:         ${CMAKE_BUILD_TYPE}")
message(STATUS "Build type Flags:   ${CMAKE_BUILD_TYPE_FLAGS}")
message(STATUS "C++ compile flags:  ${CMAKE_CXX_FLAGS}")
message(STATUS "Install Path:       ${CMAKE_INSTALL_PREFIX}")
message(STATUS "Build Apps:         ${BUILD_APPS}")
MESSAGE(STATUS "Build Tests:        ${BUILD_TESTS}")
MESSAGE(STATUS "Build Docs:         ${BUILD_DOCUMENTATION}")
message(STATUS "QT Version:         ${Qt5OpenGL_VERSION}")
message(STATUS "Eigen Version:      ${EIGEN_VERSION}")
message(STATUS "Assimp Support:     ${CUTEGL_ASSIMP_SUPPORT} (${ASSIMP_LIBRARIES})")
message(STATUS "HDF5 Support:       ${CUTEGL_HDF5_SUPPORT} (v${HDF5_VERSION})")
message(STATUS "Cuda Support:       ${CUDA_SUPPORT} (${CUDA_VERSION})")
message(STATUS "=============================================================")
