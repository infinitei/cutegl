CuteGL
=========

Simple modern OpenGL engine based on QT5 and Eigen.

![](doc/images/logo_large.png)

Dependencies
--------------
[CuteGL] needs the dependencies like Qt (cute), [OpenGL] and [Eigen]. additional requirements are as following:

 - [CMake] - a cross-platform, open-source build system
 - [Qt] - Cross platform C++ GUI library (Ver 5.4++)
 - [Eigen] - a C++ template library for linear algebra
 - [Assimp] - Open Asset Import Library **[optional]**
 - [Doxygen] - tool for generating documentation **[optional]**


Installation
--------------
Assuming an UNIX box, You can then use the following steps:

1. Clone the repo
2. `cd /path/to/repo`
4. `mkdir build`
5. `cd build`
6. `cmake ..`
7. `make -j6`
8. `make doc` [Optional]
9. `make install` [Optional]

*Author: [Abhijit Kundu]*

  [CMake]: http://www.cmake.org/
  [Eigen]: http://eigen.tuxfamily.org/  
  [QT]: http://qt-project.org/
  [OpenGL]: http://en.wikipedia.org/wiki/OpenGL
  [CuteGL]: https://bitbucket.org/infinitei/CuteGL
  [Doxygen]: http://doxygen.org
  [Assimp]: http://www.assimp.org/
  [Abhijit Kundu]: http://abhijitkundu.info/
