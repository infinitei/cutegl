/**
 * @file demoBatchSMPLRenderer.cpp
 * @brief demoBatchSMPLRenderer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Renderer/BatchSMPLRenderer.h" // Should be included 1st
#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/Utils/QtUtils.h"

#include <QGuiApplication>
#include <memory>

int main(int argc, char **argv) {
  QGuiApplication app(argc, argv);

  using namespace CuteGL;

  std::unique_ptr<BatchSMPLRenderer> renderer(new BatchSMPLRenderer);
  renderer->setDisplayGrid(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);
  viewer.setDisplayFPS(true);
  setSwapIntervalToZero(viewer);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                           Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.showAndWaitTillExposed();


  renderer->setSMPLData(10,
                        CUTEGL_ASSETS_FOLDER"/smpl_neutral_lbs_10_207_0.h5",
                        CUTEGL_ASSETS_FOLDER"/smpl_vertex_segm24_col24_14.h5");

  renderer->smplDrawer().batchId() = 5;

  return app.exec();
}


