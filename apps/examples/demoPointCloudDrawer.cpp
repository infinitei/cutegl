/**
 * @file demoPointCloudDrawer.cpp
 * @brief demoPointCloudDrawer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/BasicRenderer.h"
#include "CuteGL/Drawers/PointCloudDrawer.h"
#include "CuteGL/Drawers/BoundingBoxDrawer.h"
#include "CuteGL/Utils/QtUtils.h"
#include <QGuiApplication>
#include <memory>

namespace CuteGL {

class PointCloudRenderer : public BasicRenderer {
 public:
  PointCloudRenderer()
      : point_cloud_drawer_(),
        point_cloud_pose_(Eigen::Isometry3f::Identity()),
        bbx_drawer_() {
  }

  PointCloudDrawer point_cloud_drawer_;
  Eigen::Isometry3f point_cloud_pose_;
  BoundingBoxDrawer bbx_drawer_;

 protected:
  virtual void init(BasicShader& shader);
  virtual void draw(BasicShader& shader);

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

void PointCloudRenderer::init(BasicShader& shader) {
  glPointSize(2.0f);
  point_cloud_drawer_.init(shader.program);

  Eigen::AlignedBox3f bbx(Eigen::Vector3f::Constant(-0.7f), Eigen::Vector3f::Constant(1.2f));
  bbx_drawer_.init(shader.program, bbx);
}

void PointCloudRenderer::draw(BasicShader& shader) {
  shader.setModelPose(point_cloud_pose_);
  point_cloud_drawer_.draw();
  bbx_drawer_.draw();
}

}  // namespace CuteGL


int main(int argc, char *argv[]) {
  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<CuteGL::PointCloudRenderer> renderer(new CuteGL::PointCloudRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  CuteGL::WindowRenderViewer viewer(renderer.get());
  viewer.setBgFgColor(Qt::white, Qt::darkGray);
  viewer.resize(1024, 768);


  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(-1.0f, 0.3f, 5.0f),
                           Eigen::Vector3f::Zero(),
                           Eigen::Vector3f::UnitY());

  /// Set swap interval to 0
  setSwapIntervalToZero(viewer);

  viewer.showAndWaitTillExposed();

  typedef CuteGL::PointCloudDrawer::VertexData VertexData;
  std::vector<VertexData> particles;
  const std::size_t num_of_points_to_add = 1e6;
  particles.reserve(num_of_points_to_add);
  for (size_t i = 0; i < num_of_points_to_add; ++i) {
    particles.emplace_back(VertexData::PositionType::Random(),
                           VertexData::ColorType::Random());
  }
  renderer->point_cloud_drawer_.addPoints(particles);

  viewer.setSceneBoundingBox(-Eigen::Vector3f::Ones(), Eigen::Vector3f::Ones());
  viewer.showEntireScene();

  return app.exec();
}



