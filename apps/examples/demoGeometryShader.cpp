/**
 * @file demoGeometryShader.cpp
 * @brief demoGeometryShader
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowOpenGLSurface.h"
#include <QOpenGLShaderProgram>

#include "CuteGL/Core/Config.h"
#include "CuteGL/Core/VertexData.h"
#include "CuteGL/Core/PoseUtils.h"

#include "CuteGL/Utils/QtUtils.h"
#include <QGuiApplication>
#include <iostream>

namespace CuteGL {

class CubeWindow : public WindowOpenGLSurface {
 public:
  CubeWindow();
  ~CubeWindow();

 protected:
  void init();
  void draw();
  void teardownGL();

 private:
  // OpenGL State Information
  GLuint vbo_;
  GLuint vao_;
  QOpenGLShaderProgram program_;
  int uniform_view_T_world_loc_;
  Eigen::Isometry3f model_pose_;

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

CubeWindow::CubeWindow()
    : vbo_(0),
      vao_(0),
      program_(),
      uniform_view_T_world_loc_(-1),
      model_pose_(Eigen::Isometry3f::Identity()) {

  // Set camera pose via lookAt
  camera().extrinsics() = getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 5.0f),
                                                  Eigen::Vector3f::Zero(),
                                                  Eigen::Vector3f::UnitY());
}

CubeWindow::~CubeWindow() {
  makeCurrent();
  teardownGL();
  doneCurrent();
}

void CubeWindow::teardownGL() {
  glfuncs_->glDeleteVertexArrays(1, &vao_);
  glfuncs_->glDeleteBuffers(1, &vbo_);
}

void CubeWindow::init() {
  glfuncs_->glEnable(GL_CULL_FACE); // Enable Cull  face

  // Application specific initialization
  {
    program_.addShaderFromSourceFile(QOpenGLShader::Vertex, CUTEGL_SHADER_FOLDER "/geometry_cube_vs.glsl");
    program_.addShaderFromSourceFile(QOpenGLShader::Geometry, CUTEGL_SHADER_FOLDER "/geometry_cube_gs.glsl");
    program_.addShaderFromSourceFile(QOpenGLShader::Fragment, CUTEGL_SHADER_FOLDER "/geometry_cube_fs.glsl");
    program_.link();
    program_.bind();

    uniform_view_T_world_loc_ = program_.uniformLocation("view_T_model");

    using VertexData = VertexXYZRGBA;
    using PositionType = VertexData::PositionType;
    using ColorType = VertexData::ColorType;

    std::vector<VertexData> vertices;

    // Front Verticies
    const float width = 2.0f;
    vertices.emplace_back(PositionType( width,  width,  0.0f), ColorType( 255, 0, 0, 255 ));
    vertices.emplace_back(PositionType(-width,  width,  0.0f), ColorType( 0, 255, 0, 255 ));
    vertices.emplace_back(PositionType(-width, -width,  0.0f), ColorType( 0, 0, 255, 255 ));
    vertices.emplace_back(PositionType( width, -width,  0.0f), ColorType( 255, 255, 255, 255 ));


    // 1. Bind VAO
    glfuncs_->glGenVertexArrays(1, &vao_);
    glfuncs_->glBindVertexArray(vao_);

    // 2. Copy our vertices array in a vertex buffer for OpenGL to use
    glfuncs_->glGenBuffers(1, &vbo_);
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glfuncs_->glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexData), vertices.data(), GL_STATIC_DRAW);

    // 3. Then set the vertex attributes pointers

    // Position attribute
    const int position_index = program_.attributeLocation("vertex_position");
    assert(position_index != -1);
    program_.setAttributeArray(position_index, GL_FLOAT, (void*) offsetof(VertexData, position), 3, sizeof(VertexData));
    program_.enableAttributeArray(position_index);

    // Color attribute
    const int color_index = program_.attributeLocation("vertex_color");
    assert(color_index != -1);
    program_.setAttributeArray(color_index, GL_UNSIGNED_BYTE, (void*) offsetof(VertexData, color), 4, sizeof(VertexData));
    program_.enableAttributeArray(color_index);

    // We can unbind VBO not IBO
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
    glfuncs_->glBindVertexArray(0);

    program_.release();

  }
}

void CubeWindow::draw() {
  program_.bind();

  Eigen::Matrix4f proj_view_model_matrix = camera().projectionViewMatrix() * model_pose_.matrix();
  glfuncs_->glUniformMatrix4fv(uniform_view_T_world_loc_, 1, GL_FALSE, proj_view_model_matrix.data());
  {
    glfuncs_->glBindVertexArray(vao_);
    glfuncs_->glDrawArrays(GL_POINTS, 0, 4);
    glfuncs_->glBindVertexArray(0);
  }
  program_.release();
}


}  // namespace CuteGL


int main(int argc, char *argv[]) {
  QGuiApplication app(argc, argv);

  // Set the window up
  CuteGL::CubeWindow cube_window;

//  cube_window.resize(800, 600);
  cube_window.setDisplayFPS(true);

  /// Set swap interval to 0
  setSwapIntervalToZero(cube_window);

  cube_window.show();
  return app.exec();
}



