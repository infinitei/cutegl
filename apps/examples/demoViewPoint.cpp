/**
 * @file demoViewPoint.cpp
 * @brief demoViewPoint
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Renderer/MultiObjectRenderer.h"
#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/IO/ImportPLY.h"
#include "CuteGL/Core/PoseUtils.h"
#include "SlidersUI.h"

#include <Eigen/EulerAngles>

#include <QApplication>
#include <iostream>

int main(int argc, char **argv) {

  QApplication app(argc, argv);

  using namespace CuteGL;
  using namespace Eigen;

  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());

  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  const int W = 1024;
  const int H = 768;
  viewer.resize(W, H);

  viewer.setSceneRadius(10.0f);

  viewer.camera().extrinsics() = getExtrinsicsFromViewPoint(0.0f, 0.0f, 0.0f, 2.0f);

  viewer.showAndWaitTillExposed();

  renderer->modelDrawers().addItem(Affine3f::Identity(), loadMeshFromPLY(CUTEGL_ASSETS_FOLDER "/car.ply"));

  SlidersUI ss(3, -180, 180, 0, "ViewPoint");
  ss.show();

  renderer->modelDrawers().poses().front().setIdentity();
  QObject::connect(&ss, &SlidersUI::valueChanged, [&]() {
    Eigen::VectorXf viewpoint = ss.value().cast<float>() * M_PI / 180.0f;

    viewer.camera().extrinsics() = getExtrinsicsFromViewPoint(viewpoint.x(), viewpoint.y(), viewpoint.z(), 2.0f);
  });

  app.exec();
}


