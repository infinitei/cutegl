/**
 * @file demoModelDrawer.cpp
 * @brief demoModelDrawer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/PhongRenderer.h"
#include "CuteGL/Drawers/ModelDrawer.h"
#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/IO/ImportViaAssimp.h"

#include <QGuiApplication>
#include <memory>
#include <iostream>

namespace CuteGL {

class MeshRenderer : public PhongRenderer {
 public:
  MeshRenderer(const std::vector<MeshData>& meshes)
      : model_mat_(Eigen::Affine3f::Identity()),
        meshes_(meshes) {
  }

  const Eigen::Affine3f& modelMat() const {
    return model_mat_;
  }
  Eigen::Affine3f& modelMat() {
    return model_mat_;
  }

 protected:
  virtual void init(PhongShader& shader);
  virtual void draw(PhongShader& shader);
 private:
  Eigen::Affine3f model_mat_;
  ModelDrawer model_drawer_;
  std::vector<MeshData> meshes_;

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

void MeshRenderer::init(PhongShader& shader) {
  glEnable(GL_CULL_FACE);  // Enable Cull  face
  model_drawer_.init(shader.program, meshes_);
}

void MeshRenderer::draw(PhongShader& shader) {
  shader.setModelPose(model_mat_);
  model_drawer_.draw();
}

}  // namespace CuteGL


int main(int argc, char **argv) {
  using namespace CuteGL;

  std::string asset_file = CUTEGL_ASSETS_FOLDER "/Axis.nff";

  std:: cout << "Loding model (via Assimp) from " << asset_file << "\n";
  // Load mesh data from OFF file
  std::vector<MeshData> meshes = loadMeshesViaAssimp(asset_file);

  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<MeshRenderer> renderer(new MeshRenderer(meshes));
  renderer->setDisplayGrid(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);

  // Set camera pose via lookAt
  viewer.camera().extrinsics() = getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 2.0f),
                                                         Eigen::Vector3f::Zero(),
                                                         Eigen::Vector3f::UnitY());

  viewer.show();
  return app.exec();
}
