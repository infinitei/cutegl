/**
 * @file demoAxisDrawer.cpp
 * @brief demoAxisDrawer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/BasicRenderer.h"
#include "CuteGL/Drawers/AxisDrawer.h"

#include <QGuiApplication>
#include <iostream>

namespace CuteGL {

class AxisRenderer : public BasicRenderer {
 public:
  AxisRenderer()
   :axis_drawer_() {
  }

 protected:
  virtual void init(BasicShader& shader);
  virtual void draw(BasicShader& shader);
 private:
  AxisDrawer axis_drawer_;
};

void AxisRenderer::init(BasicShader& shader) {
  glEnable(GL_CULL_FACE);  // Enable Cull  face
  axis_drawer_.init(shader.program);
}

void AxisRenderer::draw(BasicShader& shader) {
  Eigen::Affine3f model_mat = Eigen::Affine3f::Identity();

  shader.setModelPose(model_mat);
  axis_drawer_.draw();


  model_mat.translate(Eigen::Vector3f(0.5f, 0.3f, 0.4f));
  shader.setModelPose(model_mat);
  axis_drawer_.draw();
}

}  // namespace CuteGL


int main(int argc, char **argv) {
  using namespace CuteGL;
  using Eigen::Vector3f;

  QGuiApplication app(argc, argv);


  // Create the Renderer
  std::unique_ptr <AxisRenderer> renderer(new AxisRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);


  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Vector3f(0.0f, 0.0f, 5.0f),
                           Vector3f::Zero(),
                           Vector3f::UnitY());

  viewer.show();
  return app.exec();
}
