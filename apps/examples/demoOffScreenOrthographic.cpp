/**
 * @file demoOffScreenOrthographic.cpp
 * @brief demoOffScreenOrthographic
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/OffScreenRenderViewer.h"
#include "CuteGL/Renderer/MultiObjectRenderer.h"
#include "CuteGL/Geometry/ComputeNormals.h"
#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Core/Config.h"

#include <QGuiApplication>
#include <QTime>
#include <iostream>
#include <memory>

int main(int argc, char **argv) {
  using namespace CuteGL;
  using Eigen::Vector3f;
  using Eigen::Affine3f;

  const Eigen::IOFormat fmt(4, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  QGuiApplication app(argc, argv);
  // Create the Renderer
  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(true);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(255, 255, 255);

  QSurfaceFormat format = viewer.format();
  format.setSamples(1);
  viewer.setFormat(format);

  const int W = 51;
  const int H = 49;

  const Eigen::Vector2f middle_point(W/2.0f, H/2.0f);

  viewer.resize(W, H);

  // z position of the camera center
  const float z = 5.0f;

  // Set camera pose via lookAt
  const Eigen::Isometry3f extrinsic = CuteGL::getExtrinsicsFromLookAt(Vector3f(0.0f, 0.0f, -z),
                                                                      Vector3f::Zero(),
                                                                      -Vector3f::UnitY());
  viewer.camera().extrinsics() = extrinsic;

  std::cout << "viewer.camera().extrinsics() = \n" << viewer.camera().extrinsics().matrix() << "\n";

  Eigen::Matrix4f ortho = CuteGL::getGLOrthographicProjection(1.0f, 1.0f, 4.0f, 6.0f);
  viewer.camera().intrinsics() = ortho;

  {
    std::cout << "\n----------------orthographic projection--------------\n";

    std::cout << "ortho = \n" << ortho << "\n";

    {
      Eigen::Vector3f world_frame(0, 0, 0);
      Eigen::Vector3f cam_frame = extrinsic * world_frame;
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_frame = " << world_frame.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    {
      Eigen::Vector3f world_frame(-1.0, -1.0, 0);
      Eigen::Vector3f cam_frame = extrinsic * world_frame;
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_frame = " << world_frame.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    {
      Eigen::Vector3f world_frame(-1.0, 1.0, 0);
      Eigen::Vector3f cam_frame = extrinsic * world_frame;
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_frame = " << world_frame.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    {
      Eigen::Vector3f world_frame(1.0, 1.0, 0);
      Eigen::Vector3f cam_frame = extrinsic * world_frame;
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_frame = " << world_frame.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    {
      Eigen::Vector3f world_frame(1.0, -1.0, 0);
      Eigen::Vector3f cam_frame = extrinsic * world_frame;
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_frame = " << world_frame.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

  }



  viewer.create();

  viewer.makeCurrent();

  {
    MeshData md;

    md.vertices.resize(3);
    md.vertices[0].position = MeshData::PositionType(0.5f, -0.5f, 0.0f);
    md.vertices[0].color = MeshData::ColorType(255, 0, 0, 255);

    md.vertices[1].position = MeshData::PositionType(-0.5f, -0.5f, 0.0f);
    md.vertices[1].color = MeshData::ColorType(0, 255, 0, 255);

    md.vertices[2].position = MeshData::PositionType(0.0f, 0.5f, 0.0f);
    md.vertices[2].color = MeshData::ColorType(0, 0, 255, 255);

    md.faces.resize(1, 3);
    md.faces << 0, 2, 1;  // First Triangle

    computeNormals(md);

    renderer->modelDrawers().addItem(Affine3f::Identity(), md);
  }

  {
    using VertexData = CuteGL::PointCloudDrawer::VertexData;
    using PositionType = VertexData::PositionType;
    using ColorType = VertexData::ColorType;

    std::vector<VertexData> particles;
    ColorType point_color(10, 200, 98, 255);

    double resx = 2.0 / W;
    double resy = 2.0 / H;

    double lx = -1.0 + 0.5 * resx;
    double ly = -1.0 + 0.5 * resy;
    double hx = -1.0 + (W - 1 + 0.5) * resx;
    double hy = -1.0 + (H - 1 + 0.5) * resy;


    double pt_z = -0.5f;

    particles.emplace_back(PositionType(0, 0, pt_z), point_color);
    particles.emplace_back(PositionType(hx, hy, pt_z), point_color);
    particles.emplace_back(PositionType(hx, ly, pt_z), point_color);
    particles.emplace_back(PositionType(lx, hy, pt_z), point_color);
    particles.emplace_back(PositionType(lx, ly, pt_z), point_color);


    renderer->pointCloudDrawers().addItem(Eigen::Affine3f::Identity(), particles);


    PositionType world_point_TL(lx, ly, 0.0);
    PositionType world_point_BR(hx, hy, 0.0);

    {
      Eigen::Vector3f cam_frame = extrinsic * world_point_TL;
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_point_TL = " << world_point_TL.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    {
      Eigen::Vector3f cam_frame = extrinsic * world_point_BR;
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_point_BR = " << world_point_BR.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }




    using Line = std::pair<PositionType, PositionType>;
    std::vector<Line> lines;
    lines.emplace_back(world_point_TL, world_point_BR);

    ColorType line_color(255, 100, 50, 255);
    renderer->lineDrawers().addItem(Affine3f::Identity(), lines, line_color);


    Eigen::AlignedBox3f bbx(Eigen::Vector3f(lx, ly, -1.0f), Eigen::Vector3f(hx, hy, 1.0f));
    renderer->bbxDrawers().addItem(Affine3f::Identity(), bbx);
  }

  viewer.render();

  {
    QImage image = viewer.readColorBuffer();
    image.save(QString("ortho_color_buffer.png"));
  }

  {
    QImage image = viewer.readNormalBuffer();
    image.save(QString("ortho_normal_buffer.png"));
  }

#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
  {
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> depth_data(viewer.height(), viewer.width());
    viewer.readDepthBuffer(depth_data.data());
    depth_data.colwise().reverseInPlace();

    const float max_depth = 6.0f;
    depth_data.array() *= 255.0f / max_depth;
    Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> depth_data_8CU = depth_data.cast<unsigned char>();
    QImage image(depth_data_8CU.data(), depth_data.cols(), depth_data.rows(), QImage::Format_Grayscale8);
    image.save(QString("ortho_depth_buffer.png"));
  }

  {
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> depth_data(viewer.height(), viewer.width());
    viewer.readZBuffer(depth_data.data());
    depth_data.colwise().reverseInPlace();

    const float max_depth = 1.0f;
    depth_data.array() *= 255.0f / max_depth;
    Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> depth_data_8CU = depth_data.cast<unsigned char>();
    QImage image(depth_data_8CU.data(), depth_data.cols(), depth_data.rows(), QImage::Format_Grayscale8);
    image.save(QString("ortho_z_buffer.png"));
  }
#endif

  return EXIT_SUCCESS;
}
