/**
 * @file demoMeshRenderer.cpp
 * @brief demoMeshRenderer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/BasicRenderer.h"
#include "CuteGL/Drawers/MeshDrawer.h"
#include "CuteGL/Core/Config.h"

#include <QGuiApplication>
#include <memory>
#include <iostream>

namespace CuteGL {

using MeshType = Mesh<float, float, unsigned char>;

class MeshRenderer : public BasicRenderer {
 public:
  MeshRenderer(const MeshType& mesh_data)
   :mesh_drawer_(),
    mesh_data_(mesh_data),
    model_mat_(Eigen::Affine3f::Identity()) {
  }

  const Eigen::Affine3f& modelMat() const {return model_mat_;}
  Eigen::Affine3f& modelMat() {return model_mat_;}

 protected:
  virtual void init(BasicShader& shader);
  virtual void draw(BasicShader& shader);
 private:
  MeshDrawer mesh_drawer_;
  MeshType mesh_data_;
  Eigen::Affine3f model_mat_;

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

void MeshRenderer::init(BasicShader& shader) {
  glEnable(GL_CULL_FACE);  // Enable Cull  face
  mesh_drawer_.init(shader.program, mesh_data_);
}

void MeshRenderer::draw(BasicShader& shader) {
  shader.setModelPose(model_mat_);
  mesh_drawer_.draw();
}

}  // namespace CuteGL


int main(int argc, char **argv) {
  using namespace CuteGL;
  using Eigen::Vector3f;

  QGuiApplication app(argc, argv);

  MeshType md;

  md.positions.resize(3, Eigen::NoChange);
  md.positions << 0.5f, -0.5f, 0.0f,
                  -0.5f, -0.5f, 0.0f,
                  0.0f,  0.5f, 0.0f;

  md.colors.resize(3, Eigen::NoChange);
  md.colors << 255, 0, 0, 255,
               0, 255, 0, 255,
               0, 0, 255, 255;

  md.faces.resize(1, 3);
  md.faces << 0, 2, 1;  // First Triangle

  // Create the Renderer
  std::unique_ptr <MeshRenderer> renderer(new MeshRenderer(md));
  renderer->setDisplayGrid(true);
  renderer->modelMat() = Eigen::UniformScaling<float>(1.00);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Vector3f(0.0f, 0.0f, 5.0f),
                           Vector3f::Zero(),
                           Vector3f::UnitY());

  viewer.show();
  return app.exec();
}
