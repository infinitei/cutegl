/**
 * @file demoOffScreenPerspective.cpp
 * @brief demoOffScreenPerspective
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/OffScreenRenderViewer.h"
#include "CuteGL/Renderer/MultiObjectRenderer.h"
#include "CuteGL/Geometry/ComputeNormals.h"
#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Core/Config.h"

#include <QGuiApplication>
#include <QTime>
#include <iostream>
#include <memory>

int main(int argc, char **argv) {
  using namespace CuteGL;
  using Eigen::Vector3f;
  using Eigen::Affine3f;

  const Eigen::IOFormat fmt(4, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  QGuiApplication app(argc, argv);
  // Create the Renderer
  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(true);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(255, 255, 255);

  QSurfaceFormat format = viewer.format();
  format.setSamples(1);
  viewer.setFormat(format);

  const int W = 51;
  const int H = 49;

  const Eigen::Vector2f middle_point(W/2.0f, H/2.0f);

  viewer.resize(W, H);

  // z position of the camera center
  const float z = 5.0f;

  // Set camera pose via lookAt
  const Eigen::Isometry3f extrinsic = CuteGL::getExtrinsicsFromLookAt(Vector3f(0.0f, 0.0f, -z),
                                                                      Vector3f::Zero(),
                                                                      -Vector3f::UnitY());
  viewer.camera().extrinsics() = extrinsic;

  std::cout << "viewer.camera().extrinsics() = \n" << viewer.camera().extrinsics().matrix() << "\n";


  float fx = W * z / 2.0f; // Set fx so that (1, 0, 0) is the right most point
  float fy = H * z / 2.0f; // Set fy so that (0, 1, 0) is the top most point

  Eigen::Vector2f principal_point(W/2.0f, H/2.0f);
  Eigen::Vector2f focal_length(fx, fy);


  Eigen::Matrix3f K;
  K << fx,  0, principal_point.x(),
        0, fy, principal_point.y(),
        0,  0,    1;

  std::cout << "K = \n" << K << "\n";

  Eigen::Matrix4f persp = CuteGL::getGLPerspectiveProjection(K, W, H, 4.0f, 6.0f);

  {
    Eigen::Vector2f img_point(W/2.0f, H/2.0f);
    Eigen::Vector3f cam_frame = z * ((img_point - principal_point).cwiseQuotient(focal_length)).homogeneous();
    std::cout << "cam_frame = " << cam_frame.format(fmt) << "\n";
  }
  {
    Eigen::Vector2f img_point(W, H);
    Eigen::Vector3f cam_frame = z * ((img_point - principal_point).cwiseQuotient(focal_length)).homogeneous();
    std::cout << "cam_frame = " << cam_frame.format(fmt) << "\n";
  }
  {
    Eigen::Vector2f img_point(0, 0);
    Eigen::Vector3f cam_frame = z * ((img_point - principal_point).cwiseQuotient(focal_length)).homogeneous();
    std::cout << "cam_frame = " << cam_frame.format(fmt) << "\n";
  }

  viewer.camera().intrinsics() = persp;

  {
    std::cout << "\n----------------persp projection--------------\n";

    std::cout << "persp = \n" << persp << "\n";

    {
      Eigen::Vector3f world_frame(0, 0, 0);
      Eigen::Vector3f cam_frame = extrinsic * world_frame;
      Eigen::Vector4f clip = persp * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_frame = " << world_frame.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    {
      Eigen::Vector3f world_frame(-1.0, -1.0, 0);
      Eigen::Vector3f cam_frame = extrinsic * world_frame;
      Eigen::Vector4f clip = persp * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_frame = " << world_frame.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    {
      Eigen::Vector3f world_frame(-1.0, 1.0, 0);
      Eigen::Vector3f cam_frame = extrinsic * world_frame;
      Eigen::Vector4f clip = persp * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_frame = " << world_frame.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    {
      Eigen::Vector3f world_frame(1.0, 1.0, 0);
      Eigen::Vector3f cam_frame = extrinsic * world_frame;
      Eigen::Vector4f clip = persp * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_frame = " << world_frame.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    {
      Eigen::Vector3f world_frame(1.0, -1.0, 0);
      Eigen::Vector3f cam_frame = extrinsic * world_frame;
      Eigen::Vector4f clip = persp * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_frame = " << world_frame.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

  }


  {
    Eigen::Vector2f im_frame(0, 0);
    Eigen::Vector3f ray_dir = K.inverse() * im_frame.homogeneous();
    Eigen::Vector3f cam_frame = ray_dir * (z / ray_dir.z());

    Eigen::Vector4f clip = persp * cam_frame.homogeneous();
    Eigen::Vector3f ndc = clip.hnormalized();
    Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
    std::cout << "im_frame = " << im_frame.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
  }



  viewer.create();

  viewer.makeCurrent();

  {
    MeshData md;

    md.vertices.resize(3);
    md.vertices[0].position = MeshData::PositionType(0.5f, -0.5f, 0.0f);
    md.vertices[0].color = MeshData::ColorType(255, 0, 0, 255);

    md.vertices[1].position = MeshData::PositionType(-0.5f, -0.5f, 0.0f);
    md.vertices[1].color = MeshData::ColorType(0, 255, 0, 255);

    md.vertices[2].position = MeshData::PositionType(0.0f, 0.5f, 0.0f);
    md.vertices[2].color = MeshData::ColorType(0, 0, 255, 255);

    md.faces.resize(1, 3);
    md.faces << 0, 2, 1;  // First Triangle

    computeNormals(md);

    renderer->modelDrawers().addItem(Affine3f::Identity(), md);
  }

  {
    using VertexData = CuteGL::PointCloudDrawer::VertexData;
    using PositionType = VertexData::PositionType;
    using ColorType = VertexData::ColorType;

    std::vector<VertexData> particles;
    ColorType point_color(10, 200, 98, 255);

    PositionType world_point_TL;
    PositionType world_point_BR;

    PositionType world_point_O;

    {
      Eigen::Vector2f im_frame(0.5, 0.5);
      Eigen::Vector3f ray_dir = K.inverse() * im_frame.homogeneous();
      Eigen::Vector3f cam_frame = ray_dir * (z / ray_dir.z());
      world_point_TL = extrinsic.inverse() * cam_frame;
    }

    {
      Eigen::Vector2f im_frame(W-0.5, H-0.5);
      Eigen::Vector3f ray_dir = K.inverse() * im_frame.homogeneous();
      Eigen::Vector3f cam_frame = ray_dir * (z / ray_dir.z());
      world_point_BR = extrinsic.inverse() * cam_frame;
    }

    {
      Eigen::Vector2f im_frame(W/2.0, H/2.0);
      Eigen::Vector3f ray_dir = K.inverse() * im_frame.homogeneous();
      Eigen::Vector3f cam_frame = ray_dir * (4.5 / ray_dir.z());
      world_point_O = extrinsic.inverse() * cam_frame;
    }

    double lx = world_point_TL.x();
    double ly = world_point_TL.y();
    double hx = world_point_BR.x();
    double hy = world_point_BR.y();


    particles.emplace_back(world_point_O, point_color);
    particles.emplace_back(PositionType(hx, hy, 0), point_color);
    particles.emplace_back(PositionType(hx, ly, 0), point_color);
    particles.emplace_back(PositionType(lx, hy, 0), point_color);
    particles.emplace_back(PositionType(lx, ly, 0), point_color);


    renderer->pointCloudDrawers().addItem(Eigen::Affine3f::Identity(), particles);


    {
      Eigen::Vector3f cam_frame = extrinsic * world_point_TL;
      Eigen::Vector4f clip = persp * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_point_TL = " << world_point_TL.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    {
      Eigen::Vector3f cam_frame = extrinsic * world_point_BR;
      Eigen::Vector4f clip = persp * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_point_BR = " << world_point_BR.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    {
      Eigen::Vector3f cam_frame = extrinsic * world_point_O;
      Eigen::Vector4f clip = persp * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "world_point_O = " << world_point_O.format(fmt) << " cam_frame = " << cam_frame.format(fmt) << " ndc = " << ndc.format(fmt) << " screen= " << screen.format(fmt) << "\n";
    }

    using Line = std::pair<PositionType, PositionType>;
    std::vector<Line> lines;
    lines.emplace_back(world_point_TL, world_point_BR);

    ColorType line_color(255, 100, 50, 255);
    renderer->lineDrawers().addItem(Affine3f::Identity(), lines, line_color);
  }

  viewer.render();

  {
    QImage image = viewer.readColorBuffer();
    image.save(QString("persp_color_buffer.png"));
  }

  {
    QImage image = viewer.readNormalBuffer();
    image.save(QString("persp_normal_buffer.png"));
  }

#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
  {
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> depth_data(viewer.height(), viewer.width());
    viewer.readDepthBuffer(depth_data.data());
    depth_data.colwise().reverseInPlace();

    const float max_depth = 6.0f;
    depth_data.array() *= 255.0f / max_depth;
    Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> depth_data_8CU = depth_data.cast<unsigned char>();
    QImage image(depth_data_8CU.data(), depth_data.cols(), depth_data.rows(), QImage::Format_Grayscale8);
    image.save(QString("persp_depth_buffer.png"));
  }

  {
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> depth_data(viewer.height(), viewer.width());
    viewer.readZBuffer(depth_data.data());
    depth_data.colwise().reverseInPlace();

    const float max_depth = 1.0f;
    depth_data.array() *= 255.0f / max_depth;
    Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> depth_data_8CU = depth_data.cast<unsigned char>();
    QImage image(depth_data_8CU.data(), depth_data.cols(), depth_data.rows(), QImage::Format_Grayscale8);
    image.save(QString("persp_z_buffer.png"));
  }
#endif

  return EXIT_SUCCESS;
}
