/**
 * @file demoOffScreenMeshRenderer.cpp
 * @brief demoOffScreenMeshRenderer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/OffScreenRenderViewer.h"
#include "CuteGL/Renderer/BasicRenderer.h"
#include "CuteGL/Drawers/MeshDrawer.h"

#include <QGuiApplication>
#include <memory>
#include <iostream>

namespace CuteGL {

using MeshType = Mesh<float, float, unsigned char, int>;

class MeshRenderer : public BasicRenderer {
 public:
  MeshRenderer(const MeshType& mesh_data)
   :mesh_drawer_(),
    mesh_data_(mesh_data),
    model_mat_(Eigen::Affine3f::Identity()) {
  }

  const Eigen::Affine3f& modelMat() const {return model_mat_;}
  Eigen::Affine3f& modelMat() {return model_mat_;}

 protected:
  virtual void init(BasicShader& shader);
  virtual void draw(BasicShader& shader);
 private:
  MeshDrawer mesh_drawer_;
  MeshType mesh_data_;
  Eigen::Affine3f model_mat_;

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

void MeshRenderer::init(BasicShader& shader) {
  glEnable(GL_CULL_FACE);  // Enable Cull  face
  mesh_drawer_.init(shader.program, mesh_data_);
}

void MeshRenderer::draw(BasicShader& shader) {
  shader.setModelPose(model_mat_);
  mesh_drawer_.draw();
}

}  // namespace CuteGL

int main(int argc, char **argv) {
  QGuiApplication app(argc, argv);
  using namespace CuteGL;
  using Eigen::Vector3f;

  MeshType md;

  md.positions.resize(3, Eigen::NoChange);
  md.positions << 0.5f, -0.5f, 0.0f,
                  -0.5f, -0.5f, 0.0f,
                  0.0f,  0.5f, 0.0f;

  md.colors.resize(3, Eigen::NoChange);
  md.colors << 255, 0, 0, 255,
               0, 255, 0, 255,
               0, 0, 255, 255;

  md.labels.resize(3);
  md.labels << 100, 100, 100;

  md.faces.resize(1, 3);
  md.faces << 0, 2, 1;  // First Triangle

  // Create the Renderer
  std::unique_ptr<MeshRenderer> renderer(new MeshRenderer(md));
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  const int W = 1024;
  const int H = 768;
  viewer.resize(W, H);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Vector3f(0.0f, 0.0f, 5.0f),
                           Vector3f::Zero(),
                           Vector3f::UnitY());

  viewer.create();
  viewer.makeCurrent();

  viewer.render();

  using Image = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

  {
    Image zbuffer(H, W);
    viewer.readZBuffer(zbuffer.data());
    zbuffer.colwise().reverseInPlace();

    QImage image(W, H, QImage::Format_RGB888);
    for (int x = 0; x < W; ++x) {
      for (int y = 0; y < H; ++y) {
        int cval = zbuffer(y, x) * 100;
        image.setPixel(x, y, qRgb(cval, cval, cval));
      }
    }
    image.save(QString("OffScreenMeshRenderer_zbufer.png"));
  }

  {
    QImage image = viewer.readNormalBuffer();
    image.save(QString("OffScreenMeshRenderer_normal.png"));
  }


  {
    QImage image = viewer.readColorBuffer();
    image.save(QString("OffScreenMeshRenderer_color.png"));
  }

  {
    Image buffer(H, W);
    viewer.readDepthBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    QImage image(W, H, QImage::Format_RGB888);
    for (int x = 0; x < W; ++x) {
      for (int y = 0; y < H; ++y) {
        int cval = buffer(y, x) * 100;
        image.setPixel(x, y, qRgb(cval, cval, cval));
      }
    }
    image.save(QString("OffScreenMeshRenderer_depth.png"));
  }

  {
    Image buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    std::cout << buffer.minCoeff() << "  " << buffer.maxCoeff() << "\n";

    QImage image(W, H, QImage::Format_RGB888);
    for (int x = 0; x < W; ++x) {
      for (int y = 0; y < H; ++y) {
        int cval = buffer(y, x) * 1.0;
        image.setPixel(x, y, qRgb(cval, cval, cval));
      }
    }
    image.save(QString("OffScreenMeshRenderer_label.png"));
  }


  return EXIT_SUCCESS;
}


