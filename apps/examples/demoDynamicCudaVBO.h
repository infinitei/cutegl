/**
 * @file demoDynamicCudaVBO.h
 * @brief demoDynamicCudaVBO
 *
 * @author Abhijit Kundu
 */

#ifndef APPS_EXAMPLES_DEMODYNAMICCUDAVBO_H_
#define APPS_EXAMPLES_DEMODYNAMICCUDAVBO_H_

#include "CuteGL/Utils/CudaUtils.h"

namespace CuteGL {

void launch_kernel(float3 *pos, unsigned int mesh_width, unsigned int mesh_height, float time);

}  // namespace CuteGL


#endif // end APPS_EXAMPLES_DEMODYNAMICCUDAVBO_H_float
