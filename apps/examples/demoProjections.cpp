/**
 * @file demoProjections.cpp
 * @brief demoProjections
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Core/PoseUtils.h"
#include <iostream>


int main(int argc, char **argv) {


  const int W = 15;
  const int H = 11;

  std::cout << "W = " << W << " H = " << H << "\n";

  const Eigen::Vector2f middle_point(W/2.0f, H/2.0f);

  {
    std::cout << "\n----------------perspective projection--------------\n";

    const float z = 5.0f;

    float fx = W * z / 2.0f; // Set fx so that (1, 0, 0) is the right most point
    float fy = H * z / 2.0f; // Set fy so that (0, 1, 0) is the top most point

    Eigen::Vector2f principal_point(W/2.0f, H/2.0f);
    Eigen::Vector2f focal_length(fx, fy);

    Eigen::Matrix3f K;
    K << fx,  0, principal_point.x(),
          0, fy, principal_point.y(),
          0,  0,    1;

    std::cout << "K = \n" << K << "\n";

    {
      Eigen::Vector2f img_point(W/2.0f, H/2.0f);
      Eigen::Vector3f cam_frame = z * ((img_point - principal_point).cwiseQuotient(focal_length)).homogeneous();
      std::cout << "img_point = " << img_point.transpose() << " cam_frame = " << cam_frame.transpose() << "\n";
    }
    {
      Eigen::Vector2f img_point(W, H);
      Eigen::Vector3f cam_frame = z * ((img_point - principal_point).cwiseQuotient(focal_length)).homogeneous();
      std::cout << "img_point = " << img_point.transpose() << " cam_frame = " << cam_frame.transpose() << "\n";
    }
    {
      Eigen::Vector2f img_point(0, 0);
      Eigen::Vector3f cam_frame = z * ((img_point - principal_point).cwiseQuotient(focal_length)).homogeneous();
      std::cout << "img_point = " << img_point.transpose() << " cam_frame = " << cam_frame.transpose() << "\n";
    }

    Eigen::Matrix4f persp = CuteGL::getGLPerspectiveProjection(K, W, H, 4.0f, 6.0f);
    std::cout << "persp = \n" << persp << "\n";

    {
      Eigen::Vector3f cam_frame(0, 0, 5);
      Eigen::Vector4f clip = persp * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "cam_frame = " << cam_frame.transpose() << " ndc = " << ndc.transpose() << " screen= " << screen.transpose() << "\n";
    }

    {
      Eigen::Vector3f cam_frame(-1.0, -1.0, 5);
      Eigen::Vector4f clip = persp * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "cam_frame = " << cam_frame.transpose() << " ndc = " << ndc.transpose() << " screen= " << screen.transpose() << "\n";
    }

    {
      Eigen::Vector3f cam_frame(1.0, -1.0, 5);
      Eigen::Vector4f clip = persp * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "cam_frame = " << cam_frame.transpose() << " ndc = " << ndc.transpose() << " screen= " << screen.transpose() << "\n";
    }

  }


  {
    std::cout << "\n----------------orthographic projection--------------\n";
    Eigen::Matrix4f ortho = CuteGL::getGLOrthographicProjection(1.0f, 1.0f, 4.0f, 6.0f);

    std::cout << "ortho = \n" << ortho << "\n";

    {
      Eigen::Vector3f cam_frame(0, 0, 5);
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "cam_frame = " << cam_frame.transpose() << " ndc = " << ndc.transpose() << " screen= " << screen.transpose() << "\n";
    }

    {
      Eigen::Vector3f cam_frame(-1.0, -1.0, 5);
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "cam_frame = " << cam_frame.transpose() << " ndc = " << ndc.transpose() << " screen= " << screen.transpose() << "\n";
    }

    {
      Eigen::Vector3f cam_frame(-1.0, 1.0, 5);
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "cam_frame = " << cam_frame.transpose() << " ndc = " << ndc.transpose() << " screen= " << screen.transpose() << "\n";
    }

    {
      Eigen::Vector3f cam_frame(1.0, 1.0, 5);
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "cam_frame = " << cam_frame.transpose() << " ndc = " << ndc.transpose() << " screen= " << screen.transpose() << "\n";
    }

    {
      Eigen::Vector3f cam_frame(-1.0, 1.0, 5);
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "cam_frame = " << cam_frame.transpose() << " ndc = " << ndc.transpose() << " screen= " << screen.transpose() << "\n";
    }

    {
      Eigen::Vector3f cam_frame(1.0, -1.0, 5);
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      std::cout << "cam_frame = " << cam_frame.transpose() << " ndc = " << ndc.transpose() << " screen= " << screen.transpose() << "\n";
    }

  }


  return EXIT_SUCCESS;
}



