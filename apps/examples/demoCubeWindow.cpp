/**
 * @file demoCubeWindow.cpp
 * @brief demoCubeWindow
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Utils/QtUtils.h"
#include <QGuiApplication>
#include "CubeWindow.h"
#include <iostream>


int main(int argc, char *argv[]) {
  QGuiApplication app(argc, argv);

  // Set the window up
  CuteGL::CubeWindow cube_window;

//  cube_window.resize(800, 600);
  cube_window.setDisplayFPS(true);

  /// Set swap interval to 0
  setSwapIntervalToZero(cube_window);

  cube_window.show();
  return app.exec();
}
