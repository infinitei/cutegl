/**
 * @file demoOffScreenRenderViewer.cpp
 * @brief demoOffScreenRenderViewer
 *
 * @note: method shown in this example for reading rendered images is very slow.
 * Only use it for demonstration. If speed is needed, use PBOs.
 *
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/OffScreenRenderViewer.h"
#include "CuteGL/Renderer/MultiObjectRenderer.h"
#include "CuteGL/Geometry/ComputeAlignedBox.h"
#include "CuteGL/Core/MeshUtils.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/IO/ImportViaAssimp.h"

#include <QGuiApplication>
#include <QTime>
#include <iostream>
#include <memory>

int main(int argc, char **argv) {
  using namespace CuteGL;

  QGuiApplication app(argc, argv);
  // Create the Renderer
  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);

  int W = 720;
  int H = 480;
  viewer.resize(W, H);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.5f, -0.2f, 1.0f),
                           Eigen::Vector3f::Zero(),
                           Eigen::Vector3f::UnitY());

  viewer.create();

  viewer.makeCurrent();
  {
    std::string asset_file = CUTEGL_ASSETS_FOLDER "/Sphere.nff";
    std::vector<MeshData> meshes = loadMeshesViaAssimp(asset_file);
    Eigen::AlignedBox3f bbx = computeAlignedBox(meshes);
    Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(0.5f / bbx.sizes().maxCoeff())
        * Eigen::Translation3f(-bbx.center());
    renderer->modelDrawers().addItem(model_pose, meshes);
  }

  {
    std::string asset_file = CUTEGL_ASSETS_FOLDER "/cow.off";
    std::vector<MeshData> meshes = loadMeshesViaAssimp(asset_file);
    Eigen::AlignedBox3f bbx = computeAlignedBox(meshes);
    Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(
          1.0f / bbx.sizes().maxCoeff()) * Eigen::Translation3f(-bbx.center());

    renderer->modelDrawers().addItem(model_pose, meshes);
  }

  const int num_frames = 10;
  QTime stopwatch;

  {
    std::cout << "Rendering " << num_frames << " frames at " << W << "x" << H << " ... " << std::flush;
    stopwatch.start();
    for (int i = 0; i < num_frames; ++i) {
      viewer.render();
      QImage image = viewer.readNormalBuffer();
      QString out_file = QString("OffScreenRenderViewer_%1x%2_").arg(QString::number(W), QString::number(H)) + QString("%1").arg(i, 6, 'g', -1, '0') + QString(".png");
      image.save(out_file);

  //    using Image = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  //    Image zbuffer(H, W);
  //    viewer.readZBuffer(zbuffer.data());

      for (auto& pose : renderer->modelDrawers().poses()) {
        pose = pose * Eigen::AngleAxisf(0.5f, Eigen::Vector3f::UnitY());
      }
    }
    const int time = stopwatch.elapsed();
    std::cout << " Done." << std::endl;
    std::cout << "FPS: " << (double(num_frames)/ (time / 100.0)) << "\n";
  }

  {
    int W = 320;
    int H = 240;
    viewer.resize(W, H);
    viewer.recreateFBO();

    std::cout << "Rendering " << num_frames << " frames at " << W << "x" << H << " ... " << std::flush;
    stopwatch.start();
    for (int i = 0; i < num_frames; ++i) {
      viewer.render();
      QImage image = viewer.readNormalBuffer();
      QString out_file = QString("OffScreenRenderViewer_%1x%2_").arg(QString::number(W), QString::number(H)) + QString("%1").arg(i, 6, 'g', -1, '0') + QString(".png");
      image.save(out_file);

  //    using Image = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  //    Image zbuffer(H, W);
  //    viewer.readZBuffer(zbuffer.data());

      for (auto& pose : renderer->modelDrawers().poses()) {
        pose = pose * Eigen::AngleAxisf(0.5f, Eigen::Vector3f::UnitY());
      }
    }
    const int time = stopwatch.elapsed();
    std::cout << " Done." << std::endl;
    std::cout << "FPS: " << (double(num_frames)/ (time / 100.0)) << "\n";

  }

  return EXIT_SUCCESS;
}



