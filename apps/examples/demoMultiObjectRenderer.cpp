/**
 * @file demoMultiObjectRenderer.cpp
 * @brief demoMultiObjectRenderer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Renderer/MultiObjectRenderer.h"
#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Geometry/ComputeAlignedBox.h"
#include "CuteGL/Core/MeshUtils.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/IO/ImportViaAssimp.h"
#include "CuteGL/Utils/QtUtils.h"
#include "CuteGL/Core/PoseUtils.h"

#include <QGuiApplication>
#include <memory>
#include <iostream>

int main(int argc, char **argv) {
  using namespace CuteGL;

  QGuiApplication app(argc, argv);
  // Create the Renderer
  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);
  QSurfaceFormat format = viewer.format();
  format.setSamples(16);
  viewer.setFormat(format);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.0f, 2.0f),
                           Eigen::Vector3f::Zero(),
                           Eigen::Vector3f::UnitY());

  // Set Scene radius to 2.0f
  viewer.setSceneRadius(2.0f);

  // Set swap interval to 0
  setSwapIntervalToZero(viewer);

  // Display the window
  viewer.showAndWaitTillExposed();

  {
    // Draw a camera at current camera pose
    renderer->cameraDrawers().addItem(viewer.camera().extrinsics().inverse(), 0.1f);
  }

  {
    Eigen::Isometry3f pose = getExtrinsicsFromLookAt(Eigen::Vector3f::UnitX(),Eigen::Vector3f::Zero(),Eigen::Vector3f::UnitZ()).inverse();

    pose = Eigen::AngleAxisf(0.25*M_PI, Eigen::Vector3f::UnitZ()) * pose;
    pose = Eigen::AngleAxisf(0.25*M_PI, Eigen::Vector3f::UnitY()) * pose;

    renderer->cameraDrawers().addItem(pose, 0.1f);
  }


  {
    std::string asset_file = CUTEGL_ASSETS_FOLDER "/Sphere.nff";
    std:: cout << "Loding model (via Assimp) from " << asset_file << "\n";
    std::vector<MeshData> meshes = loadMeshesViaAssimp(asset_file);
    Eigen::AlignedBox3f bbx = computeAlignedBox(meshes);
    Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(0.5f / bbx.sizes().maxCoeff())
        * Eigen::Translation3f(-bbx.center());
    renderer->modelDrawers().addItem(model_pose, meshes);
    renderer->bbxDrawers().addItem(model_pose, bbx, BoundingBoxDrawer::ColorType(250, 50, 190, 255));
  }

  {
    std::string asset_file = CUTEGL_ASSETS_FOLDER "/cow.off";
    std:: cout << "Loding model (via Assimp) from " << asset_file << "\n";
    std::vector<MeshData> meshes = loadMeshesViaAssimp(asset_file);
    Eigen::AlignedBox3f bbx = computeAlignedBox(meshes);
    Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(
          1.0f / bbx.sizes().maxCoeff()) * Eigen::Translation3f(-bbx.center());

    renderer->modelDrawers().addItem(model_pose, meshes);

    renderer->bbxDrawers().addItem(model_pose, bbx, BoundingBoxDrawer::ColorType(60, 50, 230, 255));
  }

  {
    typedef PointCloudDrawer::VertexData VertexData;
    std::vector<VertexData> particles;
    const std::size_t num_of_points_to_add = 1e4;
    particles.reserve(num_of_points_to_add);
    for (size_t i = 0; i < num_of_points_to_add; ++i) {
      particles.emplace_back(VertexData::PositionType::Random(),
                             VertexData::ColorType::Random());
    }
    renderer->pointCloudDrawers().addItem(Eigen::Affine3f::Identity(), particles);
  }

  {
    typedef PointCloudDrawer::VertexData VertexData;
    std::vector<VertexData> particles;
    const std::size_t num_of_points_to_add = 1e6;
    particles.reserve(num_of_points_to_add);
    for (size_t i = 0; i < num_of_points_to_add; ++i) {
      particles.emplace_back(VertexData::PositionType::Random(),
                             VertexData::ColorType::Random());
    }

    renderer->pointCloudDrawers().addItem(Eigen::Translation3f(1, 1, 1), particles);
  }

  {
    typedef LineDrawer::PositionType PositionType;
    std::vector<std::pair<PositionType, PositionType> > lines;
    lines.emplace_back(PositionType(-0.5f, 0.0f, 0.0f), PositionType(-0.9f, 0.7f, 0.5f));
    lines.emplace_back(PositionType(-0.9f, 0.7f, 0.5f), PositionType(-0.1f, 0.7f, 0.5f));
    lines.emplace_back(PositionType(-0.1f, 0.7f, 0.5f), PositionType(-0.1f, 0.1f, 0.1f));

    renderer->lineDrawers().addItem(Eigen::Affine3f::Identity(), lines, BoundingBoxDrawer::ColorType(70, 230, 190, 255));

  }

  renderer->bbxDrawers().addItem(Eigen::Affine3f::Identity(), Eigen::AlignedBox3f(Eigen::Vector3f::Constant(-1.2f), Eigen::Vector3f::Constant(0.3f)));

  return app.exec();
}
