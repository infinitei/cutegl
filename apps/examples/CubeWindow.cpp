/**
 * @file CubeWindow.cpp
 * @brief CubeWindow
 *
 * @author Abhijit Kundu
 */

//#define EIGEN_QT_SUPPORT
#define _USE_MATH_DEFINES

#include "CubeWindow.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/Core/VertexData.h"
#include "CuteGL/Core/PoseUtils.h"
#include <QDebug>
#include <QString>
#include <QOpenGLShaderProgram>
#include <QMatrix4x4>
#include <vector>

namespace CuteGL {

CubeWindow::CubeWindow()
    : vbo_(0),
      vao_(0),
      program_(),
      uniform_view_T_world_loc_(-1),
      model_pose_(Eigen::Isometry3f::Identity()) {

  // Set camera pose via lookAt
  camera().extrinsics() = getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 5.0f),
                                                  Eigen::Vector3f::Zero(),
                                                  Eigen::Vector3f::UnitY());
}

CubeWindow::~CubeWindow() {
  makeCurrent();
  teardownGL();
  doneCurrent();
}

void CubeWindow::init() {
  //TODO see the example cleanup function in Qt documentation
//  connect(context(), SIGNAL(aboutToBeDestroyed()), this, SLOT(teardownGL()), Qt::DirectConnection);

  glfuncs_->glEnable(GL_CULL_FACE); // Enable Cull  face

  // Application specific initialization
  {
    program_.addShaderFromSourceFile(QOpenGLShader::Vertex, CUTEGL_SHADER_FOLDER "/simple_vs.glsl");
    program_.addShaderFromSourceFile(QOpenGLShader::Fragment, CUTEGL_SHADER_FOLDER "/simple_fs.glsl");
    program_.link();
    program_.bind();

    uniform_view_T_world_loc_ = program_.uniformLocation("view_T_world");

    using VertexData = VertexXYZRGBA;
    using PositionType = VertexData::PositionType;
    using ColorType = VertexData::ColorType;

    std::vector<VertexData> cube_vertices;

    // Front Verticies
    cube_vertices.emplace_back(PositionType( 0.5f,  0.5f,  0.5f), ColorType( 255, 0, 0, 255 ));
    cube_vertices.emplace_back(PositionType(-0.5f,  0.5f,  0.5f), ColorType( 0, 255, 0, 255 ));
    cube_vertices.emplace_back(PositionType(-0.5f, -0.5f,  0.5f), ColorType( 0, 0, 255, 255 ));
    cube_vertices.emplace_back(PositionType( 0.5f, -0.5f,  0.5f), ColorType( 0, 0, 0, 255 ));

    // Back Verticies
    cube_vertices.emplace_back(PositionType( 0.5f,  0.5f, -0.5f), ColorType( 255, 0, 0, 255 ));
    cube_vertices.emplace_back(PositionType(-0.5f,  0.5f, -0.5f), ColorType( 0, 255, 0, 255 ));
    cube_vertices.emplace_back(PositionType(-0.5f, -0.5f, -0.5f), ColorType( 0, 0, 255, 255 ));
    cube_vertices.emplace_back(PositionType( 0.5f, -0.5f, -0.5f), ColorType( 255, 255, 255, 255 ));


    std::vector<VertexData> vertices;

    // face 1
    vertices.push_back(cube_vertices[0]);
    vertices.push_back(cube_vertices[1]);
    vertices.push_back(cube_vertices[2]);
    vertices.push_back(cube_vertices[2]);
    vertices.push_back(cube_vertices[3]);
    vertices.push_back(cube_vertices[0]);

    // face 2
    vertices.push_back(cube_vertices[7]);
    vertices.push_back(cube_vertices[5]);
    vertices.push_back(cube_vertices[4]);
    vertices.push_back(cube_vertices[5]);
    vertices.push_back(cube_vertices[7]);
    vertices.push_back(cube_vertices[6]);

    // face 3
    vertices.push_back(cube_vertices[0]);
    vertices.push_back(cube_vertices[4]);
    vertices.push_back(cube_vertices[5]);
    vertices.push_back(cube_vertices[5]);
    vertices.push_back(cube_vertices[1]);
    vertices.push_back(cube_vertices[0]);

    // face 4
    vertices.push_back(cube_vertices[3]);
    vertices.push_back(cube_vertices[2]);
    vertices.push_back(cube_vertices[6]);
    vertices.push_back(cube_vertices[6]);
    vertices.push_back(cube_vertices[7]);
    vertices.push_back(cube_vertices[3]);

    // face 5
    vertices.push_back(cube_vertices[2]);
    vertices.push_back(cube_vertices[1]);
    vertices.push_back(cube_vertices[5]);
    vertices.push_back(cube_vertices[2]);
    vertices.push_back(cube_vertices[5]);
    vertices.push_back(cube_vertices[6]);

    // face 6
    vertices.push_back(cube_vertices[0]);
    vertices.push_back(cube_vertices[3]);
    vertices.push_back(cube_vertices[7]);
    vertices.push_back(cube_vertices[7]);
    vertices.push_back(cube_vertices[4]);
    vertices.push_back(cube_vertices[0]);


    // 1. Bind VAO
    glfuncs_->glGenVertexArrays(1, &vao_);
    glfuncs_->glBindVertexArray(vao_);

    // 2. Copy our vertices array in a vertex buffer for OpenGL to use
    glfuncs_->glGenBuffers(1, &vbo_);
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glfuncs_->glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexData), vertices.data(), GL_STATIC_DRAW);

    // 3. Then set the vertex attributes pointers

    // Position attribute
    const int position_index = program_.attributeLocation("vertex_position");
    assert(position_index != -1);
    program_.setAttributeArray(position_index, GL_FLOAT, (void*) offsetof(VertexData, position), 3, sizeof(VertexData));
    program_.enableAttributeArray(position_index);

    // Color attribute
    const int color_index = program_.attributeLocation("vertex_color");
    assert(color_index != -1);
    program_.setAttributeArray(color_index, GL_UNSIGNED_BYTE, (void*) offsetof(VertexData, color), 4, sizeof(VertexData));
    program_.enableAttributeArray(color_index);

    // We can unbind VBO not IBO
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
    glfuncs_->glBindVertexArray(0);

    program_.release();

  }
}

void CubeWindow::draw() {
  program_.bind();

  Eigen::Matrix4f proj_view_model_matrix = camera().projectionViewMatrix() * model_pose_.matrix();
  glfuncs_->glUniformMatrix4fv(uniform_view_T_world_loc_, 1, GL_FALSE, proj_view_model_matrix.data());
  {
    glfuncs_->glBindVertexArray(vao_);
    glfuncs_->glDrawArrays(GL_TRIANGLES, 0, 36);
    glfuncs_->glBindVertexArray(0);
  }
  program_.release();
}

void CubeWindow::teardownGL() {
  glfuncs_->glDeleteVertexArrays(1, &vao_);
  glfuncs_->glDeleteBuffers(1, &vbo_);
}

void CubeWindow::update() {
  // Update model view
  model_pose_.rotate(Eigen::AngleAxisf(1.0f * M_PI / 180.0f, Eigen::Vector3f(0.4f, 0.3f, 0.3f).normalized()));
//  camera().pose().rotate(Eigen::AngleAxisf(1.0f * M_PI / 180.0f, Eigen::Vector3f(0.4f, 0.3f, 0.3f)));

  // Schedule a redraw
  QOpenGLWindow::update();
}

}  // end namespace CuteGL
