/**
 * @file demoCroppedRender.cpp
 * @brief Renders a cropped version of the scene (See demoBasicLightRenderer for uncropped version)
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/BasicLightRenderer.h"
#include "CuteGL/Core/PoseUtils.h"
#include <QGuiApplication>
#include <memory>
#include <iostream>

int main(int argc, char *argv[]) {
  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<CuteGL::BasicLightRenderer> renderer(new CuteGL::BasicLightRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  CuteGL::WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);


  // Lets say we want to render only the top 60% of the uncropped image
  // So the Cropping box params are cx= 0.5, cy= 0.3, w=1, h=0.6
  float crop_cx = 0.5f;
  float crop_cy = 0.3f;
  float crop_w = 1.0f;
  float crop_h = 0.6f;

  float crop_l = crop_cx - crop_w / 2.0;
  float crop_b = (1.0 - crop_cy) - crop_h / 2.0;


  const float original_window_width = 1024;
  const float original_window_height = 768;

  const float cropped_window_width = original_window_width * crop_w;
  const float cropped_window_height = original_window_height * crop_h;

  viewer.resize(cropped_window_width, cropped_window_height);

  // Show the window
  viewer.showAndWaitTillExposed();

  // Set camera pose via lookAt
  viewer.camera().extrinsics() = CuteGL::getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 5.0f),
                                                                 Eigen::Vector3f::Zero(),
                                                                 Eigen::Vector3f::UnitY());

  viewer.camera().intrinsics() = CuteGL::getGLPerspectiveProjection(1000.0f, 1000.0f, 0.0f, original_window_width/2.0f, original_window_height/2.0f, original_window_width, original_window_height, 0.1f, 100.0f);





  Eigen::Affine2f oTc = Eigen::Translation2f(crop_l, crop_b) * Eigen::Scaling(crop_w, crop_h);
  Eigen::Affine2f cTo = oTc.inverse();
  Eigen::Affine2f sTo = Eigen::Scaling(cropped_window_width, cropped_window_height) * cTo;

  const Eigen::Vector2f viewport_xy =  (sTo * Eigen::Vector2f::Zero());
  const Eigen::Vector2f viewport_wh =  (sTo * Eigen::Vector2f::Ones()) - viewport_xy;
  viewer.setViewPort(viewport_xy.x(), viewport_xy.y(), viewport_wh.x(), viewport_wh.y());




  return app.exec();
}
