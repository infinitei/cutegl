/**
 * @file demoDynamicMesh.cpp
 * @brief demoDynamicMesh
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/BasicRenderer.h"
#include "CuteGL/Drawers/MeshDrawer.h"
#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Core/Config.h"

#include <QGuiApplication>
#include <memory>
#include <iostream>

namespace CuteGL {

class MeshRenderer : public BasicRenderer {
 public:
  MeshRenderer()
   :mesh_drawer_(),
    model_mat_(Eigen::Affine3f::Identity()) {
  }

  void initMeshDrawer(const MeshData& mesh);

  const Eigen::Affine3f& modelMat() const {return model_mat_;}
  Eigen::Affine3f& modelMat() {return model_mat_;}

  MeshDrawer& meshDrawer() {return mesh_drawer_;}
  const MeshDrawer& meshDrawer() const {return mesh_drawer_;}

 protected:
  virtual void init(BasicShader& shader);
  virtual void draw(BasicShader& shader);
 private:
  MeshDrawer mesh_drawer_;
  Eigen::Affine3f model_mat_;

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

void MeshRenderer::init(BasicShader& shader) {
  glEnable(GL_CULL_FACE);  // Enable Cull  face

}

void MeshRenderer::initMeshDrawer(const MeshData& mesh) {
  mesh_drawer_.init(basicShader().program, mesh);
}

void MeshRenderer::draw(BasicShader& shader) {
  shader.setModelPose(model_mat_);
  mesh_drawer_.draw();
}

}  // namespace CuteGL


int main(int argc, char **argv) {
  using namespace CuteGL;
  using Eigen::Vector3f;

  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr <MeshRenderer> renderer(new MeshRenderer());
  renderer->setDisplayGrid(true);
  renderer->modelMat() = Eigen::UniformScaling<float>(1.00);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);


  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Vector3f(0.0f, 0.0f, 5.0f),
                           Vector3f::Zero(),
                           Vector3f::UnitY());

  viewer.showAndWaitTillExposed();

  MeshData md;

  md.vertices.resize(3);
  md.vertices[0].position = MeshData::PositionType(0.5f, -0.5f, 0.0f);
  md.vertices[0].color = MeshData::ColorType(255, 0, 0, 255);

  md.vertices[1].position = MeshData::PositionType(-0.5f, -0.5f, 0.0f);
  md.vertices[1].color = MeshData::ColorType(0, 255, 0, 255);

  md.vertices[2].position = MeshData::PositionType(0.0f,  0.5f, 0.0f);
  md.vertices[2].color = MeshData::ColorType(0, 0, 255, 255);

  md.faces.resize(1, 3);
  md.faces << 0, 2, 1;  // First Triangle

  renderer->initMeshDrawer(md);
  for (int i = 0; i < 100; ++i) {
    std::reverse(md.vertices.begin(), md.vertices.end());
    renderer->meshDrawer().updateMeshVertices(md.vertices);
    QCoreApplication::processEvents(QEventLoop::AllEvents, 20);
  }
  return app.exec();
}



