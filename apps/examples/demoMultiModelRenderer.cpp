/**
 * @file demoMultiModelRenderer.cpp
 * @brief demoMultiModelRenderer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Drawers/MultiDrawer.h"
#include "CuteGL/Drawers/MultiModelDrawer.h"
#include "CuteGL/Geometry/ComputeAlignedBox.h"
#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/BasicLightRenderer.h"
#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Core/MeshUtils.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/IO/ImportViaAssimp.h"
#include "CuteGL/Utils/QtUtils.h"

#include <QGuiApplication>
#include <memory>
#include <iostream>

namespace CuteGL {

class MultiModelRenderer : public BasicLightRenderer {
 public:
  typedef MultiDrawer<ModelDrawer, PhongShader> MultiModelDrawerType;

  MultiModelRenderer()
      : multi_model_drawer_(phongShader()) {
  }

  const MultiModelDrawerType& multiModelDrawer() const {return multi_model_drawer_;}
  MultiModelDrawerType& multiModelDrawer() {return multi_model_drawer_;}

 protected:
  virtual void init(PhongShader& shader);
  virtual void draw(PhongShader& shader);
 private:
  MultiModelDrawerType multi_model_drawer_;
};

void MultiModelRenderer::init(PhongShader& shader) {
  glEnable(GL_CULL_FACE);  // Enable Cull  face
}

void MultiModelRenderer::draw(PhongShader& shader) {
  multi_model_drawer_.draw();
}

}  // namespace CuteGL

int main(int argc, char **argv) {
  using namespace CuteGL;

  QGuiApplication app(argc, argv);
  // Create the Renderer
  std::unique_ptr<MultiModelRenderer> renderer(new MultiModelRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);

  // Set camera pose via lookAt
  viewer.camera().extrinsics() = getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 2.0f),
                                                         Eigen::Vector3f::Zero(),
                                                         Eigen::Vector3f::UnitY());

  // Set swap interval to 0
  setSwapIntervalToZero(viewer);

  // Display the window
  viewer.showAndWaitTillExposed();


  {
    std::string asset_file = CUTEGL_ASSETS_FOLDER "/Sphere.nff";
    std:: cout << "Loding model (via Assimp) from " << asset_file << "\n";
    std::vector<MeshData> meshes = loadMeshesViaAssimp(asset_file);
    Eigen::AlignedBox3f bbx = computeAlignedBox(meshes);
    Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(0.5f / bbx.sizes().maxCoeff())
        * Eigen::Translation3f(-bbx.center());
    renderer->multiModelDrawer().addItem(model_pose, meshes);
  }

  {
    std::string asset_file = CUTEGL_ASSETS_FOLDER "/cow.off";
    std:: cout << "Loding model (via Assimp) from " << asset_file << "\n";
    std::vector<MeshData> meshes = loadMeshesViaAssimp(asset_file);
    Eigen::AlignedBox3f bbx = computeAlignedBox(meshes);
    Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(
          1.0f / bbx.sizes().maxCoeff()) * Eigen::Translation3f(-bbx.center());

    renderer->multiModelDrawer().addItem(model_pose, meshes);
  }


  return app.exec();
}
