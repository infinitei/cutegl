/**
 * @file demoOffScreenSMPL.cpp
 * @brief demoOffScreenSMPL
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Renderer/SMPLRenderer.h"
#include "CuteGL/Surface/OffScreenRenderViewer.h"

#include <QGuiApplication>
#include <memory>
#include <iostream>

int main(int argc, char **argv) {
  QGuiApplication app(argc, argv);
  using namespace CuteGL;
  using Eigen::Vector3f;


  // Create the Renderer
  std::unique_ptr<SMPLRenderer> renderer(new SMPLRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  const int W = 640;
  const int H = 480;
  viewer.resize(W, H);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                           Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.create();
  viewer.makeCurrent();

  renderer->setSMPLData();

  viewer.render();

  using Image = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

  {
    Image zbuffer(H, W);
    viewer.readZBuffer(zbuffer.data());
    zbuffer.colwise().reverseInPlace();

    QImage image(W, H, QImage::Format_RGB888);
    for (int x = 0; x < W; ++x) {
      for (int y = 0; y < H; ++y) {
        int cval = zbuffer(y, x) * 100;
        image.setPixel(x, y, qRgb(cval, cval, cval));
      }
    }
    image.save(QString("OffScreenSMPL_zbufer.png"));
  }

  {
    QImage image = viewer.readNormalBuffer();
    image.save(QString("OffScreenSMPL_normal.png"));
  }


  {
    QImage image = viewer.readColorBuffer();
    image.save(QString("OffScreenSMPL_color.png"));
  }

  {
    Image buffer(H, W);
    viewer.readDepthBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    QImage image(W, H, QImage::Format_RGB888);
    for (int x = 0; x < W; ++x) {
      for (int y = 0; y < H; ++y) {
        int cval = buffer(y, x) * 100;
        image.setPixel(x, y, qRgb(cval, cval, cval));
      }
    }
    image.save(QString("OffScreenSMPL_depth.png"));
  }

  {
    Image buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    std::cout << buffer.minCoeff() << "  " << buffer.maxCoeff() << "\n";

    QImage image(W, H, QImage::Format_Indexed8);

    QVector<QRgb> color_table;
    color_table << qRgb(0  ,   0,   0)
                << qRgb(45 , 115, 206)
                << qRgb(129, 206, 93 )
                << qRgb(253, 249, 216)
                << qRgb(144, 60 , 92 )
                << qRgb(79 , 114, 90 )
                << qRgb(54 , 162, 56 )
                << qRgb(197, 165, 219)
                << qRgb(185, 76 , 124)
                << qRgb(116, 231, 220)
                << qRgb(113, 182, 85 )
                << qRgb(14 , 59 , 88 )
                << qRgb(17 , 157, 175)
                << qRgb(62 , 254, 20 )
                << qRgb(49 , 91 , 87 )
                << qRgb(80 , 169, 207)
                << qRgb(56 , 168, 213)
                << qRgb(177, 241, 253)
                << qRgb(202, 32 , 111)
                << qRgb(5  , 221, 155)
                << qRgb(140, 49 , 46 )
                << qRgb(135, 185, 199)
                << qRgb(250, 186, 70 )
                << qRgb(105, 167, 64 )
                << qRgb(72 , 72 , 195);
    image.setColorTable(color_table);


    for (int x = 0; x < W; ++x) {
      for (int y = 0; y < H; ++y) {
        uint8_t cval = buffer(y, x);
        image.setPixel(x, y, cval);
      }
    }
    image.save(QString("OffScreenSMPL_label.png"));
  }


  return EXIT_SUCCESS;
}


