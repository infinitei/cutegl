/**
 * @file demoImageWindow.cpp
 * @brief demoImageWindow
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowOpenGLSurface.h"
#include <QGuiApplication>
#include <QPainter>
#include <iostream>

namespace CuteGL {

class ImageWindow : public WindowOpenGLSurface {
 public:
  ImageWindow(const QImage& bg_image)
      : bg_image_(bg_image) {
    resize(bg_image_.size());
  }

 protected:
  void preDraw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glfuncs_->glDisable(GL_DEPTH_TEST);
    QPainter p(this);
    p.drawImage(0, 0, bg_image_);
    glfuncs_->glEnable(GL_DEPTH_TEST);
  }

 private:
  QImage bg_image_;
};

}  // namespace CuteGL

int main(int argc, char *argv[]) {
  if (argc < 2) {
    std::cout << "ERROR: image file not provided\n";
    std::cout << "Usage: " << argv[0] << " path/to/image\n";
    return EXIT_FAILURE;
  }

  QImage image(argv[1]);
  if(image.isNull()) {
    std::cout << "ERROR: Caannot load image from \"" << argv[1] << "\"\n";
    return EXIT_FAILURE;
  }

  QGuiApplication app(argc, argv);
  CuteGL::ImageWindow window(image);
  window.setBackgroundColor(55, 55, 55);

  window.show();
  return app.exec();
}

