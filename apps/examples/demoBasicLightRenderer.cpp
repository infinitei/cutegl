/**
 * @file demoBasicLightRenderer.cpp
 * @brief demoBasicLightRenderer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/BasicLightRenderer.h"
#include "CuteGL/Core/PoseUtils.h"
#include <QGuiApplication>
#include <memory>
#include <iostream>

int main(int argc, char *argv[]) {
  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<CuteGL::BasicLightRenderer> renderer(new CuteGL::BasicLightRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  CuteGL::WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);

  const int window_width = 1024;
  const int window_height = 768;

  viewer.resize(window_width, window_height);

  // Show the window
  viewer.showAndWaitTillExposed();

  // Set camera pose via lookAt
  viewer.camera().extrinsics() = CuteGL::getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 5.0f),
                                                                 Eigen::Vector3f::Zero(),
                                                                 Eigen::Vector3f::UnitY());

  viewer.camera().intrinsics() = CuteGL::getGLPerspectiveProjection(1000.0f, 1000.0f, 0.0f, 512.0f, 384.0f, 1024, 768, 0.1f, 100.0f);


  return app.exec();
}
