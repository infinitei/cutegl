/**
 * @file demoBasicRendererViewer.cpp
 * @brief demoBasicRendererViewer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/BasicRenderer.h"
#include "CuteGL/Core/PoseUtils.h"
#include <QGuiApplication>
#include <memory>

int main(int argc, char *argv[]) {
  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<CuteGL::BasicRenderer> renderer(new CuteGL::BasicRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  CuteGL::WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);


  // Set camera pose via lookAt
  viewer.camera().extrinsics() = CuteGL::getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 5.0f),
                                                                 Eigen::Vector3f::Zero(),
                                                                 Eigen::Vector3f::UnitY());

  viewer.show();
  return app.exec();
}




