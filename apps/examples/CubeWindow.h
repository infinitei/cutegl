/**
 * @file CubeWindow.h
 * @brief CubeWindow
 *
 * @author Abhijit Kundu
 */

#ifndef CUBEWINDOW_H_
#define CUBEWINDOW_H_

#include "CuteGL/Surface/WindowOpenGLSurface.h"
#include <QOpenGLShaderProgram>

namespace CuteGL {

class CubeWindow : public WindowOpenGLSurface {
  Q_OBJECT
 public:
  CubeWindow();
  ~CubeWindow();

 protected:
  void init();
  void draw();

 protected slots:
  void update();
  void teardownGL();

 private:
  // OpenGL State Information
  GLuint vbo_;
  GLuint vao_;
  QOpenGLShaderProgram program_;
  int uniform_view_T_world_loc_;
  Eigen::Isometry3f model_pose_;
};

}  // end namespace CuteGL

#endif // end CUBEWINDOW_H_
