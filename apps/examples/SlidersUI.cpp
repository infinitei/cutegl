/**
 * @file SlidersUI.cpp
 * @brief SlidersUI
 *
 * @author Abhijit Kundu
 */

#include "SlidersUI.h"

namespace CuteGL {

SlidersUI::SlidersUI(const std::size_t dimension,
                     const int min, const int max, const int default_value,
                     const QString& title, QWidget* parent)
    : QGroupBox(title, parent) {

  for (std::size_t i = 0; i < dimension; ++i) {
    sliders_.emplace_back(new QSlider(Qt::Horizontal));
  }

  QBoxLayout *slidersLayout = new QBoxLayout(QBoxLayout::TopToBottom);

  for (std::size_t i = 0; i < dimension; ++i) {
    sliders_[i]->setFocusPolicy(Qt::StrongFocus);
    sliders_[i]->setTickPosition(QSlider::TicksBothSides);
    sliders_[i]->setRange(min, max);
    sliders_[i]->setValue(default_value);
    sliders_[i]->setTickInterval((max - min) / 10);
    sliders_[i]->setSingleStep(1);

    connect(sliders_[i].get(), SIGNAL(valueChanged(int)), this, SIGNAL(valueChanged()));

    slidersLayout->addWidget(sliders_[i].get());
  }

  setLayout(slidersLayout);
}

Eigen::VectorXi SlidersUI::value() const {
  Eigen::VectorXi value(sliders_.size());

  for (std::size_t i = 0; i < sliders_.size(); ++i) {
    value[i] = sliders_[i]->value();
  }

  return value;
}


}  // end namespace CuteGL
