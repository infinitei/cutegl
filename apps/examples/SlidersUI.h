/**
 * @file SlidersUI.h
 * @brief SlidersUI
 *
 * @author Abhijit Kundu
 */

#ifndef SLIDERS_UI_H_
#define SLIDERS_UI_H_

#include <QGroupBox>
#include <QSlider>
#include <memory>
#include <QtWidgets>
#include <Eigen/Core>

namespace CuteGL {

class SlidersUI : public QGroupBox {
Q_OBJECT

 public:
  SlidersUI(const std::size_t dimension,
            const int min, const int max, const int default_value,
            const QString &title, QWidget *parent = 0);

  Eigen::VectorXi value() const;

signals:
  void valueChanged();

 private:
  std::vector<std::unique_ptr<QSlider>> sliders_;
};

}  // end namespace CuteGL

#endif // end SLIDERS_UI_H_
