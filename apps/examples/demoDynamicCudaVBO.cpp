/**
 * @file demoDynamicCudaVBO.cpp
 * @brief demoDynamicCudaVBO
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowOpenGLSurface.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/Core/VertexData.h"
#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Utils/OpenGLUtils.h"
#include "CuteGL/Utils/QtUtils.h"
#include "demoDynamicCudaVBO.h"

#include <QOpenGLShaderProgram>
#include <QGuiApplication>
#include <iostream>

namespace CuteGL {

class DynamicVBOViewer : public WindowOpenGLSurface {
 public:
  DynamicVBOViewer(unsigned int mesh_width = 512, unsigned int mesh_height = 512);
  ~DynamicVBOViewer();

 protected:
  void init();
  void draw();
  void teardownGL();
  void postDraw();

 private:
  // OpenGL State Information
  GLuint vbo_;
  GLuint vao_;
  QOpenGLShaderProgram program_;
  int uniform_view_T_world_loc_;
  Eigen::Isometry3f model_pose_;

  float time_;
  const unsigned int mesh_width_;
  const unsigned int mesh_height_;
  cudaGraphicsResource *cuda_vbo_resource_;

  void runCuda();

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

DynamicVBOViewer::DynamicVBOViewer(unsigned int mesh_width, unsigned int mesh_height)
    : vbo_(0),
      vao_(0),
      program_(),
      uniform_view_T_world_loc_(-1),
      model_pose_(Eigen::Isometry3f::Identity()),
      time_(0.0),
      mesh_width_(mesh_width),
      mesh_height_(mesh_height),
      cuda_vbo_resource_(nullptr) {

  // Set camera pose via lookAt
  camera().extrinsics() = getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 5.0f),
                                                  Eigen::Vector3f::Zero(),
                                                  Eigen::Vector3f::UnitY());
}

DynamicVBOViewer::~DynamicVBOViewer() {
  makeCurrent();
  teardownGL();
  doneCurrent();
}

void DynamicVBOViewer::teardownGL() {
  glfuncs_->glDeleteVertexArrays(1, &vao_);
  glfuncs_->glDeleteBuffers(1, &vbo_);
}

void DynamicVBOViewer::runCuda() {
    // map OpenGL buffer object for writing from CUDA
    float3 *dptr;
    cudaGraphicsMapResources(1, &cuda_vbo_resource_, 0);
    size_t num_bytes;
    cudaGraphicsResourceGetMappedPointer((void **)&dptr, &num_bytes, cuda_vbo_resource_);
    //printf("CUDA mapped VBO: May access %ld bytes\n", num_bytes);

    // execute the kernel
    launch_kernel(dptr, mesh_width_, mesh_height_, time_);

    // unmap buffer object
    cudaGraphicsUnmapResources(1, &cuda_vbo_resource_, 0);
}

void DynamicVBOViewer::init() {
  glfuncs_->glEnable(GL_CULL_FACE); // Enable Cull  face

  {
    unsigned int gl_cuda_device_count;
    int gl_devices[1];
    cudaGLGetDevices(&gl_cuda_device_count, gl_devices, 1, cudaGLDeviceListAll);
    //Setup CUDA
    cudaSetDevice(gl_devices[0]);
    cudaGLSetGLDevice(gl_devices[0]);
    std::cout << "Using CUDA device: "<< gl_devices[0] << std::endl;
  }

  // Application specific initialization
  {
    program_.addShaderFromSourceFile(QOpenGLShader::Vertex, CUTEGL_SHADER_FOLDER "/simple_heightmap_vs.glsl");
    program_.addShaderFromSourceFile(QOpenGLShader::Fragment, CUTEGL_SHADER_FOLDER "/simple_heightmap_fs.glsl");
    program_.link();
    program_.bind();

    uniform_view_T_world_loc_ = program_.uniformLocation("view_T_world");

    int uniform_loc_min_height = program_.uniformLocation("min_height");
    int uniform_loc_max_height = program_.uniformLocation("max_height");

    assert(uniform_loc_min_height != -1);
    assert(uniform_loc_max_height != -1);

    glfuncs_->glUniform1f(uniform_loc_min_height, -1.0f);
    glfuncs_->glUniform1f(uniform_loc_max_height, 1.0f);

    const GLsizeiptr size_of_positions = mesh_width_* mesh_height_ * sizeof(float3);

    // 1. Bind VAO
    glfuncs_->glGenVertexArrays(1, &vao_);
    glfuncs_->glBindVertexArray(vao_);

    // 2. Copy our vertices array in a vertex buffer for OpenGL to use
    glfuncs_->glGenBuffers(1, &vbo_);
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glfuncs_->glBufferData(GL_ARRAY_BUFFER,
                           size_of_positions,
                           nullptr,
                           GL_STATIC_DRAW);

    // Registers OpenGL vbo
    cudaGraphicsGLRegisterBuffer(&cuda_vbo_resource_, vbo_, cudaGraphicsRegisterFlagsWriteDiscard);
    runCuda();

    // 3. Then set the vertex attributes pointers

    // Position attribute
    const int position_index = program_.attributeLocation("vertex_position");
    assert(position_index != -1);
    glfuncs_->glVertexAttribPointer(position_index, 3, GL_FLOAT, GL_FALSE, sizeof(float3), 0);
    glfuncs_->glEnableVertexAttribArray(position_index);

    // Release VAO first
    glfuncs_->glBindVertexArray(0);
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

    program_.release();
  }
}

void DynamicVBOViewer::draw() {
  program_.bind();

  Eigen::Matrix4f proj_view_model_matrix = camera().projectionViewMatrix() * model_pose_.matrix();
  glfuncs_->glUniformMatrix4fv(uniform_view_T_world_loc_, 1, GL_FALSE, proj_view_model_matrix.data());
  {
    glfuncs_->glBindVertexArray(vao_);
    glfuncs_->glDrawArrays(GL_POINTS, 0, mesh_width_*mesh_height_);
    glfuncs_->glBindVertexArray(0);
  }
  program_.release();
}

void DynamicVBOViewer::postDraw() {
  WindowOpenGLSurface::postDraw();

  runCuda();

  time_ += 0.01f;
  update();
}

}  // namespace CuteGL


int main(int argc, char **argv) {
  QGuiApplication app(argc, argv);

  int mesh_dim = 512;
  if (argc > 1) {
    mesh_dim = std::stoi(argv[1]);
  }
  std::cout << "Using mesh dimension " << mesh_dim << "\n";

  // Set the window up
  CuteGL::DynamicVBOViewer viewer(mesh_dim, mesh_dim);

  viewer.resize(800, 600);
  viewer.setDisplayFPS(true);

  /// Set swap interval to 0
  CuteGL::setSwapIntervalToZero(viewer);

  viewer.show();
  return app.exec();
}



