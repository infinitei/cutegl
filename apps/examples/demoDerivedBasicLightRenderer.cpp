/**
 * @file demoDerivedBasicLightRenderer.cpp
 * @brief Example of how to derive from BasicLightRenderer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/BasicLightRenderer.h"
#include "CuteGL/Drawers/CubeDrawer.h"
#include "CuteGL/Drawers/BoundingBoxDrawer.h"
#include "CuteGL/Drawers/CameraDrawer.h"
#include "CuteGL/Core/PoseUtils.h"
#include <QGuiApplication>
#include <memory>

namespace CuteGL {

class MyRenderer : public BasicLightRenderer {
 public:
  MyRenderer()
      : cube_drawer_(),
        cube_pose_(Eigen::Isometry3f::Identity()),
        camera_drawer_(),
        camera_pose_(Eigen::Isometry3f::Identity()),
        bbx_drawer_() {

    camera_pose_.translate(Eigen::Vector3f(-1, 0, 2));
  }

 protected:
  virtual void init(BasicShader& shader);
  virtual void draw(BasicShader& shader);

  virtual void init(PhongShader& shader);
  virtual void draw(PhongShader& shader);

 public:
  CubeDrawer cube_drawer_;
  Eigen::Isometry3f cube_pose_;

  CameraDrawer camera_drawer_;
  Eigen::Isometry3f camera_pose_;

  BoundingBoxDrawer bbx_drawer_;

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

void MyRenderer::init(BasicShader& shader) {
  camera_drawer_.init(shader.program, 0.2f);

  Eigen::AlignedBox3f bbx(Eigen::Vector3f::Constant(-0.7f), Eigen::Vector3f::Constant(1.2f));
  bbx_drawer_.init(shader.program, bbx);
}

void MyRenderer::draw(BasicShader& shader) {
  shader.setModelPose(Eigen::Affine3f::Identity());
  bbx_drawer_.draw();

  shader.setModelPose(camera_pose_);
  camera_drawer_.draw();
}


void MyRenderer::init(PhongShader& shader) {
  cube_drawer_.init(shader.program);
}

void MyRenderer::draw(PhongShader& shader) {
  shader.setModelPose(cube_pose_);
  cube_drawer_.draw();
}

}  // namespace CuteGL


int main(int argc, char *argv[]) {
  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<CuteGL::MyRenderer> renderer(new CuteGL::MyRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  CuteGL::WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);
  QSurfaceFormat format = viewer.format();
  format.setSamples(16);
  viewer.setFormat(format);


  // Set camera pose via lookAt
  viewer.camera().extrinsics() = CuteGL::getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 5.0f),
                                                                 Eigen::Vector3f::Zero(),
                                                                 Eigen::Vector3f::UnitY());

  renderer->camera_pose_ = CuteGL::getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 5.0f),
                                                        Eigen::Vector3f::Zero(),
                                                        Eigen::Vector3f::UnitY()).inverse();

  viewer.setSceneRadius(10.0f);

  viewer.showAndWaitTillExposed();
  return app.exec();
}



