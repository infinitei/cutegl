/**
 * @file demoOffScreenOpenGLSurface.cpp
 * @brief demoOffScreenOpenGLSurface
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/OffScreenOpenGLSurface.h"
#include "CuteGL/Core/Camera.h"
#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Core/VertexData.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/Core/OpenGLFunctions.h"

#include <QGuiApplication>
#include <QOpenGLShaderProgram>
#include <QDebug>
#include <iostream>
#include <array>

namespace CuteGL {

class OffScreenTriangleRender : public OffScreenOpenGLSurface {
 public:
  OffScreenTriangleRender()
      : vbo_(0),
        vao_(0),
        program_(),
        uniform_view_T_world_loc_(-1),
        camera_(1024, 768) {

    camera().extrinsics() = getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 3.0f),
                                                      Eigen::Vector3f::Zero(),
                                                      Eigen::Vector3f::UnitY());
  }

  Camera& camera() {return camera_;} ///< non const reference to camera
  const Camera& camera() const {return camera_;} ///< const reference to camera


 protected:
  void resizeGL(int width, int height) {
    OffScreenOpenGLSurface::resizeGL(width, height);
    camera().resize(width, height);
  }

  void init() {
    // Application specific initialization
    {
      program_.addShaderFromSourceFile(QOpenGLShader::Vertex, CUTEGL_SHADER_FOLDER "/simple_vs.glsl");
      program_.addShaderFromSourceFile(QOpenGLShader::Fragment, CUTEGL_SHADER_FOLDER "/simple_fs.glsl");
      program_.link();
      program_.bind();

      uniform_view_T_world_loc_ = program_.uniformLocation("view_T_world");

      using VertexData = VertexXYZRGBA;
      using PositionType = VertexData::PositionType;
      using ColorType = VertexData::ColorType;

      std::array<VertexXYZRGBA, 3> vertices;
      vertices[0].position = PositionType(0.5f, -0.5f, 0.0f);
      vertices[0].color = ColorType(255, 0, 0, 255);

      vertices[1].position = PositionType(-0.5f, -0.5f, 0.0f);
      vertices[1].color = ColorType(0, 255, 0, 255);

      vertices[2].position = PositionType(0.0f, 0.5f, 0.0f);
      vertices[2].color = ColorType(0, 0, 255, 255);

      // 1. Bind VAO
      glfuncs_->glGenVertexArrays(1, &vao_);
      glfuncs_->glBindVertexArray(vao_);

      // 2. Copy our vertices array in a vertex buffer for OpenGL to use
      glfuncs_->glGenBuffers(1, &vbo_);
      glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
      glfuncs_->glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexData), vertices.data(), GL_STATIC_DRAW);

      // 3. Then set the vertex attributes pointers

      // Position attribute
      const int position_index = program_.attributeLocation("vertex_position");
      assert(position_index != -1);
      program_.setAttributeArray(position_index, GL_FLOAT, (void*) offsetof(VertexData, position), 3, sizeof(VertexData));
      program_.enableAttributeArray(position_index);

      // Color attribute
      const int color_index = program_.attributeLocation("vertex_color");
      assert(color_index != -1);
      program_.setAttributeArray(color_index, GL_UNSIGNED_BYTE, (void*) offsetof(VertexData, color), 4, sizeof(VertexData));
      program_.enableAttributeArray(color_index);

      // Release (unbind) all (VBO should be released after VAO)
      glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
      glfuncs_->glBindVertexArray(0);

      program_.release();
    }
  }

  void draw() {
    program_.bind();
    glfuncs_->glUniformMatrix4fv(uniform_view_T_world_loc_, 1, GL_FALSE, camera().projectionViewMatrix().data());
    {
      glfuncs_->glBindVertexArray(vao_);
      glfuncs_->glDrawArrays(GL_TRIANGLES, 0, 3);
      glfuncs_->glBindVertexArray(0);
    }
    program_.release();
  }


 private:
  GLuint vbo_;
  GLuint vao_;
  QOpenGLShaderProgram program_;
  int uniform_view_T_world_loc_;
  Camera camera_;
};

}  // namespace CuteGL



int main(int argc, char* argv[]) {
   QGuiApplication a(argc, argv);

   CuteGL::OffScreenTriangleRender offscreen_fbo;
   offscreen_fbo.resize(1024, 768);
   offscreen_fbo.create();

   offscreen_fbo.render();

   QImage image = offscreen_fbo.readColorBuffer();
   image.save(QString("offscreen_fbo.png"));

   return EXIT_SUCCESS;
}



