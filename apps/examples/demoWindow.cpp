/**
 * @file demoWindow.cpp
 * @brief demoWindow
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowOpenGLSurface.h"
#include <QGuiApplication>

int main(int argc, char *argv[]) {
  QGuiApplication app(argc, argv);

  // Set the window up
  CuteGL::WindowOpenGLSurface window;
  window.setBackgroundColor(55, 55, 55);
  window.resize(1024, 768);

  window.show();
  return app.exec();
}
