/**
 * @file demoInstancedVoxelDrawer.cpp
 * @brief demoInstancedVoxelDrawer
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/AbstractRenderer.h"
#include "CuteGL/Shaders/InstancedVoxelShader.h"
#include "CuteGL/Drawers/InstancedVoxelDrawer.h"
#include "CuteGL/Core/PoseUtils.h"
#include <QGuiApplication>
#include <memory>
#include <random>
#include <iostream>

namespace CuteGL {

class VoxelRenderer : public AbstractRenderer {
 public:

  enum ColorMode {
    CM_FREE = -1,
    CM_SEMANTIC = 0,
    CM_COLOR_HEIGHT = 1,
    CM_TEXTURE = 2,
    CM_BLEND = 3
  };


  VoxelRenderer()
      : voxel_drawer_(),
        global_pose_(Eigen::Isometry3f::Identity()),
        color_mode_(ColorMode::CM_SEMANTIC),
        blend_alpha_(0.5f) {
  }



  void toggleColorMode();

  void setColorMode(const ColorMode& cm) { color_mode_ = cm; }
  const ColorMode& colorMode() const { return color_mode_; }

  float blendAlpha() const {return blend_alpha_;}
  void changeBlendAlpha(bool increase);

  InstancedVoxelDrawer& voxelDrawer() {return voxel_drawer_;}
  const InstancedVoxelDrawer& voxelDrawer() const {return voxel_drawer_;}

 protected:
  virtual void draw(InstancedVoxelShader& shader);

 private:
  InstancedVoxelDrawer voxel_drawer_;
  Eigen::Isometry3f global_pose_;

 private:
  void initialize();
  void draw(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics);

 private:
  InstancedVoxelShader voxel_shader_;

  ColorMode color_mode_;
  float blend_alpha_;

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

void VoxelRenderer::initialize() {
  if(!hasValidContext()) {
    throw std::runtime_error("Context is NULL or Invalid");
  }

  if(!hasValidOpenGLFunctions()) {
    throw std::runtime_error("OpenGL Functions are not VALID");
  }

  DrawerBase::setOpenGLFunctions(glFuncs());

  voxel_shader_.init(glFuncs());

  voxel_drawer_.init(voxel_shader_.program);
}

void VoxelRenderer::draw(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics) {
  voxel_shader_.program.bind();
  voxel_shader_.setProjectViewMatrix(intrinsics, extrinsics);
  {
    draw(voxel_shader_);
  }
  voxel_shader_.program.release();
}

void VoxelRenderer::draw(InstancedVoxelShader& shader) {
  shader.setModelPose(global_pose_);
  voxel_drawer_.draw();
}

void VoxelRenderer::toggleColorMode() {
  switch (color_mode_) {
  case ColorMode::CM_SEMANTIC:
    color_mode_ = ColorMode::CM_COLOR_HEIGHT;
    break;
  case ColorMode::CM_COLOR_HEIGHT:
    color_mode_ = ColorMode::CM_TEXTURE;
    break;
  case ColorMode::CM_TEXTURE:
    color_mode_ = ColorMode::CM_BLEND;
    break;
  case ColorMode::CM_BLEND:
    color_mode_ = ColorMode::CM_SEMANTIC;
    break;
  default:
    break;
  }

  voxel_shader_.program.bind();
  voxel_shader_.setColorMode(color_mode_);
  voxel_shader_.program.release();
}

void VoxelRenderer::changeBlendAlpha(bool increase) {
  const float step = 0.05f;
  const float min = step * 0.999f;
  const float max = 1.0f - min;
  if (increase && (blend_alpha_ < max))
    blend_alpha_ += step;
  else if((!increase) && (blend_alpha_ > min))
    blend_alpha_ -= step;

  voxel_shader_.program.bind();
  voxel_shader_.setBlendAlpha(blend_alpha_);
  voxel_shader_.program.release();
}


class VoxelWindowRenderViewer : public WindowRenderViewer {
public:

  enum Action {
    TOGGLE_COLOR_MODE = 6,
    INCREASE_BLEND_ALPHA = 7,
    DECREASE_BLEND_ALPHA = 8
  };


  VoxelWindowRenderViewer(VoxelRenderer* renderer)
      : WindowRenderViewer(renderer),
        renderer_(renderer) {

    keyboard_handler_.registerKey(TOGGLE_COLOR_MODE, Qt::Key_M, "Toggle Color Mode");
    keyboard_handler_.registerKey(INCREASE_BLEND_ALPHA, Qt::ALT + Qt::Key_Up, "Increase blend alpha value");
    keyboard_handler_.registerKey(DECREASE_BLEND_ALPHA, Qt::ALT + Qt::Key_Down, "Decrease blend alpha value");
  }

  virtual void handleKeyboardAction(int action) {
    switch (action) {
      case TOGGLE_COLOR_MODE:
        renderer_->toggleColorMode();
        break;
      case INCREASE_BLEND_ALPHA:
        renderer_->changeBlendAlpha(true);
        break;
      case DECREASE_BLEND_ALPHA:
        renderer_->changeBlendAlpha(false);
        break;
      default:
        WindowRenderViewer::handleKeyboardAction(action);
        break;
    }
  }

protected:
  VoxelRenderer* renderer_;
};

}  // namespace CuteGL


int main(int argc, char *argv[]) {
  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<CuteGL::VoxelRenderer> renderer(new CuteGL::VoxelRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  CuteGL::VoxelWindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);


  // Set camera pose via lookAt
  viewer.camera().extrinsics() = CuteGL::getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 5.0f),
                                                                 Eigen::Vector3f::Zero(),
                                                                 Eigen::Vector3f::UnitY());

  viewer.showAndWaitTillExposed();



  std::vector<CuteGL::VoxelInfo> voxels;
  const std::size_t num_of_voxles = 8000000;
  voxels.reserve(num_of_voxles);

  std::random_device rd;
  std::mt19937 gen(rd());

  std::uniform_int_distribution<> label_dist(1, 9);
  std::uniform_real_distribution<float> position_dist(-1.0f, 1.0f);
  std::uniform_int_distribution<> color_dist(0, 255);

  for (size_t i = 0; i < num_of_voxles; ++i) {

    CuteGL::VoxelInfo vi (CuteGL::VoxelInfo::PositionType(position_dist(gen), position_dist(gen), position_dist(gen)),
                          label_dist(gen),
                          CuteGL::VoxelInfo::ColorType(color_dist(gen), color_dist(gen), color_dist(gen), 255));
    voxels.push_back(vi);
  }
  renderer->voxelDrawer().setVoxelSize(0.05f);
  renderer->voxelDrawer().addNewVoxels(voxels);



  return app.exec();
}
