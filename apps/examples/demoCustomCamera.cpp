/**
 * @file demoCustomCamera.cpp
 * @brief demoCustomCamera
 *
 * @author Abhijit Kundu
 */

#define _USE_MATH_DEFINES

#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/BasicLightRenderer.h"
#include "CuteGL/Core/PoseUtils.h"
#include <QGuiApplication>
#include <memory>
#include <iostream>

int main(int argc, char *argv[]) {
  using namespace CuteGL;
  using namespace Eigen;

  if(argc < 2) {
    std::cout << "Usage: " << argv[0] << " [camera_type]\n";
    std::cout << "   0 = " << " [perspective_camera]\n";
    std::cout << "   1 = " << " [orthographic_camera]\n";
    return EXIT_SUCCESS;
  }

  const int camera_type = std::stoi(argv[1]);
  std::cout << "Camera Type = " << camera_type << "\n";

  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<BasicLightRenderer> renderer(new BasicLightRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.setFixedCameraFrustumPlanes(true);

  const int W = 1025;
  const int H = 768;

  viewer.resize(W, H);

  // z position of the camera center
  const float z = 5.0f;

  // Set camera pose via lookAt
  viewer.camera().extrinsics() = getExtrinsicsFromLookAt(Vector3f(0.0f, 0.0f, z),
                                                         Vector3f::Zero(),
                                                         Vector3f::UnitY());

  viewer.showAndWaitTillExposed();


  // Set focal length so that (0, 1, 0) is the top most point
  float fy = H * z / 2.0f;
  float fx = W * z / 2.0f;

  Matrix4f persp_proj_mat = getGLPerspectiveProjection(fx, fy, 0.0f, W/2.0f, H/2.0f, W, H, 0.1f, 100.0f);

  Matrix4f ortho_proj_mat = getGLOrthographicProjection(1.0f, 1.0f, 0.1f, 100.0f);


  if(camera_type == 0)
    viewer.camera().intrinsics() = persp_proj_mat;
  else
    viewer.camera().intrinsics() = ortho_proj_mat;




  std::cout << "persp_proj_mat =\n" << persp_proj_mat << "\n\n";
  std::cout << "ortho_proj_mat =\n" << ortho_proj_mat << "\n\n";
  std::cout << "camera().intrinsics() =\n" << viewer.camera().intrinsics().matrix() << "\n\n";

  return app.exec();
}



