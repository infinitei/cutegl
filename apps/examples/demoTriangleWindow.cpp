/**
 * @file demoTriangleWindow.cpp
 * @brief demoTriangleWindow
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/WindowOpenGLSurface.h"
#include "CuteGL/Core/VertexData.h"
#include "CuteGL/Utils/QtUtils.h"
#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Core/Config.h"
#include <QOpenGLShaderProgram>
#include <QGuiApplication>
#include <array>

namespace CuteGL {

class TriangleWindow : public WindowOpenGLSurface {

 public:
  TriangleWindow();
  ~TriangleWindow();

  void draw();

 private:
  void init();

  // OpenGL State Information
  GLuint vbo_;
  GLuint vao_;
  QOpenGLShaderProgram program_;
  int uniform_view_T_world_loc_;
};

TriangleWindow::TriangleWindow()
    : vbo_(0),
      vao_(0),
      program_(),
      uniform_view_T_world_loc_(-1) {
  camera().extrinsics() = getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 3.0f),
                                                  Eigen::Vector3f::Zero(),
                                                  Eigen::Vector3f::UnitY());
}

TriangleWindow::~TriangleWindow() {
  makeCurrent();
  glfuncs_->glDeleteVertexArrays(1, &vao_);
  glfuncs_->glDeleteBuffers(1, &vbo_);
  doneCurrent();
}

void TriangleWindow::init() {
  // Application specific initialization
  {
    program_.addShaderFromSourceFile(QOpenGLShader::Vertex, CUTEGL_SHADER_FOLDER "/simple_vs.glsl");
    program_.addShaderFromSourceFile(QOpenGLShader::Fragment, CUTEGL_SHADER_FOLDER "/simple_fs.glsl");
    program_.link();
    program_.bind();

    uniform_view_T_world_loc_ = program_.uniformLocation("view_T_world");

    using VertexData = VertexXYZRGBA;
    using PositionType = VertexData::PositionType;
    using ColorType = VertexData::ColorType;

    std::array<VertexXYZRGBA, 3> vertices;
    vertices[0].position = PositionType(0.5f, -0.5f, 0.0f);
    vertices[0].color = ColorType(255, 0, 0, 255);

    vertices[1].position = PositionType(-0.5f, -0.5f, 0.0f);
    vertices[1].color = ColorType(0, 255, 0, 255);

    vertices[2].position = PositionType(0.0f,  0.5f, 0.0f);
    vertices[2].color = ColorType(0, 0, 255, 255);

    // 1. Bind VAO
    glfuncs_->glGenVertexArrays(1, &vao_);
    glfuncs_->glBindVertexArray(vao_);

    // 2. Copy our vertices array in a vertex buffer for OpenGL to use
    glfuncs_->glGenBuffers(1, &vbo_);
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glfuncs_->glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexData), vertices.data(), GL_STATIC_DRAW);


    // 3. Then set the vertex attributes pointers

    // Position attribute
    const int position_index = program_.attributeLocation("vertex_position");
    assert(position_index != -1);
    program_.setAttributeArray(position_index, GL_FLOAT, (void*) offsetof(VertexData, position), 3, sizeof(VertexData));
    program_.enableAttributeArray(position_index);

    // Color attribute
    const int color_index = program_.attributeLocation("vertex_color");
    assert(color_index != -1);
    program_.setAttributeArray(color_index, GL_UNSIGNED_BYTE, (void*) offsetof(VertexData, color), 4, sizeof(VertexData));
    program_.enableAttributeArray(color_index);

    // Release (unbind) all (VBO should be released after VAO)
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
    glfuncs_->glBindVertexArray(0);

    program_.release();
  }
}

void TriangleWindow::draw() {
  program_.bind();
  glfuncs_->glUniformMatrix4fv(uniform_view_T_world_loc_, 1, GL_FALSE, camera().projectionViewMatrix().data());
  {
    glfuncs_->glBindVertexArray(vao_);
    glfuncs_->glDrawArrays(GL_TRIANGLES, 0, 3);
    glfuncs_->glBindVertexArray(0);
  }
  program_.release();
}

}  // end namespace CuteGL




int main(int argc, char *argv[]) {
  QGuiApplication app(argc, argv);

  // Set the window up
  CuteGL::TriangleWindow triangle_window;
  triangle_window.showAndWaitTillExposed();
  triangle_window.setBackgroundColor(51, 76, 76);

  return app.exec();
}



