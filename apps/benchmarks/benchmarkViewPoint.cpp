/**
 * @file benchmarkViewPoint.cpp
 * @brief benchmarkViewPoint
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Utils/Timer.h"
#include <Eigen/EulerAngles>
#include <Eigen/StdVector>
#include <iostream>

template<typename Scalar>
Eigen::Transform<Scalar, 3, Eigen::Isometry>
viewPoint2pose_verA(const Scalar azimuth, const Scalar elevation, const Scalar tilt, const Scalar distance) {
  typedef Eigen::Matrix<Scalar, 3, 1> Vector3;
  typedef Eigen::Matrix<Scalar, 3, 3> Matrix3;
  typedef Eigen::Transform<Scalar, 3, Eigen::Isometry> Isometry3;

  const Vector3 eye = CuteGL::spherical2cartesian(azimuth, elevation, distance);

  const Vector3 z = (Vector3::Zero() - eye).normalized();
  const Vector3 x = ((-Vector3::UnitZ()).cross(z)).normalized(); // minus upVector since yc is pointing down
  const Vector3 y = z.cross(x);
  Matrix3 rot = (Matrix3() << x, y, z).finished();

  // Apply tilt
  rot *= Eigen::AngleAxis<Scalar>(tilt, Vector3::UnitZ()).toRotationMatrix();

  Isometry3 extrinsic = Isometry3::Identity();
  extrinsic.linear() = rot.transpose();
  extrinsic.translation() = -rot.transpose() * eye;

  return extrinsic;
}

template<typename Scalar>
Eigen::Transform<Scalar, 3, Eigen::Isometry>
viewPoint2pose_verB(const Scalar azimuth, const Scalar elevation, const Scalar tilt, const Scalar distance) {
  typedef Eigen::Transform<Scalar, 3, Eigen::Isometry> Isometry3;
  typedef Eigen::EulerSystem<-Eigen::EULER_Z, Eigen::EULER_Y, -Eigen::EULER_Z> ViewPointSystem;
  typedef Eigen::EulerAngles<Scalar, ViewPointSystem> ViewPointd;

  Isometry3 vp_pose = Isometry3::Identity();
  vp_pose.linear() = ViewPointd(tilt + M_PI/2, elevation + M_PI/2, azimuth).toRotationMatrix();
  vp_pose.translation().z() = distance;

  return vp_pose;
}

template<typename Scalar>
Eigen::Transform<Scalar, 3, Eigen::Isometry>
viewPoint2pose_verC(const Scalar azimuth, const Scalar elevation, const Scalar tilt, const Scalar distance) {
  typedef Eigen::Transform<Scalar, 3, Eigen::Isometry> Isometry3;
  typedef Eigen::AngleAxis<Scalar> AngleAxisS;
  typedef Eigen::Matrix<Scalar, 3, 3> Matrix3;
  typedef Eigen::Matrix<Scalar, 3, 1> Vector3;

  Isometry3 model_pose = Isometry3::Identity();
  Matrix3 model_rot;
  model_rot = AngleAxisS(-tilt - M_PI / 2.0, Vector3::UnitZ())
            * AngleAxisS(elevation + M_PI / 2.0, Vector3::UnitY())
            * AngleAxisS(-azimuth, Vector3::UnitZ());

  model_pose.linear() = model_rot;
  model_pose.translation().z() = distance;

  return model_pose;
}

int main(int argc, char **argv) {
  std::size_t num_of_trials = 10000;
  std::vector<Eigen::Vector3d> viewpoints(num_of_trials);

  for (Eigen::Vector3d& vp : viewpoints) {
    vp = Eigen::Vector3d::Random().cwiseProduct(Eigen::Vector3d(M_PI, M_PI / 2.0, M_PI));
  }

  std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>> poses(num_of_trials);

  CuteGL::Timer timer;

  {
    std::cout << "\n---------- Method A ------------------\n";

    timer.start();
    for (std::size_t i = 0; i < num_of_trials; ++i) {
      const Eigen::Vector3d& vp = viewpoints[i];
      poses[i] = viewPoint2pose_verA(vp.x(), vp.y(), vp.z(), 2.0);
    }
    const double elapsed_in_us = timer.elapsed_in_us();
    std::cout << "elapsed_in_us = " << elapsed_in_us << std::endl;
  }

  {
    std::cout << "\n---------- Method B ------------------\n";

    timer.start();
    for (std::size_t i = 0; i < num_of_trials; ++i) {
      const Eigen::Vector3d& vp = viewpoints[i];
      poses[i] = viewPoint2pose_verB(vp.x(), vp.y(), vp.z(), 2.0);
    }
    const double elapsed_in_us = timer.elapsed_in_us();
    std::cout << "elapsed_in_us = " << elapsed_in_us << std::endl;
  }

  {
    std::cout << "\n---------- Method C ------------------\n";

    timer.start();
    for (std::size_t i = 0; i < num_of_trials; ++i) {
      const Eigen::Vector3d& vp = viewpoints[i];
      poses[i] = viewPoint2pose_verC(vp.x(), vp.y(), vp.z(), 2.0);
    }
    const double elapsed_in_us = timer.elapsed_in_us();
    std::cout << "elapsed_in_us = " << elapsed_in_us << std::endl;
  }

  {
    std::cout << "\n---------- Method A ------------------\n";

    timer.start();
    for (std::size_t i = 0; i < num_of_trials; ++i) {
      const Eigen::Vector3d& vp = viewpoints[i];
      poses[i] = viewPoint2pose_verA(vp.x(), vp.y(), vp.z(), 2.0);
    }
    const double elapsed_in_us = timer.elapsed_in_us();
    std::cout << "elapsed_in_us = " << elapsed_in_us << std::endl;
  }

  {
    std::cout << "\n---------- Method B ------------------\n";

    timer.start();
    for (std::size_t i = 0; i < num_of_trials; ++i) {
      const Eigen::Vector3d& vp = viewpoints[i];
      poses[i] = viewPoint2pose_verB(vp.x(), vp.y(), vp.z(), 2.0);
    }
    const double elapsed_in_us = timer.elapsed_in_us();
    std::cout << "elapsed_in_us = " << elapsed_in_us << std::endl;
  }

  {
    std::cout << "\n---------- Method C ------------------\n";

    timer.start();
    for (std::size_t i = 0; i < num_of_trials; ++i) {
      const Eigen::Vector3d& vp = viewpoints[i];
      poses[i] = viewPoint2pose_verC(vp.x(), vp.y(), vp.z(), 2.0);
    }
    const double elapsed_in_us = timer.elapsed_in_us();
    std::cout << "elapsed_in_us = " << elapsed_in_us << std::endl;
  }


  return EXIT_SUCCESS;
}



