/**
 * @file benchmarkContractions.cu
 * @brief benchmarkContractions
 *
 * @author Abhijit Kundu
 */

#if defined __NVCC__
  // Disable the "pointless comparison of unsigned integer with zero" warning
  #pragma diag_suppress 186
  // Disable the "calling a __host__ function from a __host__ __device__ function is not allowed" messages
  #pragma diag_suppress 2739
#endif

#define EIGEN_DEFAULT_DENSE_INDEX_TYPE int
#define EIGEN_USE_GPU

#include <cublas_v2.h>
#include "CuteGL/Utils/CudaUtils.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/Core/SMPLData.h"
#include "CuteGL/IO/ImportSMPL.h"

template<typename T, size_t N>
std::ostream& operator<<(std::ostream& os, const Eigen::array<T, N>& val) {
  os << "[";
  for (size_t i =0 ; i < N; ++i)
    os << val[i] << " x ";
  os << "\b\b\b]  ";
  return os;
}


namespace CuteGL {

template <int NumShapeParams, typename T>
__global__
void custom_kernel_cm(const int n, const T* d_shape_displacements, const T* d_shape_params, const T* d_template_vertices, T* d_bs_plus_t) {
  int i = threadIdx.x + blockIdx.x*blockDim.x;
  if (i < n) {
    T dot = 0;
#pragma unroll
    for (int j=0; j< NumShapeParams; ++j) {
      dot += d_shape_displacements[j*n + i] * d_shape_params[j];
    }
    d_bs_plus_t[i] = d_template_vertices[i] + dot;
  }
}

template <int NumShapeParams, typename T>
__global__
void custom_kernel_cm_smem(const int n, const T* d_shape_displacements, const T* d_shape_params, const T* d_template_vertices, T* d_bs_plus_t) {
  __shared__ T smem[NumShapeParams];
  if (threadIdx.x < NumShapeParams)
    smem[threadIdx.x] = d_shape_params[threadIdx.x];
  __syncthreads();

  int i = threadIdx.x + blockIdx.x*blockDim.x;
  if (i < n) {
    T dot = 0;
#pragma unroll
    for (int j=0; j< NumShapeParams; ++j) {
      dot += d_shape_displacements[j*n + i] * smem[j];
    }
    d_bs_plus_t[i] = d_template_vertices[i] + dot;
  }
}

template <typename Scalar>
__inline__ __device__
Scalar warpReduceSum(Scalar val) {
  for (unsigned int offset = warpSize/2; offset > 0; offset /= 2)
    val += __shfl_down(val, offset);
  return val;
}

template <typename T>
__global__
void sprase_mat_mul(const int* d_jr_indices, const T* d_jr_values, const T* d_bs_plus_t, T* d_jl) {
  int i = threadIdx.x + blockIdx.x * 32;
  int jr_ind = d_jr_indices[i];

  T dot_prod = 0;
  if (jr_ind >= 0) {
    dot_prod = d_jr_values[i] * d_bs_plus_t[jr_ind*3 + threadIdx.y];
  }
  dot_prod = warpReduceSum(dot_prod);

  if (threadIdx.x == 0) {
    d_jl[blockIdx.x * 3 + threadIdx.y] = dot_prod;
  }
}

class Engine {
 public:
  using PositionScalar = float;
  using SMPLDataType = SMPLData<PositionScalar>;

  using Tensor3 = Eigen::Tensor<PositionScalar, 3, Eigen::ColMajor>;
  using Tensor2 = Eigen::Tensor<PositionScalar, 2, Eigen::ColMajor>;
  using Tensor1 = Eigen::Tensor<PositionScalar, 1, Eigen::ColMajor>;

  Engine()
      : N_(0),
        K_(0),
        num_shape_params_(0),
        num_pose_params_(0),
        d_shape_params_(nullptr),
        d_pose_params_(nullptr),
        d_template_vertices_(nullptr),
        d_shape_displacements_(nullptr),
        d_joint_regressor_(nullptr),
        d_jr_indices_(nullptr),
        d_jr_values_(nullptr),
        d_joint_locations_(nullptr),
        d_bs_plus_t_(nullptr) {
    static_assert(std::is_same<Eigen::Index, int>::value, "Need to set EIGEN_DEFAULT_DENSE_INDEX_TYPE to int");
  }

  ~Engine() {
    cudaFreeIfAllocated(d_shape_params_);
    cudaFreeIfAllocated(d_pose_params_);
    cudaFreeIfAllocated(d_template_vertices_);
    cudaFreeIfAllocated(d_shape_displacements_);
    cudaFreeIfAllocated(d_joint_regressor_);

    cudaFreeIfAllocated(d_joint_locations_);
    cudaFreeIfAllocated(d_bs_plus_t_);
  }

  void uploadSMPLData(const SMPLDataType& smpl_data) {
    N_ = smpl_data.template_vertices.rows();
    K_ = smpl_data.blend_weights.cols() - 1;
    num_shape_params_ = smpl_data.shape_displacements.dimension(0);
    num_pose_params_ = 3 * (K_ + 1);
    std::cout << "N_ = " << N_ <<  " K_ = " << K_
              <<  " num_shape_params_ = " << num_shape_params_
              <<  " num_pose_params_ = " << num_pose_params_  << std::endl;

    Eigen::Matrix<int, Eigen::Dynamic, 32, Eigen::RowMajor> jr_indices((K_ + 1), 32);
    Eigen::Matrix<PositionScalar, Eigen::Dynamic, 32, Eigen::RowMajor> jr_values((K_ + 1), 32);
    Eigen::VectorXi nz_counts(K_ + 1);

    jr_indices.setConstant(-1);
    jr_values.setZero();

    for (Eigen::Index r = 0; r < smpl_data.joint_regressor.rows(); ++r) {
      int nz_count = 0;
      for (Eigen::Index c = 0; c < smpl_data.joint_regressor.cols(); ++c) {
        PositionScalar value = smpl_data.joint_regressor(r, c);
        if(value != 0) {
          if (nz_count >= 32)
            throw std::runtime_error("Do not support more than 32 nnz per row");

          jr_indices(r, nz_count) = c;
          jr_values(r, nz_count) = value;
          ++nz_count;
        }
      }
      nz_counts[r] = nz_count;
    }

//    std::cout << "jr_indices=\n" << jr_indices << "\n";
//    std::cout << "jr_values=\n" << jr_values << "\n";
//    std::cout << "nz_counts=\n" << nz_counts << "\n";

    CHECK_CUDA(cudaMalloc(&d_jr_indices_, jr_indices.size() * sizeof(int)));
    CHECK_CUDA(cudaMalloc(&d_jr_values_, jr_values.size() * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMemcpy(d_jr_indices_, jr_indices.data(), jr_indices.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA(cudaMemcpy(d_jr_values_, jr_values.data(), jr_values.size() * sizeof(PositionScalar), cudaMemcpyHostToDevice));


    CHECK_CUDA(cudaMalloc(&d_template_vertices_, smpl_data.template_vertices.size() * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMalloc(&d_joint_regressor_, smpl_data.joint_regressor.size() * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMalloc(&d_shape_displacements_, smpl_data.shape_displacements.size() * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMemcpy(d_template_vertices_, smpl_data.template_vertices.data(), smpl_data.template_vertices.size() * sizeof(PositionScalar), cudaMemcpyHostToDevice));
    CHECK_CUDA(cudaMemcpy(d_joint_regressor_, smpl_data.joint_regressor.data(), smpl_data.joint_regressor.size() * sizeof(PositionScalar), cudaMemcpyHostToDevice));
    CHECK_CUDA(cudaMemcpy(d_shape_displacements_, smpl_data.shape_displacements.data(), smpl_data.shape_displacements.size() * sizeof(PositionScalar), cudaMemcpyHostToDevice));



    Eigen::Matrix<PositionScalar, Eigen::Dynamic, 1> shape_params(num_shape_params_);
    shape_params.setZero();
    shape_params[1] = -2.0f;
    shape_params[6] = -0.5;
    shape_params[7] = -0.5;
    shape_params[8] = -1.3;


    CHECK_CUDA(cudaMalloc(&d_shape_params_, num_shape_params_ * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMalloc(&d_pose_params_, num_pose_params_ * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMemcpy(d_shape_params_, shape_params.data(), num_shape_params_ * sizeof(PositionScalar), cudaMemcpyHostToDevice));
    CHECK_CUDA(cudaMemset(d_pose_params_, 0, num_pose_params_ * sizeof(PositionScalar)));

    CHECK_CUDA(cudaMalloc(&d_joint_locations_, (K_ + 1) * 3 * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMalloc(&d_bs_plus_t_, N_ * 3 * sizeof(PositionScalar)));
  }

//  void contraction_eigen() {
//    using DimPair = typename Tensor1::DimensionPair;
//    Eigen::array<DimPair, 1> product_dimsT3xT1(DimPair(2, 0));
//    Eigen::array<DimPair, 1> product_dimsT2xT2(DimPair(1, 0));
//
//    Eigen::CudaStreamDevice stream;
//    Eigen::GpuDevice gpu_device(&stream);
//
//    GpuTimer gpu_timer;
//    gpu_timer.Start();
//
//    // TPBS = T + B * S
//    Eigen::TensorMap<Tensor3> gput_shape_displacements(d_shape_displacements_, Eigen::array<int, 3>(3, N_, num_shape_params_));
//    Eigen::TensorMap<Tensor1> gput_betas(d_shape_params_, Eigen::array<int, 1>(num_shape_params_));
//    Eigen::TensorMap<Tensor2> gput_template_vertices(d_template_vertices_, Eigen::array<int, 2>(3, N_));
//    Eigen::TensorMap<Tensor2> gput_d_bs_plus_t(d_bs_plus_t_, Eigen::array<int, 2>(3, N_));
//    gput_d_bs_plus_t.device(gpu_device) = gput_template_vertices + gput_shape_displacements.contract(gput_betas, product_dimsT3xT1);
//
//    gpu_timer.Stop();
//    std::cout << "GPU Time = " << gpu_timer.ElapsedMillis() << " ms." << std::endl;
//  }

  void contraction_custom() {
    if (num_shape_params_ != 10)
      throw std::runtime_error("num_shape_params_ != 10");

    GpuTimer gpu_timer;
    gpu_timer.start();

    const int n  = 3 * N_;
    const int blocksize = 1024; // Minimum: NumLabels * NumLabels
    const int gridsize = (n + blocksize - 1) / blocksize;

    // TPBS = T + B * S
    custom_kernel_cm<10><<<gridsize ,blocksize>>>(n, d_shape_displacements_, d_shape_params_, d_template_vertices_, d_bs_plus_t_);

    gpu_timer.stop();
    std::cout << "GPU Time = " << gpu_timer.elapsed_in_ms() << " ms." << std::endl;
  }

  void contraction_custom_smem() {
    if (num_shape_params_ != 10)
      throw std::runtime_error("num_shape_params_ != 10");

    GpuTimer gpu_timer;
    gpu_timer.start();

    const int n  = 3 * N_;
    const int blocksize = 1024; // Minimum: NumLabels * NumLabels
    const int gridsize = (n + blocksize - 1) / blocksize;

    // TPBS = T + B * S
    custom_kernel_cm_smem<10><<<gridsize ,blocksize>>>(n, d_shape_displacements_, d_shape_params_, d_template_vertices_, d_bs_plus_t_);

    gpu_timer.stop();
    std::cout << "GPU Time = " << gpu_timer.elapsed_in_ms() << " ms." << std::endl;
  }

//  void contraction_cblas() {
//    cublasHandle_t handle;
//    CHECK_CUDA(cublasCreate(&handle));
//
//
//
//    GpuTimer gpu_timer;
//    gpu_timer.Start();
//
//    CHECK_CUDA(cublasScopy(handle,  3 * N_, d_template_vertices_, 1, d_bs_plus_t_, 1));
//
//    const float alpha = 1.0f;
//    const float beta = 1.0f;
//
//    CHECK_CUDA(cublasSgemv(handle, CUBLAS_OP_N, 3 * N_, num_shape_params_,
//                               &alpha, d_shape_displacements_, 3 * N_,
//                               d_shape_params_, 1,
//                               &beta, d_bs_plus_t_, 1));
//
//    gpu_timer.Stop();
//    std::cout << "GPU Time = " << gpu_timer.ElapsedMillis() << " ms." << std::endl;
//
//    // Destroy the cublas handle
//    CHECK_CUDA(cublasDestroy(handle));
//  }


//  void smpl_eigen() {
//    using DimPair = typename Tensor1::DimensionPair;
//    Eigen::array<DimPair, 1> product_dimsT3xT1(DimPair(2, 0));
//    Eigen::array<DimPair, 1> product_dimsT2xT2(DimPair(1, 0));
//
//    Eigen::CudaStreamDevice stream;
//    Eigen::GpuDevice gpu_device(&stream);
//
//    GpuTimer gpu_timer;
//    gpu_timer.Start();
//
//    // TPBS = T + B * S
//    Eigen::TensorMap<Tensor3> gput_shape_displacements(d_shape_displacements_, Eigen::array<int, 3>(3, N_, num_shape_params_));
//    Eigen::TensorMap<Tensor1> gput_betas(d_shape_params_, Eigen::array<int, 1>(num_shape_params_));
//    Eigen::TensorMap<Tensor2> gput_template_vertices(d_template_vertices_, Eigen::array<int, 2>(3, N_));
//    Eigen::TensorMap<Tensor2> gput_d_bs_plus_t(d_bs_plus_t_, Eigen::array<int, 2>(3, N_));
//    gput_d_bs_plus_t.device(gpu_device) = gput_template_vertices + gput_shape_displacements.contract(gput_betas, product_dimsT3xT1);
//
//
//    // J = TPBS * JR
//    Eigen::TensorMap<Tensor2> gput_joint_regressor(d_joint_regressor_, Eigen::array<int, 2>(N_, K_ + 1));
//    Eigen::TensorMap<Tensor2> gput_joint_locations(d_joint_locations_, Eigen::array<int, 2>(3, K_ + 1));
//    gput_joint_locations.device(gpu_device) =  gput_d_bs_plus_t.contract(gput_joint_regressor, product_dimsT2xT2);
//
//    gpu_timer.Stop();
//    std::cout << "GPU Time = " << gpu_timer.ElapsedMillis() << " ms." << std::endl;
//  }

  void smpl_custom() {
    if (num_shape_params_ != 10)
      throw std::runtime_error("num_shape_params_ != 10");

    GpuTimer gpu_timer;
    gpu_timer.start();

    {
      const int n  = 3 * N_;
      const int blocksize = 1024; // Minimum: NumLabels * NumLabels
      const int gridsize = (n + blocksize - 1) / blocksize;

      // TPBS = T + B * S
      custom_kernel_cm<10><<<gridsize, blocksize>>>(n, d_shape_displacements_, d_shape_params_, d_template_vertices_, d_bs_plus_t_);
    }

    // JL = SBPT * JR
    {
      dim3 blockDim(32, 3);
      int gridsize = K_ + 1;
      sprase_mat_mul<<<gridsize, blockDim>>>(d_jr_indices_, d_jr_values_, d_bs_plus_t_, d_joint_locations_);
    }

    gpu_timer.stop();
    std::cout << "GPU Time = " << gpu_timer.elapsed_in_ms() << " ms." << std::endl;
  }

  void smpl_custom_smem() {
    if (num_shape_params_ != 10)
      throw std::runtime_error("num_shape_params_ != 10");

    GpuTimer gpu_timer;
    gpu_timer.start();

    {
      const int n  = 3 * N_;
      const int blocksize = 1024; // Minimum: NumLabels * NumLabels
      const int gridsize = (n + blocksize - 1) / blocksize;

      // TPBS = T + B * S
      custom_kernel_cm_smem<10><<<gridsize, blocksize>>>(n, d_shape_displacements_, d_shape_params_, d_template_vertices_, d_bs_plus_t_);
    }

    // JL = SBPT * JR
    {
      dim3 blockDim(32, 3);
      int gridsize = K_ + 1;
      sprase_mat_mul<<<gridsize, blockDim>>>(d_jr_indices_, d_jr_values_, d_bs_plus_t_, d_joint_locations_);
    }

    gpu_timer.stop();
    std::cout << "GPU Time = " << gpu_timer.elapsed_in_ms() << " ms." << std::endl;
  }

  void smpl_cblas() {
    cublasHandle_t handle;
    CHECK_CUDA(cublasCreate(&handle));

    GpuTimer gpu_timer;
    gpu_timer.start();


    // SBPT = SD * B + T
    CHECK_CUDA(cublasScopy(handle,  3 * N_, d_template_vertices_, 1, d_bs_plus_t_, 1));
    {
      const float alpha = 1.0f;
      const float beta = 1.0f;
      CHECK_CUDA(cublasSgemv(handle, CUBLAS_OP_N, 3 * N_, num_shape_params_,
                                 &alpha, d_shape_displacements_, 3 * N_,
                                 d_shape_params_, 1,
                                 &beta, d_bs_plus_t_, 1));
    }


    // JL = SBPT * JR
    {
      const float alpha = 1.0f;
      const float beta = 0.0f;
      CHECK_CUDA(cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N,
                                 3, K_ + 1, N_,
                                 &alpha,
                                 d_bs_plus_t_, 3,
                                 d_joint_regressor_, N_,
                                 &beta,
                                 d_joint_locations_, 3));
    }


    gpu_timer.stop();
    std::cout << "GPU Time = " << gpu_timer.elapsed_in_ms() << " ms." << std::endl;

    // Destroy the cublas handle
    CHECK_CUDA(cublasDestroy(handle));
  }

  void check_result() {
    {
      Eigen::MatrixXf bs_plus_t(3, N_);
      CHECK_CUDA(cudaMemcpy(bs_plus_t.data(), d_bs_plus_t_, bs_plus_t.size() * sizeof(PositionScalar), cudaMemcpyDeviceToHost));
      Eigen::Vector3f mean_point = bs_plus_t.rowwise().sum() / N_;
      std::cout << "bs_plus_t mean point = " << mean_point.transpose() << std::endl;
    }
    {
      Eigen::MatrixXf joint_locations(3, K_+1);
      CHECK_CUDA(cudaMemcpy(joint_locations.data(), d_joint_locations_, joint_locations.size() * sizeof(PositionScalar), cudaMemcpyDeviceToHost));
      Eigen::Vector3f mean_point = joint_locations.rowwise().sum() / (K_+1);
      std::cout << "joint_locations mean point = " << mean_point.transpose() << std::endl;
    }

  }

 private:
  int N_;
  int K_;
  int num_shape_params_;
  int num_pose_params_;


  PositionScalar* d_shape_params_;
  PositionScalar* d_pose_params_;
  PositionScalar* d_template_vertices_;
  PositionScalar* d_shape_displacements_;
  PositionScalar* d_joint_regressor_;

  int* d_jr_indices_;
  PositionScalar* d_jr_values_;


  // Intermediate variables
  PositionScalar* d_joint_locations_;
  PositionScalar* d_bs_plus_t_;
};

}  // namespace CuteGl



int main(int argc, char **argv) {
  using namespace CuteGL;

  Engine eng_cm;
  eng_cm.uploadSMPLData(loadSMPLDataFromHDF5<Engine::SMPLDataType>(CUTEGL_ASSETS_FOLDER"/smpl_neutral_lbs_10_207_0.h5"));

//  {
//    std::cout << "---------eng_cm  contraction_eigen  -----------------\n";
//    eng_cm.contraction_eigen();
//    eng_cm.contraction_eigen();
//    eng_cm.contraction_eigen();
//    eng_cm.contraction_eigen();
//    eng_cm.check_result();
//  }

  {
    std::cout << "---------eng_cm  contraction_custom  -----------------\n";
    eng_cm.contraction_custom();
    eng_cm.contraction_custom();
    eng_cm.contraction_custom();
    eng_cm.contraction_custom();
    eng_cm.check_result();
  }

  {
    std::cout << "---------eng_cm  contraction_custom_smem  -----------------\n";
    eng_cm.contraction_custom_smem();
    eng_cm.contraction_custom_smem();
    eng_cm.contraction_custom_smem();
    eng_cm.contraction_custom_smem();
    eng_cm.check_result();
  }

//  {
//    std::cout << "---------eng_cm  contraction_cblas  -----------------\n";
//    eng_cm.contraction_cblas();
//    eng_cm.contraction_cblas();
//    eng_cm.contraction_cblas();
//    eng_cm.contraction_cblas();
//    eng_cm.check_result();
//  }
//
//  std::cout << "\n\n";
//
//  {
//    std::cout << "---------eng_cm  smpl_eigen  -----------------\n";
//    eng_cm.smpl_eigen();
//    eng_cm.smpl_eigen();
//    eng_cm.smpl_eigen();
//    eng_cm.smpl_eigen();
//    eng_cm.check_result();
//  }

  {
    std::cout << "---------eng_cm  smpl_custom  -----------------\n";
    eng_cm.smpl_custom();
    eng_cm.smpl_custom();
    eng_cm.smpl_custom();
    eng_cm.smpl_custom();
    eng_cm.check_result();
  }

  {
    std::cout << "---------eng_cm  smpl_custom_smem  -----------------\n";
    eng_cm.smpl_custom_smem();
    eng_cm.smpl_custom_smem();
    eng_cm.smpl_custom_smem();
    eng_cm.smpl_custom_smem();
    eng_cm.check_result();
  }

  {
    std::cout << "---------eng_cm  smpl_cblas  -----------------\n";
    eng_cm.smpl_cblas();
    eng_cm.smpl_cblas();
    eng_cm.smpl_cblas();
    eng_cm.smpl_cblas();
    eng_cm.check_result();
  }


  return EXIT_SUCCESS;
}



