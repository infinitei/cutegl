/**
 * @file benchmarkTransformationHierarchy.cu
 * @brief benchmarkTransformationHierarchy
 *
 * @author Abhijit Kundu
 */

#define EIGEN_DEFAULT_DENSE_INDEX_TYPE int
#define EIGEN_USE_GPU

#include "CuteGL/IO/ImportSMPL.h"
#include "CuteGL/Utils/CudaUtils.h"
#include "CuteGL/Core/Config.h"

#include <iostream>
#include <float.h>

namespace CuteGL {

namespace detail {

template <class T>
__global__
void angleAxisToRotMatrix(T* angles, T* rotations) {

  T* angle_axis = angles + 3 * threadIdx.x;
  T* R =  rotations + 9 * threadIdx.x;

  T theta2 = angle_axis[0] * angle_axis[0] + angle_axis[1] * angle_axis[1] + angle_axis[2] * angle_axis[2];

  // Check if near to zero
  if (theta2 > FLT_MIN) {
    const T theta = sqrt(theta2);
    const T wx = angle_axis[0] / theta;
    const T wy = angle_axis[1] / theta;
    const T wz = angle_axis[2] / theta;

    const T costheta = cos(theta);
    const T sintheta = sin(theta);

    R[0] =     costheta   + wx*wx*(1.0 -    costheta);
    R[1] =  wx*wy*(1.0 - costheta)     - wz*sintheta;
    R[2] =  wy*sintheta   + wx*wz*(1.0 -    costheta);

    R[3] =  wz*sintheta   + wx*wy*(1.0 -    costheta);
    R[4] =     costheta   + wy*wy*(1.0 -    costheta);
    R[5] = -wx*sintheta   + wy*wz*(1.0 -    costheta);

    R[6] = -wy*sintheta   + wx*wz*(1.0 -    costheta);
    R[7] =  wx*sintheta   + wy*wz*(1.0 -    costheta);
    R[8] =     costheta   + wz*wz*(1.0 -    costheta);
  } else {
    // Near zero, we switch to using the first order Taylor expansion.
    R[0] =  1.0;
    R[1] = -angle_axis[2];
    R[2] =  angle_axis[1];

    R[3] =  angle_axis[2];
    R[4] =  1.0;
    R[5] = -angle_axis[0];

    R[6] = -angle_axis[1];
    R[7] =  angle_axis[0];
    R[8] = 1.0;
  }
}


template<typename Kernel, typename Input, typename Output>
__global__
void cudaMetaKernel(const Kernel ker, int n, const Input* in, Output* out) {
  int i = threadIdx.x + blockIdx.x*blockDim.x;
  if(i<n) {
    ker(i, in, out);
  }
}

template<typename Kernel, typename InputA, typename InputB, typename Output>
__global__
void cudaMetaKernel(const Kernel ker, int n, const InputA* inA, const InputB* inB, Output* out) {
  int i = threadIdx.x + blockIdx.x*blockDim.x;
  if(i<n) {
    ker(i, inA, inB, out);
  }
}

template<typename Kernel, typename InputA, typename InputB, typename InputC, typename Output>
__global__
void cudaMetaKernel(const Kernel ker, int n, const InputA* inA, const InputB* inB, const InputC* inC, Output* out) {
  int i = threadIdx.x + blockIdx.x*blockDim.x;
  if(i<n) {
    ker(i, inA, inB, inC, out);
  }
}

template<typename Kernel, typename InputA, typename InputB, typename InputC, typename InputD, typename Output>
__global__
void cudaMetaKernel(const Kernel ker, int n, const InputA* inA, const InputB* inB, const InputC* inC, const InputD* inD, Output* out) {
  int i = threadIdx.x + blockIdx.x*blockDim.x;
  if(i<n) {
    ker(i, inA, inB, inC, inD, out);
  }
}

template<typename Kernel, typename InputA, typename InputB, typename OutputA, typename OutputB>
__global__
void cudaMetaKernel(const Kernel ker, int n, const InputA* inA, const InputB* inB, OutputA* outA, OutputB* outB) {
  int i = threadIdx.x + blockIdx.x*blockDim.x;
  if(i<n) {
    ker(i, inA, inB, outA, outB);
  }
}

template<typename Scalar>
struct MakeWR {
  EIGEN_DEVICE_FUNC
  void operator()(int i, const Scalar* in, Scalar* out) const {
    using Matrix3  = Eigen::Matrix<Scalar, 3, 3, Eigen::RowMajor>;
    using Vector3  = Eigen::Matrix<Scalar, 3, 1>;
    Vector3 x(in + i * Vector3::MaxSizeAtCompileTime);

    Eigen::Map<Matrix3> R(out + i * (Matrix3::MaxSizeAtCompileTime + Vector3::MaxSizeAtCompileTime));
    Eigen::Map<Vector3> t(out + i * (Matrix3::MaxSizeAtCompileTime + Vector3::MaxSizeAtCompileTime) + Matrix3::MaxSizeAtCompileTime);
    R.setIdentity();
    t = x;
  }
};

template<typename Scalar, typename IndexScalar>
struct MakeRR {
  using Matrix3  = Eigen::Matrix<Scalar, 3, 3, Eigen::RowMajor>;
  using Vector3  = Eigen::Matrix<Scalar, 3, 1>;
  static const int Stride = Matrix3::MaxSizeAtCompileTime + Vector3::MaxSizeAtCompileTime;

  EIGEN_DEVICE_FUNC
  void operator()(int i, const IndexScalar* parents, const Scalar* wr, const Scalar* rot, Scalar* rr) const {
    Matrix3 R_wr_i(wr + i * Stride);
    Vector3 t_wr_i(wr + i * Stride + Matrix3::MaxSizeAtCompileTime);

    Matrix3 R(rot + i * Matrix3::MaxSizeAtCompileTime);

    Eigen::Map<Matrix3> R_rr(rr + i * Stride);
    Eigen::Map<Vector3> t_rr(rr + i * Stride + Matrix3::MaxSizeAtCompileTime);

    if (i == 0) {
      R_rr = R_wr_i * R;
      t_rr = t_wr_i;
    } else {
      IndexScalar parent = parents[i];
      Matrix3 R_wr_p(wr + parent * Stride);
      Vector3 t_wr_p(wr + parent * Stride + Matrix3::MaxSizeAtCompileTime);

      R_rr =  R_wr_p.transpose() * R_wr_i * R;
      t_rr = R_wr_p.transpose() * t_wr_i - R_wr_p.transpose() * t_wr_p;
    }
  }
};


template<typename Scalar, typename IndexScalar>
__global__
void makeWP(int n, const IndexScalar* parents, const Scalar* rr, Scalar* wp) {
  using Matrix3 = Eigen::Matrix<Scalar, 3, 3, Eigen::RowMajor>;
  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;
  const int Stride = Matrix3::MaxSizeAtCompileTime + Vector3::MaxSizeAtCompileTime;

  for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < n; i += blockDim.x * gridDim.x) {
    Matrix3 R_rr_i(rr + i * Stride);
    Vector3 t_rr_i(rr + i * Stride + Matrix3::MaxSizeAtCompileTime);

    Eigen::Map<Matrix3> R_wp(wp + i * Stride);
    Eigen::Map<Vector3> t_wp(wp + i * Stride + Matrix3::MaxSizeAtCompileTime);

    IndexScalar parent = parents[i];

    if (i == 0) {
      R_wp = R_rr_i;
      t_wp = t_rr_i;
    } else {
      Matrix3 R_wp_p(wp + parent * Stride);
      Vector3 t_wp_p(wp + parent * Stride + Matrix3::MaxSizeAtCompileTime);

      R_wp = R_wp_p * R_rr_i;
      t_wp = R_wp_p * t_rr_i + t_wp_p;
    }
  }
}


template<typename Scalar>
struct MakeRW {
  using Matrix3  = Eigen::Matrix<Scalar, 3, 3, Eigen::RowMajor>;
  using Vector3  = Eigen::Matrix<Scalar, 3, 1>;
  static const int Stride = Matrix3::MaxSizeAtCompileTime + Vector3::MaxSizeAtCompileTime;

  EIGEN_DEVICE_FUNC
  void operator()(int i, const Scalar* wp, const Scalar* wr, Scalar* rw) const {
    Matrix3 R_wp_i(wp + i * Stride);
    Vector3 t_wp_i(wp + i * Stride + Matrix3::MaxSizeAtCompileTime);

    Matrix3 R_wr_i(wr + i * Stride);
    Vector3 t_wr_i(wr + i * Stride + Matrix3::MaxSizeAtCompileTime);

    Eigen::Map<Matrix3> R_rw_i(rw + i * Stride);
    Eigen::Map<Vector3> t_rw_i(rw + i * Stride + Matrix3::MaxSizeAtCompileTime);
    R_rw_i = R_wp_i * R_wr_i.transpose();

//    t_rw_i = t_wp_i - R_wp_i * R_wr_i.transpose() * t_wr_i;// Fails to compile in latest Eigen
    t_rw_i = t_wp_i;
    t_rw_i -= R_wp_i * R_wr_i.transpose() * t_wr_i;
  }
};



template <typename Scalar, typename IndexScalar>
__inline__ __device__
void compute_wp(const int i, const IndexScalar* parents, Scalar* wp) {
  using Matrix3 = Eigen::Matrix<Scalar, 3, 3, Eigen::RowMajor>;
  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;
  const int Stride = Matrix3::MaxSizeAtCompileTime + Vector3::MaxSizeAtCompileTime;

  Eigen::Map<Matrix3> R_wp(wp + i * Stride);
  Eigen::Map<Vector3> t_wp(wp + i * Stride + Matrix3::MaxSizeAtCompileTime);

  IndexScalar parent = parents[i];
  Matrix3 R_wp_p(wp + parent * Stride);
  Vector3 t_wp_p(wp + parent * Stride + Matrix3::MaxSizeAtCompileTime);

  R_wp = (R_wp_p * R_wp).eval();
  t_wp = (R_wp_p * t_wp + t_wp_p).eval();
}

template <typename T>
__inline__ __device__
void angle_axis_to_rot_mat(const T* angle_axis, T* R) {
  T theta2 = angle_axis[0] * angle_axis[0] + angle_axis[1] * angle_axis[1] + angle_axis[2] * angle_axis[2];

  // Check if near to zero
  if (theta2 > FLT_MIN) {
    const T theta = sqrt(theta2);
    const T wx = angle_axis[0] / theta;
    const T wy = angle_axis[1] / theta;
    const T wz = angle_axis[2] / theta;

    const T costheta = cos(theta);
    const T sintheta = sin(theta);

    R[0] =     costheta   + wx*wx*(1.0 -    costheta);
    R[1] =  wx*wy*(1.0 - costheta)     - wz*sintheta;
    R[2] =  wy*sintheta   + wx*wz*(1.0 -    costheta);

    R[3] =  wz*sintheta   + wx*wy*(1.0 -    costheta);
    R[4] =     costheta   + wy*wy*(1.0 -    costheta);
    R[5] = -wx*sintheta   + wy*wz*(1.0 -    costheta);

    R[6] = -wy*sintheta   + wx*wz*(1.0 -    costheta);
    R[7] =  wx*sintheta   + wy*wz*(1.0 -    costheta);
    R[8] =     costheta   + wz*wz*(1.0 -    costheta);
  } else {
    // Near zero, we switch to using the first order Taylor expansion.
    R[0] =  1.0;
    R[1] = -angle_axis[2];
    R[2] =  angle_axis[1];

    R[3] =  angle_axis[2];
    R[4] =  1.0;
    R[5] = -angle_axis[0];

    R[6] = -angle_axis[1];
    R[7] =  angle_axis[0];
    R[8] = 1.0;
  }
}

} // namespace detail


template<typename Scalar, typename IndexScalar>
__global__
void makeWP_serial1(int n, const IndexScalar* parents, const Scalar* rr, Scalar* wp) {
  using Matrix3 = Eigen::Matrix<Scalar, 3, 3, Eigen::RowMajor>;
  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;
  const int Stride = Matrix3::MaxSizeAtCompileTime + Vector3::MaxSizeAtCompileTime;

  for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < n; i += blockDim.x * gridDim.x) {
    Matrix3 R_rr_i(rr + i * Stride);
    Vector3 t_rr_i(rr + i * Stride + Matrix3::MaxSizeAtCompileTime);

    Eigen::Map<Matrix3> R_wp(wp + i * Stride);
    Eigen::Map<Vector3> t_wp(wp + i * Stride + Matrix3::MaxSizeAtCompileTime);

    IndexScalar parent = parents[i];

    if (i == 0) {
      R_wp = R_rr_i;
      t_wp = t_rr_i;
    } else {
      Matrix3 R_wp_p(wp + parent * Stride);
      Vector3 t_wp_p(wp + parent * Stride + Matrix3::MaxSizeAtCompileTime);

      R_wp = R_wp_p * R_rr_i;
      t_wp = R_wp_p * t_rr_i + t_wp_p;
    }
  }
}

template<int N, typename Scalar, typename IndexScalar>
__global__
void makeWP_serial2(const IndexScalar* parents, Scalar* wp) {
  using Matrix3 = Eigen::Matrix<Scalar, 3, 3, Eigen::RowMajor>;
  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;
  const int Stride = Matrix3::MaxSizeAtCompileTime + Vector3::MaxSizeAtCompileTime;

  for (int i = 1; i < N; ++i) {
    Eigen::Map<Matrix3> R_wp(wp + i * Stride);
    Eigen::Map<Vector3> t_wp(wp + i * Stride + Matrix3::MaxSizeAtCompileTime);

    IndexScalar parent = parents[i];
    Matrix3 R_wp_p(wp + parent * Stride);
    Vector3 t_wp_p(wp + parent * Stride + Matrix3::MaxSizeAtCompileTime);

    R_wp = (R_wp_p * R_wp).eval();
    t_wp = (R_wp_p * t_wp + t_wp_p).eval();
  }
}

template<typename Scalar, typename IndexScalar>
__global__
void makeWP_par1(int n, int max_depth, const IndexScalar* parents, const IndexScalar* levels, Scalar* wp) {
  if (threadIdx.x < n) {
    for (int i= 1; i <= max_depth; ++i) {
      if (levels[threadIdx.x] == i)
        detail::compute_wp(threadIdx.x, parents, wp);
    }
  }
}

template<typename Scalar, typename IndexScalar>
__global__
void compute_bone_transforms(const int n, const int max_depth,
                             const IndexScalar* joint_parents,
                             const IndexScalar* joint_levels,
                             const Scalar* joint_locations,
                             const Scalar* pose_params,
                             Scalar* bt) {
  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;
  using Matrix3  = Eigen::Matrix<Scalar, 3, 3, Eigen::RowMajor>;

  const int jid = threadIdx.x;
  if (jid < n) {
    // compute rr n bt
    {
      detail::angle_axis_to_rot_mat(pose_params + 3 * jid, bt + 12 * jid);
      Eigen::Map<Vector3> t_wp(bt + jid * 12 + 9);
      if (jid == 0) {
        t_wp = Vector3(joint_locations + 3 * jid);
      }
      else {
        const IndexScalar pid = joint_parents[jid];
        t_wp = Vector3(joint_locations + 3 * jid) - Vector3(joint_locations + 3 * pid);
      }
    }

    // Compute wp
    {
      for (int i = 1; i <= max_depth; ++i) {
        if (joint_levels[jid] == i)
          detail::compute_wp(jid, joint_parents, bt);
      }
    }

    // compute BT (RW)
    {
      Eigen::Map<Vector3> t_wp(bt + jid * 12 + 9);
      t_wp -= Matrix3(bt + jid * 12) * Vector3(joint_locations + 3 * jid);
    }
  }
}

class Engine {
 public:
  using PositionScalar = float;
  using IndexScalar = unsigned int;
  using SMPLDataType = SMPLData<PositionScalar, IndexScalar>;

  using VectorXPS = Eigen::Matrix<PositionScalar, Eigen::Dynamic, 1>;

  Engine()
      : N_(0),
        K_(0),
        max_joint_depth_(0),
        d_pose_params_(nullptr),
        d_joint_parents_(nullptr),
        d_joint_levels_(nullptr),
        d_joint_locations_(nullptr),
        d_bone_transforms_(nullptr),
        d_rotations_(nullptr),
        d_world_rest_(nullptr),
        d_relative_rest_(nullptr),
        d_world_posed_(nullptr) {
  }

  ~Engine() {
    cudaFreeIfAllocated(d_pose_params_);
    cudaFreeIfAllocated(d_joint_parents_);
    cudaFreeIfAllocated(d_joint_levels_);
    cudaFreeIfAllocated(d_joint_locations_);
    cudaFreeIfAllocated(d_bone_transforms_);

    cudaFreeIfAllocated(d_rotations_);
    cudaFreeIfAllocated(d_world_rest_);
    cudaFreeIfAllocated(d_relative_rest_);
    cudaFreeIfAllocated(d_world_posed_);
  }

  void uploadSMPLData(const SMPLDataType& smpl_data) {
    N_ = smpl_data.template_vertices.rows();
    K_ = smpl_data.blend_weights.cols() - 1;
    int num_pose_params = 3 * (K_ + 1);

    std::cout << "N_ = " << N_ <<  " K_ = " << K_ << std::endl;

    CHECK_CUDA(cudaMalloc(&d_pose_params_, num_pose_params * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMalloc(&d_joint_locations_, (K_ + 1) * 3 * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMalloc(&d_bone_transforms_, (K_ + 1) * 12 * sizeof(PositionScalar)));


    CHECK_CUDA(cudaMalloc(&d_joint_parents_, smpl_data.parents.size() * sizeof(IndexScalar)));
    CHECK_CUDA(cudaMemcpy(d_joint_parents_, smpl_data.parents.data(), smpl_data.parents.size() * sizeof(IndexScalar), cudaMemcpyHostToDevice));

    CHECK_CUDA(cudaMalloc(&d_rotations_, 9 * (K_ + 1) * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMalloc(&d_world_rest_, 12 * (K_ + 1) * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMalloc(&d_relative_rest_, 12 * (K_ + 1) * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMalloc(&d_world_posed_, 12 * (K_ + 1) * sizeof(PositionScalar)));


    Eigen::Matrix<PositionScalar, Eigen::Dynamic, 1> pose_params(num_pose_params);
    pose_params.setRandom();
    CHECK_CUDA(cudaMemcpy(d_pose_params_, pose_params.data(), pose_params.size() * sizeof(PositionScalar), cudaMemcpyHostToDevice));

    Eigen::Matrix<PositionScalar, Eigen::Dynamic, 3, Eigen::RowMajor> joint_locations((K_ + 1), 3);
    joint_locations.setRandom();
    CHECK_CUDA(cudaMemcpy(d_joint_locations_, joint_locations.data(), joint_locations.size() * sizeof(PositionScalar), cudaMemcpyHostToDevice));

    {
      std::cout << smpl_data.parents.transpose() << std::endl;
      Eigen::Matrix<IndexScalar, Eigen::Dynamic, 1> levels(smpl_data.parents.size());
      levels.setZero();
      for(Eigen::Index i = 1; i < smpl_data.parents.size(); ++i) {
        levels[i] = levels[smpl_data.parents[i]] + 1;
      }
      std::cout << levels.transpose() << std::endl;
      max_joint_depth_ = levels.maxCoeff();

      CHECK_CUDA(cudaMalloc(&d_joint_levels_, levels.size() * sizeof(IndexScalar)));
      CHECK_CUDA(cudaMemcpy(d_joint_levels_, levels.data(), levels.size() * sizeof(IndexScalar), cudaMemcpyHostToDevice));
    }

    {
      computeBT_old();
      expected_bone_transforms_.resize((K_ + 1) * 12);
      expected_world_posed_.resize(12 * (K_ + 1));

      CHECK_CUDA(cudaMemcpy(expected_bone_transforms_.data(), d_bone_transforms_, expected_bone_transforms_.size() * sizeof(PositionScalar), cudaMemcpyDeviceToHost));
      CHECK_CUDA(cudaMemcpy(expected_world_posed_.data(), d_world_posed_, expected_world_posed_.size() * sizeof(PositionScalar), cudaMemcpyDeviceToHost));
    }
  }

  void computeWP_serial1() {
    GpuTimer gpu_timer;
    gpu_timer.start();

    makeWP_serial1<<<1, 1>>>((K_ + 1), d_joint_parents_, d_relative_rest_, d_world_posed_);

    gpu_timer.stop();
    std::cout << "GPU Time = " << gpu_timer.elapsed_in_ms() << " ms." << std::endl;
  }

  void computeWP_serial2() {
    CHECK_CUDA(cudaMemcpy(d_world_posed_, d_relative_rest_, 12 * (K_ + 1) * sizeof(PositionScalar), cudaMemcpyDeviceToDevice));

    GpuTimer gpu_timer;
    gpu_timer.start();

    makeWP_serial2<24><<<1, 1>>>(d_joint_parents_, d_world_posed_);

    gpu_timer.stop();
    std::cout << "GPU Time = " << gpu_timer.elapsed_in_ms() << " ms." << std::endl;
  }

  void computeWP_par1() {
    CHECK_CUDA(cudaMemcpy(d_world_posed_, d_relative_rest_, 12 * (K_ + 1) * sizeof(PositionScalar), cudaMemcpyDeviceToDevice));

    GpuTimer gpu_timer;
    gpu_timer.start();

    makeWP_par1<<<1, 32>>>(K_ + 1, max_joint_depth_, d_joint_parents_, d_joint_levels_, d_world_posed_);

    gpu_timer.stop();
    std::cout << "GPU Time = " << gpu_timer.elapsed_in_ms() << " ms." << std::endl;
  }

  void check_result() {
    {
      Eigen::Matrix<PositionScalar, Eigen::Dynamic, 1> wp(12 * (K_ + 1));
      CHECK_CUDA(cudaMemcpy(wp.data(), d_world_posed_, wp.size() * sizeof(PositionScalar), cudaMemcpyDeviceToHost));
      if (!wp.isApprox(expected_world_posed_)){
        std::cout << "WRONG wp" << std::endl;
        const Eigen::IOFormat fmt(4, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
        std::cout << wp.format(fmt) << "\n\n";
        std::cout << expected_world_posed_.format(fmt) << "\n\n";
      }
      else
        std::cout << "Correct wp" << std::endl;
    }
    {
      Eigen::Matrix<PositionScalar, Eigen::Dynamic, 1> bt(12 * (K_ + 1));
      CHECK_CUDA(cudaMemcpy(bt.data(), d_bone_transforms_, bt.size() * sizeof(PositionScalar), cudaMemcpyDeviceToHost));
      if (!bt.isApprox(expected_bone_transforms_)){
        std::cout << "WRONG bt" << std::endl;
        const Eigen::IOFormat fmt(4, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
        std::cout << bt.format(fmt) << "\n\n";
        std::cout << expected_world_posed_.format(fmt) << "\n\n";
      }
      else
        std::cout << "Correct bt" << std::endl;
    }
  }

  void computeBT_old() {
    GpuTimer gpu_timer;
    gpu_timer.start();

    detail::angleAxisToRotMatrix<<<1, (K_ + 1)>>>(d_pose_params_, d_rotations_);

    detail::cudaMetaKernel<<<1, (K_ + 1)>>>(detail::MakeWR<PositionScalar>(), (K_ + 1), d_joint_locations_, d_world_rest_);

    detail::cudaMetaKernel<<<1, (K_ + 1)>>>(detail::MakeRR<PositionScalar, IndexScalar>(), (K_ + 1), d_joint_parents_, d_world_rest_, d_rotations_, d_relative_rest_);

    detail::makeWP<<<1, 1>>>((K_ + 1), d_joint_parents_, d_relative_rest_, d_world_posed_);

    detail::cudaMetaKernel<<<1, (K_ + 1)>>>(detail::MakeRW<PositionScalar>(), (K_ + 1), d_world_posed_, d_world_rest_, d_bone_transforms_);

    gpu_timer.stop();
    std::cout << "GPU Time = " << gpu_timer.elapsed_in_ms() << " ms." << std::endl;
  }

  void computeBT_new() {
    GpuTimer gpu_timer;
    gpu_timer.start();

    compute_bone_transforms<<<1, 32>>>(K_ + 1, max_joint_depth_, d_joint_parents_, d_joint_levels_, d_joint_locations_, d_pose_params_, d_bone_transforms_);

    gpu_timer.stop();
    std::cout << "GPU Time = " << gpu_timer.elapsed_in_ms() << " ms." << std::endl;
  }


 private:
  int N_;
  int K_;
  int max_joint_depth_;

  PositionScalar* d_pose_params_;

  IndexScalar* d_joint_parents_;
  IndexScalar* d_joint_levels_;
  PositionScalar* d_joint_locations_;
  PositionScalar* d_bone_transforms_;

  // Intermediate variables
  PositionScalar* d_rotations_;
  PositionScalar* d_world_rest_;
  PositionScalar* d_relative_rest_;
  PositionScalar* d_world_posed_;

  VectorXPS expected_world_posed_;
  VectorXPS expected_bone_transforms_;
};

}  // namespace CuteGL

int main(int argc, char **argv) {
  using namespace CuteGL;

  Engine eng;
  eng.uploadSMPLData(loadSMPLDataFromHDF5<Engine::SMPLDataType>(CUTEGL_ASSETS_FOLDER"/smpl_neutral_lbs_10_207_0.h5"));

  {
    std::cout << "--------- computeWP_serial1  -------------\n";
    eng.computeWP_serial1();
    eng.computeWP_serial1();
    eng.computeWP_serial1();
    eng.computeWP_serial1();
    eng.check_result();
  }

  {
    std::cout << "--------- computeWP_serial2  -------------\n";
    eng.computeWP_serial2();
    eng.computeWP_serial2();
    eng.computeWP_serial2();
    eng.computeWP_serial2();
    eng.check_result();
  }

  {
    std::cout << "--------- computeWP_par1  -------------\n";
    eng.computeWP_par1();
    eng.computeWP_par1();
    eng.computeWP_par1();
    eng.computeWP_par1();
    eng.check_result();
  }

  std::cout << "\n\n";

  {
    std::cout << "--------- computeBT_old  -------------\n";
    eng.computeBT_old();
    eng.computeBT_old();
    eng.computeBT_old();
    eng.computeBT_old();
    eng.check_result();
  }

  {
    std::cout << "--------- computeBT_new  -------------\n";
    eng.computeBT_new();
    eng.computeBT_new();
    eng.computeBT_new();
    eng.computeBT_new();
    eng.check_result();
  }

  return EXIT_SUCCESS;
}
