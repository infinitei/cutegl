/**
 * @file benchmarkOffscreenSMPL.cpp
 * @brief benchmarkOffscreenSMPL
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Renderer/SMPLRenderer.h"
#include "CuteGL/Surface/OffScreenRenderViewer.h"
#include "CuteGL/Utils/CudaTexture.h"
#include "CuteGL/Utils/Timer.h"
#include "CuteGL/Core/Config.h"

#include <QGuiApplication>
#include <QCommandLineParser>
#include <memory>
#include <iostream>
#include <QTime>


int main(int argc, char **argv) {
  QGuiApplication app(argc, argv);

  QCommandLineParser parser;
  parser.setApplicationDescription("benchmarkOffscreenSMPL");
  parser.addHelpOption();

  parser.addOptions({
          {{"H", "height"}, "Height of Rendered Image", "height", "240"},
          {{"W", "width"}, "Width of Rendered Image", "width", "320"},
          {{"N", "num_frames"}, "Number of Images to render", "num_frames", "1000"},
      });

  // Process the actual command line arguments given by the user
  parser.process(app);

  const int W = parser.value("width").toInt();
  const int H = parser.value("height").toInt();
  const int num_frames = parser.value("num_frames").toInt();

  std::cout << "Number of Images = " << num_frames << "\n";
  std::cout << "Image size = " << W << " x " << H << "\n";

  using namespace CuteGL;
  using Eigen::Vector3f;

  // Create the Renderer
  std::unique_ptr<SMPLRenderer> renderer(new SMPLRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);


  viewer.resize(W, H);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                           Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.create();
  viewer.makeCurrent();

  {
    // Choose CUDA Device to be same as the GL device
    // This should be done before any CUDA calls
    unsigned int gl_cuda_device_count;
    int gl_devices[1];
    CHECK_CUDA(cudaGLGetDevices(&gl_cuda_device_count, gl_devices, 1, cudaGLDeviceListAll));
    CHECK_CUDA(cudaSetDevice(gl_devices[0]));
    std::cout << "Using CUDA device: "<< gl_devices[0] << std::endl;
  }

  renderer->setSMPLData(CUTEGL_ASSETS_FOLDER"/smpl_neutral_lbs_10_207_0.h5",
                        CUTEGL_ASSETS_FOLDER"/smpl_vertex_segm24_col24_14.h5");

  // Render once
  viewer.render();

  using Images = Eigen::Tensor<float, 3, Eigen::RowMajor>;
  Images rendered_images(num_frames, H, W);
  Images expected_rendered_images(num_frames, H, W);


  const std::size_t rendered_images_bytes = rendered_images.size() * sizeof(float);
  std::cout << "Total Image data = " << float(rendered_images_bytes) / 1e6  << " MB.\n";

  Eigen::MatrixXf shape_data(10, num_frames);
  Eigen::MatrixXf pose_data(72, num_frames);
  shape_data.setRandom();
  pose_data.setRandom();

  std::locale loc("");
  std::cout.imbue(loc);

  const GLenum attachment = GL_COLOR_ATTACHMENT3;

  {
    std::cout << "------------------ GPU --> CPU (readPixels) -------------------------\n";
    rendered_images.setZero();

    std::cout << "Rendering " << num_frames << " frames ... " << std::flush;
    GpuTimer stopwatch;
    stopwatch.start();
    for (int i = 0; i < num_frames; ++i) {
      renderer->smplDrawer().shape() = shape_data.col(i);
      renderer->smplDrawer().pose() = pose_data.col(i);
      renderer->smplDrawer().updateShapeAndPose();
      viewer.render();
      viewer.readFrameBuffer(attachment, GL_RED, &rendered_images(i, 0, 0));
    }
    stopwatch.stop();
    std::cout << " Done." << std::endl;
    std::cout << "FPS: " << float (num_frames * 1e3)/ stopwatch.elapsed_in_ms() << std::endl;
    std::cout << "Images mean: " << rendered_images.mean() << std::endl;
    expected_rendered_images = rendered_images;
  }

  {
    std::cout << "---------------  Cuda Map RenderBuffer -----------------\n";
    rendered_images.setZero();

    float* d_images_ptr;
    const std::size_t rendered_images_bytes = rendered_images.size() * sizeof(float);
    CHECK_CUDA(cudaMalloc(&d_images_ptr, rendered_images_bytes));
    CHECK_CUDA(cudaMemset((void*)d_images_ptr, 0, rendered_images_bytes));

    cudaGraphicsResource *cuda_resource;

    viewer.fbo().bind();
    GLuint rbo_id = viewer.fbo().getRenderBufferObjectName(attachment);
    viewer.fbo().release();
    CHECK_CUDA(cudaGraphicsGLRegisterImage(&cuda_resource, rbo_id, GL_RENDERBUFFER, cudaGraphicsRegisterFlagsReadOnly));

    std::cout << "Rendering " << num_frames << " frames ... " << std::flush;
    GpuTimer stopwatch;
    stopwatch.start();
    for (int i = 0; i < num_frames; ++i) {
      renderer->smplDrawer().shape() = shape_data.col(i);
      renderer->smplDrawer().pose() = pose_data.col(i);
      renderer->smplDrawer().updateShapeAndPose();
      viewer.render();
      cudaArray *in_array;
      CHECK_CUDA(cudaGraphicsMapResources(1, &cuda_resource, 0));
      CHECK_CUDA(cudaGraphicsSubResourceGetMappedArray(&in_array, cuda_resource, 0, 0));
      copyTexture(d_images_ptr + i * W * H, in_array, W, H);
      CHECK_CUDA(cudaGraphicsUnmapResources(1, &cuda_resource, 0));
    }
    stopwatch.stop();
    std::cout << " Done." << std::endl;
    std::cout << "FPS: " << float (num_frames * 1e3)/ stopwatch.elapsed_in_ms() << std::endl;
    CHECK_CUDA(cudaMemcpy(rendered_images.data(), d_images_ptr, rendered_images_bytes, cudaMemcpyDeviceToHost));
    CHECK_CUDA(cudaFree((void*)d_images_ptr));
    CHECK_CUDA(cudaGraphicsUnregisterResource(cuda_resource));
    std::cout << "Images mean: " << rendered_images.mean() << std::endl;
    std::cout << "(expected_rendered_images == rendered_images) == " << std::boolalpha << (expected_rendered_images == rendered_images).all() << std::endl;
  }

  {
    std::cout << "------------- GPU --> GPU With PBO Ver1 -----------------\n";
    rendered_images.setZero();

    float* d_images_ptr;
    const std::size_t rendered_images_bytes = rendered_images.size() * sizeof(float);
    CHECK_CUDA(cudaMalloc(&d_images_ptr, rendered_images_bytes));
    CHECK_CUDA(cudaMemset((void*)d_images_ptr, 0, rendered_images_bytes));

    static const int NumOfPBOs = 1000;

    cudaGraphicsResource *cuda_pbo_resources[NumOfPBOs];
    GLuint pbo_ids[NumOfPBOs];
    float *dptr[NumOfPBOs];

    const size_t pbo_size = W * H * sizeof(float);

    // Create PBOS
    for (int p = 0; p < NumOfPBOs; ++p) {
      viewer.glFuncs()->glGenBuffers(1, &pbo_ids[p]);
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo_ids[p]);
      viewer.glFuncs()->glBufferData(GL_PIXEL_PACK_BUFFER, pbo_size, 0, GL_DYNAMIC_READ);
      CHECK_CUDA(cudaGraphicsGLRegisterBuffer(&cuda_pbo_resources[p], pbo_ids[p], cudaGraphicsRegisterFlagsReadOnly));
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

      CHECK_CUDA(cudaGraphicsMapResources(1, &cuda_pbo_resources[p], 0));
      size_t buffer_size;
      CHECK_CUDA(cudaGraphicsResourceGetMappedPointer((void **)&dptr[p], &buffer_size, cuda_pbo_resources[p]));
      assert(buffer_size == pbo_size);
      CHECK_CUDA(cudaGraphicsUnmapResources(1, &cuda_pbo_resources[p], 0));
    }

    std::cout << "Rendering " << num_frames << " frames ... " << std::flush;
    GpuTimer stopwatch;
    stopwatch.start();
    for (int i = 0; i < num_frames; ++i) {
      renderer->smplDrawer().shape() = shape_data.col(i);
      renderer->smplDrawer().pose() = pose_data.col(i);
      renderer->smplDrawer().updateShapeAndPose();
      viewer.render();

      const int pbo_id = i % NumOfPBOs;
      viewer.readFrameBuffer(attachment, GL_RED, GL_FLOAT, pbo_ids[pbo_id]);
    }
    viewer.glFuncs()->glFinish();

    for (int i = 0; i < num_frames; ++i) {
      const int pbo_id = i % NumOfPBOs;
      CHECK_CUDA(cudaMemcpy(d_images_ptr + i * W * H, dptr[pbo_id], pbo_size, cudaMemcpyDeviceToDevice));
    }

    stopwatch.stop();
    std::cout << " Done." << std::endl;
    std::cout << "FPS: " << float (num_frames * 1e3)/ stopwatch.elapsed_in_ms() << std::endl;
    CHECK_CUDA(cudaMemcpy(rendered_images.data(), d_images_ptr, rendered_images_bytes, cudaMemcpyDeviceToHost));
    CHECK_CUDA(cudaFree((void*)d_images_ptr));
    for (int p = 0; p < NumOfPBOs; ++p)
      CHECK_CUDA(cudaGraphicsUnregisterResource(cuda_pbo_resources[p]));
    viewer.glFuncs()->glDeleteBuffers(NumOfPBOs, pbo_ids);
    std::cout << "Images mean: " << rendered_images.mean() << std::endl;
    std::cout << "(expected_rendered_images == rendered_images) == " << std::boolalpha << (expected_rendered_images == rendered_images).all() << std::endl;
  }

  return EXIT_SUCCESS;
}
