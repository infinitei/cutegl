/**
 * @file benchmarkOffscreenStaticScene.cpp
 * @brief benchmarkOffscreenStaticScene
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Surface/OffScreenRenderViewer.h"
#include "CuteGL/Renderer/MultiObjectRenderer.h"
#include "CuteGL/Geometry/ComputeAlignedBox.h"
#include "CuteGL/Core/MeshUtils.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/IO/ImportViaAssimp.h"
#include "CuteGL/Utils/CudaTexture.h"
#include <Eigen/CXX11/Tensor>
#include <QGuiApplication>
#include <QTime>
#include <iostream>
#include <memory>


int main(int argc, char **argv) {
  using namespace CuteGL;

  QGuiApplication app(argc, argv);
  // Create the Renderer
  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);

  const int W = 320;
  const int H = 240;
  viewer.resize(W, H);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.5f, -0.2f, 1.0f),
                           Eigen::Vector3f::Zero(),
                           Eigen::Vector3f::UnitY());

  viewer.create();

  viewer.makeCurrent();
  {
    std::string asset_file = CUTEGL_ASSETS_FOLDER "/Sphere.nff";
    std::vector<MeshData> meshes = loadMeshesViaAssimp(asset_file);
    Eigen::AlignedBox3f bbx = computeAlignedBox(meshes);
    Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(0.5f / bbx.sizes().maxCoeff())
        * Eigen::Translation3f(-bbx.center());
    renderer->modelDrawers().addItem(model_pose, meshes);
  }

  {
    std::string asset_file = CUTEGL_ASSETS_FOLDER "/cow.off";
    std::vector<MeshData> meshes = loadMeshesViaAssimp(asset_file);
    Eigen::AlignedBox3f bbx = computeAlignedBox(meshes);
    Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(
          1.0f / bbx.sizes().maxCoeff()) * Eigen::Translation3f(-bbx.center());

    renderer->modelDrawers().addItem(model_pose, meshes);
  }

  const auto rest_poses = renderer->modelDrawers().poses();

  const int num_frames = 1000;
  using Images = Eigen::Tensor<float, 3, Eigen::RowMajor>;
  Images rendered_images(num_frames, H, W);
  Images expected_rendered_images(num_frames, H, W);

  std::locale loc("");
  std::cout.imbue(loc);

  const GLenum attachment = GL_COLOR_ATTACHMENT1;

  {
    std::cout << "--------------- GPU --> CPU No PBO -----------------------------\n";
    rendered_images.setZero();
    renderer->modelDrawers().poses() = rest_poses;

    std::cout << "Rendering " << num_frames << " frames ... " << std::flush;
    GpuTimer stopwatch;
    stopwatch.start();
    for (int i = 0; i < num_frames; ++i) {
      viewer.render();
      viewer.readFrameBuffer(attachment, GL_RED, &rendered_images(i, 0, 0));

      for (auto& pose : renderer->modelDrawers().poses()) {
        pose = pose * Eigen::AngleAxisf(0.5f, Eigen::Vector3f::UnitY());
      }
    }
    stopwatch.stop();
    std::cout << " Done." << std::endl;
    std::cout << "FPS: " << float (num_frames * 1e3)/ stopwatch.elapsed_in_ms() << std::endl;
    std::cout << "Images mean: " << rendered_images.mean() << std::endl;
    expected_rendered_images = rendered_images;
  }


  {
    std::cout << "--------------- Cuda Map RenderBuffer -----------------------------\n";
    rendered_images.setZero();
    renderer->modelDrawers().poses() = rest_poses;

    {
      unsigned int gl_cuda_device_count;
      int gl_devices[1];
      cudaGLGetDevices(&gl_cuda_device_count, gl_devices, 1, cudaGLDeviceListAll);
      //Setup CUDA
      cudaSetDevice(gl_devices[0]);
      cudaGLSetGLDevice(gl_devices[0]);
      std::cout << "Using CUDA device: "<< gl_devices[0] << std::endl;
    }

    float* d_images_ptr;
    const std::size_t rendered_images_bytes = rendered_images.size() * sizeof(float);
    CHECK_CUDA(cudaMalloc(&d_images_ptr, rendered_images_bytes));

    cudaGraphicsResource *cuda_resource;

    viewer.fbo().bind();
    GLuint rbo_id = viewer.fbo().getRenderBufferObjectName(attachment);
    viewer.fbo().release();
    CHECK_CUDA(cudaGraphicsGLRegisterImage(&cuda_resource, rbo_id, GL_RENDERBUFFER, cudaGraphicsRegisterFlagsReadOnly));

    std::cout << "Rendering " << num_frames << " frames ... " << std::flush;
    GpuTimer stopwatch;
    stopwatch.start();
    for (int i = 0; i < num_frames; ++i) {
      viewer.render();
      cudaArray *in_array;
      CHECK_CUDA(cudaGraphicsMapResources(1, &cuda_resource, 0));
      CHECK_CUDA(cudaGraphicsSubResourceGetMappedArray(&in_array, cuda_resource, 0, 0));
      copyTexture(d_images_ptr + i * W * H, in_array, W, H);
      CHECK_CUDA(cudaGraphicsUnmapResources(1, &cuda_resource, 0));

      for (auto& pose : renderer->modelDrawers().poses()) {
        pose = pose * Eigen::AngleAxisf(0.5f, Eigen::Vector3f::UnitY());
      }
    }
    stopwatch.stop();
    std::cout << " Done." << std::endl;
    std::cout << "FPS: " << float (num_frames * 1e3)/ stopwatch.elapsed_in_ms() << std::endl;
    CHECK_CUDA(cudaMemcpy(rendered_images.data(), d_images_ptr, rendered_images_bytes, cudaMemcpyDeviceToHost));
    CHECK_CUDA(cudaFree((void*)d_images_ptr));
    CHECK_CUDA(cudaGraphicsUnregisterResource(cuda_resource));
    std::cout << "Images mean: " << rendered_images.mean() << std::endl;
    std::cout << "(expected_rendered_images == rendered_images) == " << std::boolalpha << (expected_rendered_images == rendered_images).all() << std::endl;
  }



  {
    std::cout << "--------------- GPU --> GPU With PBO Ver1 -----------------------------\n";
    rendered_images.setZero();
    renderer->modelDrawers().poses() = rest_poses;
    float* d_images_ptr;
    const std::size_t rendered_images_bytes = rendered_images.size() * sizeof(float);
    CHECK_CUDA(cudaMalloc(&d_images_ptr, rendered_images_bytes));

    cudaGraphicsResource *cuda_pbo_resource;

    GLuint pbo_id;
    size_t pbo_size = W * H * sizeof(float);
    {
      viewer.glFuncs()->glGenBuffers(1, &pbo_id);
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo_id);
      viewer.glFuncs()->glBufferData(GL_PIXEL_PACK_BUFFER, pbo_size, 0, GL_DYNAMIC_READ);
      CHECK_CUDA(cudaGraphicsGLRegisterBuffer(&cuda_pbo_resource, pbo_id, cudaGraphicsRegisterFlagsReadOnly));
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
    }

    float *dptr;
    CHECK_CUDA(cudaGraphicsMapResources(1, &cuda_pbo_resource, 0));
    CHECK_CUDA(cudaGraphicsResourceGetMappedPointer((void **)&dptr, &pbo_size, cuda_pbo_resource));
    CHECK_CUDA(cudaGraphicsUnmapResources(1, &cuda_pbo_resource, 0));

    std::cout << "Rendering " << num_frames << " frames ... " << std::flush;
    GpuTimer stopwatch;
    stopwatch.start();
    for (int i = 0; i < num_frames; ++i) {
      viewer.render();

      viewer.glFuncs()->glBindFramebuffer(GL_READ_FRAMEBUFFER, viewer.fbo().handle());
      viewer.glFuncs()->glReadBuffer(attachment);
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo_id);
      viewer.glFuncs()->glReadPixels(0, 0, W, H, GL_RED, GL_FLOAT, 0);
      viewer.glFuncs()->glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

      CHECK_CUDA(cudaGraphicsMapResources(1, &cuda_pbo_resource, 0));

      CHECK_CUDA(cudaMemcpy(d_images_ptr + i * W * H, dptr, pbo_size, cudaMemcpyDeviceToDevice));

      CHECK_CUDA(cudaGraphicsUnmapResources(1, &cuda_pbo_resource, 0));


      for (auto& pose : renderer->modelDrawers().poses()) {
        pose = pose * Eigen::AngleAxisf(0.5f, Eigen::Vector3f::UnitY());
      }

    }
    stopwatch.stop();
    std::cout << " Done." << std::endl;
    std::cout << "FPS: " << float (num_frames * 1e3)/ stopwatch.elapsed_in_ms() << std::endl;
    CHECK_CUDA(cudaMemcpy(rendered_images.data(), d_images_ptr, rendered_images_bytes, cudaMemcpyDeviceToHost));
    CHECK_CUDA(cudaFree((void*)d_images_ptr));
    CHECK_CUDA(cudaGraphicsUnregisterResource(cuda_pbo_resource));
    viewer.glFuncs()->glDeleteBuffers(1, &pbo_id);
    std::cout << "Images mean: " << rendered_images.mean() << std::endl;
    std::cout << "(expected_rendered_images == rendered_images) == " << std::boolalpha << (expected_rendered_images == rendered_images).all() << std::endl;

  }

  {
    std::cout << "--------------- GPU --> GPU With PBO Ver2 -----------------------------\n";
    rendered_images.setZero();
    renderer->modelDrawers().poses() = rest_poses;
    float* d_images_ptr;
    const std::size_t rendered_images_bytes = rendered_images.size() * sizeof(float);
    CHECK_CUDA(cudaMalloc(&d_images_ptr, rendered_images_bytes));
    CHECK_CUDA(cudaMemset((void*)d_images_ptr, 0, rendered_images_bytes));

    static const int NumOfPBOs = 1000;

    cudaGraphicsResource *cuda_pbo_resources[NumOfPBOs];
    GLuint pbo_ids[NumOfPBOs];
    float *dptr[NumOfPBOs];

    size_t pbo_size = W * H * sizeof(float);
    for (int p = 0; p < NumOfPBOs; ++p) {
      viewer.glFuncs()->glGenBuffers(1, &pbo_ids[p]);
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo_ids[p]);
      viewer.glFuncs()->glBufferData(GL_PIXEL_PACK_BUFFER, pbo_size, 0, GL_DYNAMIC_READ);
      CHECK_CUDA(cudaGraphicsGLRegisterBuffer(&cuda_pbo_resources[p], pbo_ids[p], cudaGraphicsRegisterFlagsReadOnly));
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

      CHECK_CUDA(cudaGraphicsMapResources(1, &cuda_pbo_resources[p], 0));
      CHECK_CUDA(cudaGraphicsResourceGetMappedPointer((void **)&dptr[p], &pbo_size, cuda_pbo_resources[p]));
      CHECK_CUDA(cudaGraphicsUnmapResources(1, &cuda_pbo_resources[p], 0));
    }

    std::cout << "Rendering " << num_frames << " frames ... " << std::flush;
    GpuTimer stopwatch;
    stopwatch.start();
    for (int i = 0; i < num_frames; ++i) {
      viewer.render();

      const int pbo_id = i % NumOfPBOs;

      viewer.glFuncs()->glBindFramebuffer(GL_READ_FRAMEBUFFER, viewer.fbo().handle());
      viewer.glFuncs()->glReadBuffer(attachment);
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo_ids[pbo_id]);
      viewer.glFuncs()->glReadPixels(0, 0, W, H, GL_RED, GL_FLOAT, 0);

//      CHECK_CUDA(cudaGraphicsMapResources(1, &cuda_pbo_resources[pbo_id], 0));

      CHECK_CUDA(cudaMemcpy(d_images_ptr + i * W * H, dptr[pbo_id], pbo_size, cudaMemcpyDeviceToDevice));

//      CHECK_CUDA(cudaGraphicsUnmapResources(1, &cuda_pbo_resources[pbo_id], 0));

      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
      for (auto& pose : renderer->modelDrawers().poses()) {
        pose = pose * Eigen::AngleAxisf(0.5f, Eigen::Vector3f::UnitY());
      }
    }
    stopwatch.stop();
    std::cout << " Done." << std::endl;
    std::cout << "FPS: " << float (num_frames * 1e3)/ stopwatch.elapsed_in_ms() << std::endl;
    CHECK_CUDA(cudaMemcpy(rendered_images.data(), d_images_ptr, rendered_images_bytes, cudaMemcpyDeviceToHost));
    CHECK_CUDA(cudaFree((void*)d_images_ptr));
    for (int p = 0; p < NumOfPBOs; ++p)
      CHECK_CUDA(cudaGraphicsUnregisterResource(cuda_pbo_resources[p]));
    viewer.glFuncs()->glDeleteBuffers(NumOfPBOs, pbo_ids);
    std::cout << "Images mean: " << rendered_images.mean() << std::endl;
    std::cout << "(expected_rendered_images == rendered_images) == " << std::boolalpha << (expected_rendered_images == rendered_images).all() << std::endl;
  }

  {
    std::cout << "--------------- GPU --> GPU With PBO Ver3 -----------------------------\n";
    rendered_images.setZero();
    renderer->modelDrawers().poses() = rest_poses;
    float* d_images_ptr;
    const std::size_t rendered_images_bytes = rendered_images.size() * sizeof(float);
    CHECK_CUDA(cudaMalloc(&d_images_ptr, rendered_images_bytes));
    CHECK_CUDA(cudaMemset((void*)d_images_ptr, 0, rendered_images_bytes));

    static const int NumOfPBOs = 1000;

    cudaGraphicsResource *cuda_pbo_resources[NumOfPBOs];
    GLuint pbo_ids[NumOfPBOs];
    float *dptr[NumOfPBOs];

    size_t pbo_size = W * H * sizeof(float);
    for (int p = 0; p < NumOfPBOs; ++p) {
      viewer.glFuncs()->glGenBuffers(1, &pbo_ids[p]);
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo_ids[p]);
      viewer.glFuncs()->glBufferData(GL_PIXEL_PACK_BUFFER, pbo_size, 0, GL_DYNAMIC_READ);
      CHECK_CUDA(cudaGraphicsGLRegisterBuffer(&cuda_pbo_resources[p], pbo_ids[p], cudaGraphicsRegisterFlagsReadOnly));
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

      CHECK_CUDA(cudaGraphicsMapResources(1, &cuda_pbo_resources[p], 0));
      CHECK_CUDA(cudaGraphicsResourceGetMappedPointer((void **)&dptr[p], &pbo_size, cuda_pbo_resources[p]));
      CHECK_CUDA(cudaGraphicsUnmapResources(1, &cuda_pbo_resources[p], 0));
    }

    std::cout << "Rendering " << num_frames << " frames ... " << std::flush;
    GpuTimer stopwatch;
    stopwatch.start();
    for (int i = 0; i < num_frames; ++i) {
      viewer.render();
      const int pbo_id = i % NumOfPBOs;
      viewer.glFuncs()->glBindFramebuffer(GL_READ_FRAMEBUFFER, viewer.fbo().handle());
      viewer.glFuncs()->glReadBuffer(attachment);
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo_ids[pbo_id]);
      viewer.glFuncs()->glReadPixels(0, 0, W, H, GL_RED, GL_FLOAT, 0);
      viewer.glFuncs()->glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
      for (auto& pose : renderer->modelDrawers().poses()) {
        pose = pose * Eigen::AngleAxisf(0.5f, Eigen::Vector3f::UnitY());
      }
    }

    viewer.glFuncs()->glFinish();

    for (int i = 0; i < num_frames; ++i) {
      const int pbo_id = i % NumOfPBOs;
      CHECK_CUDA(cudaMemcpy(d_images_ptr + i * W * H, dptr[pbo_id], pbo_size, cudaMemcpyDeviceToDevice));
    }



    stopwatch.stop();
    std::cout << " Done." << std::endl;
    std::cout << "FPS: " << float (num_frames * 1e3)/ stopwatch.elapsed_in_ms() << std::endl;
    CHECK_CUDA(cudaMemcpy(rendered_images.data(), d_images_ptr, rendered_images_bytes, cudaMemcpyDeviceToHost));
    CHECK_CUDA(cudaFree((void*)d_images_ptr));
    for (int p = 0; p < NumOfPBOs; ++p)
      CHECK_CUDA(cudaGraphicsUnregisterResource(cuda_pbo_resources[p]));
    viewer.glFuncs()->glDeleteBuffers(NumOfPBOs, pbo_ids);
    std::cout << "Images mean: " << rendered_images.mean() << std::endl;
    std::cout << "(expected_rendered_images == rendered_images) == " << std::boolalpha << (expected_rendered_images == rendered_images).all() << std::endl;
  }


  {
    std::cout << "--------------------------------------------------------------\n";
    Eigen::Tensor<float, 0, Eigen::RowMajor> min = rendered_images.minimum();
    Eigen::Tensor<float, 0, Eigen::RowMajor> max = rendered_images.maximum();
    std::cout << "rendered_images = [" << rendered_images.minimum() << ", " << rendered_images.maximum() << "]\n";
    const float scale = 255.0 / max(0);
    std::cout << "Saving images... " << std::flush;
    for (int i = 0; i < num_frames; ++i) {
      QImage image(W, H, QImage::Format_RGB888);
      #pragma omp parallel for
      for (Eigen::Index y = 0; y < H; ++y)
        for (Eigen::Index x = 0; x < W; ++x) {
          int cval = rendered_images(i, y, x) * scale;
          image.setPixel(x, H - y - 1, qRgb(cval, cval, cval));
        }
      QString image_name = QString("OffscreenStaticScene_%1.png").arg(i, 5, 10, QChar('0'));
      image.save(image_name);
    }
    std::cout << "Done" << std::endl;
  }


  return EXIT_SUCCESS;
}


