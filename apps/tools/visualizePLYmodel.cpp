/**
 * @file visualizePLYmodel.cpp
 * @brief visualizePLYmodel.cpp
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/IO/ImportPLY.h"
#include "CuteGL/Renderer/MultiObjectRenderer.h"
#include "CuteGL/Geometry/ComputeAlignedBox.h"
#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Core/Config.h"
#include <QGuiApplication>
#include <memory>

#include <iostream>

int main(int argc, char **argv) {
  using namespace CuteGL;

  QGuiApplication app(argc, argv);
  // Create the Renderer
  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);
  QSurfaceFormat format = viewer.format();
  format.setSamples(16);
  viewer.setFormat(format);

  // Set camera pose via lookAt
  viewer.camera().extrinsics() = getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 2.0f),
                                                         Eigen::Vector3f::Zero(),
                                                         Eigen::Vector3f::UnitY());

  // Display the window
  viewer.showAndWaitTillExposed();




  std::string ply_file = CUTEGL_ASSETS_FOLDER "/Axis_bin.ply";
  if(argc > 1)
    ply_file = argv[1];
  else
    std::cout << "Usage: " << argv[0] << " [/path/to/PLY/model]\n";

  std::cout << "Will load PLY file from " << ply_file << "\n";

  {
    // Load pointcloud data from PLY file
    std::cout << "Loading mesh from PLY ... " << std::flush;
    MeshData mesh = loadMeshFromPLY(ply_file);
    std::cout << " Done" << std::endl;

    std::cout << "# of Faces = " << mesh.faces.rows() << "  # of vertices = "
              << mesh.vertices.size() << "\n";

    std::cout << "Computing bbx ... " << std::flush;
    Eigen::AlignedBox3f bbx = computeAlignedBox(mesh);
    std::cout << " Done" << std::endl;

    std::cout << "Adding Mesh to drawer ... " << std::flush;
    Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(
          3.0f / bbx.sizes().maxCoeff()) * Eigen::Translation3f(-bbx.center());
    renderer->modelDrawers().addItem(model_pose, mesh);
    std::cout << " Done" << std::endl;
  }

  return app.exec();
}
