/**
 * @file exportAsPLY.cpp
 * @brief exportAsPLY
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/IO/ImportPLY.h"
#include "CuteGL/IO/ImportViaAssimp.h"
#include "CuteGL/Core/MeshUtils.h"
#include <QFileInfo>
#include <iostream>

int main(int argc, char **argv) {
  using namespace CuteGL;

  if (argc < 2) {;
    std::cout << "Error Processing Inputs: Example Usage: " << argv[0] << " Original asset <Output PLY file>";
    std::cout << "\n";
    return EXIT_FAILURE;
  }

  const  QFileInfo asset_file(argv[1]);
  const QString asset_ply_file = argc > 2 ? argv[2] : asset_file.completeBaseName() + ".ply";

  std::cout << "Exporting " << asset_file.absoluteFilePath().toStdString()  << " to " << asset_ply_file.toStdString() << "  ... " << std::flush;

  MeshData mesh = joinMeshes(loadMeshesViaAssimp(asset_file.absoluteFilePath().toStdString()));

  // Save without normals and color (Since normals are bad and it also saves disk space)
  saveMeshAsBinPly(mesh, asset_ply_file.toStdString(), false, false);

  std::cout << "Done" << std::endl;

  return EXIT_SUCCESS;
}




