/**
 * @file visualizeOFFmodel.cpp
 * @brief visualizeOFFmodel
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Geometry/ComputeAlignedBox.h"
#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/ModelRenderer.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/IO/ImportOFF.h"

#include <QGuiApplication>
#include <memory>
#include <iostream>

int main(int argc, char **argv) {
  using namespace CuteGL;
  using Eigen::Vector3f;

  std::string off_file = CUTEGL_ASSETS_FOLDER "/fertility.off";
  if(argc > 1)
    off_file = argv[1];
  else
    std::cout << "Usage: " << argv[0] << " [/path/to/OFF/model]\n";
  std::cout << "Will load OFF file from " << off_file << "\n";



  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr <ModelRenderer> renderer(new ModelRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Vector3f(0.0f, 0.0f, 2.0f),
                           Vector3f::Zero(),
                           Vector3f::UnitY());

  viewer.showAndWaitTillExposed();

  // Load mesh data from OFF file
  const MeshData md = loadMeshFromOFF(off_file);

  const Eigen::AlignedBox3f bbx = computeAlignedBox(md);

  {
    const Eigen::IOFormat fmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
    std::cout << "Input Mesh BBX Extents: " << bbx.min().format(fmt) << " --- " << bbx.max().format(fmt) << "\n";
    std::cout << "Input Mesh BBX Sizes: " << bbx.sizes().format(fmt) << "\n";
    std::cout << "Input Mesh Diagonal length: " << bbx.diagonal().norm() << "\n";

  }



  // Translate and scale to center and fit
  const Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(1.0f/bbx.sizes().maxCoeff()) * Eigen::Translation3f(-bbx.center());

  // set renderer
  renderer->setModel(md, model_pose);


  return app.exec();
}
