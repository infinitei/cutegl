/**
 * @file visualizePLYpointcloud.cpp
 * @brief visualizePLYpointcloud
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/IO/ImportPLY.h"
#include "CuteGL/Renderer/MultiObjectRenderer.h"
#include "CuteGL/Geometry/ComputeAlignedBox.h"
#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Core/PoseUtils.h"
#include <QGuiApplication>
#include <memory>

#include <iostream>

int main(int argc, char **argv) {
  using namespace CuteGL;
  using namespace CuteGL;

  QGuiApplication app(argc, argv);
  // Create the Renderer
  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);
  QSurfaceFormat format = viewer.format();
  format.setSamples(16);
  viewer.setFormat(format);

  // Set camera pose via lookAt
  viewer.camera().extrinsics() = getExtrinsicsFromLookAt(Eigen::Vector3f(0.0f, 0.0f, 2.0f),
                                                         Eigen::Vector3f::Zero(),
                                                         Eigen::Vector3f::UnitY());

  // Display the window
  viewer.showAndWaitTillExposed();




  std::string ply_file = "/home/abhijit/EclipseWorkspace/SemanticSFM/build/Colored.ply";
  if(argc > 1)
    ply_file = argv[1];
  else
    std::cout << "Usage: " << argv[0] << " [/path/to/PLY/model]\n";

  std::cout << "Will load PLY file from " << ply_file << "\n";

  {
    // Load pointcloud data from PLY file
    std::cout << "Loading PointCloud ... " << std::flush;
    std::vector<VertexXYZRGBA> vertices = loadPointCloudFromPLY(ply_file);
    std::cout << " Done" << std::endl;

    std::cout << "Adding point cloud to drawer ... " << std::flush;
    renderer->pointCloudDrawers().addItem(Eigen::Affine3f::Identity(), vertices);
    std::cout << " Done" << std::endl;

    std::cout << "Computing bbx ... " << std::flush;
    Eigen::AlignedBox3f bbx = computeAlignedBox(vertices);
    std::cout << " Done" << std::endl;

    viewer.setSceneBoundingBox(bbx);
    viewer.showEntireScene();
  }

  return app.exec();
}
