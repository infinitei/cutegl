/**
 * @file visualizeAssimpModel.cpp
 * @brief visualizeAssimpModel
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Geometry/ComputeAlignedBox.h"
#include "CuteGL/Surface/WindowRenderViewer.h"
#include "CuteGL/Renderer/ModelRenderer.h"

#include "CuteGL/Core/MeshUtils.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/IO/ImportViaAssimp.h"
#include "CuteGL/Utils/QtUtils.h"

#include <QGuiApplication>
#include <memory>
#include <iostream>

int main(int argc, char **argv) {
  using namespace CuteGL;

  std::string asset_file = CUTEGL_ASSETS_FOLDER "/Sphere.nff";
  if (argc > 1)
    asset_file = argv[1];
  else
    std::cout << "Usage: " << argv[0] << " [/path/to/asset/file]\n";


  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<ModelRenderer> renderer(new ModelRenderer());
  renderer->setDisplayGrid(true);


  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.0f, 2.0f),
                           Eigen::Vector3f::Zero(),
                           Eigen::Vector3f::UnitY());

  /// Set swap interval to 0
  setSwapIntervalToZero(viewer);

  viewer.showAndWaitTillExposed();


  std:: cout << "Loading model (via Assimp) from " << asset_file << "\n";
  // Load mesh data from OFF file
  std::vector<MeshData> meshes = loadMeshesViaAssimp(asset_file);
  std::cout << "Loaded Model with " << meshes.size() << " meshes\n";
  std::cout << "# of Faces = " << numberOfFaces(meshes) << "  # of vertices = " << numberOfVertices(meshes) << "\n";

  const Eigen::AlignedBox3f bbx = computeAlignedBox(meshes);

  // Translate and scale to center and fit
  const Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(
      1.0f / bbx.sizes().maxCoeff()) * Eigen::Translation3f(-bbx.center());

  renderer->setModel(meshes, model_pose);


  return app.exec();
}
