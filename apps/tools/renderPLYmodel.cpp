/**
 * @file renderPLYmodel.cpp
 * @brief renderPLYmodel
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Geometry/ComputeAlignedBox.h"
#include "CuteGL/Renderer/MultiObjectRenderer.h"
#include "CuteGL/Surface/OffScreenRenderViewer.h"
#include "CuteGL/IO/ImportPLY.h"

#include <QGuiApplication>
#include <QFileInfo>
#include <memory>
#include <iostream>

int main(int argc, char **argv) {
  using namespace CuteGL;

  if(argc < 2) {
    std::cout << "Usage: " << argv[0] << " [/path/to/PLY/model]  [render_image_file]\n";
    return EXIT_FAILURE;
  }

  const QFileInfo cad_file(argv[1]);
  const QString render_output_file = argc > 2 ? argv[2] : cad_file.completeBaseName() + ".png";

  std::cout << "Will load PLY file from " << cad_file.absoluteFilePath().toStdString() << "\n";
  std::cout << "Will save rendered image at " << render_output_file.toStdString() << "\n";

  QGuiApplication app(argc, argv);
  // Create the Renderer
  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  // Set the viewer up
  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(255, 255, 255, 0);
  viewer.resize(1024, 768);
  QSurfaceFormat format = viewer.format();
  format.setSamples(16);
  viewer.setFormat(format);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(1.3f, -2.0f, 1.3f),
                           Eigen::Vector3f::Zero(),
                           Eigen::Vector3f::UnitZ());


  viewer.create();
  viewer.makeCurrent();

  {
    MeshData mesh = loadMeshFromPLY(cad_file.absoluteFilePath().toStdString());

    Eigen::AlignedBox3f bbx = computeAlignedBox(mesh);
    Eigen::Affine3f model_pose = Eigen::UniformScaling<float>(
          2.0f / bbx.sizes().maxCoeff()) * Eigen::Translation3f(-bbx.center());
    renderer->modelDrawers().addItem(model_pose, mesh);
  }

  viewer.render();
  QImage image = viewer.readColorBuffer();
  image.save(render_output_file);

  return EXIT_SUCCESS;
}




