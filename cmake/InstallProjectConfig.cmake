###################### InstallProjectConfig  ###########################

# Add all targets to the build-tree export set
export( TARGETS ${CuteGL_LIBS}
  FILE "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Targets.cmake")

# Create the ProjectConfig.cmake and ProjectConfigVersion files
include(CMakePackageConfigHelpers)
configure_package_config_file(	
  ${PROJECT_CMAKE_DIR}/${PROJECT_NAME}Config.cmake.in
  ${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake
  INSTALL_DESTINATION ${INSTALL_CMAKE_DIR}
  PATH_VARS INSTALL_INCLUDE_DIR
  )


# Install the ${PROJECT_NAME}Config.cmake and ${PROJECT_NAME}ConfigVersion.cmake
install(FILES ${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake
  DESTINATION ${INSTALL_CMAKE_DIR}
  COMPONENT 	Devel
  )

# Install the export set for use with the install-tree
install(EXPORT ${PROJECT_NAME}Targets
  DESTINATION ${INSTALL_CMAKE_DIR}
  NAMESPACE   CuteGL::
  COMPONENT Devel
  )
