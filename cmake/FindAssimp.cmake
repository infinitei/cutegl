# - Try to find Assimp
# Once done, this will define
#
#  ASSIMP_FOUND - system has Assimp
#  ASSIMP_INCLUDE_DIRS - the Assimp include directories
#  ASSIMP_LIBRARIES - link these to use Assimp

FIND_PATH(ASSIMP_INCLUDE_DIR
    NAMES assimp/ai_assert.h
    PATHS /usr/local/include/assimp
          /usr/local/include
          /usr/include/assimp
          /usr/include
          /sw/local/include
          "$ENV{ProgramFiles}/assimp/include"
          "$ENV{ProgramFiles}/assimp"
          "C:/Program Files/assimp/include"
          "C:/Program Files/assimp"
    )


if( MSVC )
  # in order to prevent DLL hell, each of the DLLs have to be suffixed with the major version and msvc prefix
  if( MSVC70 OR MSVC71 )
    set(MSVC_PREFIX "vc70")
  elseif( MSVC80 )
    set(MSVC_PREFIX "vc80")
  elseif( MSVC90 )
    set(MSVC_PREFIX "vc90")
  elseif( MSVC10 )
    set(MSVC_PREFIX "vc100")
  elseif( MSVC11 )
    set(MSVC_PREFIX "vc110")
  elseif( MSVC12 )
    set(MSVC_PREFIX "vc120")
  else()
    set(MSVC_PREFIX "vc130")
  endif()
  set(ASSIMP_LIBRARY_SUFFIX "-${MSVC_PREFIX}-mt" CACHE STRING "the suffix for the assimp windows library" FORCE)
else()
  set(ASSIMP_LIBRARY_SUFFIX "" CACHE STRING "the suffix for the assimp libraries" FORCE)
endif()

FIND_LIBRARY(ASSIMP_LIBRARY_RELEASE 
    NAMES assimp${ASSIMP_LIBRARY_SUFFIX}
    PATHS /usr/local/lib
          /usr/lib
          /opt/local/lib
          /sw/local/lib
          "$ENV{ProgramFiles}/assimp/lib"
          "$ENV{ProgramFiles}/assimp"
          "C:/Program Files/assimp/lib"
          "C:/Program Files/assimp"
    )
    
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Assimp 
   DEFAULT_MSG
   ASSIMP_LIBRARY_RELEASE 
   ASSIMP_INCLUDE_DIR 
)

LIST(APPEND ASSIMP_LIBRARIES ${ASSIMP_LIBRARY_RELEASE})
LIST(APPEND ASSIMP_INCLUDE_DIRS ${ASSIMP_INCLUDE_DIR} )

mark_as_advanced(
  ASSIMP_LIBRARY_DEBUG
  ASSIMP_LIBRARY_RELEASE 
  ASSIMP_INCLUDE_DIR
  )
