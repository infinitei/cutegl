/**
 * @file BatchSMPLRenderer.cpp
 * @brief BatchSMPLRenderer
 *
 * @author Abhijit Kundu
 */

#include "BatchSMPLRenderer.h"

namespace CuteGL {

BatchSMPLRenderer::BatchSMPLRenderer()
    : basic_shader_(),
      model_pose_(Eigen::Isometry3f::Identity()) {

}

void BatchSMPLRenderer::initialize() {
  if(!hasValidContext()) {
    throw std::runtime_error("Context is NULL or Invalid");
  }

  if(!hasValidOpenGLFunctions()) {
    throw std::runtime_error("OpenGL Functions are not VALID");
  }

  DrawerBase::setOpenGLFunctions(glFuncs());

  basic_shader_.init(glFuncs());
}

void BatchSMPLRenderer::draw(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics) {
  basic_shader_.program.bind();
  basic_shader_.setProjectViewMatrix(intrinsics, extrinsics);

  {
    if (smpl_drawer_) {
      basic_shader_.setModelPose(model_pose_);
      smpl_drawer_->draw();
    }
  }
  basic_shader_.program.release();
}

}  // end namespace CuteGL
