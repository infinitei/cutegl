/**
 * @file SMPLRenderer.cpp
 * @brief SMPLRenderer
 *
 * @author Abhijit Kundu
 */

#include "SMPLRenderer.h"
#include <CuteGL/Core/Config.h>

namespace CuteGL {

SMPLRenderer::SMPLRenderer()
    : model_pose_(Eigen::Isometry3f::Identity()) {
}

void SMPLRenderer::init(PhongShader& shader) {
}

void SMPLRenderer::setSMPLData() {
  setSMPLData(CUTEGL_ASSETS_FOLDER"/smpl_neutral_lbs_10_207_0.h5",
              CUTEGL_ASSETS_FOLDER"/smpl_vertex_segm24_col24_14.h5");
}

void SMPLRenderer::draw(PhongShader& shader) {
  if(smpl_drawer_) {
    shader.setModelPose(model_pose_);
    smpl_drawer_->draw();
  }
}

}  // end namespace CuteGL
