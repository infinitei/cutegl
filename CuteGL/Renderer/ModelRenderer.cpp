/**
 * @file ModelRenderer.cpp
 * @brief ModelRenderer
 *
 * @author Abhijit Kundu
 */

#include "ModelRenderer.h"

namespace CuteGL {

ModelRenderer::ModelRenderer()
    : model_pose_(Eigen::Isometry3f::Identity()) {

}

void ModelRenderer::setModel(const std::vector<MeshData>& meshes,
                             const PoseType& model_pose) {
  model_drawer_.reset(new ModelDrawer(phongShader().program, meshes));
  model_pose_ = model_pose;
}

void ModelRenderer::setModel(const MeshData& mesh, const PoseType& model_pose) {
  model_drawer_.reset(new ModelDrawer(phongShader().program, mesh));
  model_pose_ = model_pose;
}

void ModelRenderer::init(PhongShader& shader) {
}

void ModelRenderer::draw(PhongShader& shader) {
  if(model_drawer_) {
    shader.setModelPose(model_pose_);
    model_drawer_->draw();
  }
}

}  // end namespace CuteGL
