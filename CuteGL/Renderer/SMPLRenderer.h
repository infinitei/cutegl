/**
 * @file SMPLRenderer.h
 * @brief SMPLRenderer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_RENDERER_SMPLRENDERER_H_
#define CUTEGL_RENDERER_SMPLRENDERER_H_

#include <CuteGL/Drawers/SMPLDrawer.h>
#include <CuteGL/Core/SMPLData.h>
#include <CuteGL/Renderer/BasicLightRenderer.h>
#include <memory>

namespace CuteGL {

class SMPLRenderer : public BasicLightRenderer {
 public:
  using PoseType = Eigen::Affine3f;
  using SMPLDataType = SMPLData<float>;
  using SMPLDrawerType = SMPLDrawer<SMPLDataType>;

  SMPLRenderer();

  template<class... DrawerData>
  void setSMPLData(const DrawerData&... data) {
    smpl_drawer_.reset(new SMPLDrawerType(phongShader().program, data...));
  }

  // Default init
  void setSMPLData();

  const SMPLDrawerType& smplDrawer() const {return *smpl_drawer_.get();}
  SMPLDrawerType& smplDrawer() {return *smpl_drawer_.get();}

  PoseType& modelPose() {return model_pose_;}
  const PoseType& modelPose() const {return model_pose_;}

 protected:
  virtual void init(PhongShader& shader);
  virtual void draw(PhongShader& shader);

 private:
   std::unique_ptr<SMPLDrawerType> smpl_drawer_;
   PoseType model_pose_;

 public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}  // end namespace CuteGL

#endif // end CUTEGL_RENDERER_SMPLRENDERER_H_
