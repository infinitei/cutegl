/**
 * @file BasicLightRenderer.h
 * @brief BasicLightRenderer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_RENDERER_BASICLIGHTRENDERER_H_
#define CUTEGL_RENDERER_BASICLIGHTRENDERER_H_

#include "CuteGL/Renderer/AbstractRenderer.h"
#include "CuteGL/Shaders/BasicShader.h"
#include "CuteGL/Shaders/PhongShader.h"
#include "CuteGL/Drawers/AxisDrawer.h"
#include "CuteGL/Drawers/GridDrawer.h"

namespace CuteGL {

class BasicLightRenderer : public AbstractRenderer {
 public:
  BasicLightRenderer();

  BasicShader& basicShader() {return basic_shader_;}
  const BasicShader& basicShader() const {return basic_shader_;}

  PhongShader& phongShader() {return phong_shader_;}
  const PhongShader& phongShader() const {return phong_shader_;}

 protected:
  virtual void init(BasicShader& shader) {};
  virtual void init(PhongShader& shader) {};


  virtual void draw(BasicShader& shader) {};
  virtual void draw(PhongShader& shader) {};

  void drawAxis();
  void drawGrid();

  virtual void initialize();
  virtual void draw(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics);

 private:


  /// A simple no Lighting  shader (Used for overlays e.g. Grid, PointCloud)
  BasicShader basic_shader_;

  /// Phong lighting shader
  PhongShader phong_shader_;

  AxisDrawer axis_drawer_;  ///< Axis Drawer
  GridDrawer grid_drawer_;  ///< Grid Drawer
};

}  // end namespace CuteGL
#endif // end CUTEGL_RENDERER_BASICLIGHTRENDERER_H_
