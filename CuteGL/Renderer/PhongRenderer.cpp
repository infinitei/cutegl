/**
 * @file PhongRenderer.cpp
 * @brief PhongRenderer
 *
 * @author Abhijit Kundu
 */

#include "PhongRenderer.h"
#include "CuteGL/Drawers/DrawerBase.h"
#include <cassert>

namespace CuteGL {

PhongRenderer::PhongRenderer()
    : phong_shader_() {
}

void PhongRenderer::initialize() {
  if(!hasValidContext()) {
    throw std::runtime_error("Context is NULL or Invalid");
  }

  if(!hasValidOpenGLFunctions()) {
    throw std::runtime_error("OpenGL Functions are not VALID");
  }

  DrawerBase::setOpenGLFunctions(glFuncs());

  phong_shader_.init(glFuncs());

  init(phong_shader_);
}

void PhongRenderer::draw(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics) {
  phong_shader_.program.bind();
  phong_shader_.setProjectViewMatrix(intrinsics, extrinsics);
  {
    draw(phong_shader_);
  }
  phong_shader_.program.release();
}

}  // end namespace CuteGL
