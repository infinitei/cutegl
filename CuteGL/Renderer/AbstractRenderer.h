/**
 * @file AbstractRenderer.h
 * @brief AbstractRenderer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_RENDERER_ABSTRACTRENDERER_H_
#define CUTEGL_RENDERER_ABSTRACTRENDERER_H_

#include "CuteGL/Core/OpenGLFunctions.h"
#include <Eigen/Geometry>

class QOpenGLContext;

namespace CuteGL {

class AbstractRenderer {
 public:
  AbstractRenderer();
  virtual ~AbstractRenderer();

  void setContext(QOpenGLContext *context) { context_ = context; }
  QOpenGLContext* context() const { return context_; }

  void setOpenGLFunctions(OpenGLFunctions *glfuncs) { glfuncs_ = glfuncs; }
  OpenGLFunctions* glFuncs() const { return glfuncs_; }

  /// initialize function (Should be implemented in Derived Renderer)
  virtual void initialize() = 0;

  /// Draw function (Should be implemented in Derived Renderer)
  virtual void draw(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics) = 0;

  bool isGridDisplayed() const {return display_grid_;}
  void setDisplayGrid(bool enable) {display_grid_ = enable;}

  bool isAxisDisplayed() const {return display_axis_;}
  void setDisplayAxis(bool enable) {display_axis_ = enable;}

  bool hasValidContext() const;
  bool hasValidOpenGLFunctions() const;

 protected:
  QOpenGLContext* context_; ///< OpenGL context (non-owned)
  OpenGLFunctions* glfuncs_;  ///< OpenGL Function resolution

  bool display_grid_; ///< Display Grid Flag
  bool display_axis_; ///< Display Axis Flag
};

}  // end namespace CuteGL

#endif // end CUTEGL_RENDERER_ABSTRACTRENDERER_H_
