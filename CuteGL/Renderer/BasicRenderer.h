/**
 * @file BasicRenderer.h
 * @brief BasicRenderer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_RENDERER_BASICRENDERER_H_
#define CUTEGL_RENDERER_BASICRENDERER_H_

#include "CuteGL/Renderer/AbstractRenderer.h"
#include "CuteGL/Drawers/GridDrawer.h"
#include "CuteGL/Shaders/BasicShader.h"

namespace CuteGL {

class BasicRenderer : public AbstractRenderer {
 public:
  BasicRenderer();

  BasicShader& basicShader() {return basic_shader_;}
  const BasicShader& basicShader() const {return basic_shader_;}

 protected:
  virtual void init(BasicShader& shader) {};
  virtual void draw(BasicShader& shader) {};

 private:
  void initialize();
  void draw(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics);
  void drawGrid();

 private:
  BasicShader basic_shader_;

  GridDrawer grid_drawer_;
};

}  // end namespace CuteGL

#endif // end CUTEGL_RENDERER_BASICRENDERER_H_
