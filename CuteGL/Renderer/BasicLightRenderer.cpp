/**
 * @file BasicLightRenderer.cpp
 * @brief BasicLightRenderer
 *
 * @author Abhijit Kundu
 */

#include "BasicLightRenderer.h"

namespace CuteGL {

BasicLightRenderer::BasicLightRenderer()
    : basic_shader_(),
      phong_shader_(),
      axis_drawer_(),
      grid_drawer_() {
}

void BasicLightRenderer::initialize() {

  if(!hasValidContext()) {
    throw std::runtime_error("Context is NULL or Invalid");
  }

  if(!hasValidOpenGLFunctions()) {
    throw std::runtime_error("OpenGL Functions are not VALID");
  }

  DrawerBase::setOpenGLFunctions(glFuncs());

  basic_shader_.init(glFuncs());

  phong_shader_.init(glFuncs());


  {
    basic_shader_.program.bind();
    grid_drawer_.init(basic_shader_.program, 20, GridDrawer::ColorType(128, 128, 128, 255));
    init(basic_shader_);
    basic_shader_.program.release();
  }

  {
    phong_shader_.program.bind();
    axis_drawer_.init(phong_shader_.program);
    init(phong_shader_);
    phong_shader_.program.release();
  }
}

void BasicLightRenderer::draw(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics) {
  {
    basic_shader_.program.bind();
    basic_shader_.setProjectViewMatrix(intrinsics, extrinsics);

    // Client Basic Draw
    draw(basic_shader_);

    // Draw Grid
    if(display_grid_) {
      basic_shader_.setModelPose(Eigen::Affine3f::Identity());
      drawGrid();
    }

    basic_shader_.program.release();
  }

  {
    phong_shader_.program.bind();
    phong_shader_.setProjectViewMatrix(intrinsics, extrinsics);

    // Client Phong Draw
    draw(phong_shader_);

    // Draw Axis
    if (display_axis_) {
      phong_shader_.setModelPose(Eigen::Affine3f::Identity());
      drawAxis();
    }

    phong_shader_.program.release();
  }
}

void BasicLightRenderer::drawAxis() {
  axis_drawer_.draw();
}

void BasicLightRenderer::drawGrid() {
  grid_drawer_.draw();
}



}  // end namespace CuteGL
