/**
 * @file MultiObjectRenderer.h
 * @brief MultiObjectRenderer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_RENDERER_MULTIOBJECTRENDERER_H_
#define CUTEGL_RENDERER_MULTIOBJECTRENDERER_H_

#include "CuteGL/Renderer/BasicLightRenderer.h"
#include "CuteGL/Drawers/MultiDrawer.h"
#include "CuteGL/Drawers/ModelDrawer.h"
#include "CuteGL/Drawers/PointCloudDrawer.h"
#include "CuteGL/Drawers/BoundingBoxDrawer.h"
#include "CuteGL/Drawers/LineDrawer.h"
#include "CuteGL/Drawers/CameraDrawer.h"
#include "CuteGL/Drawers/GenericDrawer.h"

namespace CuteGL {

class MultiObjectRenderer : public BasicLightRenderer {
 public:

  typedef MultiDrawer<ModelDrawer, PhongShader> ModelDrawers;
  typedef MultiDrawer<GenericDrawer, PhongShader> GenericDrawers;

  typedef MultiDrawer<PointCloudDrawer, BasicShader> PointCloudDrawers;
  typedef MultiDrawer<BoundingBoxDrawer, BasicShader> BoundingBoxDrawers;
  typedef MultiDrawer<LineDrawer, BasicShader> LineDrawers;
  typedef MultiDrawer<CameraDrawer, BasicShader> CameraDrawers;


  MultiObjectRenderer();

  const ModelDrawers& modelDrawers() const {return model_drawers_;}
  ModelDrawers& modelDrawers() {return model_drawers_;}

  const GenericDrawers& genericDrawers() const {return generic_drawers_;}
  GenericDrawers& genericDrawers() {return generic_drawers_;}

  const PointCloudDrawers& pointCloudDrawers() const {return pointcloud_drawers_;}
  PointCloudDrawers& pointCloudDrawers() {return pointcloud_drawers_;}

  const BoundingBoxDrawers& bbxDrawers() const {return bbx_drawers_;}
  BoundingBoxDrawers& bbxDrawers() {return bbx_drawers_;}

  const LineDrawers& lineDrawers() const {return line_drawers_;}
  LineDrawers& lineDrawers() {return line_drawers_;}

  const CameraDrawers& cameraDrawers() const {return camera_drawers_;}
  CameraDrawers& cameraDrawers() {return camera_drawers_;}

 protected:
  virtual void init(BasicShader& shader);
  virtual void init(PhongShader& shader);


  virtual void draw(BasicShader& shader);
  virtual void draw(PhongShader& shader);

 private:
  ModelDrawers model_drawers_;
  GenericDrawers generic_drawers_;
  PointCloudDrawers pointcloud_drawers_;
  BoundingBoxDrawers bbx_drawers_;
  LineDrawers line_drawers_;
  CameraDrawers camera_drawers_;

};

}  // end namespace CuteGL
#endif // end CUTEGL_RENDERER_MULTIOBJECTRENDERER_H_
