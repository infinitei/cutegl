/**
 * @file BatchSMPLRenderer.h
 * @brief BatchSMPLRenderer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_RENDERER_BATCHSMPLRENDERER_H_
#define CUTEGL_RENDERER_BATCHSMPLRENDERER_H_

#include <CuteGL/Drawers/BatchSMPLDrawer.h>
#include <CuteGL/Core/SMPLData.h>
#include <CuteGL/Renderer/AbstractRenderer.h>
#include <CuteGL/Shaders/BasicShader.h>
#include <memory>

namespace CuteGL {

class BatchSMPLRenderer : public AbstractRenderer {
 public:
  using PoseType = Eigen::Affine3f;
  using SMPLDataType = SMPLData<float>;
  using SMPLDrawerType = BatchSMPLDrawer<SMPLDataType>;

  BatchSMPLRenderer();

  template<class ... DrawerData>
  void setSMPLData(const DrawerData&... data) {
    smpl_drawer_.reset(new SMPLDrawerType(basic_shader_.program, data...));
  }

  const SMPLDrawerType& smplDrawer() const {
    return *smpl_drawer_.get();
  }
  SMPLDrawerType& smplDrawer() {return *smpl_drawer_.get();
  }

  PoseType& modelPose() {return model_pose_;}
  const PoseType& modelPose() const {return model_pose_;}

 private:
  void initialize();
  void draw(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics);

 private:
  BasicShader basic_shader_;
  std::unique_ptr<SMPLDrawerType> smpl_drawer_;
  PoseType model_pose_;

 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}  // end namespace CuteGL

#endif // end CUTEGL_RENDERER_BATCHSMPLRENDERER_H_
