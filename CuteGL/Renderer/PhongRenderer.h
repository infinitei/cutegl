/**
 * @file PhongRenderer.h
 * @brief PhongRenderer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_RENDERER_PHONGRENDERER_H_
#define CUTEGL_RENDERER_PHONGRENDERER_H_

#include "CuteGL/Renderer/AbstractRenderer.h"
#include "CuteGL/Shaders/PhongShader.h"

namespace CuteGL {

class PhongRenderer : public AbstractRenderer {
 public:
  PhongRenderer();
  virtual ~PhongRenderer() {};

  PhongShader& phongShader() {return phong_shader_;}
  const PhongShader& phongShader() const {return phong_shader_;}

 protected:
  virtual void init(PhongShader& shader) {};
  virtual void draw(PhongShader& shader) {};

 private:
  void initialize();
  void draw(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics);

 private:
  PhongShader phong_shader_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_RENDERER_PHONGRENDERER_H_
