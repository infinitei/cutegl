/**
 * @file ModelRenderer.h
 * @brief ModelRenderer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_RENDERER_MODELRENDERER_H_
#define CUTEGL_RENDERER_MODELRENDERER_H_

#include <CuteGL/Renderer/BasicLightRenderer.h>
#include <CuteGL/Drawers/ModelDrawer.h>
#include <memory>

namespace CuteGL {

class ModelRenderer : public BasicLightRenderer {
 public:
  using PoseType = Eigen::Affine3f;

  ModelRenderer();

  void setModel(const std::vector<MeshData>& meshes,
                const PoseType& model_pose = PoseType::Identity());

  void setModel(const MeshData& mesh,
                const PoseType& model_pose = PoseType::Identity());

  PoseType& modelPose() {return model_pose_;}
  const PoseType& modelPose() const {return model_pose_;}

  const ModelDrawer& modelDrawer() const {return *model_drawer_.get();}
  ModelDrawer& modelDrawer() {return *model_drawer_.get();}

 protected:
  virtual void init(PhongShader& shader);
  virtual void draw(PhongShader& shader);


 private:
  std::unique_ptr<ModelDrawer> model_drawer_;
  PoseType model_pose_;

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}  // end namespace CuteGL
#endif // end CUTEGL_RENDERER_MODELRENDERER_H_
