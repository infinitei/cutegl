/**
 * @file AbstractRenderer.cpp
 * @brief AbstractRenderer
 *
 * @author Abhijit Kundu
 */

#include "AbstractRenderer.h"

namespace CuteGL {

AbstractRenderer::AbstractRenderer()
    : context_(nullptr),
      glfuncs_(nullptr),
      display_grid_(true),
      display_axis_(true) {
}

AbstractRenderer::~AbstractRenderer() {
}

bool AbstractRenderer::hasValidContext() const {
  return (context() != nullptr) && (context()->isValid());
}

bool AbstractRenderer::hasValidOpenGLFunctions() const {
  return (glFuncs() != nullptr) && (glfuncs_->hasBeenInitialized());
}

}  // end namespace CuteGL
