/**
 * @file BasicRenderer.cpp
 * @brief BasicRenderer
 *
 * @author Abhijit Kundu
 */

#include "BasicRenderer.h"
#include <cassert>

namespace CuteGL {

BasicRenderer::BasicRenderer()
    : basic_shader_(),
      grid_drawer_() {
}

void BasicRenderer::initialize() {
  if(!hasValidContext()) {
    throw std::runtime_error("Context is NULL or Invalid");
  }

  if(!hasValidOpenGLFunctions()) {
    throw std::runtime_error("OpenGL Functions are not VALID");
  }

  DrawerBase::setOpenGLFunctions(glFuncs());

  basic_shader_.init(glFuncs());

  basic_shader_.program.bind();

  grid_drawer_.init(basic_shader_.program, 20, GridDrawer::ColorType(128, 128, 128, 255));

  basic_shader_.program.release();

  init(basic_shader_);
}

void BasicRenderer::draw(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics) {
  basic_shader_.program.bind();
  basic_shader_.setProjectViewMatrix(intrinsics, extrinsics);

  {
    draw(basic_shader_);
  }

  {
    // Draw Grid
    if(display_grid_) {
      basic_shader_.setModelPose(Eigen::Affine3f::Identity());
      drawGrid();
    }

  }
  basic_shader_.program.release();
}

void BasicRenderer::drawGrid() {
  grid_drawer_.draw();
}

}  // end namespace CuteGL
