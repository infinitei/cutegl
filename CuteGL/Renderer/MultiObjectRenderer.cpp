/**
 * @file MultiObjectRenderer.cpp
 * @brief MultiObjectRenderer
 *
 * @author Abhijit Kundu
 */

#include "MultiObjectRenderer.h"

namespace CuteGL {

MultiObjectRenderer::MultiObjectRenderer()
  : model_drawers_(phongShader()),
    generic_drawers_(phongShader()),
    pointcloud_drawers_(basicShader()),
    bbx_drawers_(basicShader()),
    line_drawers_(basicShader()),
    camera_drawers_(basicShader()) {

}

void MultiObjectRenderer::init(BasicShader& shader) {
}

void MultiObjectRenderer::init(PhongShader& shader) {
}

void MultiObjectRenderer::draw(BasicShader& shader) {
  pointcloud_drawers_.draw();
  bbx_drawers_.draw();
  line_drawers_.draw();
  camera_drawers_.draw();
}

void MultiObjectRenderer::draw(PhongShader& shader) {
  model_drawers_.draw();
  generic_drawers_.draw();
}

}  // end namespace CuteGL
