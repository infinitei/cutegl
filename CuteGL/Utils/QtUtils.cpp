/**
 * @file QtUtils.cpp
 * @brief QtUtils
 *
 * @author Abhijit Kundu
 */

#include "QtUtils.h"
#include <QTime>
#include <QCoreApplication>

namespace CuteGL {

void delayWithoutBlockingGUI(int millisecondsToWait) {
  QTime dieTime = QTime::currentTime().addMSecs(millisecondsToWait);
  while (QTime::currentTime() < dieTime) {
    QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
  }
}

}  // end namespace CuteGL


