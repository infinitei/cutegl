/**
 * @file ColorUtils.h
 * @brief ColorUtils
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_UTILS_COLORUTILS_H_
#define CUTEGL_UTILS_COLORUTILS_H_

#include <Eigen/Core>

namespace CuteGL {

/**@brief make RGB from HSV
 *
 * @param h hue in [0, 360]
 * @param s saturation in [0, 1]
 * @param v value in [0, 1]
 * @return RGB as unsigned char scalar in range [0, 255]
 */
Eigen::Matrix<unsigned char, 3, 1> makeRGBfromHSV(float h, float s, float v);

/**@brief make RGBA in [0, 255] from HSV in [0, 1]
 *
 * @param h hue in [0, 360]
 * @param s saturation in [0, 1]
 * @param v value in [0, 1]
 * @param a alpha in [0, 1]
 * @return RGBA as unsigned char scalar in range [0, 255]
 */
Eigen::Matrix<unsigned char, 4, 1> makeRGBAfromHSV(float h, float s, float v, float a = 1.0f);

}  // namespace CuteGL


#endif // end CUTEGL_UTILS_COLORUTILS_H_
