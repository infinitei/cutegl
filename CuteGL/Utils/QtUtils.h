/**
 * @file QtUtils.h
 * @brief QtUtils
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_UTILS_QTUTILS_H_
#define CUTEGL_UTILS_QTUTILS_H_

namespace CuteGL {

/**@brief Sleep for certain time without blocking the GUI
 *
 * @param millisecondsToWait
 */
void delayWithoutBlockingGUI(int millisecondsToWait = 1);

/**@brief Sets the swap interval of an window to 0 for profiling purposes.
 *
 * This is a convenience function to set the swap interval to 0 which is
 * useful for getting the actual FPS.
 *
 * @note Call this function before the show() function of the window.
 *
 * @param window - e.g. windows of types derived from QOpenGLWindow
 */
template <class WindowType>
void setSwapIntervalToZero(WindowType& window) {
  auto format = window.format();
  format.setSwapInterval(0);
  window.setFormat(format);
}



}  // end namespace CuteGL

#endif // end CUTEGL_UTILS_QTUTILS_H_
