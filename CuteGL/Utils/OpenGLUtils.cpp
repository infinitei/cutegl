/**
 * @file OpenGLUtils.cpp
 * @brief OpenGLUtils
 *
 * @author Abhijit Kundu
 */

#include "OpenGLUtils.h"

namespace CuteGL {

std::vector<GLuint> createGLBuffers(OpenGLFunctions* glfuncs, std::size_t num_of_buffers,
                                    GLenum buffer_target, GLsizeiptr buffer_size,
                                    GLenum buffer_usage, const GLvoid *data) {
  std::vector<GLuint> buffers(num_of_buffers);
  glfuncs->glGenBuffers(buffers.size(), buffers.data());
  for (const GLuint buffer_id : buffers) {
    glfuncs->glBindBuffer(buffer_target, buffer_id);
    glfuncs->glBufferData(buffer_target, buffer_size, data, buffer_usage);
  }
  glfuncs->glBindBuffer(buffer_target, 0);
  return buffers;
}

}  // namespace CuteGL


