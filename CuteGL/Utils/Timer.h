/**
 * @file Timer.h
 * @brief Timer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_UTILS_TIMER_H_
#define CUTEGL_UTILS_TIMER_H_

#include <chrono>

namespace CuteGL {

class Timer {
  using clock = std::chrono::high_resolution_clock;
 public:
  Timer()
      : start_(clock::now()) {
  }

  inline void start() {
    start_ = clock::now();
  }

  template <typename T = std::chrono::milliseconds>
  inline T elapsed() const {
    return std::chrono::duration_cast<T>(clock::now() - start_);
  }

  inline double elapsed_in_ms() const {
    return std::chrono::duration_cast<std::chrono::milliseconds>(clock::now() - start_).count();
  }

  inline double elapsed_in_us() const {
    return std::chrono::duration_cast<std::chrono::microseconds>(clock::now() - start_).count();
  }

 private:
  std::chrono::time_point<clock> start_;
};

}  // namespace CuteGL


#endif // end CUTEGL_UTILS_TIMER_H_
