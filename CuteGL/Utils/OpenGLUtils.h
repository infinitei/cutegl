/**
 * @file OpenGLUtils.h
 * @brief OpenGLUtils
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_UTILS_OPENGLUTILS_H_
#define CUTEGL_UTILS_OPENGLUTILS_H_

#include "CuteGL/Core/OpenGLFunctions.h"
#include "CuteGL/Core/Config.h"

namespace CuteGL {

template<typename T>
struct GLTraits {
};

template<>
struct GLTraits<double> {
  static GLenum constexpr type = GL_DOUBLE;
};

template<>
struct GLTraits<float> {
  static GLenum constexpr type = GL_FLOAT;
};

template<>
struct GLTraits<unsigned char> {
  static GLenum constexpr type = GL_UNSIGNED_BYTE;
};

template<>
struct GLTraits<int> {
  static GLenum constexpr type = GL_INT;
};

template<>
struct GLTraits<short int> {
  static GLenum constexpr type = GL_SHORT;
};

/**@brief create and allocate buffers and return std::vector of the buffer ids
 *
 * @param glfuncs       - OpenGL functions
 * @param num_of_buffers- Number of buffers to create
 * @param buffer_target - e.g GL_ARRAY_BUFFER, GL_PIXEL_PACK_BUFFER
 * @param buffer_size   - size of each buffer in bytes
 * @param buffer_usage  - usage pattern e.g. GL_STATIC_DRAW, GL_DYNAMIC_READ
 * @param data          - pointer to data to be copied (or null if no data copy needed)
 * @return std::vector of the buffer ids
 */
std::vector<GLuint> createGLBuffers(OpenGLFunctions* glfuncs, std::size_t num_of_buffers, GLenum buffer_target,
                                    GLsizeiptr buffer_size, GLenum buffer_usage, const GLvoid *data = nullptr);


inline void checkOpenGLError(const char* stmt, const char* fname, int line) {
  GLenum err = glGetError();
  if (err != GL_NO_ERROR) {
    printf("OpenGL error %08x, at %s:%i - for %s\n", err, fname, line, stmt);
    exit(EXIT_FAILURE);
  }
}

}  // namespace CuteGL

#ifdef CUTEGL_DEBUG_GL
    #define CHECK_GL(stmt) do { \
            stmt; \
            CuteGL::checkOpenGLError(#stmt, __FILE__, __LINE__); \
        } while (0)
#else
    #define CHECK_GL(stmt) stmt
#endif

#endif // end CUTEGL_UTILS_OPENGLUTILS_H_
