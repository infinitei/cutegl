/**
 * @file CudaTexture.h
 * @brief CudaTexture
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_UTILS_CUDATEXTURE_H_
#define CUTEGL_UTILS_CUDATEXTURE_H_

#include "CuteGL/Utils/CudaUtils.h"

namespace CuteGL {

void copyTexture(float* dest, cudaArray *in_array, int W, int H);

}  // namespace CuteGL

#endif // end CUTEGL_UTILS_CUDATEXTURE_H_
