/**
 * @file ColorUtils.cpp
 * @brief ColorUtils
 *
 * @author Abhijit Kundu
 */

#include "ColorUtils.h"

namespace CuteGL {

Eigen::Matrix<unsigned char, 3, 1> makeRGBfromHSV(float h, float s, float v) {
  h = std::fmod(h, 360.0f);
  h /= 60;
  int hi = (int) h;
  float f = h - hi;

  Eigen::Array4i pqtv = (Eigen::Array4f(v * (1 - s),
                                        v * (1 - f * s),
                                        v * (1 - (1 - f) * s),
                                        v) * 255).round().cast<int>();

  Eigen::Matrix<unsigned char, 3, 1> rgb;

  switch (hi) {
    case 0:
      rgb.x() = pqtv.w();
      rgb.y() = pqtv.z();
      rgb.z() = pqtv.x();
      break;
    case 1:
      rgb.x() = pqtv.y();
      rgb.y() = pqtv.w();
      rgb.z() = pqtv.x();
      break;
    case 2:
      rgb.x() = pqtv.x();
      rgb.y() = pqtv.w();
      rgb.z() = pqtv.z();
      break;
    case 3:
      rgb.x() = pqtv.x();
      rgb.y() = pqtv.y();
      rgb.z() = pqtv.w();
      break;
    case 4:
      rgb.x() = pqtv.z();
      rgb.y() = pqtv.x();
      rgb.z() = pqtv.w();
      break;
    case 5:
      rgb.x() = pqtv.w();
      rgb.y() = pqtv.x();
      rgb.z() = pqtv.y();
      break;
  }

  return rgb;
}

Eigen::Matrix<unsigned char, 4, 1> makeRGBAfromHSV(float h, float s, float v, float a) {
  h = std::fmod(h, 360.0f);
  h /= 60;
  int hi = (int) h;
  float f = h - hi;

  Eigen::Array4i pqtv = (Eigen::Array4f(v * (1 - s),
                                        v * (1 - f * s),
                                        v * (1 - (1 - f) * s),
                                        v) * 255).round().cast<int>();

  Eigen::Matrix<unsigned char, 4, 1> rgba;
  rgba.w() = a * 255;

  switch (hi) {
    case 0:
      rgba.x() = pqtv.w();
      rgba.y() = pqtv.z();
      rgba.z() = pqtv.x();
      break;
    case 1:
      rgba.x() = pqtv.y();
      rgba.y() = pqtv.w();
      rgba.z() = pqtv.x();
      break;
    case 2:
      rgba.x() = pqtv.x();
      rgba.y() = pqtv.w();
      rgba.z() = pqtv.z();
      break;
    case 3:
      rgba.x() = pqtv.x();
      rgba.y() = pqtv.y();
      rgba.z() = pqtv.w();
      break;
    case 4:
      rgba.x() = pqtv.z();
      rgba.y() = pqtv.x();
      rgba.z() = pqtv.w();
      break;
    case 5:
      rgba.x() = pqtv.w();
      rgba.y() = pqtv.x();
      rgba.z() = pqtv.y();
      break;
  }

  return rgba;
}

}  // namespace CuteGL


