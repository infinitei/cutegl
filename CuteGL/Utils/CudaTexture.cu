/**
 * @file CudaTexture.cu
 * @brief CudaTexture
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Utils/CudaTexture.h"

texture<float, 2, cudaReadModeElementType> texRef;

namespace CuteGL {

__global__
void CopyTextureKernel(float* dst, int W, int H) {
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;

  // Early-out if we are beyond the texture coordinates for our texture.
  if (x > W || y > H)
    return;

  dst[y * W + x] = tex2D(texRef, x, y);
}

void copyTexture(float* dest, cudaArray *in_array, int W, int H) {
  CHECK_CUDA(cudaBindTextureToArray( texRef, in_array));

  dim3 block(16, 16, 1);
  dim3 grid((W + block.x -1) / block.x, (H + block.y -1) / block.y, 1);
  CopyTextureKernel<<< grid, block >>>(dest, W, H);

  // Unbind the texture reference
  CHECK_CUDA(cudaUnbindTexture(texRef));
}

}  // namespace CuteGL
