/**
 * @file CudaUtils.cpp
 * @brief CudaUtils
 *
 * @author Abhijit Kundu
 */

#include "CudaUtils.h"

namespace CuteGL {

GpuTimer::GpuTimer() {
  cudaEventCreate(&start_);
  cudaEventCreate(&stop_);
}

GpuTimer::~GpuTimer() {
  cudaEventDestroy(start_);
  cudaEventDestroy(stop_);
}

void GpuTimer::start() {
  cudaEventRecord(start_, 0);
}

void GpuTimer::stop() {
  cudaEventRecord(stop_, 0);
}

float GpuTimer::elapsed_in_ms() {
  float elapsed;
  cudaEventSynchronize(stop_);
  cudaEventElapsedTime(&elapsed, start_, stop_);
  return elapsed;
}


std::vector<cudaGraphicsResource*> createCudaGLBufferResources(const std::vector<GLuint>& buffers, unsigned int flags) {
  std::vector<cudaGraphicsResource*> cuda_gl_resources(buffers.size());
  for (std::size_t i = 0; i < buffers.size(); ++i) {
    CHECK_CUDA(cudaGraphicsGLRegisterBuffer(&cuda_gl_resources[i], buffers[i], flags));
  }
  return cuda_gl_resources;
}

}  // namespace CuteGL
