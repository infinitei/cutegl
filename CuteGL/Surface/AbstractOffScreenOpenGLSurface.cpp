/**
 * @file AbstractOffScreenOpenGLSurface.cpp
 * @brief AbstractOffScreenOpenGLSurface implementation
 *
 * @author Abhijit Kundu
 */

#include "AbstractOffScreenOpenGLSurface.h"
#include "CuteGL/Core/OpenGLFunctions.h"
#include <QDebug>
#include <QString>
#include <cassert>

namespace CuteGL {

AbstractOffScreenOpenGLSurface::AbstractOffScreenOpenGLSurface(QScreen* target_screen, const QSize& size)
    : QOffscreenSurface(target_screen),
      context_(nullptr),
      glfuncs_(nullptr),
      size_(size) {
  QSurfaceFormat format;
  format.setRenderableType(QSurfaceFormat::OpenGL);
  format.setProfile(QSurfaceFormat::CoreProfile);
  setFormat(format);
}

void AbstractOffScreenOpenGLSurface::create() {
  QOffscreenSurface::create();

  context_ = new QOpenGLContext(this);
  context_->setFormat(format());
  context_->create();
  if(!context_->isValid())
    throw std::runtime_error("OffScreenOpenGLSurface::create(): Cannot create valid context");

  initializeOpenGLFunctions();
  assert(isValid());

  /// Now call user defined initialization
  initializeGL();
}

AbstractOffScreenOpenGLSurface::~AbstractOffScreenOpenGLSurface() {
  doneCurrent();

  if(context_)
    delete context_;

  destroy();
}

void AbstractOffScreenOpenGLSurface::resize(const QSize& new_size) {
  size_ = new_size;
}

void AbstractOffScreenOpenGLSurface::resize(int w, int h) {
  resize(QSize(w, h));

  //user-defined resize method
  resizeGL(size().width(), size().height());
}

QSize AbstractOffScreenOpenGLSurface::size() const {
  return size_;
}

int AbstractOffScreenOpenGLSurface::width() const {
  return size_.width();
}

int AbstractOffScreenOpenGLSurface::height() const {
  return size_.height();
}

void AbstractOffScreenOpenGLSurface::initializeOpenGLFunctions() {
  // initialize OpenGL Functions
  makeCurrent();
  {
    glfuncs_ = context()->versionFunctions<OpenGLFunctions>();
    if (!glfuncs_) {
      throw std::runtime_error("Could not obtain required OpenGL context version");
    }
    glfuncs_->initializeOpenGLFunctions();
  }
  doneCurrent();
}

void AbstractOffScreenOpenGLSurface::makeCurrent() {
  if (context_ )
    context_->makeCurrent(this);
}

void AbstractOffScreenOpenGLSurface::doneCurrent() {
  if (context_)
    context_->doneCurrent();
}

bool AbstractOffScreenOpenGLSurface::isValid() const {
  return (QOffscreenSurface::isValid() && context() && glFuncs());
}

QOpenGLContext* AbstractOffScreenOpenGLSurface::context() const {
  return context_;
}


void AbstractOffScreenOpenGLSurface::printContextInformation() {
  makeCurrent();
  QString opengl_type((context()->isOpenGLES()) ? "OpenGL ES" : "OpenGL");
  QString opengl_version(reinterpret_cast<const char*>(glGetString(GL_VERSION)));

  // Get Profile Information
  QString opengl_profile;
  switch (format().profile()) {
    case QSurfaceFormat::NoProfile:
      opengl_profile = "None";
      break;
    case QSurfaceFormat::CoreProfile:
      opengl_profile = "Core";
      break;
    case QSurfaceFormat::CompatibilityProfile:
      opengl_profile = "Compatibility";
      break;
  }

  QString glsl_version(reinterpret_cast<const char*>(glGetString(GL_SHADING_LANGUAGE_VERSION)));

  qDebug() << qPrintable(opengl_type) << qPrintable(opengl_version) << "(" << qPrintable(opengl_profile) << ")"
           << " GLSL Version:" << qPrintable(glsl_version);
  doneCurrent();
}

}  // end namespace CuteGL
