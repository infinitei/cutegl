/**
 * @file OffScreenOpenGLSurface.cpp
 * @brief OffScreenOpenGLSurface
 *
 * @author Abhijit Kundu
 */

#include "OffScreenOpenGLSurface.h"
#include "CuteGL/Core/OpenGLFunctions.h"
#include <QOpenGLFramebufferObject>
#include <QDebug>
#include <cassert>
#include <Eigen/Core>

namespace CuteGL {

OffScreenOpenGLSurface::OffScreenOpenGLSurface(QScreen* target_screen, const QSize& size)
    : AbstractOffScreenOpenGLSurface(target_screen, size),
      fbo_(size.width(), size.height()),
      bg_color_(0, 0, 0, 255) {
  setViewPort(0, 0, size.width(), size.height());
}

void OffScreenOpenGLSurface::initializeGL() {
  makeCurrent();

  // turn off clamping while doing glReadPixels
  glfuncs_->glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);

  recreateFBO();

  glfuncs_->glViewport(viewport_x_, viewport_y_, viewport_width_, viewport_height_);

  // set clear background color
  glfuncs_->glClearColor(bg_color_.redF(), bg_color_.greenF(), bg_color_.blueF(), bg_color_.alphaF());

  // Enable Depth test
  glfuncs_->glEnable(GL_DEPTH_TEST);

  // Call user defined init
  init();
  doneCurrent();
}

void OffScreenOpenGLSurface::resizeGL(int width, int height) {
  setViewPort(0, 0, width, height);
}

void OffScreenOpenGLSurface::recreateFBO() {
  // Delete existing buffer (if any)
  if(fbo_ .isCreated()) {
    fbo_.destroy();
  }

  // create new frame buffer
  fbo_.resize(size().width(), size().height());
  fbo_.create(glfuncs_);
  fbo_.bind();
  assert(fbo_ .isCreated());

  fbo_.attachRenderBuffer(GL_COLOR_ATTACHMENT0, GL_RGBA);
  fbo_.attachRenderBuffer(GL_COLOR_ATTACHMENT1, GL_R32F);
  fbo_.attachRenderBuffer(GL_COLOR_ATTACHMENT2, GL_RGB32F);
  fbo_.attachRenderBuffer(GL_COLOR_ATTACHMENT3, GL_R32F);
  fbo_.attachRenderBuffer(GL_DEPTH_ATTACHMENT, GL_DEPTH_COMPONENT);

  // - Tell OpenGL which color attachments we'll use (of this framebuffer) for rendering
  GLuint attachments[4] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
  glfuncs_->glDrawBuffers(4, attachments);


  if(!fbo_.isComplete())
    throw std::runtime_error("OffScreenFrameBuffer::recreateFBO(): Failed to create valid FBO!");

  // clear frame buffer (TODO: This is not required I think)
  glfuncs_->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  fbo_.release();
}

void OffScreenOpenGLSurface::setBackgroundColor(const QColor& bg_color) {
  bg_color_ = bg_color;
  if (isValid()) {
    makeCurrent();
    glfuncs_->glClearColor(bg_color_.redF(), bg_color_.greenF(), bg_color_.blueF(), bg_color_.alphaF());
    doneCurrent();
  }
}

void OffScreenOpenGLSurface::setBackgroundColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
  setBackgroundColor(QColor(r,g,b,a));
}

void OffScreenOpenGLSurface::render() {
  // Make sure to not call render() without a valid surface!
  assert(isValid());
  // Make sure FBO has not been initialized
  assert(fbo_.isCreated());

  fbo_.bind();
  {
    // Actual painting happens here
    paintGL();
  }
  fbo_.release();
}

void OffScreenOpenGLSurface::paintGL() {
  preDraw();
  draw();
  postDraw();
}

void OffScreenOpenGLSurface::preDraw() {
  glfuncs_->glViewport(viewport_x_, viewport_y_, viewport_width_, viewport_height_);
  glfuncs_->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

QImage OffScreenOpenGLSurface::readColorBuffer() {
  QImage image(size(), QImage::Format_RGBA8888);
  readFrameBuffer(GL_COLOR_ATTACHMENT0, GL_RGBA, image.bits());
  return image.mirrored();
}

void OffScreenOpenGLSurface::readColorBuffer(GLenum type, GLvoid* pixel_data) {
  readFrameBuffer(GL_COLOR_ATTACHMENT0, GL_RGBA, type, pixel_data);
}

void OffScreenOpenGLSurface::readColorBuffer(GLenum type, GLuint pbo) {
  readFrameBuffer(GL_COLOR_ATTACHMENT0, GL_RGBA, type, pbo);
}

void OffScreenOpenGLSurface::readDepthBuffer(GLenum type, GLvoid* pixel_data) {
  readFrameBuffer(GL_COLOR_ATTACHMENT1, GL_RED, type, pixel_data);
}

void OffScreenOpenGLSurface::readDepthBuffer(GLenum type, GLuint pbo) {
  readFrameBuffer(GL_COLOR_ATTACHMENT1, GL_RED, type, pbo);
}

void OffScreenOpenGLSurface::readNormalBuffer(GLenum type, GLvoid* pixel_data) {
  readFrameBuffer(GL_COLOR_ATTACHMENT2, GL_RGB, type, pixel_data);
}

void OffScreenOpenGLSurface::readNormalBuffer(GLenum type, GLuint pbo) {
  readFrameBuffer(GL_COLOR_ATTACHMENT2, GL_RGB, type, pbo);
}

QImage OffScreenOpenGLSurface::readNormalBuffer() {
  QImage image(size(), QImage::Format_RGB888);
  readFrameBuffer(GL_COLOR_ATTACHMENT2, GL_RGB, image.bits());
  return image.mirrored();
}

void OffScreenOpenGLSurface::readLabelBuffer(GLenum type, GLvoid* pixel_data) {
  readFrameBuffer(GL_COLOR_ATTACHMENT3, GL_RED, type, pixel_data);
}

void OffScreenOpenGLSurface::readLabelBuffer(GLenum type, GLuint pbo) {
  readFrameBuffer(GL_COLOR_ATTACHMENT3, GL_RED, type, pbo);
}

void OffScreenOpenGLSurface::readZBuffer(GLenum type, GLvoid* pixel_data) {
  readFrameBuffer(GL_DEPTH_ATTACHMENT, GL_DEPTH_COMPONENT, type, pixel_data);
}

void OffScreenOpenGLSurface::readFrameBuffer(GLenum attachment, GLenum format, GLenum type, GLvoid* pixel_data) {
  // Make sure to not call render() without a valid surface!
  assert(isValid());
  // Make sure FBO has not been initialized
  assert(fbo_.isCreated());

  glfuncs_->glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo_.handle());
  glfuncs_->glReadBuffer(attachment);
  glfuncs_->glReadPixels(0, 0, size().width(), size().height(), format, type, pixel_data);
}

void OffScreenOpenGLSurface::readFrameBuffer(GLenum attachment, GLenum format, GLenum type, GLuint pbo) {
  // Make sure to not call render() without a valid surface!
  assert(isValid());
  // Make sure FBO has not been initialized
  assert(fbo_.isCreated());

  glfuncs_->glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo_.handle());
  glfuncs_->glReadBuffer(attachment);
  glfuncs_->glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo);
  glfuncs_->glReadPixels(0, 0, size().width(), size().height(), format, type, 0);
  glfuncs_->glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
}


void OffScreenOpenGLSurface::swapBuffers() {
  // blit framebuffer to back buffer
  makeCurrent();

  // make sure all paint operation have been processed
  glfuncs_->glFlush();

  glfuncs_->glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo_.handle());
  glfuncs_->glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  // blit all buffers including depth buffer for further rendering
  glfuncs_->glBlitFramebuffer(0, 0, size().width(), size().height(),
                              0, 0, size().width(), size().height(),
                              GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
  glfuncs_->glBindFramebuffer(GL_FRAMEBUFFER, fbo_.handle());

  context()->swapBuffers(this);
  doneCurrent();
}

void OffScreenOpenGLSurface::setViewPort(GLint x, GLint y, GLsizei width, GLsizei height) {
  viewport_x_ = x;
  viewport_y_ = y;
  viewport_width_ = width;
  viewport_height_ = height;
}

}  // end namespace CuteGL
