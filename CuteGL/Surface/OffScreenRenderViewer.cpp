/**
 * @file OffScreenRenderViewer.cpp
 * @brief OffScreenRenderViewer
 *
 * @author Abhijit Kundu
 */

#include "OffScreenRenderViewer.h"
#include "CuteGL/Renderer/AbstractRenderer.h"
#include "CuteGL/Core/PoseUtils.h"

namespace CuteGL {

OffScreenRenderViewer::OffScreenRenderViewer(AbstractRenderer* renderer)
  : renderer_(renderer),
    camera_() {
}

void OffScreenRenderViewer::init() {
  if(!isValid())
    throw std::runtime_error("OffScreenRenderViewer::init(): Cannot call init() without a valid surface!");


  renderer_->setContext(context());
  renderer_->setOpenGLFunctions(glFuncs());
  renderer_->initialize();
}

void OffScreenRenderViewer::draw() {
  renderer_->draw(camera().intrinsics(), camera().extrinsics());
}

void OffScreenRenderViewer::setCameraToLookAt(const Eigen::Ref<const Eigen::Vector3f>& eye,
                                             const Eigen::Ref<const Eigen::Vector3f>& target,
                                             const Eigen::Ref<const Eigen::Vector3f>& up_vector) {
  camera_.extrinsics() = getExtrinsicsFromLookAt(eye,target,up_vector);
}

void OffScreenRenderViewer::resizeGL(int width, int height) {
  OffScreenOpenGLSurface::resizeGL(width, height);
  camera_.resize(width, height);
}

}  // end namespace CuteGL
