/**
 * @file AbstractOffScreenOpenGLSurface.h
 * @brief AbstractOffScreenOpenGLSurface header
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_ABSTRACTOFFSCREENOPENGLSURFACE_H_
#define CUTEGL_ABSTRACTOFFSCREENOPENGLSURFACE_H_

#include <QOffscreenSurface>

// Forward declarations
class QOpenGLContext;

namespace CuteGL {

class OpenGLFunctions;

/**@brief OffScreen OpenGL Surface
 *
 * Simple wrapper over QOffscreenSurface for OpenGL rendering
 *
 */
class AbstractOffScreenOpenGLSurface : public QOffscreenSurface {
  Q_OBJECT

 public:

  /**@brief Primary constructor creates OffScreen OpenGL Surface for target_screen
   *
   * @note It is not usable until create() is called.
   *
   * @param target_screen
   * @param size
   */
  explicit AbstractOffScreenOpenGLSurface(QScreen* target_screen = nullptr, const QSize& size = QSize(1, 1));

  /// @brief Destructor.
  ~AbstractOffScreenOpenGLSurface();

  /**@brief primary initialize function
   *
   * 1. Allocates platform resources associated with the offscreen surface
   * 2. Initializes OpenGL context
   * 3. It is at this point that the surface format set using setFormat() gets
   *    resolved into an actual native surface.
   * 4. Calls initializeGL()
   *
   * @note Some platforms require this function to be called on the main (GUI) thread.
   *
   */
  void create();


  /// Returns true if the surface and the opengl context has been created
  /// This is true after create() has been called
  bool isValid() const;


  /// Returns the OpenGL context used for this surface
  QOpenGLContext* context() const;

  /// convenience function to print the context information
  void printContextInformation();

  /// Access to the OpenGL functions
  OpenGLFunctions* glFuncs() const {return glfuncs_;}

  /// Get the current size of the surface
  QSize size() const;

  /// Get width of the surface
  int width() const;

  /// Get height of the surface
  int height() const;

  /**@brief resize the surface with new_size
   *
   * @param new_size new size of the surface
   */
  void resize(const QSize& new_size);

  /**@brief Resize surface to size with width w and height h.
   *
   * @param w Width
   * @param h Height
   */
  void resize(int w, int h);


  /**@brief Makes the OpenGL context current for rendering.
   *
   * Prepares for rendering OpenGL content for this window by making the
   * corresponding context current and binding the framebuffer object, if
   * there is one, in that context context.
   */
  void makeCurrent();

  /// Release the OpenGL context.
  void doneCurrent();

 protected:

  /**@brief function for application specific initialization
   *
   * This function is called at the end of the create() function
   * Re-implement it in a subclass
   *
   */
  virtual void initializeGL() = 0;

  /**@brief function to handle resize event
   *
   * This virtual function is called whenever the widget has been resized.
   * Re-implement it in a subclass. The new size is passed in w and h
   *
   * @param width
   * @param height
   */
  virtual void resizeGL(int width, int height) = 0;

  /// OpenGL context
  QOpenGLContext* context_;

  /// OpenGL function resolver
  OpenGLFunctions* glfuncs_;

  /// Current size of the surface
  QSize size_;

 private:
  void initializeOpenGLFunctions();

};

}  // end namespace CuteGL
#endif // end CUTEGL_ABSTRACTOFFSCREENOPENGLSURFACE_H_
