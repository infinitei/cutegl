/**
 * @file CameraManipulator.cpp
 * @brief CameraManipulator
 *
 * @author Abhijit Kundu
 */

#define _USE_MATH_DEFINES

#include "CameraManipulator.h"
#include "CuteGL/Core/Camera.h"
#include <QWheelEvent>
#include <QMouseEvent>
#include <cmath>
#include <QVector3D>

namespace CuteGL {

CameraManipulator::CameraManipulator(Camera* camera)
    : camera_(camera),
      action_(Action::NO_ACTION),
      prev_mouse_pos_(),
      zoom_speed_coeff_(1.0f),
      scene_pivot_(Eigen::Vector3f::Zero()) {
}

void CameraManipulator::wheelEvent(QWheelEvent* ev) {
  zoom(ev->angleDelta().y() * 32E-4);
}

void CameraManipulator::mousePressEvent(QMouseEvent* ev) {
  // Save mouse pos
  prev_mouse_pos_.x() = ev->x(); prev_mouse_pos_.y() = ev->y();
  switch (ev->button()) {
    case Qt::LeftButton:
      action_ = Action::ROTATE_AROUND_PIVOT;
      break;
    case Qt::RightButton:
      action_ = Action::TRANSLATE;
      break;
    case Qt::MiddleButton:
      action_ = Action::ZOOM_IN_OUT;
      break;
    default:
      action_ = Action::NO_ACTION;
      break;
  }
}

Eigen::Vector3f getArcballVector(const Eigen::Vector2i& pt, int width, int height) {
  Eigen::Vector3f P(1.0 * pt.x() / width * 2 - 1.0, 1.0 * pt.y() / height * 2 - 1.0, 0);
  P.x() = -P.x();
  P.y() = -P.y();

  float OP_squared = P.squaredNorm();
  if (OP_squared <= 1)
    P.z() = std::sqrt(1 - OP_squared);  // Pythagorous
  else
    P.normalize();  // nearest point
  return P;
}

void CameraManipulator::mouseMoveEvent(QMouseEvent* ev) {
  switch (action_) {
    case Action::NO_ACTION: {
      break;
    }
    case Action::ROTATE_AROUND_PIVOT: {
      Eigen::Vector2i current_mouse_pos(ev->x(), ev->y());

      //rotate using an arcBall vectors
      Eigen::Vector3f vec1 = getArcballVector(prev_mouse_pos_, camera_->imageWidth(), camera_->imageHeight());
      Eigen::Vector3f vec2 = getArcballVector(current_mouse_pos, camera_->imageWidth(), camera_->imageHeight());

      // Convert the arc ball vectors to world frame
      const Eigen::Matrix3f inv_rot = camera_->extrinsics().rotation().inverse();
      vec1 = inv_rot * vec1;
      vec2 = inv_rot * vec2;

      // Compute rotation in world frame
      Eigen::Quaternionf quat = Eigen::Quaternionf::FromTwoVectors(vec1, vec2);
      quat.normalize();

      // Apply world frame rotation
      camera_->extrinsics() = camera_->extrinsics() * quat;
      break;
    }
    case Action::TRANSLATE: {
      Eigen::Vector3f rel_translation(ev->x() - prev_mouse_pos_.x(), ev->y() - prev_mouse_pos_.y(), 0.0);
      Eigen::Vector3f pivot_in_cam_frame = camera_->extrinsics() * scenePivot();
      rel_translation *= 2.0 * std::tan(22.5f * M_PI / 180.0f) * std::abs(pivot_in_cam_frame.z()) / camera_->imageHeight();
      camera_->extrinsics().pretranslate(rel_translation);
      break;
    }
    case Action::ZOOM_IN_OUT: {
      float dx = (ev->x() - prev_mouse_pos_.x()) / 800.0f;
      float dy = (ev->y() - prev_mouse_pos_.y()) / 600.0f;
      float delta = std::abs(dx) > std::abs(dy) ? dx : dy;
      zoom(delta);
      break;
    }
  }

  // Save mouse pos
  prev_mouse_pos_.x() = ev->x(); prev_mouse_pos_.y() = ev->y();
}

void CameraManipulator::mouseReleaseEvent(QMouseEvent* ev) {
  action_ = Action::NO_ACTION;
  // Save mouse pos (TODO is this needed?)
  prev_mouse_pos_.x() = ev->x(); prev_mouse_pos_.y() = ev->y();
}

void CameraManipulator::zoom(float delta) {
  camera_->extrinsics().pretranslate(Eigen::Vector3f(0.0f, 0.0f, -0.2 * zoom_speed_coeff_ * delta));
}

void CameraManipulator::translateCameraToFitSphere(
    const Eigen::Vector3f& center, const float radius) {

  const float distance = (radius / (camera_->fieldOfView().array() / 2.0f).sin()).maxCoeff();
  Eigen::Isometry3f camera_pose = camera_->extrinsics().inverse();
  const Eigen::Vector3f view_direction = camera_pose.rotation() * Eigen::Vector3f::UnitZ();

  camera_pose.translation() = center - distance * view_direction;
  camera_->extrinsics() = camera_pose.inverse();
}


}  // end namespace CuteGL
