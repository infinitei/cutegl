/**
 * @file WindowOpenGLSurface.h
 * @brief WindowOpenGLSurface
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_SURFACE_WINDOW_H_
#define CUTEGL_SURFACE_WINDOW_H_

#include "CuteGL/Core/OpenGLFunctions.h"
#include "CuteGL/Core/Camera.h"
#include "CuteGL/Surface/CameraManipulator.h"
#include "CuteGL/Surface/KeyboardHandler.h"
#include <QOpenGLWindow>
#include <Eigen/Geometry>
#include <QOpenGLDebugMessage>
#include <QTime>
#include <memory>

class QOpenGLDebugLogger;

namespace CuteGL {

class WindowOpenGLSurface : public QOpenGLWindow {
  Q_OBJECT

 public:
  WindowOpenGLSurface();
  ~WindowOpenGLSurface();

  /// Calls show and waits till the window is exposed
  void showAndWaitTillExposed();

  void setBgFgColor(const QColor& bg_color, const QColor& fg_color);
  void setBackgroundColor(unsigned char r, unsigned char g, unsigned char b);
  void setForegroundColor(unsigned char r, unsigned char g, unsigned char b);
  const QColor& foregroundColor() const {return fg_color_;}
  const QColor& backgroundColor() const {return bg_color_;}


  /// Toggle Polygon mode to wireframe of fill
  void toggleWireframeMode();
  void setWireframeMode(bool enable = true);

  /// set FPS display flag
  void setDisplayFPS(bool enable) {display_fps_ = enable;}

  Camera& camera() {return camera_;} ///< non const reference to camera
  const Camera& camera() const {return camera_;} ///< const reference to camera

  CameraManipulator& cameraManipulator() {return camera_manipuator_;} ///< non const reference to camera_manipuator
  const CameraManipulator& cameraManipulator() const {return camera_manipuator_;} ///< const reference to camera_manipuator

  KeyboardHandler& keyboardHandler() {return keyboard_handler_;} ///< non const reference to keyboard_handler
  const KeyboardHandler& keyboardHandler() const {return keyboard_handler_;} ///< const reference to keyboard_handler

  /// Convenience function to set opengl viewport
  void setViewPort(GLint x, GLint y, GLsizei width, GLsizei height);

 protected:

  /**@brief Overload this function for additional custom initialization
   *
   * This is called at the end of initializeGL(). We expect the users to
   * overload this function (instead of initializeGL()) and modify the default behavior.
   */
  virtual void init() {}



  /**@brief Used for pre drawing activities like clearing the color and depth buffer
   *
   */
  virtual void preDraw();


  /**@brief Overload this function for your drawing needs
   *
   * This is called by paintGL()
   * Users are expected to overload this function (instead of paintGL()) and modify the default behavior.
   */
  virtual void draw() {}


  /**@brief used for post draw activities like text rendering
   *
   */
  virtual void postDraw();

  /**@brief primary paint function which calls preDraw(), draw() and postDraw() in that order.
   * This is what gets called by an update function.
   * This will call preDraw(), draw() and postDraw().
   * If you want to completely change this, you can overload
   * this function, but otherwise consider overloading the
   * appropriate draw function.
   */
  virtual void paintGL();


  virtual void resizeGL(int w, int h);

  virtual void initializeGL();

  /**@brief performs a particular keyboard action
   *
   * This is called by keyPressEvent().
   *
   * @param action one of KeyboardHandler::Action enum values
   */
  virtual void handleKeyboardAction(int action);

  /**@brief Handles keyboard events
   *
   * Overloads the keyPressEvent of the QOpenGLWindow.
   * Based on the key-action registration this calls the handleKeyboardAction()
   * with appropriate Action.
   *
   * @param event
   */
  virtual void keyPressEvent(QKeyEvent * ev);


  virtual void wheelEvent(QWheelEvent * ev);

  virtual void mousePressEvent(QMouseEvent* ev);
  virtual void mouseMoveEvent(QMouseEvent* ev);
  virtual void mouseReleaseEvent(QMouseEvent* ev);

  /// Prints the context information
  void printContextInformation();

  /// Access to the OpenGL functions
  OpenGLFunctions* glFuncs() const { return glfuncs_; }

 protected slots:
  void onMessageLogged(const QOpenGLDebugMessage& message);

 private:
  void setupAnimation();

 protected:
  OpenGLFunctions* glfuncs_;
  Camera camera_;
  CameraManipulator camera_manipuator_;
  KeyboardHandler keyboard_handler_;


 private:
  bool animate_;

  QColor bg_color_; //!< background color
  QColor fg_color_; //!< foreground color
  QOpenGLDebugLogger * logger_; //!< debug logger

  // Viewport params
  GLint viewport_x_;
  GLint viewport_y_;
  GLsizei viewport_width_;
  GLsizei viewport_height_;

  // FPS stuff
  bool display_fps_;  //!< FPS display flag
  unsigned int fps_counter_;  //!< counts the # of frames passed since last fps computation
  double fps_;  //!< Actual fps value
  QTime fps_timer_;   //!< FPS timer

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}  // end namespace CuteGL

#endif // end CUTEGL_SURFACE_WINDOW_H_
