/**
 * @file OffScreenOpenGLSurface.h
 * @brief OffScreenOpenGLSurface header
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_OFFSCREENOPENGLSURFACE_H_
#define CUTEGL_OFFSCREENOPENGLSURFACE_H_

#include "CuteGL/Surface/AbstractOffScreenOpenGLSurface.h"
#include "CuteGL/Core/FrameBufferObject.h"
#include "CuteGL/Utils/OpenGLUtils.h"
#include <QImage>
#include <QColor>

namespace CuteGL {

class OffScreenOpenGLSurface : public AbstractOffScreenOpenGLSurface {
 public:
  OffScreenOpenGLSurface(QScreen* target_screen = nullptr, const QSize& size = QSize(1, 1));

  /**@brief Render to frame-buffer
   *
   * This is primary render function needed to be called before
   * grabbing the frame buffer. This calls the paintGL() which is
   * expected to be re-implemented by user.
   *
   */
  void render();

  /// Reads frame-buffer by calling readPixels (blocking)
  template <typename T>
  void readFrameBuffer(GLenum attachment, GLenum format, T* pixel_data) {
    readFrameBuffer(attachment, format, GLTraits<T>::type, pixel_data);
  }
  /// Reads frame-buffer by calling readPixels (blocking)
  void readFrameBuffer(GLenum attachment, GLenum format, GLenum type, GLvoid* pixel_data);

  /// Reads frame-buffer to PBO (non blocking)
  void readFrameBuffer(GLenum attachment, GLenum format, GLenum type, GLuint pbo);

  /// Grab color image buffer
  QImage readColorBuffer();
  template<typename T>
  void readColorBuffer(T* pixel_data) {readColorBuffer(GLTraits<T>::type, pixel_data);}
  void readColorBuffer(GLenum type, GLvoid* pixel_data);
  void readColorBuffer(GLenum type, GLuint pbo);

  /// Grab depth image buffer
  template<typename T>
  void readDepthBuffer(T* pixel_data) {readDepthBuffer(GLTraits<T>::type, pixel_data);}
  void readDepthBuffer(GLenum type, GLvoid* pixel_data);
  void readDepthBuffer(GLenum type, GLuint pbo);

  /// Read Normal buffer
  template<typename T>
  void readNormalBuffer(T* pixel_data) {readNormalBuffer(GLTraits<T>::type, pixel_data);}
  void readNormalBuffer(GLenum type, GLvoid* pixel_data);
  void readNormalBuffer(GLenum type, GLuint pbo);
  QImage readNormalBuffer();

  /// Grab Label image buffer
  template<typename T>
  void readLabelBuffer(T* pixel_data) {readLabelBuffer(GLTraits<T>::type, pixel_data);}
  void readLabelBuffer(GLenum type, GLvoid* pixel_data);
  void readLabelBuffer(GLenum type, GLuint pbo);

  /// Grab z-buffer (GL_DEPTH_COMPONENT)
  template<typename T>
  void readZBuffer(T* pixel_data) {readZBuffer(GLTraits<T>::type, pixel_data);}
  void readZBuffer(GLenum type, GLvoid* pixel_data);

  /// swap buffers
  void swapBuffers();


  /**@brief Sets the background color
   *
   * @param bg_color
   */
  void setBackgroundColor(const QColor& bg_color);

  /**@brief Sets the background color
   *
   * @param r red
   * @param g green
   * @param b blue
   * @param a alpha
   */
  void setBackgroundColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);

  /// query the current background color
  const QColor& backgroundColor() const {return bg_color_;}

  /// Return the FBO
  FrameBufferObject& fbo() {return fbo_;}
  const FrameBufferObject& fbo() const {return fbo_;}

  /// Convenience function to set opengl viewport
  void setViewPort(GLint x, GLint y, GLsizei width, GLsizei height);

  /// Recreates the FBO and framaebuffers
  void recreateFBO();

 protected:

  /**@brief Overload this function for additional custom initialization
   *
   * This is called at the end of initializeGL(). We expect the users to
   * overload this function (instead of initializeGL()) and modify the default behavior.
   */
  virtual void init() {}

  /**@brief Used for pre drawing activities like clearing the color and depth buffer
   *
   * This is called by paintGL() before calling draw()
   *
   */
  virtual void preDraw();


  /**@brief Main draw command.
   *
   * Overload this function for your drawing needs
   * This is called by paintGL()
   * Users are expected to overload this function (instead of paintGL()) and modify the default behavior.
   */
  virtual void draw() {}


  /**@brief used for post draw activities like text rendering
   *
   */
  virtual void postDraw() {}

  /**@brief primary paint function which calls preDraw(), draw() and postDraw() in that order.
   * This is what gets called by the public render function.
   * This will call preDraw(), draw() and postDraw().
   * If you want to completely change this, you can overload
   * this function, but otherwise consider overloading the
   * appropriate draw function.
   */
  virtual void paintGL();


  /**Initializes the FBO and calls init()
   *
   * User should prefer to re-implement init() in a subclass, instead of
   * initializeGL() unless a complete different behaviour is desired
   *
   */
  virtual void initializeGL();


  /**@brief resize function called by resize event
   *
   * @param width
   * @param height
   */
  virtual void resizeGL(int width, int height);

 private:
  FrameBufferObject fbo_; //<! frame buffer object

  QColor bg_color_; //!< background color

  // Viewport params
  GLint viewport_x_;
  GLint viewport_y_;
  GLsizei viewport_width_;
  GLsizei viewport_height_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_OFFSCREENOPENGLSURFACE_H_
