/**
 * @file KeyboardHandler.h
 * @brief KeyboardHandler
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_SURFACE_KEYBOARDHANDLER_H_
#define CUTEGL_SURFACE_KEYBOARDHANDLER_H_

#include <unordered_map>
#include <map>

namespace CuteGL {

class KeyboardHandler {
 public:
  KeyboardHandler();

  enum PredefinedActions {
    UNHANDLED_ACTION = -1,
    EXIT_VIEWER = 0,
    DISPLAY_FPS = 1,
    TOGGLE_WIREFRAME = 2,
    PAUSE_ANIMATION = 3,
  };

  /**@brief Registers a key with a particular action and action description
   *
   *
   * @param action      - See Action enums
   * @param key         - Value of the key (See Qt::Key)
   * @param description - Some Description string
   */
  void registerKey(int action, int key, const std::string& description);

  /**@brief Registers a key with a particular action
   *
   * @param action
   * @param key
   */
  void registerKey(int action, int key);

  /**@brief get the corresponding Action given a certain key
   *
   * Returns UNHANDLED_ACTION if key has not been registered
   *
   * @param key
   * @return result corresponding Action registered with key and UNHANDLED_ACTION if key not registered
   */
  int findAction(int key) const;

  /// Clears all key binding registrations
  void clear();

 private:
  typedef std::unordered_map<int, int> KeyToActionContainer;
  typedef std::map<int, std::string> ActionToStringContainer;  // TODO: Replace this with std::unordered_map in c++14

  KeyToActionContainer key_bindings_;  //!< Maps a key to a particular action
  ActionToStringContainer action_descriptions_; //!< Maps an action to its description string
};

}  // end namespace CuteGL
#endif // end CUTEGL_SURFACE_KEYBOARDHANDLER_H_
