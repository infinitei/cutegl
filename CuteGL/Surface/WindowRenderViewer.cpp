/**
 * @file WindowRenderViewer.cpp
 * @brief WindowRenderViewer
 *
 * @author Abhijit Kundu
 */

#include "WindowRenderViewer.h"
#include "CuteGL/Renderer/AbstractRenderer.h"
#include "CuteGL/Core/PoseUtils.h"

namespace CuteGL {

WindowRenderViewer::WindowRenderViewer(AbstractRenderer* renderer)
    : renderer_(renderer),
      fixed_camera_frustum_planes_(false),
      scene_radius_(1.0f),
      scene_pivot_(Eigen::Vector3f::Zero()),
      z_clip_coeff_(std::sqrt(3.0f)),
      z_near_coeff_(0.005f) {

  keyboardHandler().registerKey(DISPLAY_GRID, Qt::Key_G, "Toggles Grid display");
  keyboardHandler().registerKey(DISPLAY_AXIS, Qt::Key_A, "Toggles Axis display");
  keyboardHandler().registerKey(SHOW_ENTIRE_SCENE, Qt::Key_Space, "Resets the viewer to show entire scene");
}

void WindowRenderViewer::init() {
  renderer_->setContext(context());
  renderer_->setOpenGLFunctions(glFuncs());
  renderer_->initialize();
}

void WindowRenderViewer::draw() {
  renderer_->draw(camera().intrinsics(), camera().extrinsics());
}

void WindowRenderViewer::setScenePivot(const Eigen::Vector3f& scene_pivot) {
  scene_pivot_ = scene_pivot;
  camera_manipuator_.scenePivot() = scene_pivot_;
}

void WindowRenderViewer::setSceneRadius(const float scene_radius) {
  scene_radius_ = scene_radius;
  camera_manipuator_.zoomSpeedCoeff() = 0.2f * scene_radius_;
}

void WindowRenderViewer::handleKeyboardAction(int action) {
  switch (action) {
    case DISPLAY_GRID:
      renderer_->setDisplayGrid(!renderer_->isGridDisplayed());
      break;
    case DISPLAY_AXIS:
      renderer_->setDisplayAxis(!renderer_->isAxisDisplayed());
      break;
    case SHOW_ENTIRE_SCENE:
      showEntireScene();
      break;
    default:
      WindowOpenGLSurface::handleKeyboardAction(action);
      break;
  }
}

void WindowRenderViewer::wheelEvent(QWheelEvent* ev) {
  camera_manipuator_.wheelEvent(ev);
  updateCameraFrustumPlanes();
}

void WindowRenderViewer::mousePressEvent(QMouseEvent* ev) {
  camera_manipuator_.mousePressEvent(ev);
  if(camera_manipuator_.action() != CameraManipulator::Action::ZOOM_IN_OUT)
    updateCameraFrustumPlanes();
}

void WindowRenderViewer::mouseMoveEvent(QMouseEvent* ev) {
  camera_manipuator_.mouseMoveEvent(ev);
  if(camera_manipuator_.action() == CameraManipulator::Action::ZOOM_IN_OUT)
    updateCameraFrustumPlanes();
}

void WindowRenderViewer::mouseReleaseEvent(QMouseEvent* ev) {
  camera_manipuator_.mouseReleaseEvent(ev);
  if(camera_manipuator_.action() != CameraManipulator::Action::ZOOM_IN_OUT)
    updateCameraFrustumPlanes();
}

void WindowRenderViewer::setCameraToLookAt(const Eigen::Ref<const Eigen::Vector3f>& eye,
                                    const Eigen::Ref<const Eigen::Vector3f>& target,
                                    const Eigen::Ref<const Eigen::Vector3f>& up_vector) {
  camera_.extrinsics() = getExtrinsicsFromLookAt(eye,target,up_vector);
}

void WindowRenderViewer::showEntireScene() {
  cameraManipulator().translateCameraToFitSphere(scene_pivot_, scene_radius_);
  updateCameraFrustumPlanes();
}

void WindowRenderViewer::updateCameraFrustumPlanes() {
  if(!fixed_camera_frustum_planes_) {
    const float z_near = nearFrustumPlane();
    const float z_far = farFrustumPlane();
    assert(z_far > z_near);
    camera_.setFrustumNearFarPlanes(z_near, z_far);
  }
}

void WindowRenderViewer::setFixedCameraFrustumPlanes(const bool enable) {
  fixed_camera_frustum_planes_ = enable;
}

float CuteGL::WindowRenderViewer::nearFrustumPlane() const {
  Eigen::Vector3f scene_pivot_eye = camera_.extrinsics() * scene_pivot_;
  float zdist_from_scene_pivot = std::abs(scene_pivot_eye.z());

  const float z_near_scene = z_clip_coeff_ * scene_radius_;
  return std::max(zdist_from_scene_pivot - z_near_scene, z_near_coeff_ * z_near_scene);
}

float CuteGL::WindowRenderViewer::farFrustumPlane() const {
  Eigen::Vector3f scene_pivot_eye = camera_.extrinsics() * scene_pivot_;
  float zdist_from_scene_pivot = std::abs(scene_pivot_eye.z());
  return zdist_from_scene_pivot + z_clip_coeff_ * scene_radius_;
}

}  // end namespace CuteGL
