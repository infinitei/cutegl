/**
 * @file CameraManipulator.h
 * @brief CameraManipulator
 *
 * @author Abhijit Kundu
 */

#ifndef CAMERAMANIPULATOR_H_
#define CAMERAMANIPULATOR_H_

#include <Eigen/Geometry>

class QWheelEvent;
class QMouseEvent;

namespace CuteGL {

class Camera;

class CameraManipulator {
 public:
  CameraManipulator(Camera* camera);

  enum class Action {
    NO_ACTION,
    ROTATE_AROUND_PIVOT,
    TRANSLATE,
    ZOOM_IN_OUT,
  };

  /// Returns the current action
  inline Action action() const {return action_;}

  void mousePressEvent(QMouseEvent* ev);
  void mouseMoveEvent(QMouseEvent* ev);
  void mouseReleaseEvent(QMouseEvent* ev);

  void wheelEvent(QWheelEvent * ev);


  void translateCameraToFitSphere(const Eigen::Vector3f& center, const float radius);


  float& zoomSpeedCoeff() {return zoom_speed_coeff_;}
  float zoomSpeedCoeff() const {return zoom_speed_coeff_;}

  /// const reference scene pivot point
  const Eigen::Vector3f& scenePivot() const {return scene_pivot_;}
  /// reference to scene pivot point
  Eigen::Vector3f& scenePivot() {return scene_pivot_;}

 private:
  void zoom(float wheel_delta);


  Camera* camera_;
  Action action_;
  Eigen::Vector2i prev_mouse_pos_;
  float zoom_speed_coeff_;
  Eigen::Vector3f scene_pivot_;
};

}  // end namespace CuteGL
#endif // end CAMERAMANIPULATOR_H_
