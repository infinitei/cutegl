/**
 * @file WindowOpenGLSurface.cpp
 * @brief WindowOpenGLSurface
 *
 * @author Abhijit Kundu
 */

#include "WindowOpenGLSurface.h"
#include <QDebug>
#include <QString>
#include <QKeyEvent>
#include <QPainter>
#include <QCoreApplication>

namespace CuteGL {

WindowOpenGLSurface::WindowOpenGLSurface()
    : glfuncs_(nullptr),
      camera_(),
      camera_manipuator_(&camera_),
      keyboard_handler_(),
      animate_(true),
      bg_color_(55, 55, 55),
      fg_color_(Qt::lightGray),
      logger_(new QOpenGLDebugLogger(this)),
      display_fps_(false),
      fps_counter_(0),
      fps_(-1) {

  resize(camera_.imageWidth(), camera_.imageHeight());
  setViewPort(0, 0, camera_.imageWidth(), camera_.imageHeight());

  // Set the default format to Core Desktop OpenGL
  // Redefine it in your calling main function or in your subclass
  QSurfaceFormat format;
  format.setRenderableType(QSurfaceFormat::OpenGL);
  format.setProfile(QSurfaceFormat::CoreProfile);
  setFormat(format);

  // Make QOpenGLDebugLogger log to standard console
  connect(logger_, SIGNAL(messageLogged( QOpenGLDebugMessage )), this,
          SLOT(onMessageLogged( QOpenGLDebugMessage )), Qt::DirectConnection);

  fps_timer_.start();
}

WindowOpenGLSurface::~WindowOpenGLSurface() {
  makeCurrent();
  delete logger_;
  doneCurrent();
}

void WindowOpenGLSurface::showAndWaitTillExposed() {
 show();
 while (! isExposed()) {
   QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
 }
}

void WindowOpenGLSurface::toggleWireframeMode() {
  GLint polygonMode[2];
  glfuncs_->glGetIntegerv(GL_POLYGON_MODE, polygonMode);

  if (polygonMode[0] == GL_FILL)
    glfuncs_->glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  else
    glfuncs_->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  update();
}

void WindowOpenGLSurface::setWireframeMode(bool enable) {
  if (enable) {
    glfuncs_->glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  } else {
    glfuncs_->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  }
  update();
}

void WindowOpenGLSurface::initializeGL() {
  // Setup animation (defaults to true)
  setupAnimation();

  if (logger_->initialize()) {
    logger_->startLogging(QOpenGLDebugLogger::SynchronousLogging);
    logger_->enableMessages();
    logger_->disableMessages(QOpenGLDebugMessage::AnySource,
                             QOpenGLDebugMessage::AnyType,
                             QOpenGLDebugMessage::NotificationSeverity);
  }

  // initialize OpenGL Functions
  {
    glfuncs_ = context()->versionFunctions<OpenGLFunctions>();
    if (!glfuncs_) {
      throw std::runtime_error("Could not obtain required OpenGL context version");
    }
    glfuncs_->initializeOpenGLFunctions();
  }

  // set clear background color
  glfuncs_->glClearColor(bg_color_.redF(), bg_color_.greenF(), bg_color_.blueF(), bg_color_.alphaF());

  // Enable Depth test
  glfuncs_->glEnable(GL_DEPTH_TEST);

  init();
}

void WindowOpenGLSurface::resizeGL(int width, int height) {
  camera().resize(width, height);
  setViewPort(0, 0, width, height);
}

void WindowOpenGLSurface::paintGL() {
  preDraw();
  draw();
  postDraw();
}

void WindowOpenGLSurface::preDraw() {
  glViewport(  viewport_x_, viewport_y_, viewport_width_, viewport_height_);
  // Clear the colorbuffer and depthbuffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

}

void WindowOpenGLSurface::postDraw() {
  if(display_fps_) {
    glfuncs_->glDisable(GL_DEPTH_TEST);
    const unsigned int max_fps_counter = 20;
    if (++fps_counter_ == max_fps_counter) {
      fps_ = 1000.0 * max_fps_counter / fps_timer_.restart();
      fps_counter_ = 0;
    }

    QPainter p(this);
    p.setPen(fg_color_);
    p.drawText(20, 20, QString("FPS: %1").arg(fps_, 0, 'f', 2));
    p.end();
    glfuncs_->glEnable(GL_DEPTH_TEST);
  }
}


void WindowOpenGLSurface::keyPressEvent(QKeyEvent* ev) {
  int action = keyboard_handler_.findAction(ev->key() + ev->modifiers());
  if(action != KeyboardHandler::UNHANDLED_ACTION)
    handleKeyboardAction(action);
  else
    QOpenGLWindow::keyPressEvent(ev);
}

void WindowOpenGLSurface::handleKeyboardAction(int action) {
  switch (action) {
    case KeyboardHandler::EXIT_VIEWER:
      close();
      break;
    case KeyboardHandler::TOGGLE_WIREFRAME:
      toggleWireframeMode();
      break;
    case KeyboardHandler::DISPLAY_FPS:
      display_fps_ = !display_fps_;
      break;
    case KeyboardHandler::PAUSE_ANIMATION:
      animate_ = !animate_;
      setupAnimation();
      break;
    default:
      break;
  }
}

void WindowOpenGLSurface::wheelEvent(QWheelEvent* ev) {
  camera_manipuator_.wheelEvent(ev);
}

void WindowOpenGLSurface::mousePressEvent(QMouseEvent* ev) {
  camera_manipuator_.mousePressEvent(ev);
}

void WindowOpenGLSurface::mouseMoveEvent(QMouseEvent* ev) {
  camera_manipuator_.mouseMoveEvent(ev);
}

void WindowOpenGLSurface::mouseReleaseEvent(QMouseEvent* ev) {
  camera_manipuator_.mouseReleaseEvent(ev);
}


void WindowOpenGLSurface::setBgFgColor(const QColor& bg_color, const QColor& fg_color) {
  bg_color_ = bg_color;
  fg_color_ = fg_color;
}

void WindowOpenGLSurface::setBackgroundColor(unsigned char r, unsigned char g,
                                unsigned char b) {
  bg_color_ = QColor(r,g,b);
  if (isValid()) {
    makeCurrent();
    glfuncs_->glClearColor(bg_color_.redF(), bg_color_.greenF(), bg_color_.blueF(), bg_color_.alphaF());
    doneCurrent();
    update();
  }
}

void WindowOpenGLSurface::setForegroundColor(unsigned char r, unsigned char g,
                                unsigned char b) {
  fg_color_ = QColor(r,g,b);
}

void WindowOpenGLSurface::printContextInformation() {
  QString opengl_type((context()->isOpenGLES()) ? "OpenGL ES" : "OpenGL");
  QString opengl_version(reinterpret_cast<const char*>(glGetString(GL_VERSION)));

  // Get Profile Information
  QString opengl_profile;
  switch (format().profile()) {
    case QSurfaceFormat::NoProfile:
      opengl_profile = "None";
      break;
    case QSurfaceFormat::CoreProfile:
      opengl_profile = "Core";
      break;
    case QSurfaceFormat::CompatibilityProfile:
      opengl_profile = "Compatibility";
      break;
  }

  QString glsl_version(reinterpret_cast<const char*>(glGetString(GL_SHADING_LANGUAGE_VERSION)));

  qDebug() << qPrintable(opengl_type) << qPrintable(opengl_version) << "(" << qPrintable(opengl_profile) << ")"
           << " GLSL Version:" << qPrintable(glsl_version);
}

void WindowOpenGLSurface::onMessageLogged(const QOpenGLDebugMessage& message) {
  qDebug() << message;
}

void WindowOpenGLSurface::setupAnimation() {
  if (animate_) {
    // Animate continuously, throttled by the blocking swapBuffers() call the
    // QOpenGLWindow internally executes after each paint. Once that is done
    // (frameSwapped signal is emitted), we schedule a new update.
    connect(this, SIGNAL(frameSwapped()), this, SLOT(update()));
    update();
  } else {
    disconnect(this, SIGNAL(frameSwapped()), this, SLOT(update()));
  }
}

void WindowOpenGLSurface::setViewPort(GLint x, GLint y, GLsizei width, GLsizei height) {
  viewport_x_ = x;
  viewport_y_ = y;
  viewport_width_ = width;
  viewport_height_ = height;
}

}  // end namespace CuteGL
