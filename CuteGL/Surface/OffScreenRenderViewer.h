/**
 * @file OffScreenRenderViewer.h
 * @brief OffScreenRenderViewer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_OFFSCREENRENDERVIEWER_H_
#define CUTEGL_OFFSCREENRENDERVIEWER_H_

#include "CuteGL/Surface/OffScreenOpenGLSurface.h"
#include "CuteGL/Core/Camera.h"

namespace CuteGL {

class AbstractRenderer;

class OffScreenRenderViewer : public OffScreenOpenGLSurface {
 public:
  OffScreenRenderViewer(AbstractRenderer* renderer);

  void init();

  void draw();

  Camera& camera() {return camera_;} ///< non const reference to camera
  const Camera& camera() const {return camera_;} ///< const reference to camera

  /// Set camera extrinsics via lookAt positions
  void setCameraToLookAt(const Eigen::Ref<const Eigen::Vector3f>& eye,
                         const Eigen::Ref<const Eigen::Vector3f>& target,
                         const Eigen::Ref<const Eigen::Vector3f>& up_vector);

 protected:
  void resizeGL(int width, int height);

  AbstractRenderer* renderer_; // Non Owning ptr to the renderer
  Camera camera_;

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}  // end namespace CuteGL
#endif // end CUTEGL_OFFSCREENRENDERVIEWER_H_
