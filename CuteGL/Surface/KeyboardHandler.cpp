/**
 * @file KeyboardHandler.cpp
 * @brief KeyboardHandler
 *
 * @author Abhijit Kundu
 */

#include "KeyboardHandler.h"
#include <cassert>
#include <Qt>

namespace CuteGL {

KeyboardHandler::KeyboardHandler() {
  // Some default key registrations
  registerKey(EXIT_VIEWER, Qt::Key_Escape, "Exit the viewer");
  registerKey(DISPLAY_FPS, Qt::Key_F, "Toggles FPS display");
  registerKey(TOGGLE_WIREFRAME, Qt::Key_W, "Toggles Wireframe mode");
  registerKey(PAUSE_ANIMATION, Qt::Key_P, "Pause/Unpause Animation");
}

void KeyboardHandler::registerKey(int action, int key,
                                  const std::string& description) {
  assert(action != UNHANDLED_ACTION);
  key_bindings_[key] = action;
  action_descriptions_[action] = description;
}

void KeyboardHandler::registerKey(int action, int key) {
  assert(action != UNHANDLED_ACTION);
  key_bindings_[key] = action;

  // In C++17 we can simply do action_descriptions_.try_emplace(action, "");
  if(action_descriptions_.find(action) == action_descriptions_.end()) {
    action_descriptions_[action] = "";
  }
}

int KeyboardHandler::findAction(int key) const {
  KeyToActionContainer::const_iterator search = key_bindings_.find(key);
  if (search != key_bindings_.end())
    return search->second;
  else
    return UNHANDLED_ACTION;
}

void KeyboardHandler::clear() {
  key_bindings_.clear();
  action_descriptions_.clear();
}

}  // end namespace CuteGL
