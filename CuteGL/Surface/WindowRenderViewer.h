/**
 * @file WindowRenderViewer.h
 * @brief WindowRenderViewer header
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_SURFACE_WINDOWRENDERVIEWER_H_
#define CUTEGL_SURFACE_WINDOWRENDERVIEWER_H_

#include "CuteGL/Surface/WindowOpenGLSurface.h"

namespace CuteGL {

class AbstractRenderer;

/**@brief Window surface based viewer
 *
 * Displays the output of Renderer in a window based surface
 *
 */
class WindowRenderViewer : public WindowOpenGLSurface {
 public:

  /**@brief Create a window surface for viewing render targets
   *
   * Note that Viewer does not own the scene, since there can be multiple
   * viewers of the same scene.
   *
   * @param renderer ptr to renderer
   */
  WindowRenderViewer(AbstractRenderer* renderer);

  void init();

  void draw();

  enum Action {
    DISPLAY_GRID = 4,
    DISPLAY_AXIS = 5,
    SHOW_ENTIRE_SCENE = 6,
  };

  virtual void handleKeyboardAction(int action);

  virtual void wheelEvent(QWheelEvent * ev);
  virtual void mousePressEvent(QMouseEvent* ev);
  virtual void mouseMoveEvent(QMouseEvent* ev);
  virtual void mouseReleaseEvent(QMouseEvent* ev);

  /// Set camera extrinsics via lookAt positions
  void setCameraToLookAt(const Eigen::Ref<const Eigen::Vector3f>& eye,
                         const Eigen::Ref<const Eigen::Vector3f>& target,
                         const Eigen::Ref<const Eigen::Vector3f>& up_vector);

  /// Set Scene radius and pivot from bounding box extremities
  template <class EigenPointTypeA, class EigenPointTypeB>
  void setSceneBoundingBox(const EigenPointTypeA& min, const EigenPointTypeB& max) {
    setScenePivot((min + max).template cast<float>() / 2.0f);
    setSceneRadius(0.5f * (max-min).norm());
  }

  /// Set Scene radius and pivot from a bounding box object (e.g. Eigen::AlignedBox)
  template <class BoundingBoxType>
  void setSceneBoundingBox(const BoundingBoxType& bbx) {
    setSceneBoundingBox(bbx.min(), bbx.max());
  }

  /// const reference scene pivot point
  const Eigen::Vector3f& scenePivot() const {return scene_pivot_;}

  /// get scene radius
  float sceneRadius() const {return scene_radius_;}


  /// Set scene pivot point
  void setScenePivot(const Eigen::Vector3f& scene_pivot);

  /// Set scene radius
  void setSceneRadius(const float scene_radius);


  /// Translates the camera to show the entire scene (defined by scene pivot and radius)
  void showEntireScene();

  /// Update the Camera frustum and the projection matrix
  void updateCameraFrustumPlanes();

  /// If set to true, this disables updateCameraFrustumPlanes()
  void setFixedCameraFrustumPlanes(const bool enable);

  float nearFrustumPlane() const;
  float farFrustumPlane() const;

 protected:
  AbstractRenderer* renderer_; // Non Owning ptr to the renderer

  bool fixed_camera_frustum_planes_;

  float scene_radius_;
  Eigen::Vector3f scene_pivot_;

  float z_clip_coeff_;
  float z_near_coeff_;
};

}  // end namespace CuteGL

#endif // end CUTEGL_SURFACE_WINDOWRENDERVIEWER_H_
