/**
 * @file ComputeNormals.h
 * @brief ComputeNormals
 *
 * @author Abhijit Kundu
 */


#ifndef CUTEGL_GEOMETRY_COMPUTENORMALS_H_
#define CUTEGL_GEOMETRY_COMPUTENORMALS_H_

#include "CuteGL/Core/MeshData.h"
#include <vector>

namespace CuteGL {

/**@brief Recomputes the normals for each vertex
 *
 * The normals are stored in the vertices itself
 *
 * @param[in,out] vertices
 * @param[in] indices
 */
template <typename VertexData, typename IndexData>
void computeNormals(std::vector<VertexData>& vertices, const std::vector<IndexData>& indices);


/**@brief Recomputes the normals for each vertex
 *
 * The normals are stored in the vertices itself (no new vertices are created)
 *
 * @param[in,out] vertices
 * @param[in] faces
 */
template <typename VertexData, typename Derived>
void computeNormals(std::vector<VertexData>& vertices, const Eigen::DenseBase<Derived>& faces);



/**@brief Recomputes the normals for each vertex
 *
 * The normals are stored in the vertices itself (no new vertices are created)
 *
 * @param[in] positions
 * @param[in, out] normals
 * @param[in] faces
 */
template <typename DerivedP, typename DerivedN, typename DerivedF>
void computeNormals(const Eigen::DenseBase<DerivedP>& positions, const Eigen::DenseBase<DerivedN>& normals, const Eigen::DenseBase<DerivedF>& faces);


/**@brief Recomputes the normals for each vertex
 *
 * The normals are stored in the vertices itself. If two faces have normals with angles more than
 * angle_threshold, the vertices are duplicated.
 *
 *
 * @param[in,out] vertices
 * @param[in] faces
 */
template <typename VertexData, typename Derived, typename Scalar>
void computeNormals(std::vector<VertexData>& vertices, Eigen::DenseBase<Derived>& faces, const Scalar angle_threshold);



/**@brief Recomputes the normals for a mesh
 *
 * @param[in,out] mesh
 */
void computeNormals(MeshData& mesh);


/**@brief Recomputes the normals for a mesh
 *
 * @param[in,out] mesh
 */
void computeNormals(MeshData& mesh, const float angle_threshold);


/**@brief Recomputes the normals for a mesh
 *
 * @param[in,out] mesh
 */
template<class PS, class NS, class CS, class IS>
void computeNormals(Mesh<PS, NS, CS, IS>& mesh);


/**@brief Computes per-face (flat shaded) normals
 *
 * @param vertices
 * @param faces
 * @return flat shaded vertices
 */
template <typename VertexData, typename Derived>
std::vector<VertexData> createFlatShadedMeshVertices(const std::vector<VertexData>& vertices, const Eigen::DenseBase<Derived>& faces);

/**@brief Computes per-face (flat shaded) normals
 *
 * @param mesh
 * @return flat shaded vertices
 */
std::vector<MeshData::VertexData> createFlatShadedMeshVertices(const MeshData& mesh);


/**@brief Computes mesh with per-face (flat shaded) normals
 *
 * @param input mesh
 * @return mesh
 */
MeshData createFlatShadedMesh(const MeshData& mesh);

}  // namespace CuteGL

#include "CuteGL/Geometry/ComputeNormals-impl.hpp"

#endif // end CUTEGL_GEOMETRY_COMPUTENORMALS_H_
