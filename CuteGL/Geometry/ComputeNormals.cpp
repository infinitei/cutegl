/**
 * @file ComputeNormals.cpp
 * @brief ComputeNormals implementation
 *
 * @author Abhijit Kundu
 */

#include "ComputeNormals.h"

namespace CuteGL {

void computeNormals(MeshData& mesh) {
  computeNormals(mesh.vertices, mesh.faces);
}

void computeNormals(MeshData& mesh, const float angle_threshold) {
  computeNormals(mesh.vertices, mesh.faces, angle_threshold);
}

std::vector<MeshData::VertexData> createFlatShadedMeshVertices(const MeshData& mesh) {
  return createFlatShadedMeshVertices(mesh.vertices, mesh.faces);
}

MeshData createFlatShadedMesh(const MeshData& mesh) {
  MeshData flat_mesh;
  flat_mesh.vertices = createFlatShadedMeshVertices(mesh.vertices, mesh.faces);
  flat_mesh.faces = MeshData::FaceContainer::Zero(mesh.faces.rows(), 3);

  for (Eigen::Index face = 0; face < flat_mesh.faces.rows(); ++face) {
    flat_mesh.faces(face, 0) = 3 * face;
    flat_mesh.faces(face, 1) = 3 * face + 1;
    flat_mesh.faces(face, 2) = 3 * face + 2;
  }

  return flat_mesh;
}

}  // namespace CuteGL



