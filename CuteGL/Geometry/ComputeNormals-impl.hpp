/**
 * @file ComputeNormals-impl.hpp
 * @brief ComputeNormals-impl
 *
 * @author Abhijit Kundu
 */


#ifndef CUTEGL_GEOMETRY_COMPUTENORMALS_IMPL_HPP_
#define CUTEGL_GEOMETRY_COMPUTENORMALS_IMPL_HPP_

#include <Eigen/Geometry>

namespace CuteGL {

template <typename VertexData, typename IndexData>
void computeNormals(std::vector<VertexData>& vertices, const std::vector<IndexData>& indices) {
  // Assert that indices is actually an #faces X 3 matrix
  assert((indices.size() % 3) == 0);

  for (VertexData& vd : vertices) {
    vd.normal.setZero();
  }

  const std::size_t num_faces = indices.size() / 3;
  for (std::size_t face = 0;  face < num_faces; ++face) {
    const std::size_t vid = face * 3;

    VertexData& V0 = vertices[indices[vid]];
    VertexData& V1 = vertices[indices[vid+1]];
    VertexData& V2 = vertices[indices[vid+2]];

    // Compute Face normal
    using NormalType = typename VertexData::NormalType;
    const NormalType face_normal = ((V1.position-V0.position).cross(V2.position-V0.position)).normalized();
    V0.normal = (V0.normal + face_normal).normalized();
    V1.normal = (V1.normal + face_normal).normalized();
    V2.normal = (V2.normal + face_normal).normalized();
  }
}

template <typename VertexData, typename Derived>
void computeNormals(std::vector<VertexData>& vertices, const Eigen::DenseBase<Derived>& faces) {
  static_assert(Derived::ColsAtCompileTime == 3, "faces should be 3 column matrix/array only");

  for (VertexData& vd : vertices) {
    vd.normal.setZero();
  }

  const Eigen::Index num_faces = faces.rows();
  for (Eigen::Index face = 0; face < num_faces; ++face) {
    VertexData& V0 = vertices[faces(face, 0)];
    VertexData& V1 = vertices[faces(face, 1)];
    VertexData& V2 = vertices[faces(face, 2)];

    // Compute Face normal
    using NormalType = typename VertexData::NormalType;
    const NormalType face_normal = ((V1.position - V0.position).cross(V2.position - V0.position)).normalized();
    V0.normal = (V0.normal + face_normal).normalized();
    V1.normal = (V1.normal + face_normal).normalized();
    V2.normal = (V2.normal + face_normal).normalized();
  }
}

template <typename DerivedP, typename DerivedN, typename DerivedF>
void computeNormals(const Eigen::DenseBase<DerivedP>& positions, const Eigen::DenseBase<DerivedN>& normals_, const Eigen::DenseBase<DerivedF>& faces) {
  Eigen::DenseBase<DerivedN>& normals = const_cast< Eigen::DenseBase<DerivedN>& >(normals_);
  normals.derived().resize(positions.rows(), 3);
  normals.setZero();

  const Eigen::Index num_faces = faces.rows();
  for (Eigen::Index face = 0; face < num_faces; ++face) {
    const Eigen::Index v0 = faces(face, 0);
    const Eigen::Index v1 = faces(face, 1);
    const Eigen::Index v2 = faces(face, 2);

    // Compute Face normal
    using NormalType = typename DerivedN::RowXpr::PlainObject;
    const NormalType face_normal = ((positions.row(v1) - positions.row(v0)).cross(positions.row(v2) - positions.row(v0))).normalized();

    normals.row(v0) = (normals.row(v0) + face_normal).normalized();
    normals.row(v1) = (normals.row(v1) + face_normal).normalized();
    normals.row(v2) = (normals.row(v2) + face_normal).normalized();
  }
}

// TODO need to test this thing
template <typename VertexData, typename Derived, typename Scalar>
void computeNormals(std::vector<VertexData>& vertices, Eigen::DenseBase<Derived>& faces, const Scalar angle_threshold) {
  static_assert(Derived::ColsAtCompileTime == 3, "faces should be 3 column matrix/array only");

  for (VertexData& vd : vertices) {
    vd.normal.setZero();
  }

  const Eigen::Index num_faces = faces.rows();
  for (Eigen::Index face = 0; face < num_faces; ++face) {

    VertexData& V0 = vertices[faces(face, 0)];
    VertexData& V1 = vertices[faces(face, 1)];
    VertexData& V2 = vertices[faces(face, 2)];

    // Compute Face normal
    using NormalType = typename VertexData::NormalType;
    const NormalType face_normal = ((V1.position - V0.position).cross(V2.position - V0.position)).normalized();


    for (Eigen::Index i = 0; i < 3; ++i) {
      VertexData& vert = vertices[faces(face, i)];

      if(vert.normal.isZero()) {
        vert.normal = face_normal;
      }
      else if (vert.normal.dot(face_normal) < angle_threshold) {
        vert.normal = (vert.normal + face_normal).normalized();
      }
      else {
        vertices.push_back(vert);
        vertices.back().normal = face_normal;
        faces(face, i) = vertices.size() - 1;
      }
    }
  }
}

template<class PS, class NS, class CS, class IS>
void computeNormals(Mesh<PS, NS, CS, IS>& mesh) {
  computeNormals(mesh.positions, mesh.normals, mesh.faces);
}


// TODO need to test this thing
template<typename VertexData, typename Derived>
std::vector<VertexData> createFlatShadedMeshVertices(const std::vector<VertexData>& vertices,
                                                     const Eigen::DenseBase<Derived>& faces) {
  static_assert(Derived::ColsAtCompileTime == 3, "faces should be 3 column matrix/array only");

  const Eigen::Index num_faces = faces.rows();

  std::vector<MeshData::VertexData> flat_shaded_vertices(3 * num_faces);

  for (Eigen::Index face = 0; face < num_faces; ++face) {
    VertexData& V0 = flat_shaded_vertices[face * 3];
    VertexData& V1 = flat_shaded_vertices[face * 3 + 1];
    VertexData& V2 = flat_shaded_vertices[face * 3 + 2];

    V0 = vertices[faces(face, 0)];
    V1 = vertices[faces(face, 1)];
    V2 = vertices[faces(face, 2)];

    // Compute Face normal
    using NormalType = typename VertexData::NormalType;
    const NormalType face_normal = ((V1.position - V0.position).cross(V2.position - V0.position)).normalized();

    V0.normal = face_normal;
    V1.normal = face_normal;
    V2.normal = face_normal;
  }

  return flat_shaded_vertices;
}


}  // namespace CuteGL


#endif // end CUTEGL_GEOMETRY_COMPUTENORMALS_IMPL_HPP_
