/**
 * @file ComputeAlignedBox.cpp
 * @brief ComputeAlignedBox
 *
 * @author Abhijit Kundu
 */

#include "ComputeAlignedBox.h"

namespace CuteGL {

Eigen::AlignedBox<MeshData::PositionType::Scalar, 3>
computeAlignedBox(const std::vector<MeshData>& meshes) {
  Eigen::AlignedBox<MeshData::PositionType::Scalar, 3> bbx;

  for (const MeshData& mesh : meshes)
    bbx.extend(computeAlignedBox(mesh));

  return bbx;
}

Eigen::AlignedBox<MeshData::PositionType::Scalar, 3>
computeAlignedBox(const MeshData& mesh) {
  return computeAlignedBox(mesh.vertices);
}

}  // namespace CuteGL

