/**
 * @file OrientedBoxHelper.h
 * @brief OrientedBoxHelper
 *
 * @author Abhijit Kundu
 */

#include <Eigen/Geometry>

namespace CuteGL {

/**Get the corners of an oriented box as a matrix
 *
 * @param bbx_pose
 * @param bbx_size
 * @return matrix where each column is one of the corners
 */
template <class Transform, class Derived>
Eigen::Matrix<typename Derived::Scalar, 3, 8>
computeOrientedBoxCorners(const Transform& bbx_pose, const Eigen::MatrixBase<Derived>& bbx_size) {
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(Derived, 3);
  static_assert(Transform::Dim == 3, "Transform needs to be a 3D transform object");

  using Scalar = typename Derived::Scalar;
  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;
  using AlignedBox3 = Eigen::AlignedBox<Scalar, 3>;

  const Vector3 bbx_half_size = bbx_size / 2;
  const AlignedBox3 bbx(-bbx_half_size , bbx_half_size);

  Eigen::Matrix<Scalar, 3, 8> bbx_corners;
  bbx_corners.col(0) = bbx.corner(AlignedBox3::BottomLeftFloor);
  bbx_corners.col(1) = bbx.corner(AlignedBox3::BottomRightFloor);
  bbx_corners.col(2) = bbx.corner(AlignedBox3::TopLeftFloor);
  bbx_corners.col(3) = bbx.corner(AlignedBox3::TopRightFloor);
  bbx_corners.col(4) = bbx.corner(AlignedBox3::BottomLeftCeil);
  bbx_corners.col(5) = bbx.corner(AlignedBox3::BottomRightCeil);
  bbx_corners.col(6) = bbx.corner(AlignedBox3::TopLeftCeil);
  bbx_corners.col(7) = bbx.corner(AlignedBox3::TopRightCeil);

  return bbx_pose * bbx_corners;
}


template <class TransformA, class DerivedA, class TransformB, class DerivedB>
bool checkOrientedBoxCollision(const TransformA& bbx_poseA, const Eigen::MatrixBase<DerivedA>& bbx_sizeA,
                               const TransformB& bbx_poseB, const Eigen::MatrixBase<DerivedB>& bbx_sizeB) {
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(DerivedA, 3);
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(DerivedB, 3);

  using Scalar = typename DerivedA::Scalar;
  static_assert(TransformA::Dim == 3, "TransformA needs to be a 3D transform object");
  static_assert(TransformB::Dim == 3, "TransformA needs to be a 3D transform object");

  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;

  Eigen::Matrix<Scalar, 3, 8> bbx_cornersA = computeOrientedBoxCorners(bbx_poseA, bbx_sizeA);
  Eigen::Matrix<Scalar, 3, 8> bbx_cornersB = computeOrientedBoxCorners(bbx_poseB, bbx_sizeB);

  // Test along normals of bbxA
  for (Eigen::Index i = 0; i < 3; ++i) {
    Vector3 axis = bbx_poseA.rotation() * Vector3::Unit(i);

    Eigen::Matrix<Scalar, 1, 8> dotpA = axis.transpose() * bbx_cornersA;
    Eigen::Matrix<Scalar, 1, 8> dotpB = axis.transpose() * bbx_cornersB;

    Scalar minA = dotpA.minCoeff();
    Scalar maxA = dotpA.maxCoeff();

    Scalar minB = dotpB.minCoeff();
    Scalar maxB = dotpB.maxCoeff();

    bool overlap = (minA <= minB && minB <= maxA) || (minB <= minA && minA <= maxB);

    if (!overlap)
      return false;
  }

  // Test along normals of bbxB
  for (Eigen::Index j = 0; j < 3; ++j) {
    Vector3 axis = bbx_poseB.rotation() * Vector3::Unit(j);

    Eigen::Matrix<Scalar, 1, 8> dotpA = axis.transpose() * bbx_cornersA;
    Eigen::Matrix<Scalar, 1, 8> dotpB = axis.transpose() * bbx_cornersB;

    Scalar minA = dotpA.minCoeff();
    Scalar maxA = dotpA.maxCoeff();

    Scalar minB = dotpB.minCoeff();
    Scalar maxB = dotpB.maxCoeff();

    bool overlap = (minA <= minB && minB <= maxA) || (minB <= minA && minA <= maxB);

    if (!overlap)
      return false;
  }

  // Test along cross-products of normals of bbxA and bbxB
  for (Eigen::Index i = 0; i < 3; ++i)
    for (Eigen::Index j = 0; j < 3; ++j) {
      Vector3 axisA = bbx_poseA.rotation() * Vector3::Unit(i);
      Vector3 axisB = bbx_poseB.rotation() * Vector3::Unit(j);
      Vector3 axis = axisA.cross(axisB);

      Eigen::Matrix<Scalar, 1, 8> dotpA = axis.transpose() * bbx_cornersA;
      Eigen::Matrix<Scalar, 1, 8> dotpB = axis.transpose() * bbx_cornersB;

      Scalar minA = dotpA.minCoeff();
      Scalar maxA = dotpA.maxCoeff();

      Scalar minB = dotpB.minCoeff();
      Scalar maxB = dotpB.maxCoeff();

      bool overlap = (minA <= minB && minB <= maxA) || (minB <= minA && minA <= maxB);

      if (!overlap)
        return false;
    }

  // No separating axes found then they do intersect
  return true;
}

}  // namespace CuteGL


