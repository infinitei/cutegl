/**
 * @file ComputeAlignedBox.h
 * @brief ComputeAlignedBox
 *
 * @author Abhijit Kundu
 */


#ifndef CUTEGL_GEOMETRY_COMPUTE_ALIGNED_BOX_H_
#define CUTEGL_GEOMETRY_COMPUTE_ALIGNED_BOX_H_

#include "CuteGL/Core/MeshData.h"
#include <Eigen/Geometry>

namespace CuteGL {

template<class VertexData>
Eigen::AlignedBox<typename VertexData::PositionType::Scalar, 3>
computeAlignedBox(const std::vector<VertexData>& vertices) {
  Eigen::AlignedBox<typename VertexData::PositionType::Scalar, 3> bbx;

  for (const VertexData& vd : vertices)
    bbx.extend(vd.position);

  return bbx;
}

Eigen::AlignedBox<MeshData::PositionType::Scalar, 3>
computeAlignedBox(const MeshData& mesh);

Eigen::AlignedBox<MeshData::PositionType::Scalar, 3>
computeAlignedBox(const std::vector<MeshData>& meshes);

}  // namespace CuteGL


#endif // end CUTEGL_GEOMETRY_COMPUTE_ALIGNED_BOX_H_
