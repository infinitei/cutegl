/**
 * @file SMPLCudaEngine.h
 * @brief SMPLCudaEngine
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_SMPLCUDAENGINE_H_
#define CUTEGL_DRAWERS_SMPLCUDAENGINE_H_

#include <Eigen/Core>

// Forward declaration for CUDA-OpeGL Resource
struct cudaGraphicsResource;

namespace CuteGL {

template<class SMPLDataType>
struct SMPLCudaEngine {
  typedef typename SMPLDataType::PositionScalar PositionScalar;
  typedef typename SMPLDataType::IndexScalar IndexScalar;
  typedef Eigen::Map< Eigen::Matrix<PositionScalar, Eigen::Dynamic, 1> > ParamType;

  SMPLCudaEngine()
    : shape(nullptr, 0),
      pose(nullptr, 0),
      N_(0),
      K_(0),
      num_shape_params_(0),
      num_pose_params_(0),
      max_joint_depth_(0),
      h_shape_params_(nullptr),
      h_pose_params_(nullptr),
      d_shape_params_(nullptr),
      d_pose_params_(nullptr),
      d_template_vertices_(nullptr),
      d_jr_indices_(nullptr),
      d_jr_values_(nullptr),
      d_shape_displacements_(nullptr),
      d_blend_weights_(nullptr),
      d_blend_jointids_(nullptr),
      d_joint_parents_(nullptr),
      d_joint_levels_(nullptr),
      d_joint_locations_(nullptr),
      d_pre_posed_vertices_(nullptr),
      d_bone_transforms_(nullptr),
      cuda_vbo_resource_(nullptr) {
    static_assert(std::is_same<Eigen::Index, int>::value, "Need to set EIGEN_DEFAULT_DENSE_INDEX_TYPE to int");
  }

  ~SMPLCudaEngine();

  void uploadSMPLdata(const SMPLDataType& smpl_data);

  void registerVBO(unsigned int buffer);

  void updateShape();
  void updatePose();
  void updateShapeAndPose();

  ParamType shape;
  ParamType pose;

private:
  void computeBoneTransforms();
  void performLBSAndUpdateVBO();

  int N_;
  int K_;
  int num_shape_params_;
  int num_pose_params_;
  int max_joint_depth_;

  PositionScalar* h_shape_params_;
  PositionScalar* h_pose_params_;

  PositionScalar* d_shape_params_;
  PositionScalar* d_pose_params_;
  PositionScalar* d_template_vertices_;
  int* d_jr_indices_;
  PositionScalar* d_jr_values_;
  PositionScalar* d_shape_displacements_;
  PositionScalar* d_blend_weights_;
  IndexScalar* d_blend_jointids_;
  IndexScalar* d_joint_parents_;
  IndexScalar* d_joint_levels_;

  // Intermediate variables
  PositionScalar* d_joint_locations_;
  PositionScalar* d_pre_posed_vertices_;
  PositionScalar* d_bone_transforms_;

  cudaGraphicsResource *cuda_vbo_resource_;
};

}  // namespace CuteGL

#endif // end CUTEGL_DRAWERS_SMPLCUDAENGINE_H_
