/**
 * @file CubeDrawer.cpp
 * @brief CubeDrawer
 *
 * @author Abhijit Kundu
 */

#include "CubeDrawer.h"
#include "CuteGL/Core/VertexData.h"
#include <array>

namespace CuteGL {

CubeDrawer::CubeDrawer() {
}

CubeDrawer::CubeDrawer(const QOpenGLShaderProgram& program) {
  init(program);
}

void CubeDrawer::init(const QOpenGLShaderProgram& program) {

  assert(hasValidOpenGLFunctions());

  typedef VertexXYZNormalRGBA VertexData;

  std::array<VertexData::PositionType, 8> cube_corners;
  // Front Verticies positions
  cube_corners[0] = VertexData::PositionType(0.5f, 0.5f, 0.5f);
  cube_corners[1] = VertexData::PositionType(-0.5f, 0.5f, 0.5f);
  cube_corners[2] = VertexData::PositionType(-0.5f, -0.5f, 0.5f);
  cube_corners[3] = VertexData::PositionType(0.5f, -0.5f, 0.5f);

  // Back Verticies positions
  cube_corners[4] = VertexData::PositionType(0.5f, 0.5f, -0.5f);
  cube_corners[5] = VertexData::PositionType(-0.5f, 0.5f, -0.5f);
  cube_corners[6] = VertexData::PositionType(-0.5f, -0.5f, -0.5f);
  cube_corners[7] = VertexData::PositionType(0.5f, -0.5f, -0.5f);

  std::array<VertexData::ColorType, 8> cube_corner_colors;
  cube_corner_colors[0] = VertexData::ColorType(255, 0, 0, 255);
  cube_corner_colors[1] = VertexData::ColorType(0, 255, 0, 255);
  cube_corner_colors[2] = VertexData::ColorType(0, 0, 255, 255);
  cube_corner_colors[3] = VertexData::ColorType(0, 0, 0, 255);
  cube_corner_colors[4] = VertexData::ColorType(255, 0, 0, 255);
  cube_corner_colors[5] = VertexData::ColorType(0, 255, 0, 255);
  cube_corner_colors[6] = VertexData::ColorType(0, 0, 255, 255);
  cube_corner_colors[7] = VertexData::ColorType(255, 255, 255, 255);

  std::array<VertexData::NormalType, 6> face_normals;
  face_normals[0] = VertexData::NormalType(0.0f, 0.0f, 1.0f);
  face_normals[1] = VertexData::NormalType(0.0f, 0.0f, -1.0f);
  face_normals[2] = VertexData::NormalType(0.0f, 1.0f, 0.0f);
  face_normals[3] = VertexData::NormalType(0.0f, -1.0f, 0.0f);
  face_normals[4] = VertexData::NormalType(-1.0f, 0.0f, 0.0f);
  face_normals[5] = VertexData::NormalType(1.0f, 1.0f, 0.0f);

  std::vector<VertexData> vertices;

  // face 0
  vertices.emplace_back(cube_corners[0], face_normals[0], cube_corner_colors[0]);
  vertices.emplace_back(cube_corners[1], face_normals[0], cube_corner_colors[1]);
  vertices.emplace_back(cube_corners[2], face_normals[0], cube_corner_colors[2]);
  vertices.emplace_back(cube_corners[2], face_normals[0], cube_corner_colors[2]);
  vertices.emplace_back(cube_corners[3], face_normals[0], cube_corner_colors[3]);
  vertices.emplace_back(cube_corners[0], face_normals[0], cube_corner_colors[0]);

  // face 1
  vertices.emplace_back(cube_corners[7], face_normals[1], cube_corner_colors[7]);
  vertices.emplace_back(cube_corners[5], face_normals[1], cube_corner_colors[5]);
  vertices.emplace_back(cube_corners[4], face_normals[1], cube_corner_colors[4]);
  vertices.emplace_back(cube_corners[5], face_normals[1], cube_corner_colors[5]);
  vertices.emplace_back(cube_corners[7], face_normals[1], cube_corner_colors[7]);
  vertices.emplace_back(cube_corners[6], face_normals[1], cube_corner_colors[6]);

  // face 2
  vertices.emplace_back(cube_corners[0], face_normals[2], cube_corner_colors[0]);
  vertices.emplace_back(cube_corners[4], face_normals[2], cube_corner_colors[4]);
  vertices.emplace_back(cube_corners[5], face_normals[2], cube_corner_colors[5]);
  vertices.emplace_back(cube_corners[5], face_normals[2], cube_corner_colors[5]);
  vertices.emplace_back(cube_corners[1], face_normals[2], cube_corner_colors[1]);
  vertices.emplace_back(cube_corners[0], face_normals[2], cube_corner_colors[0]);

  // face 3
  vertices.emplace_back(cube_corners[3], face_normals[3], cube_corner_colors[3]);
  vertices.emplace_back(cube_corners[2], face_normals[3], cube_corner_colors[2]);
  vertices.emplace_back(cube_corners[6], face_normals[3], cube_corner_colors[6]);
  vertices.emplace_back(cube_corners[6], face_normals[3], cube_corner_colors[6]);
  vertices.emplace_back(cube_corners[7], face_normals[3], cube_corner_colors[7]);
  vertices.emplace_back(cube_corners[3], face_normals[3], cube_corner_colors[3]);

  // face 4
  vertices.emplace_back(cube_corners[2], face_normals[4], cube_corner_colors[2]);
  vertices.emplace_back(cube_corners[1], face_normals[4], cube_corner_colors[1]);
  vertices.emplace_back(cube_corners[5], face_normals[4], cube_corner_colors[5]);
  vertices.emplace_back(cube_corners[2], face_normals[4], cube_corner_colors[2]);
  vertices.emplace_back(cube_corners[5], face_normals[4], cube_corner_colors[5]);
  vertices.emplace_back(cube_corners[6], face_normals[4], cube_corner_colors[6]);

  // face 5
  vertices.emplace_back(cube_corners[0], face_normals[5], cube_corner_colors[0]);
  vertices.emplace_back(cube_corners[3], face_normals[5], cube_corner_colors[3]);
  vertices.emplace_back(cube_corners[7], face_normals[5], cube_corner_colors[7]);
  vertices.emplace_back(cube_corners[7], face_normals[5], cube_corner_colors[7]);
  vertices.emplace_back(cube_corners[4], face_normals[5], cube_corner_colors[4]);
  vertices.emplace_back(cube_corners[0], face_normals[5], cube_corner_colors[0]);

  // 1. Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // 2. Copy our vertices array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexData), vertices.data(), GL_STATIC_DRAW);

  // 3. Then set the vertex attributes pointers

  // Position attribute
  const int position_index = program.attributeLocation("vertex_position");
  assert(position_index != -1);
  glfuncs_->glVertexAttribPointer(position_index, 3, GL_FLOAT, GL_FALSE,
                        sizeof(VertexData),
                        (void*) offsetof(VertexData, position));
  glfuncs_->glEnableVertexAttribArray(position_index);

  // Normal attribute
  const int normal_index = program.attributeLocation("vertex_normal");
  if (normal_index != -1) {
    glfuncs_->glVertexAttribPointer(normal_index, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                          sizeof(VertexData),
                          (void*) offsetof(VertexData, normal));
    glfuncs_->glEnableVertexAttribArray(normal_index);
  }

    // Color attribute
  const int color_index = program.attributeLocation("vertex_color");
  if (color_index != -1) {
    glfuncs_->glVertexAttribPointer(color_index, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                          sizeof(VertexData),
                          (void*) offsetof(VertexData, color));
    glfuncs_->glEnableVertexAttribArray(color_index);
                        }

    // Release (unbind) all
  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void CubeDrawer::draw() {
  assert(hasValidOpenGLFunctions());
  glfuncs_->glBindVertexArray(vao_);
  glDrawArrays(GL_TRIANGLES, 0, 36);
  glfuncs_->glBindVertexArray(0);
}

}  // end namespace CuteGL
