/**
 * @file SMPLDrawer-impl.hpp
 * @brief SMPLDrawer-impl
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_SMPLDRAWER_IMPL_HPP_
#define CUTEGL_DRAWERS_SMPLDRAWER_IMPL_HPP_

#include "CuteGL/Geometry/ComputeNormals.h"
#include "CuteGL/Utils/OpenGLUtils.h"
#include "CuteGL/IO/ImportSMPL.h"

namespace CuteGL {

template <class SMPLDataType>
SMPLDrawer<SMPLDataType>::SMPLDrawer()
    : position_vbo_(0),
      num_of_indices_(0) {
}

template <class SMPLDataType>
SMPLDrawer<SMPLDataType>::~SMPLDrawer() noexcept {
  if (position_vbo_)
    glfuncs_->glDeleteBuffers(1, &position_vbo_);
}

template <class SMPLDataType>
SMPLDrawer<SMPLDataType>::SMPLDrawer(const QOpenGLShaderProgram& program, const SMPLDataType& smpl_data, const RGBA& color, const int label)
    : position_vbo_(0),
      num_of_indices_(0) {
  init(program, smpl_data, color, label);
}

template <class SMPLDataType>
SMPLDrawer<SMPLDataType>::SMPLDrawer(const QOpenGLShaderProgram& program, const std::string& smpl_model_file, const RGBA& color, const int label)
    : position_vbo_(0),
      num_of_indices_(0) {
  init(program, smpl_model_file, color, label);
}

template <class SMPLDataType>
void SMPLDrawer<SMPLDataType>::init(const QOpenGLShaderProgram& program, const std::string& smpl_model_file, const RGBA& color, const int label) {
  init(program, loadSMPLDataFromHDF5<SMPLDataType>(smpl_model_file), color, label);
}

template <class SMPLDataType>
void SMPLDrawer<SMPLDataType>::init(const QOpenGLShaderProgram& program, const SMPLDataType& smpl_data, const RGBA& color, const int label) {
  using NormalScalar = typename SMPLDataType::PositionScalar;
  using ColorScalar = typename RGBA::Scalar;
  using LabelScalar = int;

  assert(hasValidOpenGLFunctions());


  const int position_index = program.attributeLocation("vertex_position");
  const int normal_index = program.attributeLocation("vertex_normal");
  const int color_index = program.attributeLocation("vertex_color");
  const int label_index = program.attributeLocation("vertex_label");

  assert(position_index != -1);

  const Eigen::Index num_of_vertices = smpl_data.template_vertices.rows();
  const GLsizeiptr size_of_positions = (position_index != -1) ? 3 * num_of_vertices * sizeof(PositionScalar) : 0;
  const GLsizeiptr size_of_normals = (normal_index != -1) ? 3 * num_of_vertices * sizeof(NormalScalar) : 0;
  const GLsizeiptr size_of_colors = (color_index != -1) ? 4 * num_of_vertices * sizeof(ColorScalar) : 0;
  const GLsizeiptr size_of_labels = (label_index != -1) ? num_of_vertices * sizeof(LabelScalar) : 0;

  // Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // Initialize position buffer which will be updated quite often
  {
    glfuncs_->glGenBuffers(1, &position_vbo_);
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, position_vbo_);
    glfuncs_->glBufferData(GL_ARRAY_BUFFER,
                           size_of_positions,
                           nullptr,
                           GL_DYNAMIC_DRAW);

    // Registers OpenGL vbo with CUDA
    smpl_engine_.registerVBO(position_vbo_);

    // Position attribute
    {
      GLintptr offset = 0;
      glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_positions, smpl_data.template_vertices.data());
      glfuncs_->glVertexAttribPointer(position_index, 3, GLTraits<PositionScalar>::type, GL_FALSE,
                                      3 * sizeof(PositionScalar),
                                      (GLvoid*) offset);
      glfuncs_->glEnableVertexAttribArray(position_index);
    }
  }


  // Initialize vertex buffer for static attributes: color, normal, labels etc
  {
    glfuncs_->glGenBuffers(1, &vbo_);
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glfuncs_->glBufferData(GL_ARRAY_BUFFER,
                           size_of_normals + size_of_colors + size_of_labels,
                           nullptr,
                           GL_STATIC_DRAW);


    // Normal attribute
    if (size_of_normals) {
      GLintptr offset = 0;
      typename SMPLDataType::MatrixX3 normals;
      computeNormals(smpl_data.template_vertices, normals, smpl_data.faces);
      assert (normals.size() * sizeof(NormalScalar) == std::size_t(size_of_normals));
      glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_normals, normals.data());
      glfuncs_->glVertexAttribPointer(normal_index, 3, GLTraits<NormalScalar>::type, GL_FALSE,
                                      3 * sizeof(NormalScalar),
                                      (GLvoid*) offset);
      glfuncs_->glEnableVertexAttribArray(normal_index);
    }

    // Color attribute
    if (size_of_colors) {
      GLintptr offset = size_of_normals;
      Eigen::Matrix<ColorScalar, Eigen::Dynamic, 1> colors = color.replicate(num_of_vertices, 1);
      assert (colors.size() * sizeof(ColorScalar) == std::size_t(size_of_colors));
      glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_colors, colors.data());
      glfuncs_->glVertexAttribPointer(color_index, 4, GLTraits<ColorScalar>::type, GL_TRUE,
                                      4 * sizeof(ColorScalar),
                                      (GLvoid*) offset);
      glfuncs_->glEnableVertexAttribArray(color_index);
    }

    if (size_of_labels) {
      GLintptr offset = size_of_normals + size_of_colors;
      Eigen::Matrix<LabelScalar, Eigen::Dynamic, 1> labels(num_of_vertices);
      labels.setConstant(label);
      assert (labels.size() * sizeof(LabelScalar) == std::size_t(size_of_labels));
      glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_labels, labels.data());
      glfuncs_->glVertexAttribIPointer(label_index, 1, GLTraits<LabelScalar>::type,
                                       1 * sizeof(LabelScalar),
                                      (GLvoid*) offset);
      glfuncs_->glEnableVertexAttribArray(label_index);
    }
  }


  glfuncs_->glGenBuffers(1, &ibo_);
  glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
  num_of_indices_ = smpl_data.faces.size();
  glfuncs_->glBufferData(GL_ELEMENT_ARRAY_BUFFER, smpl_data.faces.size() * sizeof(IndexScalar), smpl_data.faces.data(), GL_STATIC_DRAW);

  // Release VAO first
  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  // upload smpl data to CUDA
  smpl_engine_.uploadSMPLdata(smpl_data);
}


template <class SMPLDataType>
template <class SMPLSegmmDataType>
SMPLDrawer<SMPLDataType>::SMPLDrawer(const QOpenGLShaderProgram& program, const SMPLDataType& smpl_data, const SMPLSegmmDataType& segm_data)
    : position_vbo_(0),
      num_of_indices_(0) {
  init(program, smpl_data, segm_data);
}

template <class SMPLDataType>
template <class SMPLSegmmDataType>
void SMPLDrawer<SMPLDataType>::init(const QOpenGLShaderProgram& program, const SMPLDataType& smpl_data, const SMPLSegmmDataType& segm_data) {
  assert(smpl_data.template_vertices.rows() == segm_data.vertex_labels.size());

  using NormalScalar = typename SMPLDataType::PositionScalar;
  using ColorScalar = typename SMPLSegmmDataType::ColorScalar;
  using LabelScalar = typename SMPLSegmmDataType::LabelScalar;

  assert(hasValidOpenGLFunctions());


  const int position_index = program.attributeLocation("vertex_position");
  const int normal_index = program.attributeLocation("vertex_normal");
  const int color_index = program.attributeLocation("vertex_color");
  const int label_index = program.attributeLocation("vertex_label");

  assert(position_index != -1);

  const Eigen::Index num_of_vertices = smpl_data.template_vertices.rows();
  const GLsizeiptr size_of_positions = (position_index != -1) ? 3 * num_of_vertices * sizeof(PositionScalar) : 0;
  const GLsizeiptr size_of_normals = (normal_index != -1) ? 3 * num_of_vertices * sizeof(NormalScalar) : 0;
  const GLsizeiptr size_of_colors = (color_index != -1) ? 4 * num_of_vertices * sizeof(ColorScalar) : 0;
  const GLsizeiptr size_of_labels = (label_index != -1) ? num_of_vertices * sizeof(LabelScalar) : 0;



  // Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // Initialize position buffer which will be updated quite often
  {
    glfuncs_->glGenBuffers(1, &position_vbo_);
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, position_vbo_);
    glfuncs_->glBufferData(GL_ARRAY_BUFFER,
                           size_of_positions,
                           nullptr,
                           GL_DYNAMIC_DRAW);

    // Registers OpenGL vbo
    smpl_engine_.registerVBO(position_vbo_);

    // Position attribute
    {
      GLintptr offset = 0;
      glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_positions, smpl_data.template_vertices.data());
      glfuncs_->glVertexAttribPointer(position_index, 3, GLTraits<PositionScalar>::type, GL_FALSE,
                                      3 * sizeof(PositionScalar),
                                      (GLvoid*) offset);
      glfuncs_->glEnableVertexAttribArray(position_index);
    }
  }

  {
    // Initialize vertex buffer for static attributes: color, normal, labels etc
    glfuncs_->glGenBuffers(1, &vbo_);
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glfuncs_->glBufferData(GL_ARRAY_BUFFER,
                           size_of_normals + size_of_colors + size_of_labels,
                           nullptr,
                           GL_STATIC_DRAW);

    // Normal attribute
    if (size_of_normals) {
      GLintptr offset = 0;
      typename SMPLDataType::MatrixX3 normals;
      computeNormals(smpl_data.template_vertices, normals, smpl_data.faces);
      assert (normals.size() * sizeof(NormalScalar) == std::size_t(size_of_normals));
      glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_normals, normals.data());
      glfuncs_->glVertexAttribPointer(normal_index, 3, GLTraits<NormalScalar>::type, GL_FALSE,
                                      3 * sizeof(NormalScalar),
                                      (GLvoid*) offset);
      glfuncs_->glEnableVertexAttribArray(normal_index);
    }

    // Color attribute
    if (size_of_colors) {
      GLintptr offset = size_of_normals;

      typename SMPLSegmmDataType::ColorDataContainer colors(num_of_vertices, 4);
      assert (colors.size() * sizeof(ColorScalar) == std::size_t(size_of_colors));
      for (Eigen::Index i= 0; i < num_of_vertices; ++i) {
        colors.row(i) = segm_data.color_map.row(segm_data.vertex_labels[i]);
      }

      glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_colors, colors.data());
      glfuncs_->glVertexAttribPointer(color_index, 4, GLTraits<ColorScalar>::type, GL_TRUE,
                                      4 * sizeof(ColorScalar),
                                      (GLvoid*) offset);
      glfuncs_->glEnableVertexAttribArray(color_index);
    }

    if (size_of_labels) {
      GLintptr offset = size_of_normals + size_of_colors;
      typename SMPLSegmmDataType::LabelDataContainer labels = segm_data.vertex_labels.array() + 1;

      assert (labels.size() * sizeof(LabelScalar) == std::size_t(size_of_labels));
      glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_labels, labels.data());
      glfuncs_->glVertexAttribIPointer(label_index, 1, GLTraits<LabelScalar>::type,
                                       1 * sizeof(LabelScalar),
                                      (GLvoid*) offset);
      glfuncs_->glEnableVertexAttribArray(label_index);
    }
  }

  glfuncs_->glGenBuffers(1, &ibo_);
  glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
  num_of_indices_ = smpl_data.faces.size();
  glfuncs_->glBufferData(GL_ELEMENT_ARRAY_BUFFER, smpl_data.faces.size() * sizeof(IndexScalar), smpl_data.faces.data(), GL_STATIC_DRAW);

  // Release VAO first
  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  // upload smpl data to CUDA
  smpl_engine_.uploadSMPLdata(smpl_data);
}

template <class SMPLDataType>
SMPLDrawer<SMPLDataType>::SMPLDrawer(const QOpenGLShaderProgram& program, const std::string& smpl_model_file, const std::string& smpl_segm_file)
    : position_vbo_(0),
      num_of_indices_(0) {
  init(program, smpl_model_file, smpl_segm_file);
}

template <class SMPLDataType>
void SMPLDrawer<SMPLDataType>::init(const QOpenGLShaderProgram& program, const std::string& smpl_model_file, const std::string& smpl_segmm_file) {
  init(program,
       loadSMPLDataFromHDF5<SMPLDataType>(smpl_model_file),
       loadSMPLSegmmDataFromHDF5<SMPLSegmmData<>>(smpl_segmm_file));
}

template <class SMPLDataType>
void SMPLDrawer<SMPLDataType>::draw() {
  assert(hasValidOpenGLFunctions());
  glfuncs_->glBindVertexArray(vao_);
  glfuncs_->glDrawElements(GL_TRIANGLES, num_of_indices_, GL_UNSIGNED_INT, 0);
  glfuncs_->glBindVertexArray(0);
}

}  // namespace CuteGL

#endif // end CUTEGL_DRAWERS_SMPLDRAWER_IMPL_HPP_
