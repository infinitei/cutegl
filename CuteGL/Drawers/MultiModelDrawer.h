/**
 * @file MultiModelDrawer.h
 * @brief MultiModelDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_MULTIMODELDRAWER_H_
#define CUTEGL_DRAWERS_MULTIMODELDRAWER_H_

#include "CuteGL/Drawers/ModelDrawer.h"
#include <Eigen/Geometry>
#include <Eigen/StdVector>

namespace CuteGL {

template<class Shader>
class MultiModelDrawer {
 public:
  MultiModelDrawer(Shader& shader)
      : shader_(shader) {
  }

  void addModel(const std::vector<MeshData>& meshes, const Eigen::Affine3f& pose = Eigen::Affine3f::Identity()) {
    model_drawers_.emplace_back(shader_.program, meshes);
    model_poses_.emplace_back(pose);
    assert(model_drawers_.size() == model_poses_.size());
  }


  void draw() {
    assert(model_drawers_.size() == model_poses_.size());
    for (std::size_t i = 0; i < model_drawers_.size(); ++i) {
      shader_.setModelPose(model_poses_[i]);
      model_drawers_[i].draw();
    }
  }

 private:
  Shader& shader_;
  std::vector<ModelDrawer> model_drawers_;
  std::vector<Eigen::Affine3f, Eigen::aligned_allocator<Eigen::Affine3f> > model_poses_;

};

}  // end namespace CuteGL
#endif // end CUTEGL_DRAWERS_MULTIMODELDRAWER_H_
