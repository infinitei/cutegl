/**
 * @file CubeDrawer.h
 * @brief CubeDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_CUBEDRAWER_H_
#define CUTEGL_DRAWERS_CUBEDRAWER_H_

#include "CuteGL/Drawers/DrawerBase.h"
#include <QOpenGLShaderProgram>

namespace CuteGL {

class CubeDrawer : public DrawerBase {
 public:
  CubeDrawer();
  CubeDrawer(const QOpenGLShaderProgram& program);

  /// Initialize VBO, VAO.
  void init(const QOpenGLShaderProgram& program);

  /// Draw
  void draw();
};

}  // end namespace CuteGL
#endif // end CUTEGL_DRAWERS_CUBEDRAWER_H_
