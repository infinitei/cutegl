/**
 * @file LineDrawer.cpp
 * @brief LineDrawer
 *
 * @author Abhijit Kundu
 */

#include "LineDrawer.h"
#include <iostream>
#include <array>

namespace CuteGL {

LineDrawer::LineDrawer(const std::size_t max_num_of_lines)
  : num_of_lines_(0),
    max_num_of_lines_(max_num_of_lines) {
}

LineDrawer::LineDrawer(const QOpenGLShaderProgram& program, const std::size_t max_num_of_lines)
    : num_of_lines_(0),
      max_num_of_lines_(max_num_of_lines) {
  init(program);
}

LineDrawer::LineDrawer(const QOpenGLShaderProgram& program,
                       const std::vector<std::pair<VertexData, VertexData> >& lines)
    : num_of_lines_(0),
      max_num_of_lines_(lines.size()) {
  init(program);
  for(const auto& line : lines) {
    addLine(line.first, line.second);
  }
}


LineDrawer::LineDrawer(const QOpenGLShaderProgram& program,
                       const std::vector<std::pair<PositionType, PositionType> >& lines,
                       const ColorType& color)
    : num_of_lines_(0),
      max_num_of_lines_(lines.size()) {
  init(program);
  for(const auto& line : lines) {
    addLine(VertexData(line.first,color), VertexData(line.second,color));
  }
}

LineDrawer::LineDrawer(const QOpenGLShaderProgram& program,
                       const std::vector<std::pair<PositionType, PositionType> >& lines,
                       const ColorType& colorA, const ColorType& colorB)
    : num_of_lines_(0),
      max_num_of_lines_(lines.size()) {
  init(program);
  for (const auto& line : lines) {
    addLine(VertexData(line.first, colorA), VertexData(line.second, colorB));
  }
}

void LineDrawer::init(const QOpenGLShaderProgram& program) {
  assert(hasValidOpenGLFunctions());

  // Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // Copy our vertices array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER, 2* max_num_of_lines_ * sizeof(VertexData), NULL, GL_STATIC_DRAW);

  // Position attribute
  const int position_index = program.attributeLocation("vertex_position");
  assert(position_index != -1);
  glfuncs_->glVertexAttribPointer(position_index, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void*) offsetof(VertexData, position));
  glfuncs_->glEnableVertexAttribArray(position_index);

  // Color attribute
  const int color_index = program.attributeLocation("vertex_color");
  if(color_index != -1) {
    glfuncs_->glVertexAttribPointer(color_index, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexData), (void*) offsetof(VertexData, color));
    glfuncs_->glEnableVertexAttribArray(color_index);
  }

  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void LineDrawer::addLine(const VertexData& from, const VertexData& to) {
  if (num_of_lines_ >= max_num_of_lines_) {
    std::cerr << "[Warning] Max num of lines reached" << std::endl;
    return;
  }

  std::array<VertexData, 2> line_vertices { { from, to } };

  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferSubData(GL_ARRAY_BUFFER,
                            2 * num_of_lines_ * sizeof(VertexData),
                            2 * sizeof(VertexData), line_vertices.data());
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  ++num_of_lines_;
}

void LineDrawer::updateLineVertices(const std::vector<std::pair<VertexData, VertexData> >& lines) {
  num_of_lines_ = 0;
  for(const auto& line : lines) {
    addLine(line.first, line.second);
  }
}

void LineDrawer::updateLineVertices(const std::vector<std::pair<PositionType, PositionType> >& lines,
                                    const ColorType& color) {

  updateLineVertices(lines, color, color);
}

void LineDrawer::updateLineVertices(const std::vector<std::pair<PositionType, PositionType> >& lines,
                                    const ColorType& colorA, const ColorType& colorB) {
  num_of_lines_ = 0;
  for (const auto& line : lines) {
    addLine(VertexData(line.first, colorA), VertexData(line.second, colorB));
  }
}

void LineDrawer::draw() {
  assert(hasValidOpenGLFunctions());
  if(num_of_lines_) {
    glfuncs_->glBindVertexArray(vao_);
    glfuncs_->glDrawArrays(GL_LINES, 0, 2* num_of_lines_); // Draw the lines
    glfuncs_->glBindVertexArray(0);
  }
}

}  // end namespace CuteGL
