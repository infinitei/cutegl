/**
 * @file CameraDrawer.h
 * @brief CameraDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_CAMERADRAWER_H_
#define CUTEGL_DRAWERS_CAMERADRAWER_H_

#include "CuteGL/Drawers/DrawerBase.h"
#include "CuteGL/Core/VertexData.h"
#include <QOpenGLShaderProgram>

namespace CuteGL {

/// Draws a simple camera useful to show its position & orientation
class CameraDrawer : public DrawerBase {
 public:
  typedef VertexXYZRGBA VertexData;
  typedef VertexData::PositionType PositionType;
  typedef VertexData::ColorType ColorType;


  CameraDrawer();

  CameraDrawer(const QOpenGLShaderProgram& program,
               const float length,
               const float fovY = 0.785398f, /*~45 deg*/
               const float aspect_ratio = 1.3333f,
               const ColorType& color = ColorType(0, 255, 0, 255));

  ~CameraDrawer() noexcept;

  /// No Copy Constructor
  CameraDrawer(const CameraDrawer& other) = delete;

  /// Move constructor
  CameraDrawer(CameraDrawer&& other) noexcept;

  /// Move assignment operator
  CameraDrawer& operator= (CameraDrawer&& other) noexcept;

  void init(const QOpenGLShaderProgram& program,
            const float length,
            const float fovY = 0.785398f, /*~45 deg*/
            const float aspect_ratio = 1.3333f,
            const ColorType& color = ColorType(0, 255, 0, 255));

  void draw();

 private:

  void initFrustum(const QOpenGLShaderProgram& program, const float length,
                   const float half_width, const float half_height,
                   const ColorType& color);

  void initUpTriangle(const QOpenGLShaderProgram& program, const float length,
                      const float half_width, const float half_height,
                      const ColorType& color);

  // Additional data for the triangle
  GLuint tri_vao_;
  GLuint tri_vbo_;
  GLuint tri_ibo_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_DRAWERS_CAMERADRAWER_H_
