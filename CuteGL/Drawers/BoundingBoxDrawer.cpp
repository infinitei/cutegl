/**
 * @file BoundingBoxDrawer.cpp
 * @brief BoundingBoxDrawer
 *
 * @author Abhijit Kundu
 */

#include "BoundingBoxDrawer.h"

namespace CuteGL {

BoundingBoxDrawer::BoundingBoxDrawer() {
}

BoundingBoxDrawer::BoundingBoxDrawer(const QOpenGLShaderProgram& program,
                                     const PositionType& min,
                                     const PositionType& max,
                                     const ColorType& color) {
  init(program, BoundingBoxType(min, max), color);
}

BoundingBoxDrawer::BoundingBoxDrawer(const QOpenGLShaderProgram& program,
                                     const BoundingBoxType& bbx,
                                     const ColorType& color) {
  init(program, bbx, color);
}

BoundingBoxDrawer::BoundingBoxDrawer(const QOpenGLShaderProgram& program,
                                     const std::array<VertexData, 8>& vertices) {
  init(program, vertices);
}

void BoundingBoxDrawer::init(const QOpenGLShaderProgram& program,
                             const BoundingBoxType& bbx,
                             const ColorType& color) {


  std::array<VertexData, 8> vertices;
  vertices[0].position = PositionType(bbx.max().x(), bbx.max().y(), bbx.max().z());
  vertices[1].position = PositionType(bbx.min().x(), bbx.max().y(), bbx.max().z());
  vertices[2].position = PositionType(bbx.min().x(), bbx.min().y(), bbx.max().z());
  vertices[3].position = PositionType(bbx.max().x(), bbx.min().y(), bbx.max().z());

  // Back Verticies positions
  vertices[4].position = PositionType(bbx.max().x(), bbx.max().y(), bbx.min().z());
  vertices[5].position = PositionType(bbx.min().x(), bbx.max().y(), bbx.min().z());
  vertices[6].position = PositionType(bbx.min().x(), bbx.min().y(), bbx.min().z());
  vertices[7].position = PositionType(bbx.max().x(), bbx.min().y(), bbx.min().z());

  for (VertexData& vd : vertices)
    vd.color = color;

  init(program, vertices);
}

void BoundingBoxDrawer::init(const QOpenGLShaderProgram& program,
                             const std::array<VertexData, 8>& vertices) {
  assert(hasValidOpenGLFunctions());

  // Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // 2. Copy our vertices array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);

  glfuncs_->glBufferData(GL_ARRAY_BUFFER,
                         vertices.size() * sizeof(VertexData),
                         vertices.data(), GL_STATIC_DRAW);


  glfuncs_->glGenBuffers(1, &ibo_);
  glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
  {
    typedef GLuint IndexData;
    std::array<IndexData, 24> indices = {
      0, 1,
      1, 2,
      2, 3,
      3, 0,
      4, 5,
      5, 6,
      6, 7,
      7, 4,
      0, 4,
      1, 5,
      2, 6,
      3, 7
    };

    glfuncs_->glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                           indices.size() * sizeof(IndexData),
                           indices.data(),
                           GL_STATIC_DRAW);
  }

  // Set the vertex attributes pointers
  // Position attribute
  const int position_index = program.attributeLocation("vertex_position");
  assert(position_index != -1);
  glfuncs_->glVertexAttribPointer(position_index, 3, GL_FLOAT, GL_FALSE,
                                  sizeof(VertexData),
                                  (void*) offsetof(VertexData, position));
  glfuncs_->glEnableVertexAttribArray(position_index);

  // Color attribute
  const int color_index = program.attributeLocation("vertex_color");
  if (color_index != -1) {
    glfuncs_->glVertexAttribPointer(color_index, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                                    sizeof(VertexData),
                                    (void*) offsetof(VertexData, color));
    glfuncs_->glEnableVertexAttribArray(color_index);
  }

    // Release VAO first
  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void BoundingBoxDrawer::draw() {
  assert(hasValidOpenGLFunctions());
  glfuncs_->glBindVertexArray(vao_);
  glfuncs_->glDrawElements(GL_LINES, 24, GL_UNSIGNED_INT, 0);
  glfuncs_->glBindVertexArray(0);
}

}  // end namespace CuteGL
