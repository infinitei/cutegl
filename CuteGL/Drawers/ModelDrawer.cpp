/**
 * @file ModelDrawer.cpp
 * @brief ModelDrawer
 *
 * @author Abhijit Kundu
 */

#include "ModelDrawer.h"

namespace CuteGL {

ModelDrawer::ModelDrawer() {
}

void ModelDrawer::init(const QOpenGLShaderProgram& program, const std::vector<MeshData>& meshes) {
  mesh_drawers_.reserve(meshes.size());
  for (const MeshData& mesh : meshes)
     mesh_drawers_.emplace_back(program, mesh);
}

void ModelDrawer::draw() {
  for (MeshDrawer& mesh_drawer : mesh_drawers_)
    mesh_drawer.draw();
}

}  // end namespace CuteGL
