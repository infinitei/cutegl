/**
 * @file LineDrawer.h
 * @brief LineDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWES_LINEDRAWER_H_
#define CUTEGL_DRAWES_LINEDRAWER_H_

#include "CuteGL/Drawers/DrawerBase.h"
#include "CuteGL/Core/VertexData.h"
#include <QOpenGLShaderProgram>

namespace CuteGL {

class LineDrawer : public DrawerBase {
 public:
  typedef VertexXYZRGBA VertexData;
  typedef VertexData::PositionType PositionType;
  typedef VertexData::ColorType ColorType;

  typedef std::vector<std::pair<VertexData, VertexData> > LinesVertexContainer;
  typedef std::vector<std::pair<PositionType, PositionType> > LinesPositionContainer;

  LineDrawer(const std::size_t max_num_of_lines = 100);
  LineDrawer(const QOpenGLShaderProgram& program, const std::size_t max_num_of_lines = 100);
  LineDrawer(const QOpenGLShaderProgram& program, const LinesVertexContainer& lines);
  LineDrawer(const QOpenGLShaderProgram& program, const LinesPositionContainer& lines, const ColorType& color);
  LineDrawer(const QOpenGLShaderProgram& program, const LinesPositionContainer& lines, const ColorType& colorA, const ColorType& colorB);

  /// Initialize VBO, VAO
  void init(const QOpenGLShaderProgram& program);


  /// Add a line
  void addLine(const VertexData& from, const VertexData& to);

  /// Update the line vertices
  void updateLineVertices(const LinesVertexContainer& lines);
  void updateLineVertices(const LinesPositionContainer& lines, const ColorType& color);
  void updateLineVertices(const LinesPositionContainer& lines, const ColorType& colorA, const ColorType& colorB);

  /// Draw
  void draw();


 private:
  std::size_t num_of_lines_;
  const std::size_t max_num_of_lines_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_DRAWES_LINEDRAWER_H_
