/**
 * @file GenericDrawer.h
 * @brief GenericDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_GENERICDRAWER_H_
#define CUTEGL_DRAWERS_GENERICDRAWER_H_

#include "CuteGL/Drawers/DrawerBase.h"
#include "CuteGL/Core/VertexData.h"
#include <QOpenGLShaderProgram>

namespace CuteGL {

class GenericDrawer : public DrawerBase {
 public:
  typedef VertexXYZNormalRGBA VertexData;

  /**@brief Default Constructor
   *
   * After this constructor, init() and addVertices() need to be called
   *
   * @param max_num_of_vertices defaults to a large value
   */
  GenericDrawer(const std::size_t max_num_of_vertices = 50000000,
                const GLenum primitive_mode = GL_TRIANGLES);

  /**@brief One shot GenericPrimitiveDrawer constructor
   *
   * @param program
   * @param vertex_data
   */
  GenericDrawer(const QOpenGLShaderProgram& program,
                const std::vector<VertexData>& vertex_data,
                const GLenum primitive_mode = GL_TRIANGLES);

  /// /// Initialize VBO, VAO
  void init(const QOpenGLShaderProgram& program, const std::vector<VertexData>& vertex_data);

  /// Initialize VBO, VAO
  void init(const QOpenGLShaderProgram& program);

  /// Add vertices (No vertices will be added if we cannot all the vertices)
  void addVertices(const std::vector<VertexData>& vertex_data);

  /// Add vertices as many as possible
  void addVerticesAsManyAsPossible(const std::vector<VertexData>& vertex_data);

  /// Draw
  void draw();

  /// @returns the number of vertices that can still be added to the drawer
  inline std::size_t spaceLeft() const {return max_num_of_vertices_ - num_of_vertices_;}

  /// @returns the num_of_vertices currently rendered
  inline std::size_t numOfVertices() const {return num_of_vertices_;}

  /// @returns the maximum num_of_vertices that can be rendered by this instance
  inline std::size_t maxnumOfVertices() const {return max_num_of_vertices_;}

  /// non const reference to primitive mode ()
  /// Can be one of GL_POINTS, GL_TRIANGLES, GL_LINE_LOOP, GL_LINES, GL_TRIANGLE_STRIP etc.
  /// See https://www.opengl.org/sdk/docs/man/html/glDrawArrays.xhtml for all primitive mode options
  GLenum& primitiveMode() {return primitive_mode_;}

  /// const reference to primitive mode
  const GLenum& primitiveMode() const {return primitive_mode_;}

 private:
  GLenum primitive_mode_;
  std::size_t num_of_vertices_;
  const std::size_t max_num_of_vertices_;
};

}  // end namespace CuteGL

#endif // end CUTEGL_DRAWERS_GENERICDRAWER_H_
