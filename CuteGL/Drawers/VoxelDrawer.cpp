/**
 * @file VoxelDrawer.cpp
 * @brief VoxelDrawer
 *
 * @author Abhijit Kundu
 */

#include "VoxelDrawer.h"
#include <cassert>
#include <iostream>

namespace CuteGL {

VoxelDrawer::VoxelDrawer(const std::size_t max_num_of_voxels)
    : num_of_voxels_(0),
      max_num_of_voxels_(max_num_of_voxels) {
}

VoxelDrawer::VoxelDrawer(const QOpenGLShaderProgram& program,
                                         const std::vector<VoxelInfo>& voxels,
                                         const std::size_t max_num_of_voxels)
    : num_of_voxels_(0),
      max_num_of_voxels_(max_num_of_voxels) {
  init(program);
  addVoxels(voxels);
}

void VoxelDrawer::init(const QOpenGLShaderProgram& program) {
  assert(hasValidOpenGLFunctions());

  // Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // Copy our vertices array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER, max_num_of_voxels_ * sizeof(VoxelInfo), NULL, GL_STATIC_DRAW);

  {
    const int center_index = program.attributeLocation("voxel_center");
    assert(center_index != -1);
    glfuncs_->glVertexAttribPointer(center_index, 3, GL_FLOAT, GL_FALSE,
                                    sizeof(VoxelInfo),
                                    (void*) offsetof(VoxelInfo, center));
    glfuncs_->glEnableVertexAttribArray(center_index);
  }
  {
    const int label_index = program.attributeLocation("voxel_label");
    assert(label_index != -1);
    glfuncs_->glVertexAttribIPointer(label_index, 1, GL_INT,
                                    sizeof(VoxelInfo),
                                    (void*) offsetof(VoxelInfo, label));
    glfuncs_->glEnableVertexAttribArray(label_index);
  }
  {
    const int color_index = program.attributeLocation("voxel_color");
    assert(color_index != -1);
    glfuncs_->glVertexAttribPointer(color_index, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                                    sizeof(VoxelInfo),
                                    (void*) offsetof(VoxelInfo, color));
    glfuncs_->glEnableVertexAttribArray(color_index);
  }

  // Release (unbind) all
  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VoxelDrawer::addVoxels(const std::vector<VoxelInfo>& voxels) {
  if ((voxels.size() == 0) || ((num_of_voxels_ + voxels.size()) > max_num_of_voxels_)) {
    std::cerr << "[Warning] Weird number of voxels requested to add" << std::endl;
    return;
  }

  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, num_of_voxels_ * sizeof(VoxelInfo), voxels.size() * sizeof(VoxelInfo), voxels.data());
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  num_of_voxels_ += voxels.size();
}

void VoxelDrawer::addPointsAsManyAsPossible(const std::vector<VoxelInfo>& voxels) {
  if (voxels.size() == 0) {
    std::cerr << "[Warning] Requested to add Zero number of points" << std::endl;
    return;
  }

  const std::size_t space_left = spaceLeft();

  if (voxels.size() > space_left) {
    std::cerr << "[Warning] Capacity Reached: Some points cannot be added" << std::endl;
  }

  const std::size_t num_of_voxels_added = std::min(voxels.size(), space_left);

  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, num_of_voxels_ * sizeof(VoxelInfo), num_of_voxels_added * sizeof(VoxelInfo), voxels.data());
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  num_of_voxels_ += num_of_voxels_added;
}

void VoxelDrawer::draw() {
  assert(hasValidOpenGLFunctions());
  if(num_of_voxels_) {
    glfuncs_->glBindVertexArray(vao_);
    glfuncs_->glDrawArrays(GL_POINTS, 0, num_of_voxels_); // Draw the voxels
    glfuncs_->glBindVertexArray(0);
  }
}

}  // end namespace CuteGL
