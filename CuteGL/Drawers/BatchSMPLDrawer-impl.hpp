/**
 * @file BatchSMPLDrawer-impl.hpp
 * @brief BatchSMPLDrawer-impl
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_BATCHSMPLDRAWER_IMPL_HPP_
#define CUTEGL_DRAWERS_BATCHSMPLDRAWER_IMPL_HPP_

#include "CuteGL/Geometry/ComputeNormals.h"
#include "CuteGL/Utils/OpenGLUtils.h"
#include "CuteGL/IO/ImportSMPL.h"

namespace CuteGL {

template <class SMPLDataType>
BatchSMPLDrawer<SMPLDataType>::BatchSMPLDrawer()
    : batch_id_(0),
      num_of_indices_(0) {
}

template <class SMPLDataType>
BatchSMPLDrawer<SMPLDataType>::~BatchSMPLDrawer() noexcept {
  if (vaos_.size())
    glfuncs_->glDeleteBuffers(vaos_.size(), vaos_.data());

  if (position_vbos_.size())
    glfuncs_->glDeleteBuffers(position_vbos_.size(), position_vbos_.data());
}

template <class SMPLDataType>
BatchSMPLDrawer<SMPLDataType>::BatchSMPLDrawer(const QOpenGLShaderProgram& program, const std::size_t batch_size, const SMPLDataType& smpl_data, const RGBA& color, const int label)
    : batch_id_(0),
      num_of_indices_(0) {
  init(program, batch_size, smpl_data, color, label);
}

template <class SMPLDataType>
BatchSMPLDrawer<SMPLDataType>::BatchSMPLDrawer(const QOpenGLShaderProgram& program, const std::size_t batch_size, const std::string& smpl_model_file, const RGBA& color, const int label)
    : batch_id_(0),
      num_of_indices_(0) {
  init(program, batch_size, smpl_model_file, color, label);
}

template <class SMPLDataType>
void BatchSMPLDrawer<SMPLDataType>::init(const QOpenGLShaderProgram& program, const std::size_t batch_size, const std::string& smpl_model_file, const RGBA& color, const int label) {
  init(program, batch_size, loadSMPLDataFromHDF5<SMPLDataType>(smpl_model_file), color, label);
}

template <class SMPLDataType>
void BatchSMPLDrawer<SMPLDataType>::init(const QOpenGLShaderProgram& program, const std::size_t batch_size, const SMPLDataType& smpl_data, const RGBA& color, const int label) {
  using NormalScalar = typename SMPLDataType::PositionScalar;
  using ColorScalar = typename RGBA::Scalar;
  using LabelScalar = int;

  assert(hasValidOpenGLFunctions());
  assert(batch_size > 0);


  const int position_index = program.attributeLocation("vertex_position");
  const int normal_index = program.attributeLocation("vertex_normal");
  const int color_index = program.attributeLocation("vertex_color");
  const int label_index = program.attributeLocation("vertex_label");

  assert(position_index != -1);

  const Eigen::Index num_of_vertices = smpl_data.template_vertices.rows();
  const GLsizeiptr size_of_positions = (position_index != -1) ? 3 * num_of_vertices * sizeof(PositionScalar) : 0;
  const GLsizeiptr size_of_normals = (normal_index != -1) ? 3 * num_of_vertices * sizeof(NormalScalar) : 0;
  const GLsizeiptr size_of_colors = (color_index != -1) ? 4 * num_of_vertices * sizeof(ColorScalar) : 0;
  const GLsizeiptr size_of_labels = (label_index != -1) ? num_of_vertices * sizeof(LabelScalar) : 0;

  position_vbos_.resize(batch_size, 0);
  vaos_.resize(batch_size, 0);


  // We will first generate all the buffers and then generate vaos and connect attributes

  // Generate all the buffers and upload data
  {
    // Initialize position buffers which will be updated quite often
    {
      glfuncs_->glGenBuffers(batch_size, position_vbos_.data());
      for (std::size_t i = 0; i < batch_size; ++i) {
        glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, position_vbos_[i]);
        glfuncs_->glBufferData(GL_ARRAY_BUFFER, size_of_positions, smpl_data.template_vertices.data(), GL_DYNAMIC_DRAW);
      }
    }

    // Initialize vertex buffer for static attributes: color, normal, labels etc
    {
      glfuncs_->glGenBuffers(1, &vbo_);
      glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
      glfuncs_->glBufferData(GL_ARRAY_BUFFER,
                             size_of_normals + size_of_colors + size_of_labels,
                             nullptr,
                             GL_STATIC_DRAW);

      // Normal attribute
      if (size_of_normals) {
        GLintptr offset = 0;
        typename SMPLDataType::MatrixX3 normals;
        computeNormals(smpl_data.template_vertices, normals, smpl_data.faces);
        assert (normals.size() * sizeof(NormalScalar) == std::size_t(size_of_normals));
        glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_normals, normals.data());
      }

      // Color attribute
      if (size_of_colors) {
        GLintptr offset = size_of_normals;
        Eigen::Matrix<ColorScalar, Eigen::Dynamic, 1> colors = color.replicate(num_of_vertices, 1);
        assert (colors.size() * sizeof(ColorScalar) == std::size_t(size_of_colors));
        glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_colors, colors.data());
      }

      // Label attribute
      if (size_of_labels) {
        GLintptr offset = size_of_normals + size_of_colors;
        Eigen::Matrix<LabelScalar, Eigen::Dynamic, 1> labels(num_of_vertices);
        labels.setConstant(label);
        assert (labels.size() * sizeof(LabelScalar) == std::size_t(size_of_labels));
        glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_labels, labels.data());
      }
    }

    // Initialize element buffer object
    {
      glfuncs_->glGenBuffers(1, &ibo_);
      glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
      num_of_indices_ = smpl_data.faces.size();
      glfuncs_->glBufferData(GL_ELEMENT_ARRAY_BUFFER, smpl_data.faces.size() * sizeof(IndexScalar), smpl_data.faces.data(), GL_STATIC_DRAW);
    }

  }

  // Generate vaos and connect attributes
  {
    // Generate VAOs
    glfuncs_->glGenVertexArrays(batch_size, vaos_.data());

    for (std::size_t i = 0; i < batch_size; ++i) {
      // Bind VAO
      glfuncs_->glBindVertexArray(vaos_[i]);

      // Connect attributes from position_vbos_
      {
        glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, position_vbos_[i]);

        GLintptr offset = 0;
        glfuncs_->glVertexAttribPointer(position_index, 3, GLTraits<PositionScalar>::type, GL_FALSE, 3 * sizeof(PositionScalar), (GLvoid*) offset);
        glfuncs_->glEnableVertexAttribArray(position_index);
      }

      // Connect attributes from vbo_
      {
        glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);

        // Normal attribute
        if (size_of_normals) {
          GLintptr offset = 0;
          glfuncs_->glVertexAttribPointer(normal_index, 3, GLTraits<NormalScalar>::type, GL_FALSE, 3 * sizeof(NormalScalar), (GLvoid*) offset);
          glfuncs_->glEnableVertexAttribArray(normal_index);
        }

        // Color attribute
        if (size_of_colors) {
          GLintptr offset = size_of_normals;
          glfuncs_->glVertexAttribPointer(color_index, 4, GLTraits<ColorScalar>::type, GL_TRUE, 4 * sizeof(ColorScalar), (GLvoid*) offset);
          glfuncs_->glEnableVertexAttribArray(color_index);
        }

        // Label attribute
        if (size_of_labels) {
          GLintptr offset = size_of_normals + size_of_colors;
          glfuncs_->glVertexAttribIPointer(label_index, 1, GLTraits<LabelScalar>::type, 1 * sizeof(LabelScalar), (GLvoid*) offset);
          glfuncs_->glEnableVertexAttribArray(label_index);
        }
      }

      // Connect EBO
      {
        glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
      }


      // Release VAO first
      glfuncs_->glBindVertexArray(0);
      glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
      glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

  }

  // Registers OpenGL vbo with CUDA
  smpl_engine_.registerVBOs(position_vbos_);

  // upload smpl data to CUDA
  smpl_engine_.uploadSMPLdata(smpl_data);
}


template <class SMPLDataType>
template <class SMPLSegmmDataType>
BatchSMPLDrawer<SMPLDataType>::BatchSMPLDrawer(const QOpenGLShaderProgram& program, const std::size_t batch_size, const SMPLDataType& smpl_data, const SMPLSegmmDataType& segm_data)
    : batch_id_(0),
      num_of_indices_(0) {
  init(program, batch_size, smpl_data, segm_data);
}

template <class SMPLDataType>
template <class SMPLSegmmDataType>
void BatchSMPLDrawer<SMPLDataType>::init(const QOpenGLShaderProgram& program, const std::size_t batch_size, const SMPLDataType& smpl_data, const SMPLSegmmDataType& segm_data) {
  assert(batch_size > 0);
  assert(smpl_data.template_vertices.rows() == segm_data.vertex_labels.size());

  using NormalScalar = typename SMPLDataType::PositionScalar;
  using ColorScalar = typename SMPLSegmmDataType::ColorScalar;
  using LabelScalar = typename SMPLSegmmDataType::LabelScalar;

  assert(hasValidOpenGLFunctions());


  const int position_index = program.attributeLocation("vertex_position");
  const int normal_index = program.attributeLocation("vertex_normal");
  const int color_index = program.attributeLocation("vertex_color");
  const int label_index = program.attributeLocation("vertex_label");

  assert(position_index != -1);

  const Eigen::Index num_of_vertices = smpl_data.template_vertices.rows();
  const GLsizeiptr size_of_positions = (position_index != -1) ? 3 * num_of_vertices * sizeof(PositionScalar) : 0;
  const GLsizeiptr size_of_normals = (normal_index != -1) ? 3 * num_of_vertices * sizeof(NormalScalar) : 0;
  const GLsizeiptr size_of_colors = (color_index != -1) ? 4 * num_of_vertices * sizeof(ColorScalar) : 0;
  const GLsizeiptr size_of_labels = (label_index != -1) ? num_of_vertices * sizeof(LabelScalar) : 0;

  position_vbos_.resize(batch_size, 0);
  vaos_.resize(batch_size, 0);


  // We will first generate all the buffers and then generate vaos and connect attributes

  // Generate all the buffers and upload data
  {
    // Initialize position buffers which will be updated quite often
    {
      glfuncs_->glGenBuffers(batch_size, position_vbos_.data());
      for (std::size_t i = 0; i < batch_size; ++i) {
        glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, position_vbos_[i]);
        glfuncs_->glBufferData(GL_ARRAY_BUFFER, size_of_positions, smpl_data.template_vertices.data(), GL_DYNAMIC_DRAW);
      }
    }

    // Initialize vertex buffer for static attributes: color, normal, labels etc
    {
      glfuncs_->glGenBuffers(1, &vbo_);
      glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
      glfuncs_->glBufferData(GL_ARRAY_BUFFER,
                             size_of_normals + size_of_colors + size_of_labels,
                             nullptr,
                             GL_STATIC_DRAW);

      // Normal attribute
      if (size_of_normals) {
        GLintptr offset = 0;
        typename SMPLDataType::MatrixX3 normals;
        computeNormals(smpl_data.template_vertices, normals, smpl_data.faces);
        assert (normals.size() * sizeof(NormalScalar) == std::size_t(size_of_normals));
        glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_normals, normals.data());
      }

      // Color attribute
      if (size_of_colors) {
        GLintptr offset = size_of_normals;
        typename SMPLSegmmDataType::ColorDataContainer colors(num_of_vertices, 4);
        assert (colors.size() * sizeof(ColorScalar) == std::size_t(size_of_colors));
        for (Eigen::Index i= 0; i < num_of_vertices; ++i)
          colors.row(i) = segm_data.color_map.row(segm_data.vertex_labels[i]);
        glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_colors, colors.data());
      }

      // Label attribute
      if (size_of_labels) {
        GLintptr offset = size_of_normals + size_of_colors;
        typename SMPLSegmmDataType::LabelDataContainer labels = segm_data.vertex_labels.array() + 1;
        assert (labels.size() * sizeof(LabelScalar) == std::size_t(size_of_labels));
        glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_labels, labels.data());
      }
    }

    // Initialize element buffer object
    {
      glfuncs_->glGenBuffers(1, &ibo_);
      glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
      num_of_indices_ = smpl_data.faces.size();
      glfuncs_->glBufferData(GL_ELEMENT_ARRAY_BUFFER, smpl_data.faces.size() * sizeof(IndexScalar), smpl_data.faces.data(), GL_STATIC_DRAW);
    }

    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
    glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  }

  // Generate vaos and connect attributes
  {
    // Generate VAOs
    glfuncs_->glGenVertexArrays(batch_size, vaos_.data());

    for (std::size_t i = 0; i < batch_size; ++i) {
      // Bind VAO
      glfuncs_->glBindVertexArray(vaos_[i]);

      // Connect attributes from position_vbos_
      {
        glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, position_vbos_[i]);

        GLintptr offset = 0;
        glfuncs_->glVertexAttribPointer(position_index, 3, GLTraits<PositionScalar>::type, GL_FALSE, 3 * sizeof(PositionScalar), (GLvoid*) offset);
        glfuncs_->glEnableVertexAttribArray(position_index);
      }

      // Connect attributes from vbo_
      {
        glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);

        // Normal attribute
        if (size_of_normals) {
          GLintptr offset = 0;
          glfuncs_->glVertexAttribPointer(normal_index, 3, GLTraits<NormalScalar>::type, GL_FALSE, 3 * sizeof(NormalScalar), (GLvoid*) offset);
          glfuncs_->glEnableVertexAttribArray(normal_index);
        }

        // Color attribute
        if (size_of_colors) {
          GLintptr offset = size_of_normals;
          glfuncs_->glVertexAttribPointer(color_index, 4, GLTraits<ColorScalar>::type, GL_TRUE, 4 * sizeof(ColorScalar), (GLvoid*) offset);
          glfuncs_->glEnableVertexAttribArray(color_index);
        }

        // Label attribute
        if (size_of_labels) {
          GLintptr offset = size_of_normals + size_of_colors;
          glfuncs_->glVertexAttribIPointer(label_index, 1, GLTraits<LabelScalar>::type, 1 * sizeof(LabelScalar), (GLvoid*) offset);
          glfuncs_->glEnableVertexAttribArray(label_index);
        }
      }

      // Connect EBO
      {
        glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
      }


      // Release VAO first
      glfuncs_->glBindVertexArray(0);
      glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
      glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

  }

  // Registers OpenGL vbo with CUDA
  smpl_engine_.registerVBOs(position_vbos_);


  // upload smpl data to CUDA
  smpl_engine_.uploadSMPLdata(smpl_data);
}

template <class SMPLDataType>
BatchSMPLDrawer<SMPLDataType>::BatchSMPLDrawer(const QOpenGLShaderProgram& program, const std::size_t batch_size, const std::string& smpl_model_file, const std::string& smpl_segm_file)
    : batch_id_(0),
      num_of_indices_(0) {
  init(program, batch_size, smpl_model_file, smpl_segm_file);
}

template <class SMPLDataType>
void BatchSMPLDrawer<SMPLDataType>::init(const QOpenGLShaderProgram& program, const std::size_t batch_size, const std::string& smpl_model_file, const std::string& smpl_segmm_file) {
  init(program, batch_size,
       loadSMPLDataFromHDF5<SMPLDataType>(smpl_model_file),
       loadSMPLSegmmDataFromHDF5<SMPLSegmmData<>>(smpl_segmm_file));
}

template <class SMPLDataType>
void BatchSMPLDrawer<SMPLDataType>::draw() {
  assert(hasValidOpenGLFunctions());
  assert(vaos_.size() > batch_id_);
  glfuncs_->glBindVertexArray(vaos_[batch_id_]);
  glfuncs_->glDrawElements(GL_TRIANGLES, num_of_indices_, GL_UNSIGNED_INT, 0);
  glfuncs_->glBindVertexArray(0);
}

}  // namespace CuteGL



#endif // end CUTEGL_DRAWERS_BATCHSMPLDRAWER_IMPL_HPP_
