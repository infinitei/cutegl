/**
 * @file GridDrawer.cpp
 * @brief GridDrawer
 *
 * @author Abhijit Kundu
 */

#include "GridDrawer.h"
#include "CuteGL/Core/OpenGLFunctions.h"
#include <cassert>

namespace CuteGL {

GridDrawer::GridDrawer()
    : num_of_vertices_(0) {
}

GridDrawer::GridDrawer(const QOpenGLShaderProgram& program,
                       const int num_of_subdivisions, const ColorType& color)
    : num_of_vertices_(0) {
  init(program, num_of_subdivisions, color);
}

void GridDrawer::init(const QOpenGLShaderProgram& program,
                      const int num_of_subdivisions,
                      const ColorType& color) {
  assert(hasValidOpenGLFunctions());

  num_of_vertices_ = 4 * (num_of_subdivisions + 1);

  std::vector<VertexData> vertices;
  vertices.reserve(num_of_vertices_);
  const float size = 1.0f;
  for (int i = 0; i <= num_of_subdivisions; ++i) {
    const float pos = size * (2.0f * i / num_of_subdivisions - 1.0f);
    vertices.emplace_back(XYZ(pos, -size, 0.0f), color);
    vertices.emplace_back(XYZ(pos, size, 0.0f), color);
    vertices.emplace_back(XYZ(-size, pos, 0.0f), color);
    vertices.emplace_back(XYZ(size, pos, 0.0f), color);
  }

  // 1. Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // 2. Copy our vertices array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexData), vertices.data(), GL_STATIC_DRAW);

  // 3. Then set the vertex attributes pointers
  // Position attribute
  const int position_index = program.attributeLocation("vertex_position");
  assert(position_index != -1);
  glfuncs_->glVertexAttribPointer(position_index, 3, GL_FLOAT, GL_FALSE,
                        sizeof(VertexData),
                        (void*) offsetof(VertexData, position));
  glfuncs_->glEnableVertexAttribArray(position_index);

  // Color attribute
  const int color_index = program.attributeLocation("vertex_color");
  if (color_index != -1) {
    glfuncs_->glVertexAttribPointer(color_index, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                          sizeof(VertexData),
                          (void*) offsetof(VertexData, color));
    glfuncs_->glEnableVertexAttribArray(color_index);
  }

    // (unbind) all
  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GridDrawer::draw() {
  assert(hasValidOpenGLFunctions());
  assert(num_of_vertices_);
  glfuncs_->glBindVertexArray(vao_);
  glfuncs_->glDrawArrays(GL_LINES, 0, num_of_vertices_);
  glfuncs_->glBindVertexArray(0);
}

}  // end namespace CuteGL
