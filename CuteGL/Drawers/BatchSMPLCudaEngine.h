/**
 * @file BatchSMPLCudaEngine.h
 * @brief BatchSMPLCudaEngine
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_BATCHSMPLCUDAENGINE_H_
#define CUTEGL_DRAWERS_BATCHSMPLCUDAENGINE_H_

#include <Eigen/Core>
#include <CuteGL/Utils/CudaUtils.h>

namespace CuteGL {

template<class SMPLDataType>
struct BatchSMPLCudaEngine {
  typedef typename SMPLDataType::PositionScalar PositionScalar;
  typedef typename SMPLDataType::IndexScalar IndexScalar;
  typedef Eigen::Map<Eigen::Matrix<PositionScalar, Eigen::Dynamic, Eigen::Dynamic> > Params;
  typedef cudaGraphicsResource* CudaGraphicsResourceType;

  struct SerialImpl {};
  struct StreamImpl {};
  struct BatchImpl {};

  BatchSMPLCudaEngine()
    : shape_params(nullptr, 0, 0),
      pose_params(nullptr, 0, 0),
      N_(0),
      K_(0),
      num_shape_params_(0),
      num_pose_params_(0),
      max_joint_depth_(0),
      h_shape_params_(nullptr),
      h_pose_params_(nullptr),
      d_shape_params_(nullptr),
      d_pose_params_(nullptr),
      d_template_vertices_(nullptr),
      d_jr_indices_(nullptr),
      d_jr_values_(nullptr),
      d_shape_displacements_(nullptr),
      d_blend_weights_(nullptr),
      d_blend_jointids_(nullptr),
      d_joint_parents_(nullptr),
      d_joint_levels_(nullptr),
      d_joint_locations_(nullptr),
      d_pre_posed_vertices_(nullptr),
      d_bone_transforms_(nullptr) {
    static_assert(std::is_same<Eigen::Index, int>::value, "Need to set EIGEN_DEFAULT_DENSE_INDEX_TYPE to int");
  }

  ~BatchSMPLCudaEngine();

  void uploadSMPLdata(const SMPLDataType& smpl_data);

  void setCudaDeviceToCurrentGLDevice();

  void registerVBOs(const std::vector<GLuint>& gl_position_buffers);
  void mapVBOs();
  void updateVBOpointers();
  void unmapVBOs();

  void updateShapeAndPose(bool rebind = false) {
    if (rebind) {
      mapVBOs();
      updateVBOpointers();
      updateShapeAndPose(StreamImpl());
      unmapVBOs();
    }
    else {
      updateShapeAndPose(StreamImpl());
    }
  }

  void updateShape(bool rebind = false) {
    if (rebind) {
      mapVBOs();
      updateVBOpointers();
      updateShape(StreamImpl());
      unmapVBOs();
    }
    else {
      updateShape(StreamImpl());
    }
  }

  void updatePose(bool rebind = false) {
    if (rebind) {
      mapVBOs();
      updateVBOpointers();
      updatePose(StreamImpl());
      unmapVBOs();
    }
    else {
      updatePose(StreamImpl());
    }
  }

  Params shape_params;
  Params pose_params;

private:

  void updateShapeAndPose(StreamImpl);
  void updateShapeAndPose(SerialImpl);
  void updateShape(StreamImpl);
  void updatePose(StreamImpl);

  int N_;
  int K_;
  int num_shape_params_;
  int num_pose_params_;
  int max_joint_depth_;

  PositionScalar* h_shape_params_;
  PositionScalar* h_pose_params_;

  PositionScalar* d_shape_params_;
  PositionScalar* d_pose_params_;
  PositionScalar* d_template_vertices_;
  int* d_jr_indices_;
  PositionScalar* d_jr_values_;
  PositionScalar* d_shape_displacements_;
  PositionScalar* d_blend_weights_;
  IndexScalar* d_blend_jointids_;
  IndexScalar* d_joint_parents_;
  IndexScalar* d_joint_levels_;

  // Intermediate variables
  PositionScalar* d_joint_locations_;
  PositionScalar* d_pre_posed_vertices_;
  PositionScalar* d_bone_transforms_;

  std::vector<CudaGraphicsResourceType> cuda_vbo_resources_;
  std::vector<PositionScalar*> cuda_vbo_ptrs_;
};

}  // end namespace CuteGL

#endif // end CUTEGL_DRAWERS_BATCHSMPLCUDAENGINE_H_
