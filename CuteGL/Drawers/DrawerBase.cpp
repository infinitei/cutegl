/**
 * @file DrawerBase.cpp
 * @brief DrawerBase
 *
 * @author Abhijit Kundu
 */

#include "DrawerBase.h"
#include <iostream>
#include <algorithm>
#include <cassert>

namespace CuteGL {

DrawerBase::DrawerBase()
    : vao_(0),
      vbo_(0),
      ibo_(0) {
}

DrawerBase::~DrawerBase() noexcept {
  assert(hasValidOpenGLFunctions());

  if (vao_) {
    glfuncs_->glDeleteVertexArrays(1, &vao_);
  }

  if (vbo_) {
    glfuncs_->glDeleteBuffers(1, &vbo_);
  }

  if (ibo_) {
    glfuncs_->glDeleteBuffers(1, &ibo_);
  }

}

DrawerBase::DrawerBase(DrawerBase&& other) noexcept
     : vao_(other.vao_),
       vbo_(other.vbo_),
       ibo_(other.ibo_) {
  other.vao_ = 0;
  other.vbo_ = 0;
  other.ibo_ = 0;
}

DrawerBase& DrawerBase::operator =(DrawerBase&& other) noexcept {
  vao_ = other.vao_;
  vbo_ = other.vao_;
  ibo_ = other.ibo_;
  other.vao_ = 0;
  other.vbo_ = 0;
  other.ibo_ = 0;
  return *this;
}

}  // end namespace CuteGL
