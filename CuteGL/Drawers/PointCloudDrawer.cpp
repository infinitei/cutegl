/**
 * @file PointCloudDrawer.cpp
 * @brief PointCloudDrawer
 *
 * @author Abhijit Kundu
 */

#include "PointCloudDrawer.h"
#include <iostream>

namespace CuteGL {

PointCloudDrawer::PointCloudDrawer(const std::size_t max_num_of_points)
    : num_of_points_(0),
      max_num_of_points_(max_num_of_points) {
}

PointCloudDrawer::PointCloudDrawer(const QOpenGLShaderProgram& program,
                                   const std::vector<VertexData>& vertex_data,
                                   const std::size_t max_num_of_points)
    : num_of_points_(0),
      max_num_of_points_(max_num_of_points) {
  init(program);
  addPoints(vertex_data);
}

void PointCloudDrawer::init(const QOpenGLShaderProgram& program,
                            const std::vector<VertexData>& vertex_data) {
  init(program);
  addPoints(vertex_data);
}

void PointCloudDrawer::init(const QOpenGLShaderProgram& program) {
  assert(hasValidOpenGLFunctions());

  // Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // Copy our vertices array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER, max_num_of_points_ * sizeof(VertexData), NULL, GL_STATIC_DRAW);

  // Position attribute
  const int position_index = program.attributeLocation("vertex_position");
  assert(position_index != -1);
  glfuncs_->glVertexAttribPointer(position_index, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void*) offsetof(VertexData, position));
  glfuncs_->glEnableVertexAttribArray(position_index);

  // Color attribute
  const int color_index = program.attributeLocation("vertex_color");
  if(color_index != -1) {
    glfuncs_->glVertexAttribPointer(color_index, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexData), (void*) offsetof(VertexData, color));
    glfuncs_->glEnableVertexAttribArray(color_index);
  }

  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void PointCloudDrawer::addPoints(const std::vector<VertexData>& vertex_data) {
  if ((vertex_data.size() == 0)
      || ((num_of_points_ + vertex_data.size()) > max_num_of_points_)) {
    std::cerr << "[Warning] Weird number of points requested to add" << std::endl;
    return;
  }

  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, num_of_points_ * sizeof(VertexData), vertex_data.size() * sizeof(VertexData), vertex_data.data());
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  num_of_points_ += vertex_data.size();
}

void PointCloudDrawer::addPointsAsManyAsPossible(const std::vector<VertexData>& vertex_data) {
  if (vertex_data.size() == 0) {
    std::cerr << "[Warning] Requested to add Zero number of points" << std::endl;
    return;
  }

  const std::size_t space_left = spaceLeft();

  if (vertex_data.size() > space_left) {
    std::cerr << "[Warning] Capacity Reached: Some points cannot be added" << std::endl;
  }

  const std::size_t num_of_points_added = std::min(vertex_data.size(), space_left);

  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, num_of_points_ * sizeof(VertexData), num_of_points_added * sizeof(VertexData), vertex_data.data());
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  num_of_points_ += num_of_points_added;
}

void PointCloudDrawer::draw() {
  assert(hasValidOpenGLFunctions());
  if(num_of_points_) {
    glfuncs_->glBindVertexArray(vao_);
    glfuncs_->glDrawArrays(GL_POINTS, 0, num_of_points_); // Draw the points
    glfuncs_->glBindVertexArray(0);
  }
}

}  // end namespace CuteGL
