/**
 * @file CameraDrawer.cpp
 * @brief CameraDrawer
 *
 * @author Abhijit Kundu
 */

#include "CameraDrawer.h"
#include "CuteGL/Core/VertexData.h"
#include <array>
#include <cassert>


#include <iostream>

namespace CuteGL {

CameraDrawer::CameraDrawer()
    : tri_vao_(0),
      tri_vbo_(0),
      tri_ibo_(0) {
}

CameraDrawer::CameraDrawer(const QOpenGLShaderProgram& program,
                           const float length,
                           const float fovY,
                           const float aspect_ratio,
                           const ColorType& color)
    : tri_vao_(0),
      tri_vbo_(0),
      tri_ibo_(0) {
  init(program, length, fovY, aspect_ratio, color);
}

void CameraDrawer::init(const QOpenGLShaderProgram& program,
                        const float length,
                        const float fovY,
                        const float aspect_ratio,
                        const ColorType& color) {
  assert(hasValidOpenGLFunctions());

  const float half_height = std::tan(fovY * 0.5f) * length;
  const float half_width = aspect_ratio * half_height;

  // Init the camera frustum
  initFrustum(program, length, half_width, half_height, color);

  // Init the up traingle
  initUpTriangle(program, length, half_width, half_height, color);
}

void CameraDrawer::draw() {
  assert(hasValidOpenGLFunctions());
  assert(vao_ != 0);
  glfuncs_->glBindVertexArray(vao_);
  glfuncs_->glDrawElements(GL_LINES, 16, GL_UNSIGNED_INT, 0);
  glfuncs_->glBindVertexArray(0);

  assert(tri_vao_ != 0);
  glfuncs_->glBindVertexArray(tri_vao_);
  glfuncs_->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
  glfuncs_->glBindVertexArray(0);
}

void CameraDrawer::initFrustum(const QOpenGLShaderProgram& program,
                               const float length, const float half_width,
                               const float half_height,
                               const ColorType& color) {

  // Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // Copy our vertices array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  {
    std::array<VertexData, 5> vertices;

    vertices[0].position = PositionType(0, 0, 0);
    vertices[1].position = PositionType(-half_width, half_height, length);
    vertices[2].position = PositionType(half_width, half_height, length);
    vertices[3].position = PositionType(half_width, -half_height, length);
    vertices[4].position = PositionType(-half_width, -half_height, length);

    for (VertexData& vd : vertices)
      vd.color = color;

    glfuncs_->glBufferData(GL_ARRAY_BUFFER,
                           vertices.size() * sizeof(VertexData),
                           vertices.data(), GL_STATIC_DRAW);

  }

  glfuncs_->glGenBuffers(1, &ibo_);
  glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
  {
    typedef GLuint IndexData;
    std::array<IndexData, 16> indices = {
        0, 1,
        0, 2,
        0, 3,
        0, 4,
        1, 2,
        2, 3,
        3, 4,
        4, 1
    };

    glfuncs_->glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                           indices.size() * sizeof(IndexData), indices.data(),
                           GL_STATIC_DRAW);
  }

  // Set the vertex attributes pointers
  // Position attribute
  const int position_index = program.attributeLocation("vertex_position");
  assert(position_index != -1);
  glfuncs_->glVertexAttribPointer(position_index, 3, GL_FLOAT, GL_FALSE,
                                  sizeof(VertexData),
                                  (void*) offsetof(VertexData, position));
  glfuncs_->glEnableVertexAttribArray(position_index);

  // Color attribute
  const int color_index = program.attributeLocation("vertex_color");
  if (color_index != -1) {
    glfuncs_->glVertexAttribPointer(color_index, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                                    sizeof(VertexData),
                                    (void*) offsetof(VertexData, color));
    glfuncs_->glEnableVertexAttribArray(color_index);
  }

    // Release VAO first
  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
  glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

CameraDrawer::~CameraDrawer() noexcept {
  if (tri_vao_)
    glfuncs_->glDeleteVertexArrays(1, &tri_vao_);
  if (tri_vbo_)
    glfuncs_->glDeleteBuffers(1, &tri_vbo_);
  if (tri_ibo_)
    glfuncs_->glDeleteBuffers(1, &tri_ibo_);
}


CameraDrawer::CameraDrawer(CameraDrawer&& other) noexcept
     : DrawerBase(std::move(other)),
       tri_vao_(other.tri_vao_),
       tri_vbo_(other.tri_vbo_),
       tri_ibo_(other.tri_ibo_) {
  other.tri_vao_ = 0;
  other.tri_vbo_ = 0;
  other.tri_ibo_ = 0;
}

CameraDrawer& CameraDrawer::operator =(CameraDrawer&& other) noexcept {
  DrawerBase::operator=(std::move(other));
  tri_vao_ = other.tri_vao_;
  tri_vbo_ = other.tri_vao_;
  tri_ibo_ = other.tri_ibo_;
  other.tri_vao_ = 0;
  other.tri_vbo_ = 0;
  other.tri_ibo_ = 0;
  return *this;
}


void CameraDrawer::initUpTriangle(const QOpenGLShaderProgram& program,
                                  const float length, const float half_width,
                                  const float half_height,
                                  const ColorType& color) {

  std::array<VertexData, 3> vertices;

  const float trig_top = -(half_height + 0.7f * half_height);
  const float trig_bottom = -(half_height + 0.1f * half_height);
  const float trig_half_width = 0.6f * half_width;

  vertices[0].position = PositionType(0.0f, trig_top, length);
  vertices[1].position = PositionType(trig_half_width, trig_bottom, length);
  vertices[2].position = PositionType(-trig_half_width, trig_bottom, length);

  // set the color
  for (VertexData& vd : vertices)
    vd.color = color;

  // 1. Bind VAO
  glfuncs_->glGenVertexArrays(1, &tri_vao_);
  glfuncs_->glBindVertexArray(tri_vao_);

  // 2. Copy our vertices array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &tri_vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, tri_vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexData), vertices.data(), GL_STATIC_DRAW);


  glfuncs_->glGenBuffers(1, &tri_ibo_);
  glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tri_ibo_);

  {
    typedef GLuint IndexData;
    std::array<IndexData, 6> indices = {
        0, 1, 2,
        0, 2, 1
    };

    glfuncs_->glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                           indices.size() * sizeof(IndexData), indices.data(),
                           GL_STATIC_DRAW);
  }


  // Set the vertex attributes pointers
  // Position attribute
  const int position_index = program.attributeLocation("vertex_position");
  assert(position_index != -1);
  glfuncs_->glVertexAttribPointer(position_index, 3, GL_FLOAT, GL_FALSE,
                                  sizeof(VertexData),
                                  (void*) offsetof(VertexData, position));
  glfuncs_->glEnableVertexAttribArray(position_index);

  // Color attribute
  const int color_index = program.attributeLocation("vertex_color");
  if (color_index != -1) {
    glfuncs_->glVertexAttribPointer(color_index, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                                    sizeof(VertexData),
                                    (void*) offsetof(VertexData, color));
    glfuncs_->glEnableVertexAttribArray(color_index);
  }

    // Release VAO first
  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
  glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

}  // end namespace CuteGL

