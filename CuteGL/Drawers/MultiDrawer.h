/**
 * @file MultiDrawer.h
 * @brief MultiDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_MULTIDRAWER_H_
#define CUTEGL_DRAWERS_MULTIDRAWER_H_

#include <Eigen/Geometry>
#include <vector>

namespace CuteGL {

template<class Drawer, class Shader, class Pose = Eigen::Affine3f>
class MultiDrawer {
 public:
  typedef Drawer DrawerType;
  typedef Shader ShaderType;
  typedef Pose PoseType;

  typedef std::vector<DrawerType> Drawers;
  typedef std::vector<PoseType, Eigen::aligned_allocator<PoseType> > Poses;

  MultiDrawer(ShaderType& shader)
      : shader_(shader) {
  }

  /**@brief Add (creates) a drawer item at certain world pose
   *
   * @param pose - model pose (should be convertible to PoseType)
   * @param data - drawer initialization data
   */
  template<class PoseType, class... DrawerData>
  void addItem(const PoseType& pose, const DrawerData&... data) {
    drawers_.emplace_back(shader_.program, data...);
    poses_.emplace_back(pose);
    assert(drawers_.size() == poses_.size());
  }

  void draw() {
    assert(drawers_.size() == poses_.size());
    for (std::size_t i = 0; i < drawers_.size(); ++i) {
      shader_.setModelPose(poses_[i]);
      drawers_[i].draw();
    }
  }

  const Poses& poses() const {return poses_;}
  Poses& poses() {return poses_;}

  /// Remove all drawers and poses
  void clear() {
    drawers_.clear();
    poses_.clear();
  }

  /// Get number of drawers
  std::size_t size() const {
    assert(drawers_.size() == poses_.size());
    return drawers_.size();
  }

 private:
  ShaderType& shader_;
  std::vector<DrawerType> drawers_;
  std::vector<PoseType, Eigen::aligned_allocator<PoseType> > poses_;
};

}  // namespace CuteGL


#endif // end CUTEGL_DRAWERS_MULTIDRAWER_H_
