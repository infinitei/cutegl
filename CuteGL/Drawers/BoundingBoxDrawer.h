/**
 * @file BoundingBoxDrawer.h
 * @brief BoundingBoxDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_BOUNDINGBOXDRAWER_H_
#define CUTEGL_DRAWERS_BOUNDINGBOXDRAWER_H_

#include "CuteGL/Drawers/DrawerBase.h"
#include "CuteGL/Core/VertexData.h"
#include <QOpenGLShaderProgram>
#include <Eigen/Geometry>
#include <array>


namespace CuteGL {

/// A simple Bounding Box Drawer
class BoundingBoxDrawer : public DrawerBase {
 public:
  typedef VertexXYZRGBA VertexData;
  typedef VertexData::PositionType PositionType;
  typedef VertexData::ColorType ColorType;
  typedef Eigen::AlignedBox3f BoundingBoxType;

  BoundingBoxDrawer();
  BoundingBoxDrawer(const QOpenGLShaderProgram& program, const PositionType& min, const PositionType& max, const ColorType& color = ColorType(255,0,0, 255));
  BoundingBoxDrawer(const QOpenGLShaderProgram& program, const BoundingBoxType& bbx, const ColorType& color = ColorType(255,0,0, 255));
  BoundingBoxDrawer(const QOpenGLShaderProgram& program, const std::array<VertexData, 8>& vertices);

  /// Initialize VBO, VAO.
  void init(const QOpenGLShaderProgram& program, const BoundingBoxType& bbx, const ColorType& color = ColorType(255,0,0, 255));
  void init(const QOpenGLShaderProgram& program, const std::array<VertexData, 8>& vertices);

  /// Draw
  void draw();
};

}  // end namespace CuteGL

#endif // end CUTEGL_DRAWERS_BOUNDINGBOXDRAWER_H_
