/**
 * @file AxisDrawer.cpp
 * @brief AxisDrawer
 *
 * @author Abhijit Kundu
 */

#include "AxisDrawer.h"
#include "CuteGL/Core/Config.h"
#include "CuteGL/IO/ImportPLY.h"

namespace CuteGL {

AxisDrawer::AxisDrawer() {
}

void AxisDrawer::init(const QOpenGLShaderProgram& program) {
  const std::string axis_file = CUTEGL_ASSETS_FOLDER "/Axis_bin.ply";
  drawer_.init(program, loadMeshFromPLY(axis_file));
}

void AxisDrawer::draw() {
  drawer_.draw();
}

}  // end namespace CuteGL
