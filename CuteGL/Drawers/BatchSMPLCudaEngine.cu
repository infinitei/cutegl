/**
 * @file BatchSMPLCudaEngine.cu
 * @brief BatchSMPLCudaEngine
 *
 * @author Abhijit Kundu
 */

#if defined __NVCC__
  // Disable the "pointless comparison of unsigned integer with zero" warning
  #pragma diag_suppress 186
  // Disable the "calling a __host__ function from a __host__ __device__ function is not allowed" messages
  #pragma diag_suppress 2739
#endif

#define EIGEN_DEFAULT_DENSE_INDEX_TYPE int
#define EIGEN_USE_GPU

#include "BatchSMPLCudaEngine.h"
#include "CuteGL/Core/SMPLData.h"
#include <float.h>

namespace CuteGL {

namespace detail {

template <typename Scalar, typename IndexScalar>
__inline__ __device__
void compute_wp(const int i, const IndexScalar* parents, Scalar* wp) {
  using Matrix3 = Eigen::Matrix<Scalar, 3, 3, Eigen::RowMajor>;
  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;
  const int Stride = Matrix3::MaxSizeAtCompileTime + Vector3::MaxSizeAtCompileTime;

  Eigen::Map<Matrix3> R_wp(wp + i * Stride);
  Eigen::Map<Vector3> t_wp(wp + i * Stride + Matrix3::MaxSizeAtCompileTime);

  IndexScalar parent = parents[i];
  Matrix3 R_wp_p(wp + parent * Stride);
  Vector3 t_wp_p(wp + parent * Stride + Matrix3::MaxSizeAtCompileTime);

  R_wp = (R_wp_p * R_wp).eval();
  t_wp = (R_wp_p * t_wp + t_wp_p).eval();
}

template <typename T>
__inline__ __device__
void angle_axis_to_rot_mat(const T* angle_axis, T* R) {
  T theta2 = angle_axis[0] * angle_axis[0] + angle_axis[1] * angle_axis[1] + angle_axis[2] * angle_axis[2];

  // Check if near to zero
  if (theta2 > FLT_MIN) {
    const T theta = sqrt(theta2);
    const T wx = angle_axis[0] / theta;
    const T wy = angle_axis[1] / theta;
    const T wz = angle_axis[2] / theta;

    const T costheta = cos(theta);
    const T sintheta = sin(theta);

    R[0] =     costheta   + wx*wx*(1.0 -    costheta);
    R[1] =  wx*wy*(1.0 - costheta)     - wz*sintheta;
    R[2] =  wy*sintheta   + wx*wz*(1.0 -    costheta);

    R[3] =  wz*sintheta   + wx*wy*(1.0 -    costheta);
    R[4] =     costheta   + wy*wy*(1.0 -    costheta);
    R[5] = -wx*sintheta   + wy*wz*(1.0 -    costheta);

    R[6] = -wy*sintheta   + wx*wz*(1.0 -    costheta);
    R[7] =  wx*sintheta   + wy*wz*(1.0 -    costheta);
    R[8] =     costheta   + wz*wz*(1.0 -    costheta);
  } else {
    // Near zero, we switch to using the first order Taylor expansion.
    R[0] =  1.0;
    R[1] = -angle_axis[2];
    R[2] =  angle_axis[1];

    R[3] =  angle_axis[2];
    R[4] =  1.0;
    R[5] = -angle_axis[0];

    R[6] = -angle_axis[1];
    R[7] =  angle_axis[0];
    R[8] = 1.0;
  }
}

} // namespace detail

template<typename Scalar, typename IndexScalar>
__global__
void compute_bone_transforms(const int n, const int max_depth,
                             const IndexScalar* joint_parents,
                             const IndexScalar* joint_levels,
                             const Scalar* joint_locations,
                             const Scalar* pose_params,
                             Scalar* bt) {
  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;
  using Matrix3  = Eigen::Matrix<Scalar, 3, 3, Eigen::RowMajor>;

  const int jid = threadIdx.x;
  if (jid < n) {
    // compute rr n bt
    {
      detail::angle_axis_to_rot_mat(pose_params + 3 * jid, bt + 12 * jid);
      Eigen::Map<Vector3> t_wp(bt + jid * 12 + 9);
      if (jid == 0) {
        t_wp = Vector3(joint_locations + 3 * jid);
      }
      else {
        const IndexScalar pid = joint_parents[jid];
        t_wp = Vector3(joint_locations + 3 * jid) - Vector3(joint_locations + 3 * pid);
      }
    }

    // Compute wp
    {
      for (int i = 1; i <= max_depth; ++i) {
        if (joint_levels[jid] == i)
          detail::compute_wp(jid, joint_parents, bt);
      }
    }

    // compute BT (RW)
    {
      Eigen::Map<Vector3> t_wp(bt + jid * 12 + 9);
      t_wp -= Matrix3(bt + jid * 12) * Vector3(joint_locations + 3 * jid);
    }
  }
}


template<typename Scalar, typename IndexScalar>
__global__
void compute_lbs(const int n, const IndexScalar* blend_jointids, const Scalar* blend_weights, const Scalar* bone_tfms, const Scalar* pre_posed_verts, Scalar* posed_verts) {
  // Assumes blockDim.x > 24 * 12

  // Copy bone_tfms to shared memory since it is small enough
  __shared__ Scalar bone_tfms_smem[24 * 12];
  if (threadIdx.x < 24 * 12) {
    bone_tfms_smem[threadIdx.x ] = bone_tfms[threadIdx.x ];
  }
  __syncthreads();

  using Matrix3  = Eigen::Matrix<Scalar, 3, 3, Eigen::RowMajor>;
  using Vector3  = Eigen::Matrix<Scalar, 3, 1>;
  static const int MatVecStride = Matrix3::MaxSizeAtCompileTime + Vector3::MaxSizeAtCompileTime;
  static const int VecStride = Vector3::MaxSizeAtCompileTime;

  for (int vid = blockIdx.x * blockDim.x + threadIdx.x;
        vid < n;
        vid += blockDim.x * gridDim.x) {

    Vector3 vert = Vector3::Zero();
    Vector3 pre_posed(pre_posed_verts + vid * VecStride);

    for (Eigen::Index j = 0 ; j < 4; ++j) {
      Eigen::Index index = vid * 4  + j;
      IndexScalar jid = blend_jointids[index];
      vert += blend_weights[index] * (Matrix3(bone_tfms_smem + jid * MatVecStride) * pre_posed + Vector3(bone_tfms_smem + jid * MatVecStride + Matrix3::MaxSizeAtCompileTime));
    }
    Eigen::Map<Vector3>(posed_verts + vid * VecStride) = vert;

  }
}

template<typename Scalar, typename IndexScalar>
__global__
void compute_lbs_shfl(const int n, const IndexScalar* blend_jointids, const Scalar* blend_weights, const Scalar* bone_tfms, const Scalar* pre_posed_verts, Scalar* posed_verts) {
  // assumes blockDim.x == 32
  // Assumes blockDim.x > 12 blockDim.y > 24

  // Copy bone_tfms to shared memory since it is small enough
  __shared__ Scalar bone_tfms_smem[24 * 12];
  if (threadIdx.x < 12 && threadIdx.y < 24) {
    int i = threadIdx.y * 12 + threadIdx.x;
    bone_tfms_smem[i] = bone_tfms[i];
  }
  __syncthreads();

  const int tx_by_4 = threadIdx.x / 4;
  const int tx_mod_4 = threadIdx.x % 4;
  const int tx_by_16 = threadIdx.x / 16;
  const int tx_mod_16 = threadIdx.x % 16;

  for (int vid = (blockIdx.x * blockDim.y + threadIdx.y) * 2 + tx_by_16;
      vid < n;
      vid += gridDim.x * blockDim.y * 2) {

    // Get appropiate jid to each thread
    int jid = 0;
    if ( tx_mod_4 == 0) {
      jid = blend_jointids[vid * 4 + tx_by_4 % 4];
    }
    jid = __shfl(jid, tx_by_4 * 4);


    // Get appropiate ppv to each thread
    Scalar ppv = 1;
    if ( tx_mod_16 < 3) {
      ppv = pre_posed_verts[vid * 3 + tx_mod_16];
    }
    ppv = __shfl(ppv, 16 * tx_by_16 + tx_mod_4);

    Scalar tx = ppv * bone_tfms_smem[12*jid + tx_mod_4];
    Scalar ty = ppv * bone_tfms_smem[12*jid + tx_mod_4 + 4];
    Scalar tz = ppv * bone_tfms_smem[12*jid + tx_mod_4 + 8];

    tx += __shfl_down(tx, 2);
    tx += __shfl_down(tx, 1);

    ty += __shfl_down(ty, 2);
    ty += __shfl_down(ty, 1);

    tz += __shfl_down(tz, 2);
    tz += __shfl_down(tz, 1);

    if (tx_mod_4 == 0) {
      Scalar blend_wt = blend_weights[vid * 4 + tx_by_4 % 4];
      tx *= blend_wt;
      ty *= blend_wt;
      tz *= blend_wt;
    }

    tx += __shfl_down(tx, 4);
    tx += __shfl_down(tx, 8);

    ty += __shfl_down(ty, 4);
    ty += __shfl_down(ty, 8);

    tz += __shfl_down(tz, 4);
    tz += __shfl_down(tz, 8);

    if (tx_mod_16 == 0) {
      posed_verts[vid * 3] = tx;
      posed_verts[vid * 3 + 1] = ty;
      posed_verts[vid * 3 + 2] = tz;
    }
  }
}

template <class SMPLDataType>
BatchSMPLCudaEngine<SMPLDataType>::~BatchSMPLCudaEngine() {
  cudaFreeHostIfAllocated(h_shape_params_);
  cudaFreeHostIfAllocated(h_pose_params_);

  cudaFreeIfAllocated(d_shape_params_);
  cudaFreeIfAllocated(d_pose_params_);
  cudaFreeIfAllocated(d_template_vertices_);
  cudaFreeIfAllocated(d_jr_indices_);
  cudaFreeIfAllocated(d_jr_values_);
  cudaFreeIfAllocated(d_shape_displacements_);
  cudaFreeIfAllocated(d_blend_weights_);
  cudaFreeIfAllocated(d_blend_jointids_);
  cudaFreeIfAllocated(d_joint_parents_);
  cudaFreeIfAllocated(d_joint_levels_);
  cudaFreeIfAllocated(d_joint_locations_);
  cudaFreeIfAllocated(d_pre_posed_vertices_);
  cudaFreeIfAllocated(d_bone_transforms_);
}

template <class SMPLDataType>
void BatchSMPLCudaEngine<SMPLDataType>::uploadSMPLdata(const SMPLDataType& smpl_data) {
  CHECK_CUDA(cudaMalloc(&d_template_vertices_, smpl_data.template_vertices.size() * sizeof(PositionScalar)));
  CHECK_CUDA(cudaMalloc(&d_shape_displacements_, smpl_data.shape_displacements.size() * sizeof(PositionScalar)));
  CHECK_CUDA(cudaMalloc(&d_joint_parents_, smpl_data.parents.size() * sizeof(IndexScalar)));

  CHECK_CUDA(cudaMemcpy(d_template_vertices_, smpl_data.template_vertices.data(), smpl_data.template_vertices.size() * sizeof(PositionScalar), cudaMemcpyHostToDevice));
  CHECK_CUDA(cudaMemcpy(d_shape_displacements_, smpl_data.shape_displacements.data(), smpl_data.shape_displacements.size() * sizeof(PositionScalar), cudaMemcpyHostToDevice));
  CHECK_CUDA(cudaMemcpy(d_joint_parents_, smpl_data.parents.data(), smpl_data.parents.size() * sizeof(IndexScalar), cudaMemcpyHostToDevice));

  N_ = smpl_data.template_vertices.rows();
  K_ = smpl_data.blend_weights.cols() - 1;
  num_shape_params_ = smpl_data.shape_displacements.dimension(0);
  num_pose_params_ = 3 * (K_ + 1);

  assert(num_shape_params_ == 10);
  assert(num_pose_params_ == 72);

  // Create sparse matrix for LBS blend weights
  {
    Eigen::Matrix<PositionScalar, Eigen::Dynamic, 4, Eigen::RowMajor> blend_weights(N_, 4);
    Eigen::Matrix<IndexScalar, Eigen::Dynamic, 4, Eigen::RowMajor> blend_jointids(N_, 4);
    for (Eigen::Index i = 0; i < smpl_data.blend_weights.rows(); ++i) {
      Eigen::Index jid = 0;
      for (Eigen::Index j = 0; j < smpl_data.blend_weights.cols(); ++j) {
        PositionScalar weight = smpl_data.blend_weights(i, j);
        if (weight > 0.0) {
          blend_weights(i, jid) = weight;
          blend_jointids(i, jid) = j;
          ++jid;
          if (jid == 4)
            break;
        }
      }
      assert(jid == 4);
    }

    CHECK_CUDA(cudaMalloc(&d_blend_weights_, blend_weights.size() * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMalloc(&d_blend_jointids_, blend_jointids.size() * sizeof(IndexScalar)));
    CHECK_CUDA(cudaMemcpy(d_blend_weights_, blend_weights.data(), blend_weights.size() * sizeof(PositionScalar), cudaMemcpyHostToDevice));
    CHECK_CUDA(cudaMemcpy(d_blend_jointids_, blend_jointids.data(), blend_jointids.size() * sizeof(IndexScalar), cudaMemcpyHostToDevice));
  }

  // create sparse matrix for joint regressor
  {
    Eigen::Matrix<int, Eigen::Dynamic, 32, Eigen::RowMajor> jr_indices((K_ + 1), 32);
    Eigen::Matrix<PositionScalar, Eigen::Dynamic, 32, Eigen::RowMajor> jr_values((K_ + 1), 32);

    jr_indices.setConstant(-1);
    jr_values.setZero();

    for (Eigen::Index r = 0; r < smpl_data.joint_regressor.rows(); ++r) {
      int nz_count = 0;
      for (Eigen::Index c = 0; c < smpl_data.joint_regressor.cols(); ++c) {
        PositionScalar value = smpl_data.joint_regressor(r, c);
        if (value != 0) {
          if (nz_count >= 32)
            throw std::runtime_error("Do not support more than 32 nnz per row");

          jr_indices(r, nz_count) = c;
          jr_values(r, nz_count) = value;
          ++nz_count;
        }
      }
    }

    CHECK_CUDA(cudaMalloc(&d_jr_indices_, jr_indices.size() * sizeof(int)));
    CHECK_CUDA(cudaMalloc(&d_jr_values_, jr_values.size() * sizeof(PositionScalar)));
    CHECK_CUDA(cudaMemcpy(d_jr_indices_, jr_indices.data(), jr_indices.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA(cudaMemcpy(d_jr_values_, jr_values.data(), jr_values.size() * sizeof(PositionScalar), cudaMemcpyHostToDevice));
  }

  // Create joints kinetics tree levels
  {
    Eigen::Matrix<IndexScalar, Eigen::Dynamic, 1> levels(smpl_data.parents.size());
    levels.setZero();
    for (Eigen::Index i = 1; i < smpl_data.parents.size(); ++i)
      levels[i] = levels[smpl_data.parents[i]] + 1;
    max_joint_depth_ = levels.maxCoeff();
    assert(max_joint_depth_ > 0);
    if (max_joint_depth_ >= 32)
      throw std::runtime_error("Do not support kinetics tree with depth more than 32");

    CHECK_CUDA(cudaMalloc(&d_joint_levels_, levels.size() * sizeof(IndexScalar)));
    CHECK_CUDA(cudaMemcpy(d_joint_levels_, levels.data(), levels.size() * sizeof(IndexScalar), cudaMemcpyHostToDevice));
  }


  const int batch_size = cuda_vbo_resources_.size();
  assert(batch_size > 0);

  CHECK_CUDA(cudaMalloc(&d_shape_params_, batch_size * num_shape_params_ * sizeof(PositionScalar)));
  CHECK_CUDA(cudaMalloc(&d_pose_params_, batch_size * num_pose_params_ * sizeof(PositionScalar)));

  CHECK_CUDA(cudaHostAlloc(&h_shape_params_, batch_size * num_shape_params_ * sizeof(PositionScalar), cudaHostAllocWriteCombined));
  CHECK_CUDA(cudaHostAlloc(&h_pose_params_, batch_size * num_pose_params_ * sizeof(PositionScalar), cudaHostAllocWriteCombined));

  // Map the host params as Eigen vector for easy updating
  new (&shape_params) Params(h_shape_params_, num_shape_params_, batch_size);
  new (&pose_params) Params(h_pose_params_, num_pose_params_, batch_size);

  assert(shape_params.size() == num_shape_params_ * batch_size);
  assert(pose_params.size() == num_pose_params_ * batch_size);

  shape_params.setZero();
  pose_params.setZero();

  CHECK_CUDA(cudaMalloc(&d_joint_locations_, batch_size * (K_ + 1) * 3 * sizeof(PositionScalar)));
  CHECK_CUDA(cudaMalloc(&d_pre_posed_vertices_, batch_size * N_ * 3 * sizeof(PositionScalar)));
  CHECK_CUDA(cudaMalloc(&d_bone_transforms_, batch_size * (K_ + 1) * 12 * sizeof(PositionScalar)));

  updateShapeAndPose(true);
}

template<class SMPLDataType>
void BatchSMPLCudaEngine<SMPLDataType>::setCudaDeviceToCurrentGLDevice() {
  unsigned int gl_cuda_device_count;
  int gl_devices[1];
  CHECK_CUDA(cudaGLGetDevices(&gl_cuda_device_count, gl_devices, 1, cudaGLDeviceListAll));
  CHECK_CUDA(cudaSetDevice(gl_devices[0]));
  std::cout << "CUDA Device = " << gl_devices[0] << std::endl;
}

template<class SMPLDataType>
void BatchSMPLCudaEngine<SMPLDataType>::registerVBOs(const std::vector<GLuint>& gl_position_buffers) {
  cuda_vbo_resources_.resize(gl_position_buffers.size());
  cuda_vbo_ptrs_.resize(gl_position_buffers.size());

  for (std::size_t i = 0; i < gl_position_buffers.size(); ++i) {
    CHECK_CUDA(cudaGraphicsGLRegisterBuffer(&cuda_vbo_resources_[i], gl_position_buffers[i], cudaGraphicsRegisterFlagsWriteDiscard));
  }
}

template<class SMPLDataType>
void BatchSMPLCudaEngine<SMPLDataType>::mapVBOs() {
  // map buffer objects
  CHECK_CUDA(cudaGraphicsMapResources(cuda_vbo_resources_.size(), cuda_vbo_resources_.data(), 0));
}

template<class SMPLDataType>
void BatchSMPLCudaEngine<SMPLDataType>::updateVBOpointers() {
  assert(cuda_vbo_ptrs_.size() == cuda_vbo_resources_.size());
  for (std::size_t i = 0; i < cuda_vbo_resources_.size(); ++i) {
    size_t num_bytes;
    CHECK_CUDA(cudaGraphicsResourceGetMappedPointer((void **)&cuda_vbo_ptrs_[i], &num_bytes, cuda_vbo_resources_[i]));
    assert(num_bytes >= 3 * N_ * sizeof(PositionScalar));
  }
}

template<class SMPLDataType>
void BatchSMPLCudaEngine<SMPLDataType>::unmapVBOs() {
  // unmap buffer objects
  CHECK_CUDA(cudaGraphicsUnmapResources(cuda_vbo_resources_.size(), cuda_vbo_resources_.data(), 0));
}

template <int NumShapeParams, typename T>
__global__
void compute_shape_displacements(const int n, const T* d_shape_displacements, const T* d_template_vertices, const T* d_shape_params, T* d_bs_plus_t) {
  __shared__ T smem[NumShapeParams];
  if (threadIdx.x < NumShapeParams)
    smem[threadIdx.x] = d_shape_params[threadIdx.x];
  __syncthreads();

  int i = threadIdx.x + blockIdx.x*blockDim.x;
  if (i < n) {
    T dot = 0;
#pragma unroll
    for (int j=0; j< NumShapeParams; ++j) {
      dot += d_shape_displacements[j*n + i] * smem[j];
    }
    d_bs_plus_t[i] = d_template_vertices[i] + dot;
  }
}

template <typename Scalar>
__inline__ __device__
Scalar warpReduceSum(Scalar val) {
  for (unsigned int offset = warpSize/2; offset > 0; offset /= 2)
    val += __shfl_down(val, offset);
  return val;
}

template <typename T>
__global__
void compute_joint_locations(const int* d_jr_indices, const T* d_jr_values, const T* d_bs_plus_t, T* d_jl) {
  int i = threadIdx.x + blockIdx.x * 32;
  int jr_ind = d_jr_indices[i];

  T dot_prod = 0;
  if (jr_ind >= 0) {
    dot_prod = d_jr_values[i] * d_bs_plus_t[jr_ind*3 + threadIdx.y];
  }
  dot_prod = warpReduceSum(dot_prod);

  if (threadIdx.x == 0) {
    d_jl[blockIdx.x * 3 + threadIdx.y] = dot_prod;
  }
}

template<class SMPLDataType>
void BatchSMPLCudaEngine<SMPLDataType>::updateShapeAndPose(StreamImpl) {
  const int batch_size = cuda_vbo_resources_.size();
  assert(shape_params.size() == num_shape_params_ * batch_size);
  assert(pose_params.size() == num_pose_params_ * batch_size);


  std::vector<cudaStream_t> streams(batch_size);

  for (int i = 0; i < batch_size; ++i) {
    CHECK_CUDA(cudaStreamCreate(&streams[i]));

    PositionScalar* d_sp = d_shape_params_ + i * num_shape_params_;
    PositionScalar* d_pp = d_pose_params_ + i * num_pose_params_;

    PositionScalar* d_ppv = d_pre_posed_vertices_ + i * N_ * 3;
    PositionScalar* d_jl = d_joint_locations_ + i * (K_ + 1) * 3;
    PositionScalar* d_bt = d_bone_transforms_ + i * (K_ + 1) * 12;

    CHECK_CUDA(cudaMemcpyAsync(d_sp, &shape_params(0, i), num_shape_params_ * sizeof(PositionScalar), cudaMemcpyHostToDevice, streams[i]));
    CHECK_CUDA(cudaMemcpyAsync(d_pp, &pose_params(0, i), num_pose_params_ * sizeof(PositionScalar), cudaMemcpyHostToDevice, streams[i]));

    {
      const int n  = 3 * N_;
      const int blocksize = 1024;
      const int gridsize = (n + blocksize - 1) / blocksize;

      // TPBS = T + B * S
      compute_shape_displacements<10><<<gridsize, blocksize, 10 * sizeof(PositionScalar), streams[i]>>>(n, d_shape_displacements_, d_template_vertices_, d_sp, d_ppv);
    }

    // JL = SBPT * JR
    {
      dim3 blockDim(32, 3);
      int gridsize = K_ + 1;
      compute_joint_locations<<<gridsize, blockDim, 0, streams[i]>>>(d_jr_indices_, d_jr_values_, d_ppv, d_jl);
    }

    // compute Bone transforms
    {
      compute_bone_transforms<<<1, 32, 0, streams[i]>>>(K_ + 1, max_joint_depth_, d_joint_parents_, d_joint_levels_, d_jl, d_pp, d_bt);
    }

    //@note  We are not doing pose-blend update

    // Perform LBS with bone transforms and update VBO
    {
      const int blockSize = 1024;
      const int gridsize = (N_ + blockSize - 1) / blockSize;
      compute_lbs<<<gridsize, blockSize, 24 * 12 * sizeof(PositionScalar), streams[i]>>>(N_, d_blend_jointids_, d_blend_weights_, d_bt, d_ppv, cuda_vbo_ptrs_[i]);
    }

    CHECK_CUDA(cudaStreamDestroy(streams[i]));
  }
}

template<class SMPLDataType>
void BatchSMPLCudaEngine<SMPLDataType>::updateShapeAndPose(SerialImpl) {
  const int batch_size = cuda_vbo_resources_.size();
  assert(shape_params.size() == num_shape_params_ * batch_size);
  assert(pose_params.size() == num_pose_params_ * batch_size);

  CHECK_CUDA(cudaMemcpy(d_shape_params_, shape_params.data(), batch_size * num_shape_params_ * sizeof(PositionScalar), cudaMemcpyHostToDevice));
  CHECK_CUDA(cudaMemcpy(d_pose_params_, pose_params.data(), batch_size * num_pose_params_ * sizeof(PositionScalar), cudaMemcpyHostToDevice));


  for (int i = 0; i < batch_size; ++i) {
    PositionScalar* d_sp = d_shape_params_ + i * num_shape_params_;
    PositionScalar* d_pp = d_pose_params_ + i * num_pose_params_;

    PositionScalar* d_ppv = d_pre_posed_vertices_ + i * N_ * 3;
    PositionScalar* d_jl = d_joint_locations_ + i * (K_ + 1) * 3;
    PositionScalar* d_bt = d_bone_transforms_ + i * (K_ + 1) * 12;

    {
      const int n  = 3 * N_;
      const int blocksize = 1024;
      const int gridsize = (n + blocksize - 1) / blocksize;

      // TPBS = T + B * S
      compute_shape_displacements<10><<<gridsize, blocksize>>>(n, d_shape_displacements_, d_template_vertices_, d_sp, d_ppv);
    }

    // JL = SBPT * JR
    {
      dim3 blockDim(32, 3);
      int gridsize = K_ + 1;
      compute_joint_locations<<<gridsize, blockDim>>>(d_jr_indices_, d_jr_values_, d_ppv, d_jl);
    }

    // compute Bone transforms
    {
      compute_bone_transforms<<<1, 32>>>(K_ + 1, max_joint_depth_, d_joint_parents_, d_joint_levels_, d_jl, d_pp, d_bt);
    }

    //@note  We are not doing pose-blend update

    // Perform LBS with bone transforms and update VBO
    {
      const int blockSize = 1024;
      const int gridsize = (N_ + blockSize - 1) / blockSize;
      compute_lbs<<<gridsize, blockSize>>>(N_, d_blend_jointids_, d_blend_weights_, d_bt, d_ppv, cuda_vbo_ptrs_[i]);
    }
  }
}


template<class SMPLDataType>
void BatchSMPLCudaEngine<SMPLDataType>::updateShape(StreamImpl) {
  const int batch_size = cuda_vbo_resources_.size();
  assert(shape_params.size() == num_shape_params_ * batch_size);


  std::vector<cudaStream_t> streams(batch_size);

  for (int i = 0; i < batch_size; ++i) {
    CHECK_CUDA(cudaStreamCreate(&streams[i]));

    PositionScalar* d_sp = d_shape_params_ + i * num_shape_params_;
    PositionScalar* d_pp = d_pose_params_ + i * num_pose_params_;

    PositionScalar* d_ppv = d_pre_posed_vertices_ + i * N_ * 3;
    PositionScalar* d_jl = d_joint_locations_ + i * (K_ + 1) * 3;
    PositionScalar* d_bt = d_bone_transforms_ + i * (K_ + 1) * 12;

    CHECK_CUDA(cudaMemcpyAsync(d_sp, &shape_params(0, i), num_shape_params_ * sizeof(PositionScalar), cudaMemcpyHostToDevice, streams[i]));

    {
      const int n  = 3 * N_;
      const int blocksize = 1024;
      const int gridsize = (n + blocksize - 1) / blocksize;

      // TPBS = T + B * S
      compute_shape_displacements<10><<<gridsize, blocksize, 10 * sizeof(PositionScalar), streams[i]>>>(n, d_shape_displacements_, d_template_vertices_, d_sp, d_ppv);
    }

    // JL = SBPT * JR
    {
      dim3 blockDim(32, 3);
      int gridsize = K_ + 1;
      compute_joint_locations<<<gridsize, blockDim, 0, streams[i]>>>(d_jr_indices_, d_jr_values_, d_ppv, d_jl);
    }

    // compute Bone transforms
    {
      compute_bone_transforms<<<1, 32, 0, streams[i]>>>(K_ + 1, max_joint_depth_, d_joint_parents_, d_joint_levels_, d_jl, d_pp, d_bt);
    }

    //@note  We are not doing pose-blend update

    // Perform LBS with bone transforms and update VBO
    {
      const int blockSize = 1024;
      const int gridsize = (N_ + blockSize - 1) / blockSize;
      compute_lbs<<<gridsize, blockSize, 24 * 12 * sizeof(PositionScalar), streams[i]>>>(N_, d_blend_jointids_, d_blend_weights_, d_bt, d_ppv, cuda_vbo_ptrs_[i]);
    }

    CHECK_CUDA(cudaStreamDestroy(streams[i]));
  }
}

template<class SMPLDataType>
void BatchSMPLCudaEngine<SMPLDataType>::updatePose(StreamImpl) {
  const int batch_size = cuda_vbo_resources_.size();
  assert(pose_params.size() == num_pose_params_ * batch_size);


  std::vector<cudaStream_t> streams(batch_size);

  for (int i = 0; i < batch_size; ++i) {
    CHECK_CUDA(cudaStreamCreate(&streams[i]));

    PositionScalar* d_pp = d_pose_params_ + i * num_pose_params_;

    PositionScalar* d_ppv = d_pre_posed_vertices_ + i * N_ * 3;
    PositionScalar* d_jl = d_joint_locations_ + i * (K_ + 1) * 3;
    PositionScalar* d_bt = d_bone_transforms_ + i * (K_ + 1) * 12;

    CHECK_CUDA(cudaMemcpyAsync(d_pp, &pose_params(0, i), num_pose_params_ * sizeof(PositionScalar), cudaMemcpyHostToDevice, streams[i]));

    // compute Bone transforms
    {
      compute_bone_transforms<<<1, 32, 0, streams[i]>>>(K_ + 1, max_joint_depth_, d_joint_parents_, d_joint_levels_, d_jl, d_pp, d_bt);
    }

    //@note  We are not doing pose-blend update

    // Perform LBS with bone transforms and update VBO
    {
      const int blockSize = 1024;
      const int gridsize = (N_ + blockSize - 1) / blockSize;
      compute_lbs<<<gridsize, blockSize, 24 * 12 * sizeof(PositionScalar), streams[i]>>>(N_, d_blend_jointids_, d_blend_weights_, d_bt, d_ppv, cuda_vbo_ptrs_[i]);
    }

    CHECK_CUDA(cudaStreamDestroy(streams[i]));
  }
}

// Instantiate for SMPLData<float>
template class BatchSMPLCudaEngine<SMPLData<float> >;

// TODO: For some reason this fails to compile
//template class SMPLCuda<SMPLData<double> >;

}  // end namespace CuteGL
