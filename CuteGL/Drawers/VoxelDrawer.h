/**
 * @file VoxelDrawer.h
 * @brief VoxelDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_VOXELDRAWER_H_
#define CUTEGL_DRAWERS_VOXELDRAWER_H_

#include "CuteGL/Drawers/DrawerBase.h"
#include <Eigen/Core>
#include <QOpenGLShaderProgram>

namespace CuteGL {

struct VoxelInfo {
  typedef Eigen::Vector3f PositionType;
  typedef Eigen::Matrix<unsigned char, 4, 1> ColorType;

  VoxelInfo()
      : center(PositionType::Zero()),
        label(0),
        color(25, 25, 25, 255) {
  }

  template<class DerivedA, class DerivedB>
  VoxelInfo(const Eigen::MatrixBase<DerivedA>& voxel_center, int voxel_label,
            const Eigen::MatrixBase<DerivedB>& texture)
      : center(voxel_center),
        label(voxel_label),
        color(texture) {
  }

  PositionType center;
  int label;
  ColorType color;
};

class VoxelDrawer : public DrawerBase {
 public:
  VoxelDrawer(const std::size_t max_num_of_voxels = 50000000);

  VoxelDrawer(const QOpenGLShaderProgram& program,
                      const std::vector<VoxelInfo>& voxels,
                      const std::size_t max_num_of_voxels = 50000000);

  /// Initialize VBO, VAO.
  void init(const QOpenGLShaderProgram& program);

  /// Add points (No points will be added if we cannot all the points)
  void addVoxels(const std::vector<VoxelInfo>& voxels);

  /// Add points as many as possible
  void addPointsAsManyAsPossible(const std::vector<VoxelInfo>& voxels);

  /// Draw
  void draw();

  /// @returns the number of voxels that can still be added to the drawer
  inline std::size_t spaceLeft() const {return max_num_of_voxels_ - num_of_voxels_;}

  /// @returns the num of voxels currently rendered
  inline std::size_t numOfVoxels() const {return num_of_voxels_;}

  /// @returns the maximum num o voxels that can be rendered by this instance
  inline std::size_t maxNumOfVoxels() const {return max_num_of_voxels_;}

 private:
  std::size_t num_of_voxels_;
  const std::size_t max_num_of_voxels_;
};
}  // end namespace CuteGL

#endif // end CUTEGL_DRAWERS_VOXELDRAWER_H_
