/**
 * @file SMPLDrawer.h
 * @brief SMPLDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_SMPLDRAWER_H_
#define CUTEGL_DRAWERS_SMPLDRAWER_H_

#include "CuteGL/Drawers/SMPLCudaEngine.h"
#include "CuteGL/Core/SMPLData.h"
#include "CuteGL/Core/VertexData.h"
#include "CuteGL/Drawers/DrawerBase.h"
#include <QOpenGLShaderProgram>

namespace CuteGL {

template <class SMPLDataType>
class SMPLDrawer : public DrawerBase {
 public:
  using PositionScalar = typename SMPLDataType::PositionScalar;
  using IndexScalar = typename SMPLDataType::IndexScalar;
  using SMPLEngine = SMPLCudaEngine<SMPLDataType>;
  using ParamType = typename SMPLEngine::ParamType;

  /// Default constructor. Should use init() to actually initialize
  SMPLDrawer();

  ~SMPLDrawer() noexcept;

  /// One shot constructor
  SMPLDrawer(const QOpenGLShaderProgram& program, const SMPLDataType& smpl_data, const RGBA& color = RGBA(192, 192, 192, 255), const int label = 1);
  void init(const QOpenGLShaderProgram& program, const SMPLDataType& smpl_data, const RGBA& color = RGBA(192, 192, 192, 255), const int label = 1);

  SMPLDrawer(const QOpenGLShaderProgram& program, const std::string& smpl_model_file, const RGBA& color = RGBA(192, 192, 192, 255), const int label = 1);
  void init(const QOpenGLShaderProgram& program, const std::string& smpl_model_file, const RGBA& color = RGBA(192, 192, 192, 255), const int label = 1);

  template<class SMPLSegmmDataType>
  SMPLDrawer(const QOpenGLShaderProgram& program, const SMPLDataType& smpl_data, const SMPLSegmmDataType& segm_data);
  template<class SMPLSegmmDataType>
  void init(const QOpenGLShaderProgram& program, const SMPLDataType& smpl_data, const SMPLSegmmDataType& segm_data);

  SMPLDrawer(const QOpenGLShaderProgram& program, const std::string& smpl_model_file, const std::string& smpl_segm_file);
  void init(const QOpenGLShaderProgram& program, const std::string& smpl_model_file, const std::string& smpl_segm_file);


  /// Draw
  void draw();


  inline void updateShape() {smpl_engine_.updateShape();}
  inline void updatePose() {smpl_engine_.updatePose();}
  inline void updateShapeAndPose() {smpl_engine_.updateShapeAndPose();}

  ParamType& shape() {return smpl_engine_.shape;}
  const ParamType& shape() const {return smpl_engine_.shape;}

  ParamType& pose() {return smpl_engine_.pose;}
  const ParamType& pose() const {return smpl_engine_.pose;}


 private:
  // We use separate vbo for position attribute.
  // For other attributes we use the inherited vbo_ from DrawerBase
  GLuint position_vbo_;

  std::size_t num_of_indices_;
  SMPLEngine smpl_engine_;
};

}  // end namespace CuteGL

#include "CuteGL/Drawers/SMPLDrawer-impl.hpp"

#endif // end CUTEGL_DRAWERS_SMPLDRAWER_H_
