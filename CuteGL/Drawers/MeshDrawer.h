/**
 * @file MeshDrawer.h
 * @brief MeshDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_MESHDRAWER_H_
#define CUTEGL_DRAWERS_MESHDRAWER_H_

#include "CuteGL/Drawers/DrawerBase.h"
#include "CuteGL/Core/MeshData.h"
#include <QOpenGLShaderProgram>

namespace CuteGL {

class MeshDrawer : public DrawerBase {
 public:

  /// Default constructor. Should use init() to actually initialize
  MeshDrawer();

  /// One shot constructor
  MeshDrawer(const QOpenGLShaderProgram& program, const MeshData& mesh_data);

  /// One shot constructor
  template<class PS, class NS, class CS, class LS, class IS>
  MeshDrawer(const QOpenGLShaderProgram& program, const Mesh<PS, NS, CS, LS, IS>& mesh);

  /// Setup OpenGL buffers and send them to GPU
  void init(const QOpenGLShaderProgram& program, const MeshData& mesh_data);

  /// Setup OpenGL buffers and send them to GPU
  template<class PS, class NS, class CS, class LS, class IS>
  void init(const QOpenGLShaderProgram& program, const Mesh<PS, NS, CS, LS, IS>& mesh);

  template <class Derived>
  void updateVBO(const Eigen::MatrixBase<Derived>& vbo_data, GLintptr offset = 0);

  /// Update the mesh vertices (but number of vertices should be same as the mesh it was initialized with)
  void updateMeshVertices(const std::vector<MeshData::VertexData>& vertices);

  /// Draw
  void draw();

 private:
  std::size_t num_of_indices_;
};

}  // end namespace CuteGL

#include "CuteGL/Drawers/MeshDrawer-impl.hpp"

#endif // end CUTEGL_DRAWERS_MESHDRAWER_H_
