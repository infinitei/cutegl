/**
 * @file BatchSMPLDrawer.cpp
 * @brief BatchSMPLDrawer
 *
 * @author Abhijit Kundu
 */

#include "BatchSMPLDrawer.h"
#include "CuteGL/Core/SMPLData.h"

namespace CuteGL {

// Instantiate for SMPLData<float>
template class BatchSMPLDrawer<SMPLData<float> >;

}  // end namespace CuteGL


