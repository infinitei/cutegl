/**
 * @file InstancedVoxelDrawer.h
 * @brief InstancedVoxelDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_INSTANCEDVOXELDRAWER_H_
#define CUTEGL_DRAWERS_INSTANCEDVOXELDRAWER_H_

#include "CuteGL/Drawers/DrawerBase.h"
#include <Eigen/Core>
#include <QOpenGLShaderProgram>

namespace CuteGL {

struct VoxelInfo {
  typedef Eigen::Vector3f PositionType;
  typedef Eigen::Matrix<unsigned char, 4, 1> ColorType;

  VoxelInfo()
      : center(PositionType::Zero()),
        label(0),
        color(25, 25, 25, 255) {
  }

  template<class DerivedA, class DerivedB>
  VoxelInfo(const Eigen::MatrixBase<DerivedA>& voxel_center, int voxel_label,
            const Eigen::MatrixBase<DerivedB>& texture)
      : center(voxel_center),
        label(voxel_label),
        color(texture) {
  }

  PositionType center;
  int label;
  ColorType color;
};

class InstancedVoxelDrawer : public DrawerBase {
 public:
  InstancedVoxelDrawer(const std::size_t max_num_of_voxels = 50000000);
  ~InstancedVoxelDrawer() noexcept;

  InstancedVoxelDrawer(const QOpenGLShaderProgram& program,
              const std::vector<VoxelInfo>& voxels,
              const std::size_t max_num_of_voxels = 50000000);

  /// Initialize VBO, VAO.
  void init(const QOpenGLShaderProgram& program, const float voxel_size = 1.0f);

  void addNewVoxels(const std::vector<VoxelInfo>& voxels);

  void setVoxelSize(const float voxel_size = 1.0f);

  /// Draw
  void draw();

 private:
  GLuint instance_vbo_;
  std::size_t num_of_voxels_;
  const std::size_t max_num_of_voxels_;
};

}  // end namespace CuteGL

#endif // end CUTEGL_DRAWERS_INSTANCEDVOXELDRAWER_H_
