/**
 * @file MeshDrawer.cpp
 * @brief MeshDrawer
 *
 * @author Abhijit Kundu
 */

#include "MeshDrawer.h"
#include <iostream>

namespace CuteGL {

MeshDrawer::MeshDrawer()
    : num_of_indices_(0) {
}

MeshDrawer::MeshDrawer(const QOpenGLShaderProgram& program,
                       const MeshData& mesh_data)
    : num_of_indices_(0) {
  init(program, mesh_data);
}

void MeshDrawer::init(const QOpenGLShaderProgram& program, const MeshData& mesh_data) {
  assert(hasValidOpenGLFunctions());

  /// type of vertex
  typedef MeshData::VertexData VertexData;
  typedef MeshData::IndexData IndexData;

  // Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // 2. Copy our vertices array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER,
                         mesh_data.vertices.size() * sizeof(VertexData),
                         mesh_data.vertices.data(),
                         GL_STATIC_DRAW);

  // Position attribute
  const int position_index = program.attributeLocation("vertex_position");
  assert(position_index != -1);
  glfuncs_->glVertexAttribPointer(position_index, 3, GL_FLOAT, GL_FALSE,
                                  sizeof(VertexData),
                                  (void*) offsetof(VertexData, position));
  glfuncs_->glEnableVertexAttribArray(position_index);

  // Normal attribute
  const int normal_index = program.attributeLocation("vertex_normal");
  if (normal_index != -1) {
    glfuncs_->glVertexAttribPointer(normal_index, 3, GL_FLOAT, GL_FALSE,
                                    sizeof(VertexData),
                                    (void*) offsetof(VertexData, normal));
    glfuncs_->glEnableVertexAttribArray(normal_index);
  }

  // Color attribute
  const int color_index = program.attributeLocation("vertex_color");
  if (color_index != -1) {
    glfuncs_->glVertexAttribPointer(color_index, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                                    sizeof(VertexData),
                                    (void*) offsetof(VertexData, color));
    glfuncs_->glEnableVertexAttribArray(color_index);
  }

  glfuncs_->glGenBuffers(1, &ibo_);
  glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
  num_of_indices_ = mesh_data.faces.size();
  glfuncs_->glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                         mesh_data.faces.size() * sizeof(IndexData),
                         mesh_data.faces.data(),
                         GL_STATIC_DRAW);

  // Release VAO first
  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void MeshDrawer::updateMeshVertices(const std::vector<MeshData::VertexData>& vertices) {
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  int buffer_size = 0;
  glfuncs_->glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &buffer_size);
  if(std::size_t(buffer_size) != vertices.size() * sizeof(MeshData::VertexData)) {
    std::cerr << "[Warning] Vertex buffer size does not match." << std::endl;
    glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
    return;
  }

  glfuncs_->glBufferSubData(GL_ARRAY_BUFFER,
                            0,
                            vertices.size() * sizeof(MeshData::VertexData),
                            vertices.data());
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void MeshDrawer::draw() {
  assert(hasValidOpenGLFunctions());
  glfuncs_->glBindVertexArray(vao_);
  glfuncs_->glDrawElements(GL_TRIANGLES, num_of_indices_, GL_UNSIGNED_INT, 0);
  glfuncs_->glBindVertexArray(0);
}

}  // end namespace CuteGL
