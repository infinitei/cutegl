/**
 * @file ModelDrawer.h
 * @brief ModelDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_MODELDRAWER_H_
#define CUTEGL_DRAWERS_MODELDRAWER_H_

#include "CuteGL/Drawers/MeshDrawer.h"

namespace CuteGL {

class ModelDrawer {
 public:
  ModelDrawer();

  template<class... DrawerData>
  ModelDrawer(const QOpenGLShaderProgram& program, const DrawerData&... data) {
    init(program, data...);
  }

  /// Initialize the geometry from a set of meshes
  void init(const QOpenGLShaderProgram& program, const std::vector<MeshData>& meshes);

  /// Initialize the geometry for a single Mesh data (e.g MeshaData or Mesh<>)
  template<class... DrawerData>
  void init(const QOpenGLShaderProgram& program, const DrawerData&... data) {
    mesh_drawers_.emplace_back(program, data...);
  }

  /// Draws all the meshes
  void draw();

 private:
  std::vector<MeshDrawer> mesh_drawers_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_DRAWERS_MODELDRAWER_H_
