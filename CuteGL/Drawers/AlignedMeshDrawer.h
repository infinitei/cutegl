/**
 * @file AlignedMeshDrawer.h
 * @brief AlignedMeshDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_ALIGNEDMESHDRAWER_H_
#define CUTEGL_DRAWERS_ALIGNEDMESHDRAWER_H_

#include "CuteGL/Drawers/DrawerBase.h"
#include "CuteGL/Core/MeshData.h"
#include <QOpenGLShaderProgram>

namespace CuteGL {

/// Vertex with position, 8UC3 RGB color attribute, and index (0-5) for normal (Axis aligned Mesh)
struct AlignedMeshVertexData {
	typedef Eigen::Matrix<Eigen::half, 3, 1> PositionType;
	typedef Eigen::Matrix<unsigned char, 3, 1> ColorType;
	typedef unsigned char NormalIndex;


	AlignedMeshVertexData()
		: position(),
		color(),
		normal_index(0) {
	}

	template<class DerivedP, class DerivedC>
	AlignedMeshVertexData(const Eigen::MatrixBase<DerivedP>& pos,
		const Eigen::MatrixBase<DerivedC>& col,
		const NormalIndex normal_id)
		: position(pos.template cast<PositionType::Scalar>()),
		color(col),
		normal_index(normal_id) {
	}

	PositionType position;
	ColorType color;
	NormalIndex normal_index;
};

struct AlignedMeshData {
	typedef AlignedMeshVertexData VertexData;
	typedef unsigned int IndexData;

	std::vector<VertexData> vertices;
	std::vector<IndexData> indices;
};

class AlignedMeshDrawer : public DrawerBase {
 public:

  /// Default constructor. Should use init() to actually initialize
  AlignedMeshDrawer();

  /// One shot constructor
  AlignedMeshDrawer(const QOpenGLShaderProgram& program, const AlignedMeshData& mesh_data);

  /// Setup OpenGL buffers and send them to GPU
  void init(const QOpenGLShaderProgram& program, const AlignedMeshData& mesh_data);

  /// Draw
  void draw();

 private:
  std::size_t num_of_indices_;
};

}  // end namespace CuteGL

#endif // end CUTEGL_DRAWERS_ALIGNEDMESHDRAWER_H_
