/**
 * @file StaticOpenGLFunctions.h
 * @brief StaticOpenGLFunctions
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_CORE_STATICOPENGLFUNCTIONS_H_
#define CUTEGL_CORE_STATICOPENGLFUNCTIONS_H_

#include "CuteGL/Core/OpenGLFunctions.h"

namespace CuteGL {

class StaticOpenGLFunctions {
 public:
  StaticOpenGLFunctions();

  /// Set OpenGl functions
  static void setOpenGLFunctions(OpenGLFunctions* glfuncs);

 protected:
  static OpenGLFunctions* glfuncs_;  ///< OpenGL Function resolution (non owned)

  /// return true if the glfuncs_ is ready to use
  static bool hasValidOpenGLFunctions();
};

}  // end namespace CuteGL
#endif // end CUTEGL_CORE_STATICOPENGLFUNCTIONS_H_
