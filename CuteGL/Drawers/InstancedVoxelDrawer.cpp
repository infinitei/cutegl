/**
 * @file InstancedVoxelDrawer.cpp
 * @brief InstancedVoxelDrawer
 *
 * @author Abhijit Kundu
 */

#include "InstancedVoxelDrawer.h"
#include "CuteGL/Core/VertexData.h"
#include <cassert>
#include <array>
#include <iostream>

namespace CuteGL {

InstancedVoxelDrawer::InstancedVoxelDrawer(const std::size_t max_num_of_voxels) :
    instance_vbo_(0),
    num_of_voxels_(0),
    max_num_of_voxels_(max_num_of_voxels) {
}

InstancedVoxelDrawer::~InstancedVoxelDrawer() noexcept {
  if (instance_vbo_)
    glfuncs_->glDeleteBuffers(1, &instance_vbo_);
}

InstancedVoxelDrawer::InstancedVoxelDrawer(const QOpenGLShaderProgram& program,
                         const std::vector<VoxelInfo>& voxels,
                         const std::size_t max_num_of_voxels)
    : instance_vbo_(0),
      num_of_voxels_(0),
      max_num_of_voxels_(max_num_of_voxels) {
  init(program);
  addNewVoxels(voxels);
}

void InstancedVoxelDrawer::init(const QOpenGLShaderProgram& program, const float voxel_size) {
  assert(hasValidOpenGLFunctions());

  typedef VertexXYZNormal VertexData;

  // Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // Copy our Instances array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &instance_vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, instance_vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER, max_num_of_voxels_ * sizeof(VoxelInfo), NULL, GL_STATIC_DRAW);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Copy our vertices array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(VertexData), NULL, GL_STATIC_DRAW);

  // Then set the vertex attributes pointers

  // Position attribute
  {
    const int position_index = program.attributeLocation("vertex_position");
    assert(position_index != -1);
    glfuncs_->glVertexAttribPointer(position_index, 3, GL_FLOAT, GL_FALSE,
                          sizeof(VertexData),
                          (void*) offsetof(VertexData, position));
    glfuncs_->glEnableVertexAttribArray(position_index);
  }

  // Normal attribute
  {
    const int normal_index = program.attributeLocation("vertex_normal");
    if (normal_index != -1) {
      glfuncs_->glVertexAttribPointer(normal_index, 3, GL_FLOAT, GL_FALSE,
                            sizeof(VertexData),
                            (void*) offsetof(VertexData, normal));
      glfuncs_->glEnableVertexAttribArray(normal_index);
    }
  }

  using IndexData = unsigned int;
  std::vector<IndexData> indices;
  for(unsigned int i =0; i < 6; ++i) {
    unsigned int vertex_index = i * 4;
    indices.emplace_back(vertex_index + 0);
    indices.emplace_back(vertex_index + 1);
    indices.emplace_back(vertex_index + 2);

    indices.emplace_back(vertex_index + 2);
    indices.emplace_back(vertex_index + 3);
    indices.emplace_back(vertex_index + 0);
  }

  glfuncs_->glGenBuffers(1, &ibo_);
  glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
  glfuncs_->glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                         indices.size() * sizeof(IndexData),
                         indices.data(),
                         GL_STATIC_DRAW);




  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, instance_vbo_);

  {
    const int center_index = program.attributeLocation("voxel_center");
    assert(center_index != -1);
    glfuncs_->glVertexAttribPointer(center_index, 3, GL_FLOAT, GL_FALSE,
                                    sizeof(VoxelInfo),
                                    (void*) offsetof(VoxelInfo, center));
    glfuncs_->glEnableVertexAttribArray(center_index);
    glfuncs_->glVertexAttribDivisor(center_index, 1);
  }
  {
    const int label_index = program.attributeLocation("voxel_label");
    assert(label_index != -1);
    glfuncs_->glVertexAttribIPointer(label_index, 1, GL_INT,
                                    sizeof(VoxelInfo),
                                    (void*) offsetof(VoxelInfo, label));
    glfuncs_->glEnableVertexAttribArray(label_index);
    glfuncs_->glVertexAttribDivisor(label_index, 1);
  }
  {
    const int color_index = program.attributeLocation("voxel_color");
    assert(color_index != -1);
    glfuncs_->glVertexAttribPointer(color_index, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                                    sizeof(VoxelInfo),
                                    (void*) offsetof(VoxelInfo, color));
    glfuncs_->glEnableVertexAttribArray(color_index);
    glfuncs_->glVertexAttribDivisor(color_index, 1);
  }

  // Release (unbind) all
  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  setVoxelSize(voxel_size);
}

void InstancedVoxelDrawer::addNewVoxels(const std::vector<VoxelInfo>& voxels) {
  if ((voxels.size() == 0)
      || ((num_of_voxels_ + voxels.size()) > max_num_of_voxels_)) {
    std::cerr << "[Warning] Weird number of voxels requested to add" << std::endl;
    return;
  }

  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, instance_vbo_);
  glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, num_of_voxels_ * sizeof(VoxelInfo), voxels.size() * sizeof(VoxelInfo), voxels.data());
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  num_of_voxels_ += voxels.size();
}

void InstancedVoxelDrawer::setVoxelSize(const float size) {
  assert(hasValidOpenGLFunctions());

  typedef VertexXYZNormal VertexData;
  typedef VertexData::PositionType PositionType;
  typedef VertexData::NormalType NormalType;

  const float voxel_radius = size / 2;

  std::array<PositionType, 8> cube_corners;
  // Front Verticies positions
  cube_corners[0] = PositionType(voxel_radius, voxel_radius, voxel_radius);
  cube_corners[1] = PositionType(-voxel_radius, voxel_radius, voxel_radius);
  cube_corners[2] = PositionType(-voxel_radius, -voxel_radius, voxel_radius);
  cube_corners[3] = PositionType(voxel_radius, -voxel_radius, voxel_radius);

  // Back Verticies positions
  cube_corners[4] = PositionType(voxel_radius, voxel_radius, -voxel_radius);
  cube_corners[5] = PositionType(-voxel_radius, voxel_radius, -voxel_radius);
  cube_corners[6] = PositionType(-voxel_radius, -voxel_radius, -voxel_radius);
  cube_corners[7] = PositionType(voxel_radius, -voxel_radius, -voxel_radius);

  std::array<NormalType, 6> face_normals;
  face_normals[0] = NormalType(0.0f, 0.0f, 1.0f);
  face_normals[1] = NormalType(0.0f, 0.0f, -1.0f);
  face_normals[2] = NormalType(0.0f, 1.0f, 0.0f);
  face_normals[3] = NormalType(0.0f, -1.0f, 0.0f);
  face_normals[4] = NormalType(1.0f, 0.0f, 0.0f);
  face_normals[5] = NormalType(-1.0f, 1.0f, 0.0f);

  std::vector<VertexData> vertices;

  // face 0
  vertices.emplace_back(cube_corners[0], face_normals[0]);
  vertices.emplace_back(cube_corners[1], face_normals[0]);
  vertices.emplace_back(cube_corners[2], face_normals[0]);
  vertices.emplace_back(cube_corners[3], face_normals[0]);

  // face 1
  vertices.emplace_back(cube_corners[5], face_normals[1]);
  vertices.emplace_back(cube_corners[4], face_normals[1]);
  vertices.emplace_back(cube_corners[7], face_normals[1]);
  vertices.emplace_back(cube_corners[6], face_normals[1]);

  // face 2
  vertices.emplace_back(cube_corners[0], face_normals[2]);
  vertices.emplace_back(cube_corners[4], face_normals[2]);
  vertices.emplace_back(cube_corners[5], face_normals[2]);
  vertices.emplace_back(cube_corners[1], face_normals[2]);

  // face 3
  vertices.emplace_back(cube_corners[3], face_normals[3]);
  vertices.emplace_back(cube_corners[2], face_normals[3]);
  vertices.emplace_back(cube_corners[6], face_normals[3]);
  vertices.emplace_back(cube_corners[7], face_normals[3]);

  // face 4
  vertices.emplace_back(cube_corners[4], face_normals[4]);
  vertices.emplace_back(cube_corners[0], face_normals[4]);
  vertices.emplace_back(cube_corners[3], face_normals[4]);
  vertices.emplace_back(cube_corners[7], face_normals[4]);

  // face 5
  vertices.emplace_back(cube_corners[1], face_normals[5]);
  vertices.emplace_back(cube_corners[5], face_normals[5]);
  vertices.emplace_back(cube_corners[6], face_normals[5]);
  vertices.emplace_back(cube_corners[2], face_normals[5]);

  assert(vertices.size() == 24);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexData), vertices.data(), GL_STATIC_DRAW);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void InstancedVoxelDrawer::draw() {
  assert(hasValidOpenGLFunctions());

  glfuncs_->glBindVertexArray(vao_);
  glfuncs_->glDrawElementsInstanced(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0, num_of_voxels_);
  glfuncs_->glBindVertexArray(0);
}

}  // end namespace CuteGL
