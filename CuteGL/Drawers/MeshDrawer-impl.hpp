/**
 * @file MeshDrawer-impl.hpp
 * @brief MeshDrawer-impl
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_MESHDRAWER_IMPL_HPP_
#define CUTEGL_DRAWERS_MESHDRAWER_IMPL_HPP_

#include "CuteGL/Utils/OpenGLUtils.h"

namespace CuteGL {

template<class PS, class NS, class CS, class LS, class IS>
MeshDrawer::MeshDrawer(const QOpenGLShaderProgram& program,
                       const Mesh<PS, NS, CS, LS, IS>& mesh)
    : num_of_indices_(0) {
  init(program, mesh);
}


template<class PS, class NS, class CS, class LS, class IS>
void MeshDrawer::init(const QOpenGLShaderProgram& program, const Mesh<PS, NS, CS, LS, IS>& mesh) {
  using MeshType = Mesh<PS, NS, CS, LS, IS>;
  using PositionScalar = typename MeshType::PositionScalar;
  using NormalScalar = typename MeshType::NormalScalar;
  using ColorScalar = typename MeshType::ColorScalar;
  using LabelScalar = typename MeshType::LabelScalar;
  using IndexScalar = typename MeshType::IndexScalar;

  const int position_index = program.attributeLocation("vertex_position");
  const int normal_index = program.attributeLocation("vertex_normal");
  const int color_index = program.attributeLocation("vertex_color");
  const int label_index = program.attributeLocation("vertex_label");


  const GLsizeiptr size_of_positions = (position_index != -1) ? mesh.sizeOfPositions() : 0;
  const GLsizeiptr size_of_normals = (normal_index != -1) ? mesh.sizeOfNormals() : 0;
  const GLsizeiptr size_of_colors = (color_index != -1) ? mesh.sizeOfColors() : 0;
  const GLsizeiptr size_of_labels = (label_index != -1) ? mesh.sizeOfLabels() : 0;

  assert(hasValidOpenGLFunctions());

  // Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // 2. Initialize (allocate memory) for vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER,
                         size_of_positions + size_of_normals + size_of_colors + size_of_labels,
                         nullptr,
                         GL_STATIC_DRAW);

  // Position attribute

  assert(position_index != -1);
  {
    GLintptr offset = 0;
    glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_positions, mesh.positions.data());
    glfuncs_->glVertexAttribPointer(position_index, 3, GLTraits<PositionScalar>::type, GL_FALSE,
                                    3 * sizeof(PositionScalar),
                                    (GLvoid*) offset);
    glfuncs_->glEnableVertexAttribArray(position_index);
  }


  // Normal attribute
  if (size_of_normals) {
    GLintptr offset = size_of_positions;
    glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_normals, mesh.normals.data());
    glfuncs_->glVertexAttribPointer(normal_index, 3, GLTraits<NormalScalar>::type, GL_FALSE,
                                    3 * sizeof(NormalScalar),
                                    (GLvoid*) offset);
    glfuncs_->glEnableVertexAttribArray(normal_index);
  }

  // Color attribute

  if (size_of_colors) {
    GLintptr offset = size_of_positions + size_of_normals;
    glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_colors, mesh.colors.data());
    glfuncs_->glVertexAttribPointer(color_index, 4, GLTraits<ColorScalar>::type, GL_TRUE,
                                    4 * sizeof(ColorScalar),
                                    (GLvoid*) offset);
    glfuncs_->glEnableVertexAttribArray(color_index);
  }

  if (size_of_labels) {
    GLintptr offset = size_of_positions + size_of_normals + size_of_colors;
    glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, size_of_labels, mesh.labels.data());
    glfuncs_->glVertexAttribIPointer(label_index, 1, GLTraits<LabelScalar>::type,
                                     1 * sizeof(LabelScalar),
                                    (GLvoid*) offset);
    glfuncs_->glEnableVertexAttribArray(label_index);
  }

  glfuncs_->glGenBuffers(1, &ibo_);
  glfuncs_->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
  num_of_indices_ = mesh.faces.size();
  glfuncs_->glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.faces.size() * sizeof(IndexScalar), mesh.faces.data(), GL_STATIC_DRAW);

  // Release VAO first
  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

template <class Derived>
void MeshDrawer::updateVBO(const Eigen::MatrixBase<Derived>& vbo_data, GLintptr offset) {
  // TODO check range
  using Scalar = typename Derived::Scalar;
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, offset, vbo_data.size() * sizeof(Scalar), vbo_data.derived().data());
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}


}  // namespace CuteGL

#endif // end CUTEGL_DRAWERS_MESHDRAWER_IMPL_HPP_
