/**
 * @file AxisDrawer.h
 * @brief AxisDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_AXISDRAWER_H_
#define CUTEGL_DRAWERS_AXISDRAWER_H_

#include "CuteGL/Drawers/ModelDrawer.h"

namespace CuteGL {

class AxisDrawer {
 public:
  AxisDrawer();

  void init(const QOpenGLShaderProgram& program);

  void draw();

 private:
  ModelDrawer drawer_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_DRAWERS_AXISDRAWER_H_
