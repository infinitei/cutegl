/**
 * @file SMPLDrawer.cpp
 * @brief SMPLDrawer
 *
 * @author Abhijit Kundu
 */

#include "SMPLDrawer.h"
#include "CuteGL/Core/SMPLData.h"

namespace CuteGL {

// Instantiate for SMPLData<float>
template class SMPLDrawer<SMPLData<float> >;

}  // end namespace CuteGL
