/**
 * @file DrawerBase.h
 * @brief DrawerBase
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_DRAWERBASE_H_
#define CUTEGL_DRAWERS_DRAWERBASE_H_

#include "CuteGL/Drawers/StaticOpenGLFunctions.h"

namespace CuteGL {

/**@brief Drawer Base class
 *
 * Copying is not supported, use Move instead.
 *
 * Copying is not allowed since we have a custom
 * non-trivial destructor which deletes the gl arrays.
 *
 * This allows the derived classes to not worry about
 * OpenGL resource management.
 *
 */
class DrawerBase : public StaticOpenGLFunctions {
 public:
  DrawerBase();
  ~DrawerBase() noexcept;

  /// No Copy Constructor
  DrawerBase(const DrawerBase& other) = delete;

  /// No Copy assignment operator
  DrawerBase& operator= (const DrawerBase& other) = delete;

  /// Move constructor
  DrawerBase(DrawerBase&& other) noexcept;

  /// Move assignment operator
  DrawerBase& operator= (DrawerBase&& other) noexcept;

 protected:
  GLuint vao_;
  GLuint vbo_;
  GLuint ibo_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_DRAWERS_DRAWERBASE_H_
