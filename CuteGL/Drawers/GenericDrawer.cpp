/**
 * @file GenericDrawer.cpp
 * @brief GenericDrawer
 *
 * @author Abhijit Kundu
 */

#include "GenericDrawer.h"

#include <iostream>

namespace CuteGL {

GenericDrawer::GenericDrawer(const std::size_t max_num_of_vertices, const GLenum primitive_mode)
    : primitive_mode_(primitive_mode),
      num_of_vertices_(0),
      max_num_of_vertices_(max_num_of_vertices) {
}

GenericDrawer::GenericDrawer(const QOpenGLShaderProgram& program,
                                   const std::vector<VertexData>& vertex_data,
                                   const GLenum primitive_mode)
    : primitive_mode_(primitive_mode),
      num_of_vertices_(0),
      max_num_of_vertices_(vertex_data.size()) {
  init(program);
  addVertices(vertex_data);
}

void GenericDrawer::init(const QOpenGLShaderProgram& program,
                            const std::vector<VertexData>& vertex_data) {
  init(program);
  addVertices(vertex_data);
}

void GenericDrawer::init(const QOpenGLShaderProgram& program) {
  assert(hasValidOpenGLFunctions());

  // Bind VAO
  glfuncs_->glGenVertexArrays(1, &vao_);
  glfuncs_->glBindVertexArray(vao_);

  // Copy our vertices array in a vertex buffer for OpenGL to use
  glfuncs_->glGenBuffers(1, &vbo_);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferData(GL_ARRAY_BUFFER, max_num_of_vertices_ * sizeof(VertexData), NULL, GL_STATIC_DRAW);

  // Position attribute
  const int position_index = program.attributeLocation("vertex_position");
  assert(position_index != -1);
  glfuncs_->glVertexAttribPointer(position_index, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void*) offsetof(VertexData, position));
  glfuncs_->glEnableVertexAttribArray(position_index);


  // Normal attribute
  const int normal_index = program.attributeLocation("vertex_normal");
  if (normal_index != -1) {
    glfuncs_->glVertexAttribPointer(normal_index, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void*) offsetof(VertexData, normal));
    glfuncs_->glEnableVertexAttribArray(normal_index);
  }

  // Color attribute
  const int color_index = program.attributeLocation("vertex_color");
  if(color_index != -1) {
    glfuncs_->glVertexAttribPointer(color_index, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexData), (void*) offsetof(VertexData, color));
    glfuncs_->glEnableVertexAttribArray(color_index);
  }

  glfuncs_->glBindVertexArray(0);
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GenericDrawer::addVertices(const std::vector<VertexData>& vertex_data) {
  if ((vertex_data.size() == 0)
      || ((num_of_vertices_ + vertex_data.size()) > max_num_of_vertices_)) {
    std::cerr << "[Warning] Weird number of vertices requested to add" << std::endl;
    return;
  }

  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, num_of_vertices_ * sizeof(VertexData), vertex_data.size() * sizeof(VertexData), vertex_data.data());
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  num_of_vertices_ += vertex_data.size();
}

void GenericDrawer::addVerticesAsManyAsPossible(const std::vector<VertexData>& vertex_data) {
  if (vertex_data.size() == 0) {
    std::cerr << "[Warning] Requested to add Zero number of vertices" << std::endl;
    return;
  }

  const std::size_t space_left = spaceLeft();

  if (vertex_data.size() > space_left) {
    std::cerr << "[Warning] Capacity Reached: Some vertices cannot be added" << std::endl;
  }

  const std::size_t num_of_vertices_added = std::min(vertex_data.size(), space_left);

  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glfuncs_->glBufferSubData(GL_ARRAY_BUFFER, num_of_vertices_ * sizeof(VertexData), num_of_vertices_added * sizeof(VertexData), vertex_data.data());
  glfuncs_->glBindBuffer(GL_ARRAY_BUFFER, 0);

  num_of_vertices_ += num_of_vertices_added;
}

void GenericDrawer::draw() {
  assert(hasValidOpenGLFunctions());
  if(num_of_vertices_) {
    glfuncs_->glBindVertexArray(vao_);
    glfuncs_->glDrawArrays(primitive_mode_, 0, num_of_vertices_);
    glfuncs_->glBindVertexArray(0);
  }
}

}  // end namespace CuteGL
