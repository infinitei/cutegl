/**
 * @file GridDrawer.h
 * @brief GridDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_GRIDDRAWER_H_
#define CUTEGL_DRAWERS_GRIDDRAWER_H_

#include "CuteGL/Drawers/DrawerBase.h"
#include "CuteGL/Core/VertexData.h"
#include <QOpenGLShaderProgram>

namespace CuteGL {

class GridDrawer : public DrawerBase {
 public:
  typedef VertexXYZRGBA VertexData;
  typedef VertexData::PositionType PositionType;
  typedef VertexData::ColorType ColorType;

  GridDrawer();
  GridDrawer(const QOpenGLShaderProgram& program,
             const int num_of_subdivisions = 10,
             const ColorType& color = ColorType(128, 128, 128, 255));

  void init(const QOpenGLShaderProgram& program,
            const int num_of_subdivisions = 10,
            const ColorType& color = ColorType(128, 128, 128, 255));

  /// Draw
  void draw();

 private:
  int num_of_vertices_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_DRAWERS_GRIDDRAWER_H_
