/**
 * @file BatchSMPLDrawer.h
 * @brief BatchSMPLDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_BATCHSMPLDRAWER_H_
#define CUTEGL_DRAWERS_BATCHSMPLDRAWER_H_

#include "CuteGL/Drawers/BatchSMPLCudaEngine.h"
#include "CuteGL/Core/SMPLData.h"
#include "CuteGL/Core/VertexData.h"
#include "CuteGL/Drawers/DrawerBase.h"
#include <QOpenGLShaderProgram>

namespace CuteGL {

template <class SMPLDataType>
class BatchSMPLDrawer : public DrawerBase {
 public:
  using PositionScalar = typename SMPLDataType::PositionScalar;
  using IndexScalar = typename SMPLDataType::IndexScalar;
  using SMPLEngine = BatchSMPLCudaEngine<SMPLDataType>;
  using Params = typename SMPLEngine::Params;

  /// Default constructor. Should use init() to actually initialize
  BatchSMPLDrawer();

  ~BatchSMPLDrawer() noexcept;

  /// One shot constructor
  BatchSMPLDrawer(const QOpenGLShaderProgram& program, const std::size_t batch_size, const SMPLDataType& smpl_data, const RGBA& color = RGBA(192, 192, 192, 255), const int label = 1);
  void init(const QOpenGLShaderProgram& program, const std::size_t batch_size, const SMPLDataType& smpl_data, const RGBA& color = RGBA(192, 192, 192, 255), const int label = 1);

  BatchSMPLDrawer(const QOpenGLShaderProgram& program, const std::size_t batch_size, const std::string& smpl_model_file, const RGBA& color = RGBA(192, 192, 192, 255), const int label = 1);
  void init(const QOpenGLShaderProgram& program, const std::size_t batch_size, const std::string& smpl_model_file, const RGBA& color = RGBA(192, 192, 192, 255), const int label = 1);

  template<class SMPLSegmmDataType>
  BatchSMPLDrawer(const QOpenGLShaderProgram& program, const std::size_t batch_size, const SMPLDataType& smpl_data, const SMPLSegmmDataType& segm_data);
  template<class SMPLSegmmDataType>
  void init(const QOpenGLShaderProgram& program, const std::size_t batch_size, const SMPLDataType& smpl_data, const SMPLSegmmDataType& segm_data);

  BatchSMPLDrawer(const QOpenGLShaderProgram& program, const std::size_t batch_size, const std::string& smpl_model_file, const std::string& smpl_segm_file);
  void init(const QOpenGLShaderProgram& program, const std::size_t batch_size, const std::string& smpl_model_file, const std::string& smpl_segm_file);

  inline std::size_t& batchId() {return batch_id_;}
  inline const std::size_t& batchId() const {return batch_id_;}

  /// Draw
  void draw();

  template<class... Args>
  void updateShapeAndPose(const Args&... args) {smpl_engine_.updateShapeAndPose(args...);}

  template<class... Args>
  void updatePose(const Args&... args) {smpl_engine_.updatePose(args...);}

  template<class... Args>
  void updateShape(const Args&... args) {smpl_engine_.updateShape(args...);}


  Params& shape_params() {return smpl_engine_.shape_params;}
  const Params& shape_params() const {return smpl_engine_.shape_params;}

  Params& pose_params() {return smpl_engine_.pose_params;}
  const Params& pose_params() const {return smpl_engine_.pose_params;}


  SMPLEngine& engine() {return smpl_engine_;}
  const SMPLEngine& engine() const {return smpl_engine_;}


 private:
  std::size_t batch_id_;

  // We will use multiple vao each with a different position vbo
  std::vector<GLuint> vaos_;

  // We use separate vbo for position attribute.
  // For other attributes we use the inherited vbo_ from DrawerBase
  std::vector<GLuint> position_vbos_;


  std::size_t num_of_indices_;
  SMPLEngine smpl_engine_;
};

}  // end namespace CuteGL

#include "CuteGL/Drawers/BatchSMPLDrawer-impl.hpp"



#endif // end CUTEGL_DRAWERS_BATCHSMPLDRAWER_H_
