/**
 * @file PointCloudDrawer.h
 * @brief PointCloudDrawer
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_DRAWERS_POINTCLOUDDRAWER_H_
#define CUTEGL_DRAWERS_POINTCLOUDDRAWER_H_

#include "CuteGL/Drawers/DrawerBase.h"
#include "CuteGL/Core/VertexData.h"
#include <QOpenGLShaderProgram>

namespace CuteGL {

class PointCloudDrawer : public DrawerBase {
 public:
  typedef VertexXYZRGBA VertexData;

  /**@brief Default Constructor
   *
   * After this constructor, init() and addPoints() need to be called
   *
   * @param max_num_of_points defaults to a large value
   */
  PointCloudDrawer(const std::size_t max_num_of_points = 50000000);

  /**@brief One shot PointCloudDrawer constructor
   *
   * @param program
   * @param vertex_data
   * @param max_num_of_points
   */
  PointCloudDrawer(const QOpenGLShaderProgram& program,
                   const std::vector<VertexData>& vertex_data,
                   const std::size_t max_num_of_points = 50000000);

  /// /// Initialize VBO, VAO
  void init(const QOpenGLShaderProgram& program, const std::vector<VertexData>& vertex_data);

  /// Initialize VBO, VAO
  void init(const QOpenGLShaderProgram& program);

  /// Add points (No points will be added if we cannot all the points)
  void addPoints(const std::vector<VertexData>& vertex_data);

  /// Add points as many as possible
  void addPointsAsManyAsPossible(const std::vector<VertexData>& vertex_data);

  /// Draw
  void draw();

  /// @returns the number of points that can still be added to the drawer
  inline std::size_t spaceLeft() const {return max_num_of_points_ - num_of_points_;}


  /// @returns the num_of_points currently rendered
  inline std::size_t numOfPoints() const {return num_of_points_;}

  /// @returns the maximum num_of_points that can be rendered by this instance
  inline std::size_t maxNumOfPoints() const {return max_num_of_points_;}

 private:
  std::size_t num_of_points_;
  const std::size_t max_num_of_points_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_DRAWERS_POINTCLOUDDRAWER_H_
