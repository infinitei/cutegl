/**
 * @file StaticOpenGLFunctions.cpp
 * @brief StaticOpenGLFunctions
 *
 * @author Abhijit Kundu
 */

#include "StaticOpenGLFunctions.h"
#include <cassert>

namespace CuteGL {

OpenGLFunctions* StaticOpenGLFunctions::glfuncs_;

StaticOpenGLFunctions::StaticOpenGLFunctions() {
}

void StaticOpenGLFunctions::setOpenGLFunctions(OpenGLFunctions* glfuncs) {
  glfuncs_ = glfuncs;
  assert(hasValidOpenGLFunctions());
}

bool StaticOpenGLFunctions::hasValidOpenGLFunctions() {
  return (glfuncs_ != nullptr) && (glfuncs_ != 0) && (glfuncs_->hasBeenInitialized());
}

}  // end namespace CuteGL
