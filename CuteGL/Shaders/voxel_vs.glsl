#version 330 core

layout(location = 0) in vec3 voxel_center;
layout(location = 1) in int voxel_label;
layout(location = 2) in vec4 voxel_color;

uniform int color_mode;
uniform float blend_alpha;

uniform float min_height;
uniform float max_height;

out vec4 vColor; // Output to geometry (or fragment) shader
out int vLabel; // Output to geometry (or fragment) shader


vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 heightmap(float height) {
  float adjusted_height = (height - min_height) / (max_height - min_height);
  return hsv2rgb(vec3(adjusted_height,1.0, 0.93));
}

vec3 label2rgb(int label) {
  vec3 rgb;
  switch (label) {
  case 0:
    rgb = vec3(0.0, 1, 1); // FREE
    break;
  case 1:
    rgb = vec3(0.5, 0.25, 0.5); // ROAD
    break;
  case 2:
    rgb = vec3(0.0, 0.0, 0.75); // SIDEWALK
    break;
  case 3:
    rgb = vec3(0.5, 0.0, 0.0); // BUILDING
    break;
  case 4:
    rgb = vec3(0.5, 0.5, 0.0); // TREE
    break;
  case 5:
    rgb = vec3(0.25, 0.25, 0.5); // FENCE
    break;
  case 6:
    rgb = vec3(0.25, 0.0, 0.5); // CAR
    break;
  case 7:
    rgb = vec3(0.25, 0.25, 0.0); // PEDESTRIAN
    break;
  case 8:
    rgb = vec3(0.75, 0.75, 0.5); // COLUMN_POLE
    break;
  case 9:
    rgb = vec3(0.0, 0.5, 0.75); // BICYCLIST
    break;
  default:
    rgb = vec3(0.5, 0.5, 0.5); //Unknown
    break;
  }
  return rgb;
}

void main()
{
  gl_Position = vec4(voxel_center, 1.0);

  vLabel = voxel_label;

  if (color_mode == 0) {  //CM_SEMANTIC
    // last 8 bits store the label type
    vColor = vec4(label2rgb(voxel_label & 0xFF), voxel_color.a);
  }
  else if (color_mode == -1) { //CM_FREE
    vColor = vec4(0.0, 1.0, 1.0, 0.1); //FREE
  }
  else if(color_mode == 1) {  //CM_HEIGHT
    vColor = vec4(heightmap(voxel_center[2]), voxel_color.a);
  }
  else if (color_mode == 2) { //CM_TEXTURE
    vColor = voxel_color;
  }
  else if (color_mode == 3) { //CM_BLEND
    vColor = vec4(blend_alpha * label2rgb(voxel_label) + (1.0 - blend_alpha) * voxel_color.rgb, voxel_color.a);
  }
  else
    vColor = vec4(1, 0.0, 0.0, voxel_color.a); //Unknown
}
