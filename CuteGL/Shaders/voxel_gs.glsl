#version 330 core

layout(points) in;
layout (triangle_strip,max_vertices=12) out;

in vec4 vColor[]; // Output from vertex shader for each vertex
in int vLabel[]; // Output from vertex shader for each vertex

out vec4 fColor; // Output to fragment shader
out vec3 position_world;  // position in world coordinate
out vec3 normal_world;    // vertex normal in world coordinate


uniform mat4 view_T_world;   // proj * view
uniform mat4 world_T_model;  // model matrix
// Normal matrix but for this shader it is also assumed to be colwise normalized
uniform mat3 normal_matrix;

// Camera Center coord in world frame
uniform vec3 cam_center;

uniform float voxel_size;

const vec4 cube_verts[8] = vec4[8](
  //------- +x Face ----------//
  vec4( 0.5f,  0.5f, -0.5f, 0),
  vec4( 0.5f,  0.5f,  0.5f, 0),
  vec4( 0.5f, -0.5f, -0.5f, 0),
  vec4( 0.5f, -0.5f,  0.5f, 0),
  //------- -x Face ----------//
  vec4(-0.5f,  0.5f, -0.5f, 0),
  vec4(-0.5f,  0.5f,  0.5f, 0),
  vec4(-0.5f, -0.5f, -0.5f, 0),
  vec4(-0.5f, -0.5f,  0.5f, 0));

// normals in world frame are computed normal_world = normalize(normal_matrix * normal);
// But since normals for cube are special case, we can do some optimization
// Assuming normal_matrix has been colwise normalized, normal_world[0] along +x axis
// is just the 0th column of the normal_matrix.

// const vec3 normals[6] = vec3[6](
//   vec3(1.0f, 0.0f, 0.0f),
//   vec3(-1.0f, 0.0f, 0.0f)
//   vec3(0.0f, 1.0f, 0.0f),
//   vec3(0.0f, -1.0f, 0.0f),
//   vec3(0.0f, 0.0f, 1.0f),
//   vec3(0.0f, 0.0f, -1.0f),
//   );

void main()
{
  fColor = vColor[0];

  vec4 voxel_center = world_T_model * gl_in[0].gl_Position;

  vec4 verts_world[8];
  vec4 verts_view[8];
  for (int i=0;i<8; i++)
  {
    verts_world[i] = world_T_model * (gl_in[0].gl_Position + voxel_size * cube_verts[i]);
    verts_view[i]= view_T_world * verts_world[i];
  }

  vec3 voxel_direction = voxel_center.xyz - cam_center;

  // Face 0 with normal = vec3(1.0f, 0.0f, 0.0f)
  if((vLabel[0] & (1 << 8)) == 0)
  {
    
    normal_world = normal_matrix[0];

    if(dot(voxel_direction, normal_world) < 0) {

      position_world = vec3(verts_world[0]);
      gl_Position = verts_view[0];
      EmitVertex();

      position_world = vec3(verts_world[1]);
      gl_Position = verts_view[1];
      EmitVertex();

      position_world = vec3(verts_world[2]);
      gl_Position = verts_view[2];
      EmitVertex();

      position_world = vec3(verts_world[3]);
      gl_Position = verts_view[3];
      EmitVertex();

      EndPrimitive();
    }


  }

  // Face 1 with normal = vec3(-1.0f, 0.0f, 0.0f)
  if((vLabel[0] & (1 << 9)) == 0)
  {
    
    normal_world = -normal_matrix[0];

    if(dot(voxel_direction, normal_world) < 0) {

      position_world = vec3(verts_world[5]);
      gl_Position = verts_view[5];
      EmitVertex();

      position_world = vec3(verts_world[4]);
      gl_Position = verts_view[4];
      EmitVertex();

      position_world = vec3(verts_world[7]);
      gl_Position = verts_view[7];
      EmitVertex();

      position_world = vec3(verts_world[6]);
      gl_Position = verts_view[6];
      EmitVertex();

      EndPrimitive();
    }
  }

  // Face 2 with normal = vec3(0.0f, 1.0f, 0.0f)
  if((vLabel[0] & (1 << 10)) == 0)
  {
    
    normal_world = normal_matrix[1];

    if(dot(voxel_direction, normal_world) < 0) {

      position_world = vec3(verts_world[4]);
      gl_Position = verts_view[4];
      EmitVertex();

      position_world = vec3(verts_world[5]);
      gl_Position = verts_view[5];
      EmitVertex();

      position_world = vec3(verts_world[0]);
      gl_Position = verts_view[0];
      EmitVertex();

      position_world = vec3(verts_world[1]);
      gl_Position = verts_view[1];
      EmitVertex();

      EndPrimitive();

    }
  }

  // Face 3 with normal = vec3(0.0f, -1.0f, 0.0f)
  if((vLabel[0] & (1 << 11)) == 0)
  {
    
    normal_world = -normal_matrix[1];

    if(dot(voxel_direction, normal_world) < 0) {

      position_world = vec3(verts_world[2]);
      gl_Position = verts_view[2];
      EmitVertex();

      position_world = vec3(verts_world[3]);
      gl_Position = verts_view[3];
      EmitVertex();

      position_world = vec3(verts_world[6]);
      gl_Position = verts_view[6];
      EmitVertex();

      position_world = vec3(verts_world[7]);
      gl_Position = verts_view[7];
      EmitVertex();

      EndPrimitive();
    }
  }

  // Face 4 with normal = vec3(0.0f, 0.0f, 1.0f)
  if((vLabel[0] & (1 << 12)) == 0)
  {
    normal_world = normal_matrix[2];

    if(dot(voxel_direction, normal_world) < 0) {

      position_world = vec3(verts_world[1]);
      gl_Position = verts_view[1];
      EmitVertex();

      position_world = vec3(verts_world[5]);
      gl_Position = verts_view[5];
      EmitVertex();

      position_world = vec3(verts_world[3]);
      gl_Position = verts_view[3];
      EmitVertex();

      position_world = vec3(verts_world[7]);
      gl_Position = verts_view[7];
      EmitVertex();

      EndPrimitive();
    }
  }

  // Face 5 with normal = vec3(0.0f, 0.0f, -1.0f)
  if((vLabel[0] & (1 << 13)) == 0)
  {
    normal_world = -normal_matrix[2];

    if(dot(voxel_direction, normal_world) < 0) {

      position_world = vec3(verts_world[4]);
      gl_Position = verts_view[4];
      EmitVertex();

      position_world = vec3(verts_world[0]);
      gl_Position = verts_view[0];
      EmitVertex();

      position_world = vec3(verts_world[6]);
      gl_Position = verts_view[6];
      EmitVertex();

      position_world = vec3(verts_world[2]);
      gl_Position = verts_view[2];
      EmitVertex();

      EndPrimitive();
    }
  }

}
