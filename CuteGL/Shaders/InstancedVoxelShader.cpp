/**
 * @file InstancedVoxelShader.cpp
 * @brief InstancedVoxelShader
 *
 * @author Abhijit Kundu
 */

#include "InstancedVoxelShader.h"
#include "CuteGL/Core/Config.h"

namespace CuteGL {

InstancedVoxelShader::InstancedVoxelShader()
    : uniform_loc_color_mode_(-1),
      uniform_loc_blend_alpha_(-1),
      uniform_loc_min_height_(-1),
      uniform_loc_max_height_(-1) {
}

void InstancedVoxelShader::init(OpenGLFunctions* glfuncs) {
  initFromShadersFiles(glfuncs,
                       CUTEGL_SHADER_FOLDER "/instanced_voxel_vs.glsl",
                       CUTEGL_SHADER_FOLDER "/instanced_voxel_fs.glsl");
}

void InstancedVoxelShader::setUniformLocations() {
  PhongShader::setUniformLocations();

  uniform_loc_color_mode_ = program.uniformLocation("color_mode");
  uniform_loc_blend_alpha_ = program.uniformLocation("blend_alpha");

  assert(uniform_loc_color_mode_ != -1);
  assert(uniform_loc_blend_alpha_ != -1);

  glfuncs_->glUniform1i(uniform_loc_color_mode_, 0);
  glfuncs_->glUniform1f(uniform_loc_blend_alpha_, 0.5f);

  uniform_loc_min_height_ = program.uniformLocation("min_height");
  uniform_loc_max_height_ = program.uniformLocation("max_height");

  assert(uniform_loc_min_height_ != -1);
  assert(uniform_loc_max_height_ != -1);

  glfuncs_->glUniform1f(uniform_loc_min_height_, 0.0f);
  glfuncs_->glUniform1f(uniform_loc_max_height_, 1.0f);
}

void InstancedVoxelShader::setColorMode(int color_mode) {
  assert(glfuncs_ != nullptr);
  assert(glfuncs_->hasBeenInitialized());
  assert(uniform_loc_color_mode_ != -1);
  glfuncs_->glUniform1i(uniform_loc_color_mode_, color_mode);
}

void InstancedVoxelShader::setBlendAlpha(float alpha) {
  assert(glfuncs_ != nullptr);
  assert(glfuncs_->hasBeenInitialized());
  assert(uniform_loc_blend_alpha_ != -1);
  glfuncs_->glUniform1f(uniform_loc_blend_alpha_, alpha);
}

void InstancedVoxelShader::setHeight(float min, float max) {
  assert(min < max);
  assert(glfuncs_ != nullptr);
  assert(glfuncs_->hasBeenInitialized());
  assert(uniform_loc_min_height_ != -1);
  glfuncs_->glUniform1f(uniform_loc_min_height_, min);
  assert(uniform_loc_max_height_ != -1);
  glfuncs_->glUniform1f(uniform_loc_max_height_, max);
}


}  // end namespace CuteGL
