#version 330 core

layout(location = 0) in vec3 vertex_position;
layout(location = 2) in vec4 vertex_color;

uniform mat4 view_T_world; // This is the proj * camera_T_world

out vec4 vertex_rgba; // Specify a color output to the fragment shader

void main()
{
  gl_Position = view_T_world * vec4(vertex_position, 1.0);
  vertex_rgba = vertex_color;
}
