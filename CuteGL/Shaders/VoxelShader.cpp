/**
 * @file VoxelShader.cpp
 * @brief VoxelShader
 *
 * @author Abhijit Kundu
 */

#include "VoxelShader.h"
#include "CuteGL/Core/Config.h"

namespace CuteGL {

VoxelShader::VoxelShader()
    : uniform_loc_color_mode_(-1),
      uniform_loc_blend_alpha_(-1),
      uniform_loc_min_height_(-1),
      uniform_loc_max_height_(-1),
      uniform_loc_voxel_size_(-1) {
}

void VoxelShader::init(OpenGLFunctions* glfuncs) {
  assert(glfuncs != 0);
  assert(glfuncs != nullptr);
  glfuncs_ = glfuncs;

  program.addShaderFromSourceFile(QOpenGLShader::Vertex, CUTEGL_SHADER_FOLDER "/voxel_vs.glsl");
  program.addShaderFromSourceFile(QOpenGLShader::Geometry, CUTEGL_SHADER_FOLDER "/voxel_gs.glsl");
  program.addShaderFromSourceFile(QOpenGLShader::Fragment, CUTEGL_SHADER_FOLDER "/voxel_fs.glsl");
  program.link();

  program.bind();
  uniform_loc_view_T_world_ = program.uniformLocation("view_T_world");
  uniform_loc_world_T_model_ = program.uniformLocation("world_T_model");
  uniform_loc_normal_matrix_ = program.uniformLocation("normal_matrix");
  uniform_loc_cam_center_ = program.uniformLocation("cam_center");

  assert(uniform_loc_view_T_world_ != -1);
  assert(uniform_loc_world_T_model_ != -1);
  assert(uniform_loc_normal_matrix_ != -1);
  assert(uniform_loc_cam_center_ != -1);

  glfuncs_->glUniform3f(program.uniformLocation("light_position_world"), 2.2f, 2.0f, 2.0f);
  glfuncs_->glUniform3f(program.uniformLocation("light_color"), 1.0f, 1.0f, 1.0f);

  uniform_loc_color_mode_ = program.uniformLocation("color_mode");
  uniform_loc_blend_alpha_ = program.uniformLocation("blend_alpha");

  assert(uniform_loc_color_mode_ != -1);
  assert(uniform_loc_blend_alpha_ != -1);

  glfuncs_->glUniform1i(uniform_loc_color_mode_, 0);
  glfuncs_->glUniform1f(uniform_loc_blend_alpha_, 0.5f);

  uniform_loc_min_height_ = program.uniformLocation("min_height");
  uniform_loc_max_height_ = program.uniformLocation("max_height");

  assert(uniform_loc_min_height_ != -1);
  assert(uniform_loc_max_height_ != -1);

  glfuncs_->glUniform1f(uniform_loc_min_height_, 0.0f);
  glfuncs_->glUniform1f(uniform_loc_max_height_, 1.0f);

  uniform_loc_voxel_size_ = program.uniformLocation("voxel_size");
  glfuncs_->glUniform1f(uniform_loc_voxel_size_, 1.0f);

  program.release();
}

void VoxelShader::setModelPose(const Eigen::Affine3f& model_pose) {
  assert(uniform_loc_world_T_model_ != -1);
  assert(uniform_loc_normal_matrix_ != -1);
  assert(glfuncs_ != nullptr);

  glfuncs_->glUniformMatrix4fv(uniform_loc_world_T_model_, 1, GL_FALSE, model_pose.data());
  Eigen::Matrix3f normalMatrix = model_pose.linear().inverse().transpose();

  // We do an extra normalization
  normalMatrix.colwise().normalize();
  glfuncs_->glUniformMatrix3fv(uniform_loc_normal_matrix_, 1, GL_FALSE, normalMatrix.data());
}

void VoxelShader::setColorMode(int color_mode) {
  assert(glfuncs_ != nullptr);
  assert(glfuncs_->hasBeenInitialized());
  assert(uniform_loc_color_mode_ != -1);
  glfuncs_->glUniform1i(uniform_loc_color_mode_, color_mode);
}

void VoxelShader::setBlendAlpha(float alpha) {
  assert(glfuncs_ != nullptr);
  assert(glfuncs_->hasBeenInitialized());
  assert(uniform_loc_blend_alpha_ != -1);
  glfuncs_->glUniform1f(uniform_loc_blend_alpha_, alpha);
}

void VoxelShader::setHeight(float min, float max) {
  assert(min < max);
  assert(glfuncs_ != nullptr);
  assert(glfuncs_->hasBeenInitialized());
  assert(uniform_loc_min_height_ != -1);
  glfuncs_->glUniform1f(uniform_loc_min_height_, min);
  assert(uniform_loc_max_height_ != -1);
  glfuncs_->glUniform1f(uniform_loc_max_height_, max);
}

void VoxelShader::setVoxelSize(float voxel_size) {
  assert(glfuncs_ != nullptr);
  assert(glfuncs_->hasBeenInitialized());
  assert(uniform_loc_voxel_size_ != -1);
  glfuncs_->glUniform1f(uniform_loc_voxel_size_, voxel_size);
}

}  // end namespace CuteGL
