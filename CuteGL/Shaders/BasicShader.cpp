/**
 * @file BasicShader.cpp
 * @brief BasicShader
 *
 * @author Abhijit Kundu
 */

#include "BasicShader.h"
#include "CuteGL/Core/Config.h"
#include <cassert>

namespace CuteGL {

BasicShader::BasicShader()
  : program(),
    glfuncs_(nullptr),
    uniform_loc_view_T_world_(-1),
    uniform_loc_world_T_model_(-1),
    uniform_loc_cam_center_(-1) {
}


void BasicShader::init(OpenGLFunctions* glfuncs) {
  initFromShadersFiles(glfuncs,
                       CUTEGL_SHADER_FOLDER "/basic_vs.glsl",
                       CUTEGL_SHADER_FOLDER "/basic_fs.glsl");
}

void BasicShader::initFromShadersFiles(OpenGLFunctions* glfuncs, const QString& vertex_shader_path, const QString& fragment_shader_path) {
  assert(glfuncs != 0);
  assert(glfuncs != nullptr);
  glfuncs_ = glfuncs;
  assert(glfuncs_->hasBeenInitialized());

  program.addShaderFromSourceFile(QOpenGLShader::Vertex, vertex_shader_path);
  program.addShaderFromSourceFile(QOpenGLShader::Fragment, fragment_shader_path);
  program.link();

  program.bind();

  setUniformLocations();

  program.release();
}

void BasicShader::setUniformLocations() {
  uniform_loc_view_T_world_ = program.uniformLocation("view_T_world");
  uniform_loc_world_T_model_ = program.uniformLocation("world_T_model");
  uniform_loc_cam_center_ = program.uniformLocation("cam_center");

  assert(uniform_loc_view_T_world_ != -1);
  assert(uniform_loc_world_T_model_ != -1);
  assert(uniform_loc_cam_center_ != -1);
}

void BasicShader::setProjectViewMatrix(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics) {
  assert(glfuncs_ != nullptr);
  assert(glfuncs_->hasBeenInitialized());

  assert(uniform_loc_view_T_world_ != -1);
  const Eigen::Matrix4f view_T_world = (intrinsics * extrinsics).matrix();
  glfuncs_->glUniformMatrix4fv(uniform_loc_view_T_world_, 1, GL_FALSE, view_T_world.data());

  assert(uniform_loc_cam_center_ != -1);
  const Eigen::Vector3f cam_center = extrinsics.inverse().translation();
  glfuncs_->glUniform3f(uniform_loc_cam_center_, cam_center.x(), cam_center.y(), cam_center.z());
}

void BasicShader::setModelPose(const Eigen::Affine3f& model_pose) {
  assert(glfuncs_ != nullptr);
  assert(glfuncs_->hasBeenInitialized());
  assert(uniform_loc_world_T_model_ != -1);
  glfuncs_->glUniformMatrix4fv(uniform_loc_world_T_model_, 1, GL_FALSE, model_pose.data());
}

}  // end namespace CuteGL
