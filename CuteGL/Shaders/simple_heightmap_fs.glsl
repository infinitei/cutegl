#version 330 core

in vec4 vertex_rgba; // The input variable from the vertex shader (same name and same type)

out vec4 color;

void main()
{
  color = vertex_rgba;
}