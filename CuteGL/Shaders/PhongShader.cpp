/**
 * @file PhongShader.cpp
 * @brief PhongShader
 *
 * @author Abhijit Kundu
 */

#include "PhongShader.h"
#include "CuteGL/Core/Config.h"
#include <cassert>

namespace CuteGL {

PhongShader::PhongShader()
    : uniform_loc_normal_matrix_(-1),
      uniform_loc_light_position_world_(-1),
      uniform_loc_light_color_(-1) {
}

void PhongShader::init(OpenGLFunctions* glfuncs) {
  initFromShadersFiles(glfuncs,
                       CUTEGL_SHADER_FOLDER "/phong_vs.glsl",
                       CUTEGL_SHADER_FOLDER "/phong_fs.glsl");
}

void PhongShader::setUniformLocations() {
  BasicShader::setUniformLocations();

  uniform_loc_normal_matrix_ = program.uniformLocation("normal_matrix");
  assert(uniform_loc_normal_matrix_ != -1);

  uniform_loc_light_position_world_ = program.uniformLocation("light_position_world");
  assert(uniform_loc_light_position_world_ != -1);

  uniform_loc_light_color_ = program.uniformLocation("light_color");
  assert(uniform_loc_light_color_ != -1);

  glfuncs_->glUniform3f(uniform_loc_light_position_world_, 5.0f, -5.0f, 30.0f);
  glfuncs_->glUniform3f(uniform_loc_light_color_, 1.0f, 1.0f, 1.0f);
}

void PhongShader::setModelPose(const Eigen::Affine3f& model_pose) {
  assert(uniform_loc_world_T_model_ != -1);
  assert(uniform_loc_normal_matrix_ != -1);
  assert(glfuncs_ != nullptr);

  glfuncs_->glUniformMatrix4fv(uniform_loc_world_T_model_, 1, GL_FALSE, model_pose.data());
  Eigen::Matrix3f normalMatrix = model_pose.linear().inverse().transpose();
  glfuncs_->glUniformMatrix3fv(uniform_loc_normal_matrix_, 1, GL_FALSE, normalMatrix.data());
}

void PhongShader::setLightPosition(const Eigen::Vector3f& light_position_world) {
  assert(uniform_loc_light_position_world_ != -1);
  assert(glfuncs_ != nullptr);
  glfuncs_->glUniform3fv(uniform_loc_light_position_world_, 1, light_position_world.data());
}

void PhongShader::setLightPosition(const float x, const float y, const float z) {
  assert(uniform_loc_light_position_world_ != -1);
  assert(glfuncs_ != nullptr);
  glfuncs_->glUniform3f(uniform_loc_light_position_world_, x, y, z);
}

void PhongShader::setLightColor(const Eigen::Vector3f& light_color) {
  assert(uniform_loc_light_color_ != -1);
  assert(glfuncs_ != nullptr);
  glfuncs_->glUniform3fv(uniform_loc_light_color_, 1, light_color.data());
}

void PhongShader::setLightColor(const float r, const float g, const float b) {
  assert(uniform_loc_light_color_ != -1);
  assert(glfuncs_ != nullptr);
  glfuncs_->glUniform3f(uniform_loc_light_color_, r, g, b);
}

}  // end namespace CuteGL
