#version 330 core

in vec4 vertex_rgba;     // vertex color
in vec3 position_world;  // position_world
flat in int label_final; // final vertex_label computed by vs

uniform vec3 cam_center; // Camera Center coord in world frame

layout (location = 0) out vec4 color; // Final frag color
layout (location = 1) out float depth; // depth
layout (location = 3) out float label; // label

void main()
{
  color = vertex_rgba;
  depth = distance(position_world, cam_center);
  label = float(label_final);
}