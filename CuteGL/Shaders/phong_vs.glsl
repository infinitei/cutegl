#version 330 core

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;
layout(location = 2) in vec4 vertex_color;
layout(location = 3) in int vertex_label;

uniform mat4 view_T_world;	 // proj * view
uniform mat4 world_T_model;  // model matrix
uniform mat3 normal_matrix;  // Normal matrix for the world_T_model 

out vec4 vertex_rgba;     // vertex color
out vec3 position_world;  // position in world coordinate
out vec3 normal_world;    // vertex normal in world coordinate
flat out int label_final;  // final vertex label

void main()
{
  vec4 position_world_homeogenous = world_T_model * vec4(vertex_position, 1.0f);
  position_world = position_world_homeogenous.xyz;
  gl_Position = view_T_world * position_world_homeogenous;

  vertex_rgba = vertex_color;

  normal_world = normalize(normal_matrix * vertex_normal);

  label_final = vertex_label;
}
