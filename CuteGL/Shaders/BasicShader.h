/**
 * @file BasicShader.h
 * @brief BasicShader
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_SHADERS_BASICSHADER_H_
#define CUTEGL_SHADERS_BASICSHADER_H_

#include "CuteGL/Core/OpenGLFunctions.h"
#include <QOpenGLShaderProgram>
#include <Eigen/Geometry>

namespace CuteGL {

class BasicShader {
 public:
  BasicShader();

  void init(OpenGLFunctions* glfuncs);

  QOpenGLShaderProgram program;

  void setProjectViewMatrix(const Eigen::Projective3f& intrinsics, const Eigen::Isometry3f& extrinsics);
  void setModelPose(const Eigen::Affine3f& model_pose);

 protected:
  void initFromShadersFiles(OpenGLFunctions* glfuncs, const QString& vertex_shader_path, const QString& fragment_shader_path);
  virtual void setUniformLocations();

  OpenGLFunctions* glfuncs_;  ///< OpenGL Function resolution
  int uniform_loc_view_T_world_;
  int uniform_loc_world_T_model_;
  int uniform_loc_cam_center_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_SHADERS_BASICSHADER_H_
