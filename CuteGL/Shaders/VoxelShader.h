/**
 * @file VoxelShader.h
 * @brief VoxelShader
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_SHADERS_VOXELSHADER_H_
#define CUTEGL_SHADERS_VOXELSHADER_H_

#include "CuteGL/Shaders/PhongShader.h"

namespace CuteGL {

class VoxelShader : public PhongShader {
 public:
  VoxelShader();

  void init(OpenGLFunctions* glfuncs);

  void setModelPose(const Eigen::Affine3f& model_pose);

  void setColorMode(int color_mode);
  void setBlendAlpha(float alpha);
  void setHeight(float min, float max);
  void setVoxelSize(float voxel_size);

 protected:
  int uniform_loc_color_mode_;
  int uniform_loc_blend_alpha_;

  int uniform_loc_min_height_;
  int uniform_loc_max_height_;

  int uniform_loc_voxel_size_;
};

}  // end namespace CuteGL

#endif // end CUTEGL_SHADERS_VOXELSHADER_H_
