#version 330 core

layout(location = 0) in vec3 vertex_position;

uniform mat4 view_T_world; // This is the proj * camera_T_world

uniform float min_height;
uniform float max_height;

out vec4 vertex_rgba; // Specify a color output to the fragment shader

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 heightmap(float height) {
  float adjusted_height = (height - min_height) / (max_height - min_height);
  return hsv2rgb(vec3(adjusted_height,1.0, 0.93));
}

void main()
{
  gl_Position = view_T_world * vec4(vertex_position, 1.0);
  vertex_rgba = vec4(heightmap(vertex_position[2]), 1.0);
}
