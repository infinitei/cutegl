#version 330 core

layout(points) in;
layout (triangle_strip,max_vertices=24) out;

in vec4 vColor[]; // Output from vertex shader for each vertex
out vec4 fColor; // Output to fragment shader

uniform mat4 view_T_model; // This is the proj * view * model


const vec4 cuve_verts[8] = vec4[8](
  //------- Top Face ----------//
  vec4( 0.5f,  0.5f,  0.5f, 1),
  vec4(-0.5f,  0.5f,  0.5f, 1),
  vec4( 0.5f, -0.5f,  0.5f, 1),
  vec4(-0.5f, -0.5f,  0.5f, 1),
  //----- Bottom Face --------//
  vec4( 0.5f,  0.5f, -0.5f, 1),
  vec4(-0.5f,  0.5f, -0.5f, 1),
  vec4( 0.5f, -0.5f, -0.5f, 1),
  vec4(-0.5f, -0.5f, -0.5f, 1));

void main()
{
  fColor = vColor[0]; // Point has only one vertex

  vec4 transVerts[8];
  for (int i=0;i<8; i++)
  {
    transVerts[i]= view_T_model * (gl_in[0].gl_Position + cuve_verts[i]);
  }

  // Face 0
  {
    gl_Position = transVerts[0];
    EmitVertex();

    gl_Position = transVerts[1];
    EmitVertex();

    gl_Position = transVerts[2];
    EmitVertex();

    gl_Position = transVerts[3];
    EmitVertex();

    EndPrimitive();
  }

  // Face 1
  {
    gl_Position = transVerts[5];
    EmitVertex();

    gl_Position = transVerts[4];
    EmitVertex();

    gl_Position = transVerts[7];
    EmitVertex();

    gl_Position = transVerts[6];
    EmitVertex();

    EndPrimitive();
  }

  // Face 2
  {
    gl_Position = transVerts[4];
    EmitVertex();

    gl_Position = transVerts[5];
    EmitVertex();

    gl_Position = transVerts[0];
    EmitVertex();

    gl_Position = transVerts[1];
    EmitVertex();

    EndPrimitive();
  }

    // Face 3
  {
    gl_Position = transVerts[2];
    EmitVertex();

    gl_Position = transVerts[3];
    EmitVertex();

    gl_Position = transVerts[6];
    EmitVertex();

    gl_Position = transVerts[7];
    EmitVertex();

    EndPrimitive();
  }

  // Face 4
  {
    gl_Position = transVerts[4];
    EmitVertex();

    gl_Position = transVerts[0];
    EmitVertex();

    gl_Position = transVerts[6];
    EmitVertex();

    gl_Position = transVerts[2];
    EmitVertex();

    EndPrimitive();
  }

  // Face 5
  {
    gl_Position = transVerts[1];
    EmitVertex();

    gl_Position = transVerts[5];
    EmitVertex();

    gl_Position = transVerts[3];
    EmitVertex();

    gl_Position = transVerts[7];
    EmitVertex();

    EndPrimitive();
  }

}
