#version 330 core

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec4 vertex_color;

out vec4 vColor; // Output to geometry (or fragment) shader

void main()
{
  gl_Position = vec4(vertex_position, 1.0);

  vColor = vertex_color;
}
