/**
 * @file PhongShader.h
 * @brief PhongShader
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_SHADERS_PHONGSHADER_H_
#define CUTEGL_SHADERS_PHONGSHADER_H_

#include "CuteGL/Shaders/BasicShader.h"

namespace CuteGL {

class PhongShader : public BasicShader {
 public:
  PhongShader();

  using BasicShader::init;
  void init(OpenGLFunctions* glfuncs);

  /// set model pose uniform variable (you may need to bind the program before calling this function)
  void setModelPose(const Eigen::Affine3f& model_pose);

  /// set light position from Vector3f (you may need to bind the program before calling this function)
  void setLightPosition(const Eigen::Vector3f& light_position_world);
  /// set light position from x, y, z (you may need to bind the program before calling this function)
  void setLightPosition(const float x, const float y, const float z);

  /// set light color from Vector3f normalized color (you may need to bind the program before calling this function)
  void setLightColor(const Eigen::Vector3f& light_color);
  /// set light color from r, g, b normalized color (you may need to bind the program before calling this function)
  void setLightColor(const float r, const float g, const float b);

 protected:
  virtual void setUniformLocations();
  int uniform_loc_normal_matrix_;
  int uniform_loc_light_position_world_;
  int uniform_loc_light_color_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_SHADERS_PHONGSHADER_H_
