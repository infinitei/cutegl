#version 330 core

in vec4 vertex_rgba;       // vertex color
in vec3 position_world;   // position_world
in vec3 normal_world;    // vertex normal

uniform vec3 light_position_world;
uniform vec3 light_color;
uniform vec3 cam_center; // Camera Center coord in world frame

layout (location = 0) out vec4 color; // Final frag color
layout (location = 1) out float depth; // depth
layout (location = 2) out vec3 normal; // normal

void main()
{
  // Compute ambient componenet
  float ambient_weight = 0.2f;
  vec3 ambient = ambient_weight * light_color;


  // Compute diffuse componenet
  // We assume normal_world is normalized
  // So we use clamped dot product of light_dir and normal_world
  vec3 light_dir = normalize(light_position_world - position_world);
  float diffuse_weight = 0.8f * max(dot(normal_world, light_dir), 0.0);
  vec3 diffuse = diffuse_weight * light_color;


  // Do Phong
  vec3 phong_result = (ambient + diffuse) * vertex_rgba.rgb;

  color = vec4(phong_result, vertex_rgba.a);
  depth = distance(position_world, cam_center);
  normal = normal_world;
}