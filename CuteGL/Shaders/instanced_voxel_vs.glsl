#version 330 core

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;

layout(location = 2) in vec3 voxel_center;
layout(location = 3) in int voxel_label;
layout(location = 4) in vec4 voxel_color;

uniform mat4 view_T_world;	 // proj * view
uniform mat4 world_T_model;  // model matrix
uniform mat3 normal_matrix;  // Normal matrix for the world_T_model 

out vec4 vertex_rgba;      // vertex color
out vec3 position_world;  // position in world coordinate
out vec3 normal_world;    // vertex normal in world coordinate

uniform int color_mode;
uniform float blend_alpha;

uniform float min_height;
uniform float max_height;


vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 heightmap(float height) {
  float adjusted_height = (height - min_height) / (max_height - min_height);
  return hsv2rgb(vec3(adjusted_height,1.0, 0.93));
}

vec3 label2rgb(int label) {
  vec3 rgb;
  switch (label) {
  case 0:
    rgb = vec3(0.0, 1, 1); // FREE
    break;
  case 1:
    rgb = vec3(0.5, 0.25, 0.5); // ROAD
    break;
  case 2:
    rgb = vec3(0.0, 0.0, 0.75); // SIDEWALK
    break;
  case 3:
    rgb = vec3(0.5, 0.0, 0.0); // BUILDING
    break;
  case 4:
    rgb = vec3(0.5, 0.5, 0.0); // TREE
    break;
  case 5:
    rgb = vec3(0.25, 0.25, 0.5); // FENCE
    break;
  case 6:
    rgb = vec3(0.25, 0.0, 0.5); // CAR
    break;
  case 7:
    rgb = vec3(0.25, 0.25, 0.0); // PEDESTRIAN
    break;
  case 8:
    rgb = vec3(0.75, 0.75, 0.5); // COLUMN_POLE
    break;
  case 9:
    rgb = vec3(0.0, 0.5, 0.75); // BICYCLIST
    break;
  default:
    rgb = vec3(0.5, 0.5, 0.5); //Unknown
    break;
  }
  return rgb;
}

void main()
{
  vec4 position_world_homeogenous = world_T_model * vec4(vertex_position + voxel_center, 1.0f);
  
  gl_Position = view_T_world * position_world_homeogenous;
  
  position_world = vec3(position_world_homeogenous);
  normal_world = normalize(normal_matrix * vertex_normal);

  if (color_mode == 0) {  //CM_SEMANTIC
    vertex_rgba = vec4(label2rgb(voxel_label), voxel_color.a);    
  }
  else if (color_mode == -1) { //CM_FREE
    vertex_rgba = vec4(0.0, 1.0, 1.0, 0.1); //FREE
  }
  else if(color_mode == 1) {  //CM_HEIGHT
    vertex_rgba = vec4(heightmap(position_world[2]), voxel_color.a);
  }
  else if (color_mode == 2) { //CM_TEXTURE
    vertex_rgba = voxel_color;
  }
  else if (color_mode == 3) { //CM_BLEND
    vertex_rgba = vec4(blend_alpha * label2rgb(voxel_label) + (1.0 - blend_alpha) * voxel_color.rgb, voxel_color.a);
  }
  else
    vertex_rgba = vec4(1, 0.0, 0.0, voxel_color.a); //Unknown
}
