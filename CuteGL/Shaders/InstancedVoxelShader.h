/**
 * @file InstancedVoxelShader.h
 * @brief InstancedVoxelShader
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_SHADERS_INSTANCEDVOXELSHADER_H_
#define CUTEGL_SHADERS_INSTANCEDVOXELSHADER_H_

#include "CuteGL/Shaders/PhongShader.h"

namespace CuteGL {

class InstancedVoxelShader : public PhongShader {
 public:
  InstancedVoxelShader();

  void init(OpenGLFunctions* glfuncs);
  void init(OpenGLFunctions* glfuncs, const QString& vertex_shader_path, const QString& fragment_shader_path);

  void setColorMode(int color_mode);
  void setBlendAlpha(float alpha);

  void setHeight(float min, float max);

 protected:
  virtual void setUniformLocations();
  int uniform_loc_color_mode_;
  int uniform_loc_blend_alpha_;

  int uniform_loc_min_height_;
  int uniform_loc_max_height_;
};

}  // end namespace CuteGL

#endif // end CUTEGL_SHADERS_INSTANCEDVOXELSHADER_H_
