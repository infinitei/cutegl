#version 330 core

in vec4 fColor;

in vec3 position_world;   // position_world
in vec3 normal_world;    // vertex normal

uniform vec3 light_position_world;
uniform vec3 light_color;

out vec4 outColor; // Final frag color

void main()
{
    // Compute ambient componenet
  float ambient_weight = 0.85f;
  vec3 ambient = ambient_weight * light_color;

  // Compute diffuse componenet
  // We assume normal_world is normalized
  // So we use clamped dot product of light_dir and normal_world
  vec3 light_dir = normalize(light_position_world - position_world);
  float diffuse_weight = 0.2f * max(dot(normal_world, light_dir), 0.0);
  vec3 diffuse = diffuse_weight * light_color;

    // Do Phong
  vec3 phong_result = (ambient + diffuse) * fColor.rgb;

  outColor = vec4(phong_result, fColor.a);
}