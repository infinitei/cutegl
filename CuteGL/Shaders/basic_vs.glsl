#version 330 core

layout(location = 0) in vec3 vertex_position;
layout(location = 2) in vec4 vertex_color;
layout(location = 3) in int vertex_label;

uniform mat4 view_T_world;	// This is the proj * camera_T_world
uniform mat4 world_T_model;  // This is the model matrix


out vec4 vertex_rgba; // Specify a color output to the fragment shader
out vec3 position_world;  // position in world coordinate
flat out int label_final; // final vertex label

void main()
{
  vec4 position_world_homeogenous = world_T_model * vec4(vertex_position, 1.0f);
  position_world = position_world_homeogenous.xyz;
  gl_Position = view_T_world * position_world_homeogenous;
  vertex_rgba = vertex_color;
  label_final = vertex_label;
}
