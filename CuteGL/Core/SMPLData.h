/**
 * @file SMPLData.h
 * @brief SMPLData
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_CORE_SMPLDATA_H_
#define CUTEGL_CORE_SMPLDATA_H_

#include <Eigen/CXX11/Tensor>

namespace CuteGL {

template <class PositionScalar_ = float, class IndexScalar_ = unsigned int>
struct SMPLData {
  using PositionScalar = PositionScalar_;
  using IndexScalar = IndexScalar_;

  using MatrixX1u = Eigen::Matrix<IndexScalar, Eigen::Dynamic, 1>;
  using MatrixX3u = Eigen::Matrix<IndexScalar, Eigen::Dynamic, 3, Eigen::RowMajor>;
  using MatrixX3 = Eigen::Matrix<PositionScalar, Eigen::Dynamic, 3, Eigen::RowMajor>;
  using MatrixXX = Eigen::Matrix<PositionScalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;


  using Tensor3 = Eigen::Tensor<PositionScalar, 3, Eigen::RowMajor>;
  using Tensor2 = Eigen::Tensor<PositionScalar, 2, Eigen::RowMajor>;
  using Tensor1 = Eigen::Tensor<PositionScalar, 1, Eigen::RowMajor>;

  // Template mesh
  MatrixX1u parents;
  MatrixX3u faces;
  MatrixX3 template_vertices;

  // Learned Model parameters
  MatrixXX blend_weights;
  MatrixXX joint_regressor;
  Tensor3 pose_displacements;
  Tensor3 shape_displacements;
};

template <class ColorScalar_ = unsigned char, class LabelScalar_ = int>
struct SMPLSegmmData {
  using ColorScalar = ColorScalar_;
  using LabelScalar = LabelScalar_;
  using LabelDataContainer = Eigen::Matrix<LabelScalar, Eigen::Dynamic, 1>;
  using ColorDataContainer = Eigen::Matrix<ColorScalar, Eigen::Dynamic, 4, Eigen::RowMajor>;

  LabelDataContainer vertex_labels;
  ColorDataContainer color_map;
};

using SMPLData32F32U = SMPLData<float, uint32_t>;

}  // namespace CuteGL

#endif // end CUTEGL_CORE_SMPLDATA_H_
