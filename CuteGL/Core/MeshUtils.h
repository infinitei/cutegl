/**
 * @file MeshUtils.h
 * @brief MeshUtils
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_CORE_MESHUTILS_H_
#define CUTEGL_CORE_MESHUTILS_H_

#include <numeric>
#include <Eigen/Geometry>

namespace CuteGL {

/**@brief Computes the total number of vertices in a mesh container
 *
 * @param meshes some mesh container like std::vector<MeshData>
 * @return total number of vertices
 */
template<class Meshes>
std::size_t numberOfVertices(const Meshes& meshes) {
  using MeshData = typename Meshes::value_type;
  return std::accumulate(std::begin(meshes), std::end(meshes), std::size_t(0),
                         [](const std::size_t sum, const MeshData& mesh) {
                           return sum + mesh.vertices.size();
                         });
}

/**@brief Computes the total number of faces in a mesh container
 *
 * @param meshes some mesh container like std::vector<MeshData>
 * @return total number of faces
 */
template<class Meshes>
std::size_t numberOfFaces(const Meshes& meshes) {
  using MeshData = typename Meshes::value_type;
  return std::accumulate(std::begin(meshes), std::end(meshes), std::size_t(0),
                         [](const std::size_t sum, const MeshData& mesh) {
                           return sum + mesh.faces.rows();
                         });
}


/**@brief Join multiple meshes to make a single mesh
 *
 * @param meshes some mesh container like std::vector<MeshData>
 * @return a single combined mesh
 */
template<class Meshes>
typename Meshes::value_type joinMeshes(const Meshes& meshes) {
  using MeshData = typename Meshes::value_type;
  using IndexData = typename MeshData::IndexData;


  std::size_t num_of_vertices  = numberOfVertices(meshes);
  std::size_t num_of_faces  = numberOfFaces(meshes);

  MeshData md;
  md.vertices.resize(num_of_vertices);
  md.faces.resize(num_of_faces, 3);

  IndexData vertices_count = 0;
  Eigen::Index faces_count = 0;
  for (const MeshData& mesh : meshes) {
    // Copy the vertices
    std::copy(mesh.vertices.begin(), mesh.vertices.end(), md.vertices.begin() + vertices_count);

    // Copy faces (+ where indices increased by curr_index)
    md.faces.block(faces_count,0,mesh.faces.rows(),3) = mesh.faces.array() + vertices_count;

    // Increment index (number of vertices copied)
    vertices_count += mesh.vertices.size();
    faces_count += mesh.faces.rows();
  }

  return md;
}

template<class Mesh>
void transformMesh(Mesh& mesh, const Eigen::Affine3f& tfm) {
  const Eigen::Matrix3f normalMatrix = tfm.linear().inverse().transpose();
  for (auto& vd : mesh.vertices) {
    vd.position = tfm * vd.position;
    vd.normal = (normalMatrix * vd.normal).normalized();
  }
}

template<class Mesh>
void transformMesh(Mesh& mesh, const Eigen::Affine3f& tfm, const Eigen::Matrix3f& normalMatrix) {
  for (auto& vd : mesh.vertices) {
    vd.position = tfm * vd.position;
    vd.normal = (normalMatrix * vd.normal).normalized();
  }
}


template<class Meshes>
void transformMeshes(Meshes& meshes, const Eigen::Affine3f& tfm) {
  using MeshData = typename Meshes::value_type;
  const Eigen::Matrix3f normalMatrix = tfm.linear().inverse().transpose();
  for (MeshData& mesh : meshes) {
    transformMesh(mesh, tfm, normalMatrix);
  }
}

template<class Mesh, class ColorType>
void colorizeMesh(Mesh& mesh, const ColorType& color) {
  for (auto& vd : mesh.vertices) {
    vd.color = color;
  }
}

template<class Meshes, class ColorType>
void colorizeMeshes(Meshes& meshes, const ColorType& color) {
  using MeshData = typename Meshes::value_type;
  for (MeshData& mesh : meshes) {
    colorizeMesh(mesh, color);
  }
}

}  // namespace CuteGL


#endif // end CUTEGL_CORE_MESHUTILS_H_
