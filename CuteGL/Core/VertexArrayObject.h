/**
 * @file VertexArrayObject.h
 * @brief VertexArrayObject
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_CORE_VERTEXARRAYOBJECT_H_
#define CUTEGL_CORE_VERTEXARRAYOBJECT_H_

class QOpenGLFunctions_3_3_Core;
class QOpenGLContext;

namespace CuteGL {

class VertexArrayObject  {
 public:
  VertexArrayObject();

  void create();
  void create(QOpenGLFunctions_3_3_Core* funcs);

  void bind();

  void release();

  void destroy();

  bool isCreated() const {return object_id_!= 0;}
  unsigned int objectId() const {return object_id_;}

 private:
  QOpenGLFunctions_3_3_Core* funcs_;
  unsigned int object_id_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_CORE_VERTEXARRAYOBJECT_H_
