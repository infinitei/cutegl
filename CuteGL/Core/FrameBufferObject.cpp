/**
 * @file FrameBufferObject.cpp
 * @brief FrameBufferObject
 *
 * @author Abhijit Kundu
 */

#include "FrameBufferObject.h"
#include <cassert>

namespace CuteGL {

FrameBufferObject::FrameBufferObject(GLsizei width, GLsizei height)
  : glfuncs_(nullptr),
    handle_(0),
    binded_(false),
    width_(width),
    height_(height) {
}

void FrameBufferObject::create(OpenGLFunctions* glfuncs) {
  assert(glfuncs != nullptr);
  glfuncs_ = glfuncs;
  glfuncs_->glGenFramebuffers(1, &handle_);
}

void FrameBufferObject::destroy() {
  if (render_buffers_.size())
    glfuncs_->glDeleteRenderbuffers(render_buffers_.size(), render_buffers_.data());
  glfuncs_->glDeleteFramebuffers(1, &handle_);
}

void FrameBufferObject::bind() {
  if(handle_ == 0)
    throw std::runtime_error("FrameBufferObject::bind() : FBO has not been created yet");
  glfuncs_->glBindFramebuffer(GL_FRAMEBUFFER, handle_);
  binded_ = true;
}

void FrameBufferObject::release() {
  glfuncs_->glBindFramebuffer(GL_FRAMEBUFFER, 0);
  binded_ = false;
}

bool FrameBufferObject::isComplete() const {
  assert(binded_);
  return (glfuncs_->glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
}

void FrameBufferObject::resize(GLsizei width, GLsizei height) {
  width_ = width;
  height_ = height;
}

void FrameBufferObject::attachRenderBuffer(GLenum attachment,
                                           GLenum storage_format) {
  assert(binded_);
  render_buffers_.push_back(GLuint());
  GLuint& rbo = render_buffers_.back();
  glfuncs_->glGenRenderbuffers(1, &rbo);
  glfuncs_->glBindRenderbuffer(GL_RENDERBUFFER, rbo);
  glfuncs_->glRenderbufferStorage(GL_RENDERBUFFER, storage_format, width_,height_);
  glfuncs_->glFramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, rbo);
}

GLuint FrameBufferObject::getRenderBufferObjectName(GLenum attachment) const {
  assert(binded_);
  GLint ret = 0;
  glfuncs_->glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, attachment, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &ret);
  return ret;
}

}  // end namespace CuteGL
