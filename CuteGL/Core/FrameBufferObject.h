/**
 * @file FrameBufferObject.h
 * @brief FrameBufferObject
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_CORE_FRAMEBUFFEROBJECT_H_
#define CUTEGL_CORE_FRAMEBUFFEROBJECT_H_

#include "CuteGL/Core/OpenGLFunctions.h"

namespace CuteGL {

class FrameBufferObject {
 public:
  FrameBufferObject(GLsizei width = 1, GLsizei height = 1);

  void resize(GLsizei width, GLsizei height);

  void create(OpenGLFunctions* glfuncs);
  void destroy();

  void bind();
  void release();

  GLuint handle() const {return handle_;}
  bool isComplete() const;
  bool isCreated() const {return handle_!= 0;}
  bool isBinded() const {return binded_;}

  GLsizei width() const {return width_;}
  GLsizei height() const {return height_;}

  /**@brief Create and attach a render buffer
   *
   * @param attachment  - attachment type e.g GL_COLOR_ATTACHMENTi, GL_DEPTH_ATTACHMENT
   * @param storage_format - storage format e.g GL_RGBA, GL_RGB, GL_DEPTH_COMPONENT
   */
  void attachRenderBuffer(GLenum attachment, GLenum storage_format);

  /**@brief returns the object name (renderbuffer id) corresponding to an attachment
   *
   * @param attachment - attachment type e.g GL_COLOR_ATTACHMENTi, GL_DEPTH_ATTACHMENT
   * @return renderbuffer object name if found, 0 otherwise
   */
  GLuint getRenderBufferObjectName(GLenum attachment) const;

 private:
  OpenGLFunctions* glfuncs_;
  GLuint handle_;
  bool binded_;
  GLsizei width_;
  GLsizei height_;
  std::vector<GLuint> render_buffers_;
};

}  // end namespace CuteGL
#endif // end CUTEGL_CORE_FRAMEBUFFEROBJECT_H_
