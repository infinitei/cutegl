/**
 * @file OpenGLFunctions.cpp
 * @brief OpenGLFunctions
 *
 * @author Abhijit Kundu
 */

#include "OpenGLFunctions.h"

namespace CuteGL {

bool OpenGLFunctions::hasBeenInitialized() const {
  return d_ptr != 0;
}

}  // namespace CuteGL

