/**
 * @file VertexData.h
 * @brief VertexData
 *
 * @author Abhijit Kundu
 */


#ifndef CUTEGL_CORE_VERTEXDATA_H_
#define CUTEGL_CORE_VERTEXDATA_H_

#include <Eigen/Core>

namespace CuteGL {

typedef Eigen::Vector3f XYZ;
typedef Eigen::Vector3f Normal;
typedef Eigen::Vector2f UV;
typedef Eigen::Matrix<unsigned char, 4, 1> RGBA;


/// Vertex with position attribute only
struct VertexXYZ {
  typedef Eigen::Vector3f PositionType;
  template<class Derived>
  VertexXYZ(const Eigen::MatrixBase<Derived>& pos)
      : position(pos) {
  }
  PositionType position;
};

/// Vertex with position and RGBA color attribute
struct VertexXYZRGBA {
  typedef Eigen::Vector3f PositionType;
  typedef Eigen::Matrix<unsigned char, 4, 1> ColorType;

  VertexXYZRGBA() {}

  template<class DerivedA, class DerivedB>
  VertexXYZRGBA(const Eigen::MatrixBase<DerivedA>& pos, const Eigen::MatrixBase<DerivedB>& col)
      : position(pos),
        color(col) {
  }
  PositionType position;
  ColorType color;
};

/// Vertex with position, normal, RGBA color attribute and texture co-ordinate
struct VertexXYZNormalRGBAUV {
  typedef Eigen::Vector3f PositionType;
  typedef Eigen::Vector3f NormalType;
  typedef Eigen::Matrix<unsigned char, 4, 1> ColorType;

  PositionType position;
  NormalType normal;
  ColorType color;
  Eigen::Vector2f uv;
};

/// Vertex with position, normal and RGBA color attribute
struct VertexXYZNormalRGBA {
  typedef Eigen::Vector3f PositionType;
  typedef Eigen::Vector3f NormalType;
  typedef Eigen::Matrix<unsigned char, 4, 1> ColorType;

  VertexXYZNormalRGBA() {}

  template<class DerivedP, class DerivedN, class DerivedC>
  VertexXYZNormalRGBA(const Eigen::MatrixBase<DerivedP>& pos,
                     const Eigen::MatrixBase<DerivedN>& norm,
                     const Eigen::MatrixBase<DerivedC>& col)
                     : position(pos),
                       normal(norm),
                       color(col) {
  }

  PositionType position;
  NormalType normal;
  ColorType color;
};

/// Vertex with position, normal
struct VertexXYZNormal {
  typedef Eigen::Vector3f PositionType;
  typedef Eigen::Vector3f NormalType;

  VertexXYZNormal() {
  }

  template<class DerivedP, class DerivedN>
  VertexXYZNormal(const Eigen::MatrixBase<DerivedP>& pos,
                  const Eigen::MatrixBase<DerivedN>& norm)
      : position(pos),
        normal(norm) {
  }

  PositionType position;
  NormalType normal;
};

/// Vertex with position attribute and texture co-ordinate
struct VertexXYZUV {
  typedef Eigen::Vector3f PositionType;

  template<class DerivedA, class DerivedB>
  VertexXYZUV(const Eigen::MatrixBase<DerivedA>& pos, const Eigen::MatrixBase<DerivedB>& tex_uv)
      : position(pos),
        uv(tex_uv) {
  }
  PositionType position;
  Eigen::Vector2f uv;
};


/// generic setter of vertex position
template <class VertexType, class PositionType>
void setVertexPosition(VertexType& vd, const PositionType& position);

/// generic setter of vertex normal
template <class VertexType, class NormalType>
void setVertexNormal(VertexType& vd, const NormalType& normal);

/// generic setter of vertex color
template <class VertexType, class ColorType>
void setVertexColor(VertexType& vd, const ColorType& color);

}  // end namespace CuteGL

#include "CuteGL/Core/VertexData-impl.hpp"

#endif // end CUTEGL_CORE_VERTEXDATA_H_
