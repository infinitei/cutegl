/**
 * @file VertexTraits.h
 * @brief VertexTraits
 *
 * @author Abhijit Kundu
 */


#ifndef CUTEGL_CORE_VERTEXTRAITS_H_
#define CUTEGL_CORE_VERTEXTRAITS_H_

#include <type_traits>

namespace CuteGL {

template<typename T, typename = void>
struct HasPosition : std::false_type { };

template<typename T>
struct HasPosition<T, decltype(std::declval<T>().position, void())> : std::true_type { };

template<typename T, typename = void>
struct HasColor : std::false_type { };

template<typename T>
struct HasColor<T, decltype(std::declval<T>().color, void())> : std::true_type { };

template<typename T, typename = void>
struct HasNormal : std::false_type { };

template<typename T>
struct HasNormal<T, decltype(std::declval<T>().normal, void())> : std::true_type { };

}  // namespace CuteGL


#endif // end CUTEGL_CORE_VERTEXTRAITS_H_
