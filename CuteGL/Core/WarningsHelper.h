/**
 * @file WarningsHelper.h
 * @brief WarningsHelper
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_CORE_WARNINGSHELPER_H_
#define CUTEGL_CORE_WARNINGSHELPER_H_

namespace CuteGL {

template <typename... Ts>
inline void ignore_unused(Ts const& ...)
{}

template <typename... Ts>
inline void ignore_unused()
{}

}  // namespace CuteGL

#endif // end CUTEGL_CORE_WARNINGSHELPER_H_
