/**
 * @file Camera.h
 * @brief Camera
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_CORE_CAMERA_H_
#define CUTEGL_CORE_CAMERA_H_

#include <Eigen/Geometry>

namespace CuteGL {

/**@brief Simple OpenGL Camera
 *
 * This camera object used by the CuteGL's viewer for setting
 * the viewer pose and projection settings (i.e. view and projection).
 *
 */
class Camera {
 public:
  Camera(int width= 800, int height = 600);

  Eigen::Isometry3f& extrinsics() {return extrinsics_;}
  const Eigen::Isometry3f& extrinsics() const {return extrinsics_;}

  Eigen::Projective3f& intrinsics() {return intrinsics_;}
  const Eigen::Projective3f& intrinsics() const {return intrinsics_;}

  /// get the full projection matrix of the camera
  Eigen::Matrix4f projectionViewMatrix() const {return (intrinsics_ * extrinsics_).matrix();}

  /// get the camera center position
  Eigen::Vector3f position() const {return extrinsics_.inverse().translation();}

  inline int imageWidth() const {return image_width_;}
  inline int imageHeight() const {return image_height_;}

  void resize(int w, int h);

  /// Get Vertical Field of view
  float fovY() const;

  /// Get Horizontal Field of view
  float fovX() const;

  /// Returns [fovX(), fovY]
  Eigen::Vector2f fieldOfView() const;

  /// updates the intrinsic matrix with new near and far values for camera frustum
  void setFrustumNearFarPlanes(float znear, float zfar);

 private:

  void recomputeIntrinsics();

  int image_width_, image_height_;
  Eigen::Isometry3f extrinsics_;
  Eigen::Projective3f intrinsics_;

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}  // end namespace CuteGL

#endif // end CUTEGL_CORE_CAMERA_H_
