/**
 * @file MeshData.h
 * @brief MeshData
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_CORE_MESHDATA_H_
#define CUTEGL_CORE_MESHDATA_H_

#include "CuteGL/Core/VertexData.h"
#include <vector>

namespace CuteGL {

/// Simple Mesh Data Structure: set of VertexData and faces (indices)
struct MeshData {
  typedef VertexXYZNormalRGBA VertexData;
  typedef unsigned int IndexData;

  typedef VertexData::PositionType PositionType;
  typedef VertexData::NormalType NormalType;
  typedef VertexData::ColorType ColorType;

  typedef std::vector<VertexData> VertexContainer;
  typedef Eigen::Matrix<IndexData, Eigen::Dynamic, 3, Eigen::RowMajor> FaceContainer;

  VertexContainer vertices;
  FaceContainer faces;
};

template <class PositionScalar_= float, class NormalScalar_= float, class ColorScalar_ = unsigned char, class LabelScalar_ = int, class IndexScalar_ = unsigned int>
struct Mesh {
  typedef PositionScalar_ PositionScalar;
  typedef NormalScalar_ NormalScalar;
  typedef ColorScalar_ ColorScalar;
  typedef LabelScalar_ LabelScalar;
  typedef IndexScalar_ IndexScalar;

  typedef Eigen::Matrix<PositionScalar, Eigen::Dynamic, 3, Eigen::RowMajor> PositionDataContainer;
  typedef Eigen::Matrix<NormalScalar, Eigen::Dynamic, 3, Eigen::RowMajor> NormalDataContainer;
  typedef Eigen::Matrix<ColorScalar, Eigen::Dynamic, 4, Eigen::RowMajor> ColorDataContainer;
  typedef Eigen::Matrix<LabelScalar, Eigen::Dynamic, 1> LabelDataContainer;
  typedef Eigen::Matrix<IndexScalar, Eigen::Dynamic, 3, Eigen::RowMajor> FaceContainer;

  typedef Eigen::Matrix<PositionScalar, 1, 3, Eigen::RowMajor> PositionType;
  typedef Eigen::Matrix<NormalScalar, 1, 3, Eigen::RowMajor> NormalType;
  typedef Eigen::Matrix<ColorScalar, 1, 4, Eigen::RowMajor> ColorType;

  inline typename PositionDataContainer::RowXpr position(const Eigen::Index index) {
    return positions.row(index);
  }

  inline typename PositionDataContainer::ConstRowXpr position(const Eigen::Index index) const {
    return positions.row(index);
  }

  inline typename NormalDataContainer::RowXpr normal(const Eigen::Index index) {
    return normals.row(index);
  }

  inline typename NormalDataContainer::ConstRowXpr normal(const Eigen::Index index) const {
    return normals.row(index);
  }

  inline typename ColorDataContainer::RowXpr color(const Eigen::Index index) {
    return colors.row(index);
  }

  inline typename ColorDataContainer::ConstRowXpr color(const Eigen::Index index) const {
    return colors.row(index);
  }

  inline const LabelScalar& label(const Eigen::Index index) const {
    return labels[index];
  }

  inline LabelScalar& label(const Eigen::Index index) {
    return labels[index];
  }

  void resizeVertices(const Eigen::Index num_of_vertices) {
    positions.resize(num_of_vertices, 3);
    normals.resize(num_of_vertices, 3);
    colors.resize(num_of_vertices, 3);
  }

  void resizeFaces(const Eigen::Index num_of_faces) {
    faces.resize(num_of_faces, 3);
  }

  std::size_t sizeOfPositions() const {return positions.size() * sizeof(PositionScalar);}
  std::size_t sizeOfNormals() const {return normals.size() * sizeof(NormalScalar);}
  std::size_t sizeOfColors() const {return colors.size() * sizeof(ColorScalar);}
  std::size_t sizeOfLabels() const {return labels.size() * sizeof(LabelScalar);}

  PositionDataContainer positions;
  NormalDataContainer normals;
  ColorDataContainer colors;
  LabelDataContainer labels;
  FaceContainer faces;
};

}  // namespace CuteGL


#endif // end CUTEGL_CORE_MESHDATA_H_
