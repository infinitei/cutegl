/**
 * @file PoseUtils.h
 * @brief PoseUtils
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_CORE_POSEUTILS_H_
#define CUTEGL_CORE_POSEUTILS_H_

#include <Eigen/Geometry>
#include <Eigen/EulerAngles>

namespace CuteGL {

/// wrap x -> [0,max)
template<typename Scalar>
inline Scalar wrapMax(Scalar x, Scalar max) {
  return std::fmod(max + std::fmod(x, max), max);
}
/// wrap x -> [min,max)
template<typename Scalar>
inline Scalar wrapMinMax(Scalar x, Scalar min, Scalar max) {
  return min + wrapMax(x - min, max - min);
}

/**@brief wrap an angle to [-pi, pi)
 *
 * @param angle_in_radians
 * @return wrapped angle in [-pi, pi)
 */
template<typename Scalar>
inline Scalar wrapToPi(const Scalar angle_in_radians) {
  return wrapMinMax(angle_in_radians, -Scalar(M_PI), +Scalar(M_PI));
}

/// Functor version of wrapToPi to wrap and angle to [-pi, pi)
struct WrapToPi {
  WrapToPi() {}
  template<typename Scalar>
  const Scalar operator()(const Scalar& angle_in_radians) const {
    return wrapMinMax(angle_in_radians, -Scalar(M_PI), +Scalar(M_PI));
  }
};

/**@brief get cartesian coordinate from spherical coordinate
 *
 * @note We use the ISO (physics) convention with elevation_deg = 90 - inclination
 *
 * @param azimuth   - the angle from the north
 * @param elevation - the angle from the horizon
 * @param radial    - distance of object center
 * @return cartesian 3d position
 */
template<typename Scalar>
Eigen::Matrix<Scalar, 3, 1> spherical2cartesian(const Scalar azimuth, const Scalar elevation, const Scalar radial) {
  Eigen::Matrix<Scalar, 3, 1> cart;
  cart.x() = (radial * std::cos(elevation) * std::cos(azimuth));
  cart.y() = (radial * std::cos(elevation) * std::sin(azimuth));
  cart.z() = (radial * std::sin(elevation));
  return cart;
}

/**@brief Get Camera look at matrix
 *
 * @param eye       - eye (camera) coordinate
 * @param target    - target coordinate
 * @param up_vector - up direction
 * @return Eigen::Isometry object representing view matrix
 */
template<typename DerivedA, typename DerivedB, typename DerivedC>
Eigen::Transform<typename DerivedA::Scalar, 3, Eigen::Isometry>
getExtrinsicsFromLookAt(
    const Eigen::MatrixBase<DerivedA>& eye,
    const Eigen::MatrixBase<DerivedB>& target,
    const Eigen::MatrixBase<DerivedC>& up_vector) {

  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(DerivedA, 3);
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(DerivedB, 3);
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(DerivedC, 3);

  typedef typename DerivedA::Scalar Scalar;

  typedef Eigen::Matrix<Scalar, 3, 1> Vector3;
  typedef Eigen::Matrix<Scalar, 3, 3> Matrix3;
  typedef Eigen::Transform<Scalar, 3, Eigen::Isometry> Isometry3;

  Vector3 z = (target - eye).normalized();
  Vector3 x = ((-up_vector).cross(z)).normalized(); // minus upVector since yc is pointing down
  Vector3 y = z.cross(x);

  Matrix3 rot = (Matrix3() << x, y, z).finished();

  Isometry3 extrinsic = Isometry3::Identity();
  extrinsic.linear() = rot.transpose();
  extrinsic.translation() = -rot.transpose() * eye;

  return extrinsic;
}

/**@brief Get Camera extrinsic from viewpoint (azimuth, elevation, tilt, distance)
 *
 * @note all angles are expected in radians
 * @note if you are looking for camera extrinsics, just take the inverse
 *
 * @param azimuth     - the angle from the north
 * @param elevation   - the angle from the horizon
 * @param tilt        - camera roll along +z
 * @param distance    - distance of object center from camera
 * @return Eigen::Isometry object representing camera extrinsics
 */
template<typename Scalar>
Eigen::Transform<Scalar, 3, Eigen::Isometry>
getExtrinsicsFromViewPoint(const Scalar azimuth, const Scalar elevation, const Scalar tilt, const Scalar distance) {
  typedef Eigen::Transform<Scalar, 3, Eigen::Isometry> Isometry3;
  typedef Eigen::EulerSystem<-Eigen::EULER_Z, Eigen::EULER_Y, -Eigen::EULER_Z> ViewPointSystem;
  typedef Eigen::EulerAngles<Scalar, ViewPointSystem> ViewPointd;

  Isometry3 extrinsic = Isometry3::Identity();
  extrinsic.linear() = ViewPointd(tilt + M_PI/2, elevation + M_PI/2, azimuth).toRotationMatrix();
  extrinsic.translation().z() = distance;

  return extrinsic;
}

/**@brief Get Camera extrinsic from viewpoint [azimuth, elevation, tilt, distance]
 *
 * @param viewpoint - vector of size 4 [azimuth, elevation, tilt, distance]
 * @return camera extrinsic (or object pose relative to camera) as Eigen::Isometry3
 */
template<class Derived>
Eigen::Transform<typename Derived::Scalar, 3, Eigen::Isometry>
getExtrinsicsFromViewPoint(const Eigen::MatrixBase<Derived>& viewpoint) {
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(Derived, 4);
  return getExtrinsicsFromViewPoint(viewpoint.x(), viewpoint.y(), viewpoint.z(), viewpoint.w());
}


/** @brief Get rotation from viewpoint [azimuth, elevation, tilt]
 *
 * This is same as the linear part of the extrinsic matrix returned by getExtrinsicsFromViewPoint
 *
 * @param azimuth     - the angle from the north
 * @param elevation   - the angle from the horizon
 * @param tilt        - camera roll along +z
 * @return rotation matrix (extrinsic) defined by azimuth, elevation and tilt
 */
template<typename Scalar>
Eigen::Matrix<Scalar, 3, 3>
getRotationFromViewPoint(const Scalar azimuth, const Scalar elevation, const Scalar tilt) {
  typedef Eigen::EulerSystem<-Eigen::EULER_Z, Eigen::EULER_Y, -Eigen::EULER_Z> ViewPointSystem;
  typedef Eigen::EulerAngles<Scalar, ViewPointSystem> ViewPointd;
  return ViewPointd(tilt + M_PI/2, elevation + M_PI/2, azimuth).toRotationMatrix();
}

/**@brief Get rotation from viewpoint [azimuth, elevation, tilt]
 *
 * This is same as the linear part of the extrinsic matrix returned by getExtrinsicsFromViewPoint
 *
 * @param viewpoint - vector of size 3 [azimuth, elevation, tilt]
 * @return rotation matrix defined by azimuth, elevation and tilt
 */
template<class Derived>
Eigen::Matrix<typename Derived::Scalar, 3, 3>
getRotationFromViewPoint(const Eigen::MatrixBase<Derived>& viewpoint) {
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(Derived, 3);
  return getRotationFromViewPoint(viewpoint.x(), viewpoint.y(), viewpoint.z());
}

/**@brief Get viewpoint [azimuth, elevation, tilt] from rotation matrix
 *
 * @param rot_matrix 3x3 matrix
 * @return viewpoint as vector of size 3 [azimuth, elevation, tilt]
 */
template<class Derived>
Eigen::Matrix<typename Derived::Scalar, 3, 1>
getViewPointFromRotation(const Eigen::MatrixBase<Derived>& rot_matrix) {
  EIGEN_STATIC_ASSERT_MATRIX_SPECIFIC_SIZE(Derived, 3, 3);
  using Scalar = typename Derived::Scalar;
  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;
  using ViewPointSystem = Eigen::EulerSystem<-Eigen::EULER_Z, Eigen::EULER_Y, -Eigen::EULER_Z>;
  using ViewPointd = Eigen::EulerAngles<Scalar, ViewPointSystem>;

  Vector3 aet;
  {
    ViewPointd vp(rot_matrix);
    aet.x() = vp.angles().z();
    aet.y() = vp.angles().y() - M_PI / 2;
    aet.z() = wrapToPi(vp.angles().x() - M_PI / 2);
  }
  return aet;
}

/**@brief Creates perspective projection matrix (intrinsic) matrix for OpenGL
 *
 * See http://www.songho.ca/opengl/gl_projectionmatrix.html for explanation.
 * But we use a different (standard in computer vision) eye coordinate convention:
 * +z forward
 * +x right
 * +y down
 *
 * @param fovY    - field of view in Y direction (in radians)
 * @param aspect  - aspect ration = width / height
 * @param near_z- - near z clipping plane
 * @param far_z   - far z clipping plane
 * @return 4x4 projection matrix
 */
template <typename Scalar>
Eigen::Matrix<Scalar ,4, 4 > getGLPerspectiveProjection(Scalar fovY, Scalar aspect, Scalar near_z, Scalar far_z) {
  Scalar theta = fovY / 2;
  Scalar range = far_z - near_z;
  Scalar invtan = 1./std::tan(theta);

  Eigen::Matrix<Scalar ,4, 4 > projection_matrix;
  projection_matrix.setZero();

  projection_matrix(0,0) = invtan / aspect;
  projection_matrix(1,1) = -invtan;
  projection_matrix(2,2) = (near_z + far_z) / range;
  projection_matrix(3,2) = 1;
  projection_matrix(2,3) = -2 * near_z * far_z / range;
  projection_matrix(3,3) = 0;

  return projection_matrix;
}

/**@brief Creates perspective projection matrix (intrinsic) matrix for OpenGL
 *
 * @param fx
 * @param fy
 * @param skew
 * @param cx
 * @param cy
 * @param img_width
 * @param img_height
 * @param near_z
 * @param far_z
 * @return 4x4 projection matrix
 */
template<typename Scalar,typename RangeScalar, typename SizeScalar>
Eigen::Matrix<Scalar, 4, 4> getGLPerspectiveProjection(Scalar fx, Scalar fy,
                                                       Scalar skew,
                                                       Scalar cx, Scalar cy,
                                                       SizeScalar img_width, SizeScalar img_height,
                                                       RangeScalar near_z, RangeScalar far_z) {
  assert(far_z >= near_z);

  using Matrix4 = Eigen::Matrix<Scalar, 4, 4>;

  // left, right, top bottom
  Scalar L = 0;
  Scalar R = img_width;
  Scalar B = img_height;
  Scalar T = 0;

  // near and far clipping planes, these only matter for the mapping from
  // world-space z-coordinate into the depth coordinate for OpenGL
  Scalar N = near_z;
  Scalar F = far_z;

  // construct an orthographic projection to normalized device coordinates
  Matrix4 ortho = Matrix4::Zero();
  ortho(0,0) =  2.0/(R-L); ortho(0,3) = -(R+L)/(R-L);
  ortho(1,1) =  2.0/(T-B); ortho(1,3) = -(T+B)/(T-B);
  ortho(2,2) =  2.0/(F-N); ortho(2,3) = -(F+N)/(F-N);
  ortho(3,3) =  1.0;


  // perspective projection matrix, this is similar to intrinsic matrix,
  // with an added row, to map the z-coordinate to OpenGL.
  Matrix4 tproj = Matrix4::Zero();
  tproj(0,0) = fx; tproj(0,1) = skew; tproj(0,2) = cx;
                   tproj(1,1) = fy;   tproj(1,2) = cy;
                                      tproj(2,2) = (N+F); tproj(2,3) = -N*F;
                                      tproj(3,2) = 1.0;


  return ortho * tproj;
}

/**@brief Creates perspective projection matrix (intrinsic) matrix for OpenGL
 *
 * @param K
 * @param img_width
 * @param img_height
 * @param near_z
 * @param far_z
 * @return 4x4 projection matrix
 */
template<class Derived,typename RangeScalar, typename SizeScalar>
Eigen::Matrix<typename Derived::Scalar, 4, 4> getGLPerspectiveProjection(const Eigen::MatrixBase<Derived>& K,
                                                                         SizeScalar img_width, SizeScalar img_height,
                                                                         RangeScalar near_z, RangeScalar far_z) {

  return getGLPerspectiveProjection(K(0, 0), K(1,1), K(0,1), K(0,2), K(1,2), img_width, img_height, near_z, far_z);
}


/**@brief Creates orthographic projection matrix (intrinsic) matrix for OpenGL
 *
 * See http://www.songho.ca/opengl/gl_projectionmatrix.html for explanation.
 * But we use a different (standard in computer vision) eye coordinate convention:
 * +z forward
 * +x right
 * +y down
 *
 * @param half_width  - Screen half size in X direction
 * @param half_height - Screen half size in Y direction
 * @param N  - near clipping plane
 * @param F  - far clipping plane
 * @return 4x4 projection matrix
 */
template<typename Scalar, typename SizeScalar>
Eigen::Matrix<Scalar, 4, 4> getGLOrthographicProjection(SizeScalar half_width,
                                                        SizeScalar half_height,
                                                        Scalar N,
                                                        Scalar F) {
  Eigen::Matrix<Scalar, 4, 4> ortho;
  ortho.setZero();

  // left, right, top bottom
  Scalar L = -half_width;
  Scalar R = half_width;
  Scalar B = half_height;
  Scalar T = -half_height;

  // near and far clipping planes, these only matter for the mapping from
  // world-space z-coordinate into the depth coordinate for OpenGL

  // construct an orthographic projection to normalized device coordinates
  ortho(0,0) =  2.0/(R-L); ortho(0,3) = -(R+L)/(R-L);
  ortho(1,1) =  2.0/(T-B); ortho(1,3) = -(T+B)/(T-B);
  ortho(2,2) =  2.0/(F-N); ortho(2,3) = -(F+N)/(F-N);
  ortho(3,3) =  1.0;

  return ortho;
}

/**@brief Creates orthographic projection matrix (intrinsic) matrix for OpenGL
 *
 * See http://www.songho.ca/opengl/gl_projectionmatrix.html for explanation.
 * But this function use a different +z as forward instead of negative
 *
 * @param L  - left side
 * @param R  - right side
 * @param T  - top side
 * @param B  - bottom side
 * @param N  - near clipping plane
 * @param F  - far clipping plane
 * @return 4x4 projection matrix
 *
 *
 */
template<typename Scalar, typename SizeScalar>
Eigen::Matrix<Scalar, 4, 4> getGLOrthographicProjection(Scalar L,
                                                        Scalar R,
                                                        Scalar T,
                                                        Scalar B,
                                                        Scalar N,
                                                        Scalar F) {
  Eigen::Matrix<Scalar, 4, 4> ortho;
  ortho.setZero();

  // near and far clipping planes, these only matter for the mapping from
  // world-space z-coordinate into the depth coordinate for OpenGL

  // construct an orthographic projection to normalized device coordinates
  ortho(0,0) =  2.0/(R-L); ortho(0,3) = -(R+L)/(R-L);
  ortho(1,1) =  2.0/(T-B); ortho(1,3) = -(T+B)/(T-B);
  ortho(2,2) =  2.0/(F-N); ortho(2,3) = -(F+N)/(F-N);
  ortho(3,3) =  1.0;

  return ortho;
}

}  // end namespace CuteGL

#endif // end CUTEGL_CORE_POSEUTILS_H_
