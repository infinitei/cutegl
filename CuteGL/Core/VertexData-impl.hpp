/**
 * @file VertexData-impl.hpp
 * @brief VertexData-impl
 *
 * @author Abhijit Kundu
 */


#ifndef CUTEGL_CORE_VERTEXDATA_IMPL_HPP_
#define CUTEGL_CORE_VERTEXDATA_IMPL_HPP_

#include "CuteGL/Core/VertexTraits.h"

namespace CuteGL {

namespace detail {

template <class VertexType, class PositionType>
void setVertexPositionImpl(VertexType& vd, const PositionType& position, std::true_type) {
  vd.position = position;
}
template <class VertexType, class PositionType>
void setVertexPositionImpl(VertexType& vd, const PositionType& position, std::false_type) {
}

template <class VertexType, class NormalType>
void setVertexNormalImpl(VertexType& vd, const NormalType& normal, std::true_type) {
  vd.normal = normal;
}
template <class VertexType, class NormalType>
void setVertexNormalImpl(VertexType& vd, const NormalType& normal, std::false_type) {
}

template <class VertexType, class ColorType>
void setVertexColorImpl(VertexType& vd, const ColorType& color, std::true_type) {
  vd.color = color;
}
template <class VertexType, class ColorType>
void setVertexColorImpl(VertexType& vd, const ColorType& color, std::false_type) {
}


}  // namespace detail

/// generic setter of vertex position
template <class VertexType, class PositionType>
void setVertexPosition(VertexType& vd, const PositionType& position) {
  detail::setVertexPositionImpl(vd, position, HasPosition<VertexType>());
}

/// generic setter of vertex normal
template <class VertexType, class NormalType>
void setVertexNormal(VertexType& vd, const NormalType& normal) {
  detail::setVertexNormalImpl(vd, normal, HasNormal<VertexType>());
}

/// generic setter of vertex color
template <class VertexType, class ColorType>
void setVertexColor(VertexType& vd, const ColorType& color) {
  detail::setVertexColorImpl(vd, color, HasColor<VertexType>());
}



}  // namespace CuteGL


#endif // end CUTEGL_CORE_VERTEXDATA_IMPL_HPP_
