/**
 * @file VertexArrayObject.cpp
 * @brief VertexArrayObject
 *
 * @author Abhijit Kundu
 */

#include "VertexArrayObject.h"
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLContext>

namespace CuteGL {

VertexArrayObject::VertexArrayObject()
    : funcs_(nullptr),
      object_id_(0) {
}

void VertexArrayObject::create() {
  QOpenGLContext* context = QOpenGLContext::currentContext();
  if (!context) {
    std::runtime_error("Cannot Get Current Context");
  }
  funcs_ = context->versionFunctions<QOpenGLFunctions_3_3_Core>();
  if (!funcs_) {
    std::runtime_error("Cannot Resolve Functions");
  }
  funcs_->initializeOpenGLFunctions();

  funcs_->glGenVertexArrays(1, &object_id_);
}

void VertexArrayObject::create(QOpenGLFunctions_3_3_Core* funcs) {
  funcs_ = funcs;
  if (!funcs_) {
    std::runtime_error("Cannot Resolve Functions");
  }
  funcs_->glGenVertexArrays(1, &object_id_);
}

void VertexArrayObject::bind() {
  funcs_->glBindVertexArray(object_id_);
}

void VertexArrayObject::release() {
  funcs_->glBindVertexArray(0);
}

void VertexArrayObject::destroy() {
  funcs_->glDeleteVertexArrays(1, &object_id_);
}

}  // end namespace CuteGL
