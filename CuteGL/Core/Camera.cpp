/**
 * @file Camera.cpp
 * @brief Camera
 *
 * @author Abhijit Kundu
 */
#define _USE_MATH_DEFINES

#include "Camera.h"
#include "PoseUtils.h"

namespace CuteGL {

Camera::Camera(int width, int height)
    : image_width_(width),
      image_height_(height),
      extrinsics_(Eigen::Isometry3f::Identity()) {
  recomputeIntrinsics();
}

// TODO (current implementation makes sense for perspective camera only)
void Camera::resize(int w, int h) {
  image_width_ = w;
  image_height_ = h;

  // Now update the intrinsics with the new aspect ratio (Keep FOV same)
  const float aspect_ratio = float(image_width_) / image_height_;
  const float inv_tan_half_fovy = -intrinsics_(1, 1);
  intrinsics_(0,0) = inv_tan_half_fovy/ aspect_ratio;
}

void Camera::recomputeIntrinsics() {
  float fovY = 45.0f * M_PI / 180.0f;
  float aspect_ratio = float(image_width_) / image_height_;
  intrinsics_.matrix() = getGLPerspectiveProjection(fovY, aspect_ratio, 0.1f, 100.0f);
}

// TODO (current implementation makes sense for perspective camera only)
void Camera::setFrustumNearFarPlanes(float near_z, float far_z) {
  float range = far_z - near_z;
  intrinsics_(2,2) = (near_z + far_z) / range;
  intrinsics_(2,3) = -2 * near_z * far_z / range;
}


float Camera::fovY() const {
  float inv_tan_half_fovy = -intrinsics_(1, 1);
  return 2.0f * std::atan(1.f/inv_tan_half_fovy);
}

float Camera::fovX() const {
  float aspect_ratio = float(image_width_) / image_height_;
  return aspect_ratio * fovY();
}

Eigen::Vector2f Camera::fieldOfView() const {
  const float aspect_ratio = float(image_width_) / image_height_;
  const float fovy = fovY();
  return Eigen::Vector2f(aspect_ratio * fovy, fovy);
}


}  // end namespace CuteGL
