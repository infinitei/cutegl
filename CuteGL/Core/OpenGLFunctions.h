/**
 * @file OpenGLFunctions.h
 * @brief OpenGLFunctions
 *
 * @author Abhijit Kundu
 */


#ifndef CUTEGL_CORE_OPENGLFUNCTIONS_H_
#define CUTEGL_CORE_OPENGLFUNCTIONS_H_

#include <QOpenGLFunctions_4_3_Core>

namespace CuteGL {

/// OpenGL version used by CuteGL

class OpenGLFunctions : public QOpenGLFunctions_4_3_Core {
public:
  OpenGLFunctions() {}
  bool hasBeenInitialized() const;
};

}  // namespace CuteGL


#endif // end CUTEGL_CORE_OPENGLFUNCTIONS_H_
