/**
 * @file ImportPLY.h
 * @brief ImportPLY
 *
 * @author Abhijit Kundu
 */


#ifndef CUTEGL_IO_IMPORTPLY_H_
#define CUTEGL_IO_IMPORTPLY_H_

#include "CuteGL/Core/MeshData.h"
#include <string>
#include <vector>

namespace CuteGL {

/**@brief loads MeshData from a PLY file
 *
 * @param file_name
 * @return mesh data
 */
MeshData loadMeshFromPLY(const std::string& file_name);

/**@brief loads point cloud (vertices) from a PLY file
 *
 * @param file_name
 * @return pointcloud as std::vector of VertexXYZRGB
 */
std::vector<VertexXYZRGBA> loadPointCloudFromPLY(const std::string& file_name);


/**@brief saves MeshData to an ASCII PLY file
 *
 * @param mesh_data
 * @param filename
 * @param save_normal
 * @param save_color
 */
void saveMeshAsASCIIPly(const MeshData& mesh_data, const std::string& filename,
                        const bool save_normal = true, const bool save_color = true);


/**@brief saves MeshData to an Binary PLY file
 *
 * @param mesh_data
 * @param filename
 * @param save_normal
 * @param save_color
 */
void saveMeshAsBinPly(const MeshData& mesh_data, const std::string& filename,
                      const bool save_normal = true, const bool save_color = true);


/**@brief save pointcloud as ASCII PLY file
 *
 * @param vertices - std::vector of VertexXYZRGBA
 * @param filename - filename to save to
 */
void savePointCloudAsASCIIPly(const std::vector<VertexXYZRGBA>& vertices, const std::string& filename);

/**@brief save pointcloud as Binary PLY file
 *
 * @param vertices - std::vector of VertexXYZRGBA
 * @param filename - filename to save to
 */
void savePointCloudAsBinPly(const std::vector<VertexXYZRGBA>& vertices, const std::string& filename);


/**@brief save pointcloud as ASCII PLY file
 *
 * @param point_positions - std::vector of point coordinates
 * @param filename        - filename to save to
 */
template <class Derived>
void savePointCloudAsASCIIPly(const Eigen::DenseBase<Derived>& point_positions, const std::string& filename);


/**@brief save pointcloud as Binary PLY file
 *
 * @param point_positions
 * @param filename
 */
template <class Derived>
void savePointCloudAsBinPly(const Eigen::DenseBase<Derived>& point_positions, const std::string& filename);

/**@brief save pointcloud as ASCII PLY file
 *
 * @param point_positions
 * @param filename
 */
template <class PointType, class PA>
void savePointCloudAsASCIIPly(const std::vector<PointType, PA>& point_positions, const std::string& filename);


/**@brief save pointcloud as Binary PLY file
 *
 * @param point_positions - std::vector of point coordinates
 * @param filename        - filename to save to
 */
template <class PointType, class PA>
void savePointCloudAsBinPly(const std::vector<PointType, PA>& point_positions, const std::string& filename);


}  // namespace CuteGL

#include "CuteGL/IO/ImportPLY-impl.hpp"

#endif // end CUTEGL_IO_IMPORTPLY_H_
