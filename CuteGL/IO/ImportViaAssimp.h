/**
 * @file ImportViaAssimp.h
 * @brief ImportViaAssimp
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_IO_IMPORTVIAASSIMP_H_
#define CUTEGL_IO_IMPORTVIAASSIMP_H_

#include "CuteGL/Core/MeshData.h"
#include <string>
#include <vector>

namespace CuteGL {

std::vector<MeshData> loadMeshesViaAssimp(const std::string& asset_file,
                                          const MeshData::ColorType& color = MeshData::ColorType(180, 180, 180, 255));

}  // namespace CuteGL

#endif // end CUTEGL_IO_IMPORTVIAASSIMP_H_


