/**
 * @file ImportOFF.h
 * @brief ImportOFF
 *
 * @author Abhijit Kundu
 */


#ifndef CUTEGL_IO_IMPORTOFF_H_
#define CUTEGL_IO_IMPORTOFF_H_

#include "CuteGL/Core/MeshData.h"
#include <string>
#include <vector>

namespace CuteGL {

/**@brief Reads a mesh data from a OFF file
 *
 * @param[in] file_name
 * @param[out] vertices
 * @param[out] indices
 */
template <typename VertexData, typename IndexData>
void importOFF(const std::string& file_name,
               std::vector<VertexData>& vertices,
               std::vector<IndexData>& indices);


/**@brief creates MeshData from a OFF file
 *
 * The function also computes the normals since they are
 * not available in a standard OFF file.
 *
 * @param file_name
 * @param color defaults to (0.7, 07., 0.7)
 * @return a MeshData object
 */
MeshData loadMeshFromOFF(const std::string& file_name,
                         const MeshData::ColorType& color = MeshData::ColorType(180, 180, 180, 255));

}  // namespace CuteGL

#include "CuteGL/IO/ImportOFF-impl.hpp"

#endif // end CUTEGL_IO_IMPORTOFF_H_
