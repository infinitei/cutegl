/**
 * @file ImportPLY-impl.hpp
 * @brief ImportPLY-impl
 *
 * @author Abhijit Kundu
 */


#ifndef CUTEGL_IO_IMPORTPLY_IMPL_HPP_
#define CUTEGL_IO_IMPORTPLY_IMPL_HPP_

#include <fstream>

namespace CuteGL {

template <class Derived>
void savePointCloudAsASCIIPly(const Eigen::DenseBase<Derived>& point_positions, const std::string& filename) {
  static_assert(Derived::ColsAtCompileTime == 3, "Expects a N x 3 matrix");
  std::ofstream ofs(filename.c_str());
  if (!ofs.is_open())
    throw std::runtime_error("Cannot save at " + filename);

  ofs << "ply"
      << '\n' << "format ascii 1.0"
      << '\n' << "element vertex " << point_positions.rows()
      << '\n' << "property float x"
      << '\n' << "property float y"
      << '\n' << "property float z"
      << '\n' << "end_header" << '\n';

  for (Eigen::Index i = 0; i < point_positions.rows(); ++i) {
    ofs << point_positions(i, 0) << ' '
        << point_positions(i, 1) << ' '
        << point_positions(i, 2) << '\n';
  }
  ofs.close();
}

template <class Derived>
void savePointCloudAsBinPly(const Eigen::DenseBase<Derived>& point_positions, const std::string& filename) {
  static_assert(Derived::ColsAtCompileTime == 3, "Expects a N x 3 matrix");

  std::ofstream ofs(filename.c_str(), std::ios::binary | std::ios::out);
  if (!ofs.is_open())
    throw std::runtime_error("Cannot save at " + filename);

  ofs << "ply"
      << '\n' << "format binary_little_endian 1.0"
      << '\n' << "element vertex " << point_positions.rows()
      << '\n' << "property float x"
      << '\n' << "property float y"
      << '\n' << "property float z"
      << '\n' << "end_header" << '\n';

  for (Eigen::Index i = 0; i < point_positions.rows(); ++i) {
    const Eigen::Vector3f position = point_positions.row(i).template cast<float>();
    ofs.write(reinterpret_cast<const char*>(position.data()), 3 * sizeof(float));
  }
  ofs.close();
}

template <class PointType, class PA>
void savePointCloudAsASCIIPly(const std::vector<PointType, PA>& point_positions, const std::string& filename) {
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(PointType, 3);

  std::ofstream ofs(filename.c_str());
  if (!ofs.is_open())
    throw std::runtime_error("Cannot save at " + filename);

  ofs << "ply"
      << '\n' << "format ascii 1.0"
      << '\n' << "element vertex " << point_positions.size()
      << '\n' << "property float x"
      << '\n' << "property float y"
      << '\n' << "property float z"
      << '\n' << "end_header" << '\n';

  for (std::size_t i = 0; i < point_positions.size(); ++i) {
    const PointType& point_position = point_positions[i];
    ofs << point_position[0] << ' '
        << point_position[1] << ' '
        << point_position[2] << '\n';
  }
  ofs.close();
}

template <class PointType, class PA>
void savePointCloudAsBinPly(const std::vector<PointType, PA>& point_positions, const std::string& filename) {
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(PointType, 3);

  std::ofstream ofs(filename.c_str(), std::ios::binary | std::ios::out);
  if (!ofs.is_open())
    throw std::runtime_error("Cannot save at " + filename);

  ofs << "ply"
      << '\n' << "format binary_little_endian 1.0"
      << '\n' << "element vertex " << point_positions.size()
      << '\n' << "property float x"
      << '\n' << "property float y"
      << '\n' << "property float z"
      << '\n' << "end_header" << '\n';

  for (std::size_t i = 0; i < point_positions.size(); ++i) {
    const Eigen::Vector3f position = point_positions[i].template cast<float>();
    ofs.write(reinterpret_cast<const char*>(position.data()), 3 * sizeof(float));
  }
  ofs.close();
}

}  // namespace CuteGL


#endif // end CUTEGL_IO_IMPORTPLY_IMPL_HPP_
