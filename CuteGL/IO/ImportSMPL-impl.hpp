/**
 * @file ImportSMPL-impl.hpp
 * @brief ImportSMPL-impl
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_IO_IMPORTSMPL_IMPL_HPP_
#define CUTEGL_IO_IMPORTSMPL_IMPL_HPP_

#include "CuteGL/IO/H5EigenTensor.h"
#include "CuteGL/Core/WarningsHelper.h"

namespace CuteGL {

template<class SMPLDataType>
SMPLDataType loadSMPLDataFromHDF5(const std::string& smpl_model_h5_file) {
  SMPLDataType smpl_data;
  loadSMPLDataFromHDF5(smpl_data, smpl_model_h5_file);
  return smpl_data;
}

template<class SMPLDataType>
void loadSMPLDataFromHDF5(SMPLDataType& smpl_data, const std::string& smpl_model_h5_file) {
  H5::H5File file(smpl_model_h5_file, H5F_ACC_RDONLY);

  H5Eigen::load(file, "parents", smpl_data.parents);
  H5Eigen::load(file, "faces", smpl_data.faces);

  H5Eigen::load(file, "template_vertices", smpl_data.template_vertices);
  H5Eigen::load(file, "blend_weights", smpl_data.blend_weights);
  H5Eigen::load(file, "joint_regressor", smpl_data.joint_regressor);

  H5Eigen::load(file, "pose_displacements", smpl_data.pose_displacements);

  {
    typename SMPLDataType::Tensor3 temp;
    H5Eigen::load(file, "shape_displacements", temp);
    // shape_displacements is originally N x 3 x 10, we will change it to 10 x N x 3 order
    Eigen::array<int, 3> shuffle({2, 0, 1});
    smpl_data.shape_displacements = temp.shuffle(shuffle);
  }

  {
    typename SMPLDataType::Tensor3 temp;
    H5Eigen::load(file, "pose_displacements", temp);
    // shape_displacements is originally N x 3 x 9*K, we will change it to 9*K x N x 3 order
    Eigen::array<int, 3> shuffle({2, 0, 1});
    smpl_data.pose_displacements = temp.shuffle(shuffle);
  }



  // Check dimensions
  const Eigen::Index N = smpl_data.template_vertices.rows();
  const Eigen::Index K = smpl_data.blend_weights.cols() - 1;

  // parents K+1 vector
  assert(smpl_data.parents.size() == K+1);

  // blend_weights N x K+1
  assert(smpl_data.blend_weights.rows() == N);
  assert(smpl_data.blend_weights.cols() == K+1);

  // joint_regressor K+1 x N
  assert(smpl_data.joint_regressor.rows() == K+1);
  assert(smpl_data.joint_regressor.cols() == N);

  // shape_displacements 10 x N x 3
  assert(smpl_data.shape_displacements.dimension(1) == N);
  assert(smpl_data.shape_displacements.dimension(2) == 3);

  // pose_displacements N x 3 x 9K
  assert(smpl_data.pose_displacements.dimension(0) == 9*K);
  assert(smpl_data.pose_displacements.dimension(1) == N);
  assert(smpl_data.pose_displacements.dimension(2) == 3);

  // ignore unused warning
  CuteGL::ignore_unused(N, K);
}

template<class SMPLSegmmDataType>
SMPLSegmmDataType loadSMPLSegmmDataFromHDF5(const std::string& smpl_segmm_h5_file) {
  SMPLSegmmDataType smpl_segmm_data;
  loadSMPLSegmmDataFromHDF5(smpl_segmm_data, smpl_segmm_h5_file);
  return smpl_segmm_data;
}

template<class SMPLSegmmDataType>
void loadSMPLSegmmDataFromHDF5(SMPLSegmmDataType& smpl_segmm_data, const std::string& smpl_segmm_h5_file) {
  H5::H5File file(smpl_segmm_h5_file, H5F_ACC_RDONLY);

  H5Eigen::load(file, "color_map", smpl_segmm_data.color_map);
  H5Eigen::load(file, "vertex_segmm", smpl_segmm_data.vertex_labels);

  assert(smpl_segmm_data.color_map.cols() == 4);
  assert(smpl_segmm_data.vertex_labels.minCoeff() == 0);
  assert(smpl_segmm_data.vertex_labels.maxCoeff() == (smpl_segmm_data.color_map.rows() - 1));

}

}  // namespace CuteGL

#endif // end CUTEGL_IO_IMPORTSMPL_IMPL_HPP_
