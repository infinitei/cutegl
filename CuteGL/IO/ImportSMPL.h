/**
 * @file ImportSMPL.h
 * @brief ImportSMPL
 *
 * @author Abhijit Kundu
 */

#ifndef CUTEGL_IO_IMPORTSMPL_H_
#define CUTEGL_IO_IMPORTSMPL_H_

#include "CuteGL/Core/SMPLData.h"

namespace CuteGL {

SMPLData32F32U loadSMPLData32F32UFromHDF5(const std::string& smpl_model_h5_file);

template <class SMPLDataType>
SMPLDataType loadSMPLDataFromHDF5(const std::string& smpl_model_h5_file);

template <class SMPLDataType>
void loadSMPLDataFromHDF5(SMPLDataType& smpl_data, const std::string& smpl_model_h5_file);

template <class SMPLSegmmDataType>
SMPLSegmmDataType loadSMPLSegmmDataFromHDF5(const std::string& smpl_segmm_h5_file);

template <class SMPLSegmmDataType>
void loadSMPLSegmmDataFromHDF5(SMPLSegmmDataType& smpl_segmm_data, const std::string& smpl_segmm_h5_file);

}  // namespace CuteGL

#include "CuteGL/IO/ImportSMPL-impl.hpp"

#endif // end CUTEGL_IO_IMPORTSMPL_H_
