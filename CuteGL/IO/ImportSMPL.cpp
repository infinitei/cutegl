/**
 * @file ImportSMPL.cpp
 * @brief ImportSMPL
 *
 * @author Abhijit Kundu
 */

#include "ImportSMPL.h"

namespace CuteGL {

SMPLData32F32U loadSMPLData32F32UFromHDF5(const std::string& smpl_model_h5_file) {
  static_assert(std::is_same<Eigen::Index, int>::value, "Need to set EIGEN_DEFAULT_DENSE_INDEX_TYPE to int");
  SMPLData32F32U smpl_data;
  loadSMPLDataFromHDF5(smpl_data, smpl_model_h5_file);
  return smpl_data;
}

}  // namespace CuteGL


