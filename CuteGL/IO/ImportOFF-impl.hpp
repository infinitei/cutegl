/**
 * @file ImportOFF-impl.hpp
 * @brief ImportOFF-impl
 *
 * @author Abhijit Kundu
 */


#ifndef CUTEGL_IO_IMPORTOFF_IMPL_HPP_
#define CUTEGL_IO_IMPORTOFF_IMPL_HPP_

#include <fstream>
#include <array>

namespace CuteGL {

template <typename VertexData, typename Derived>
void importOFF(const std::string& file_name,
               std::vector<VertexData>& vertices,
               Eigen::DenseBase<Derived> const & faces_) {

  std::ifstream file(file_name.c_str());
  if (!file.is_open()) {
    throw std::runtime_error("Import OFF Error: Cannot open File from " + file_name);
    return;
  }

  /// Check the header
  {
    std::string header_line;
    std::getline(file, header_line);
    if (header_line != "OFF") {
      throw std::runtime_error("Import OFF Error: Incorrect header: " + header_line + " while reading OFF file");
      file.close();
      return;
    }
  }

  std::size_t number_of_vertices = 0;
  std::size_t number_of_faces = 0;
  std::size_t number_of_edges = 0;

  // Get the number of vertices/ faces while ignoring comments
  {
    std::string line;
    while (std::getline(file, line)) {
      if (line.at(0) == '#')
        continue;

      std::istringstream iss(line);

      if (!(iss >> number_of_vertices >> number_of_faces >> number_of_edges)) {
        throw std::runtime_error("Import OFF Error: Cannot get mesh size");
        file.close();
        return;
      }

      break;
    }
  }


  // Get the vertices
  {
    vertices.reserve(number_of_vertices);
    for (std::size_t i = 0; i < number_of_vertices; ++i) {
      std::string line;
      if (!std::getline(file, line)) {
        throw std::runtime_error(
            "Import OFF Error: Cannot read line for Vertex# "
                + std::to_string(i) + " / "
                + std::to_string(number_of_vertices));
        file.close();
        return;
      }

      std::istringstream iss(line);
      float x, y, z;

      if (!(iss >> x >> y >> z)) {
        throw std::runtime_error("Import OFF Error: Cannot read vertex");
        file.close();
        return;
      }

      VertexData vd;
      vd.position.x() = x;
      vd.position.y() = y;
      vd.position.z() = z;
      vertices.push_back(vd);
    }
  }

  {
    using IndexData = typename Derived::Scalar;
    std::vector<IndexData> indices;
    indices.reserve(number_of_faces * 3);
    for (std::size_t i = 0; i < number_of_faces; ++i) {
      std::string line;
      if (!std::getline(file, line)) {
        throw std::runtime_error(
            "Import OFF Error: Cannot read line for Face# "
                + std::to_string(i) + " / "
                + std::to_string(number_of_faces));
        file.close();
        return;
      }

      std::istringstream iss(line);

      int face_vertex_count;
      iss >> face_vertex_count;

      if (face_vertex_count == 3) {
        for (int fv = 0; fv < 3; ++fv) {
          IndexData index;
          iss >> index;
          indices.push_back(index);
        }
      }
      else if(face_vertex_count == 4) {
        // Convert the quad to 2 triangles
        std::array<IndexData, 4> quad;
        iss >> quad[0] >> quad[1] >> quad[2] >> quad[3];
        indices.push_back(quad[0]);
        indices.push_back(quad[1]);
        indices.push_back(quad[2]);
        indices.push_back(quad[3]);
        indices.push_back(quad[0]);
        indices.push_back(quad[2]);
      }
      else {
        throw std::runtime_error("Import OFF Error: Currently we only support 3/4 vertex face only");
        file.close();
        return;
      }
    }

    using FacesContainer = Eigen::DenseBase<Derived>;
    FacesContainer& faces = const_cast<FacesContainer& >(faces_);
    Eigen::Index number_of_face_trangles = indices.size() / 3;
    faces.derived().resize(number_of_face_trangles, 3);

    for ( Eigen::Index face_id = 0; face_id < number_of_face_trangles; ++face_id) {
      faces(face_id, 0) = indices[3*face_id];
      faces(face_id, 1) = indices[3*face_id + 1];
      faces(face_id, 2) = indices[3*face_id + 2];
    }
  }

  file.close();
}

}  // namespace CuteGL

#endif // end CUTEGL_IO_IMPORTOFF_IMPL_HPP_
