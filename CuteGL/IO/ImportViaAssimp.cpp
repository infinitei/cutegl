/**
 * @file ImportViaAssimp.cpp
 * @brief ImportViaAssimp
 *
 * @author Abhijit Kundu
 */

#include "ImportViaAssimp.h"
#include <Eigen/Geometry>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <iostream>

namespace CuteGL {

namespace detail {

MeshData extractMeshData(const aiMesh* mesh, const Eigen::Affine3f& tfm) {

  MeshData md;

  assert(mesh->HasPositions());
  assert(mesh->HasNormals());

  // Get all vertexdata from this mesh
  md.vertices.resize(mesh->mNumVertices);
  for (size_t i = 0; i < md.vertices.size(); ++i) {
    MeshData::VertexData& vd = md.vertices[i];

    //position
    vd.position.x() = mesh->mVertices[i].x;
    vd.position.y()= mesh->mVertices[i].y;
    vd.position.z() = mesh->mVertices[i].z;

    //normals
    vd.normal.x() = mesh->mNormals[i].x;
    vd.normal.y() = mesh->mNormals[i].y;
    vd.normal.z() = mesh->mNormals[i].z;
  }

  // Get all Indices data
  md.faces.resize(mesh->mNumFaces, 3);
  for (unsigned int i = 0; i < mesh->mNumFaces; ++i) {
    const aiFace& face = mesh->mFaces[i];
    // TODO: This can be more than 3??
    assert(face.mNumIndices == 3);
    for (Eigen::Index j = 0; j < 3; ++j) //0..2
      md.faces(i, j)  = face.mIndices[j];
  }

  // Now apply transformation to all vertex data
  const Eigen::Matrix3f normal_matrix = tfm.linear().inverse().transpose();
  for(MeshData::VertexData& vd : md.vertices) {
    // Transform Vertex position
    vd.position = tfm * vd.position;

    // Transform vertex normal
    vd.normal = (normal_matrix * vd.normal).normalized();
  }

  return md;
}

void recursiveCreateMeshes(const aiScene* scene,
                           const aiNode* node,
                           const Eigen::Affine3f& current_tfm,
                           std::vector<MeshData>& mesh_datas) {

  // Get node transformation matrix
  aiMatrix4x4 rel_tfm = node->mTransformation;
  // OpenGL matrices are column major
  ///rel_tfm.Transpose();

  Eigen::Matrix4f rel;
  rel<< rel_tfm.a1, rel_tfm.a2, rel_tfm.a3, rel_tfm.a4,
        rel_tfm.b1, rel_tfm.b2, rel_tfm.b3, rel_tfm.b4,
        rel_tfm.c1, rel_tfm.c2, rel_tfm.c3, rel_tfm.c4,
        rel_tfm.d1, rel_tfm.d2, rel_tfm.d3, rel_tfm.d4;

  const Eigen::Affine3f tfm (current_tfm.matrix() * rel);

  //process
  for (unsigned int i = 0; i < node->mNumMeshes; ++i) {
    const aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
    mesh_datas.push_back(extractMeshData(mesh, tfm));
  }

  //recursion
  for (unsigned int i = 0; i < node->mNumChildren; ++i) {
    recursiveCreateMeshes(scene, node->mChildren[i], tfm, mesh_datas);
  }
}

} // end namespace detail

std::vector<MeshData> loadMeshesViaAssimp(const std::string& asset_file,  const MeshData::ColorType& color) {
  std::vector<MeshData> mesh_datas;

  Assimp::Importer importer;
  importer.SetPropertyInteger(AI_CONFIG_PP_SBP_REMOVE, aiPrimitiveType_POINT | aiPrimitiveType_LINE);

  const aiScene* scene = importer.ReadFile( asset_file,
                     aiProcess_GenSmoothNormals       |
                     aiProcess_Triangulate            |
                     aiProcess_JoinIdenticalVertices  |
                     aiProcess_SortByPType);

  if (!scene) {
    std::cout << importer.GetErrorString() << std::endl;
    throw std::runtime_error("ERROR loading Meshes Via Assimp from \"" + asset_file +"\"");
    return mesh_datas;
  }

  if (scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE ) {
    std::cout << "Assimp Scene Flags Incomplete" << std::endl;
    throw std::runtime_error("ERROR loading Meshes Via Assimp from \"" + asset_file +"\"");
    return mesh_datas;
  }

  Eigen::Affine3f init_transform = Eigen::Affine3f::Identity();

  detail::recursiveCreateMeshes(scene, scene->mRootNode, init_transform, mesh_datas);

  // Now set the colors
  for (MeshData& md : mesh_datas) {
    for (MeshData::VertexData& vd : md.vertices)
      vd.color = color;
  }

  return mesh_datas;
}

}  // namespace CuteGL

