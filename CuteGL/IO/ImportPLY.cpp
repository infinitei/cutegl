/**
 * @file ImportPLY.cpp
 * @brief ImportPLY
 *
 * @author Abhijit Kundu
 */

#include "ImportPLY.h"
#include "CuteGL/Core/VertexTraits.h"
#include "CuteGL/Core/MeshUtils.h"
#include "CuteGL/Geometry/ComputeNormals.h"

#include <array>
#include <tuple>

namespace CuteGL {


struct PlyElement {
  std::string element_type;
  std::size_t num_of_elements;
  std::vector<std::string> properties;
};

enum class PLYFormat {
  ASCII,
  BINARY_LITTLE_ENDIAN,
  BINARY_BIG_ENDIAN,
};

void removeCR(std::string& str) {
  str.erase(std::remove(str.begin(), str.end(), '\r'), str.end());
}

std::pair<PLYFormat, std::vector<PlyElement>> parsePLYheader(std::istream &istream) {
  {
    std::string magic_token;
    std::getline(istream, magic_token);
    removeCR(magic_token); // Remove CR (possible with Windows, since we are in binary mode)

    if (magic_token != "ply")
      throw std::runtime_error("Error: PLY magic keyword is wrong. Read token(" + magic_token + ").");
  }

  std::vector<PlyElement> ply_elements;
  PLYFormat format;

  for( std::string line; std::getline( istream, line ); ) {
    // Remove CR (possible with Windows, since we are in binary mode)
    removeCR(line);

    std::istringstream iss(line);

    std::string keyword;
    iss >> keyword;

    if (keyword == "format") {
      std::string format_token, version_token;
      iss >> format_token >> version_token;

      if (format_token == "ascii") {
        format = PLYFormat::ASCII;
      }
      else if (format_token == "binary_little_endian") {
        format = PLYFormat::BINARY_LITTLE_ENDIAN;
      }
      else if (format_token == "binary_big_endian") {
        format = PLYFormat::BINARY_BIG_ENDIAN;
      }
      else
        throw std::runtime_error("Error: Unknown PLY format: " + format_token);

      if (version_token != "1.0")
        throw std::runtime_error("Error: Only supports PLY v1.0");

    }
    else if (keyword == "element") {
      PlyElement ply_element;
      iss >> ply_element.element_type >> ply_element.num_of_elements;
      ply_elements.push_back(ply_element);

    }
    else if (keyword == "property") {
      // Add the property line string to the last element
      ply_elements.back().properties.push_back(line);
    }
    else if (keyword == "comment") {
      // this is comment line
      continue;
    }
    else if (keyword == "obj_info") {
      // This is treated as comment
      continue;
    }
    else if (keyword == "end_header") {
      // end of header should break now
      break;
    }
    else {
      throw std::runtime_error("Error: Unhandled keyword in PLY header");
    }
  }

  return std::make_pair(format, ply_elements);
}

enum class VertexAtrributes {
  // Position X, Y, Z
  X,  Y,  Z,
  // NORMALS NX, NY, NZ
  NX,  NY,  NZ,
  // Colors R, G, B, A
  R,  G,  B, A,
  // Normalized Colors R, G, B, A
  NR,  NG,  NB, NA,
  IGNORE,
};

std::ostream& operator<<(std::ostream& lhs, VertexAtrributes attr) {
  switch (attr) {
    case VertexAtrributes::X: lhs << "X"; break;
    case VertexAtrributes::Y: lhs << "Y"; break;
    case VertexAtrributes::Z: lhs << "Z"; break;
    case VertexAtrributes::NX: lhs << "NX"; break;
    case VertexAtrributes::NY: lhs << "NY"; break;
    case VertexAtrributes::NZ: lhs << "NZ"; break;
    case VertexAtrributes::NR: lhs << "NR"; break;
    case VertexAtrributes::NG: lhs << "NG"; break;
    case VertexAtrributes::NB: lhs << "NB"; break;
    case VertexAtrributes::NA: lhs << "NA"; break;
    case VertexAtrributes::R: lhs << "R"; break;
    case VertexAtrributes::G: lhs << "G"; break;
    case VertexAtrributes::B: lhs << "B"; break;
    case VertexAtrributes::A: lhs << "A"; break;
    case VertexAtrributes::IGNORE: lhs << "IGNORE"; break;
  }
  return lhs;
}

std::vector<VertexAtrributes> getVertexAtrributes(
    const std::vector<std::string>& vertex_properties) {
  std::vector<VertexAtrributes> attributes;
  attributes.reserve(vertex_properties.size());
  for (const std::string& prop : vertex_properties) {
    std::istringstream iss(prop);
    std::string ignore_token, prop_data_type_token, attr_token;
    iss >> ignore_token >> prop_data_type_token >> attr_token;

    assert(ignore_token == "property");
    if(attr_token == "x") {
      attributes.push_back(VertexAtrributes::X);
    }
    else if(attr_token == "y") {
      attributes.push_back(VertexAtrributes::Y);
    }
    else if(attr_token == "z") {
       attributes.push_back(VertexAtrributes::Z);
     }
    else if(attr_token == "nx") {
       attributes.push_back(VertexAtrributes::NX);
     }
    else if(attr_token == "ny") {
       attributes.push_back(VertexAtrributes::NY);
     }
    else if(attr_token == "nz") {
       attributes.push_back(VertexAtrributes::NZ);
     }
    else if(attr_token == "red") {
      if(prop_data_type_token == "uchar")
        attributes.push_back(VertexAtrributes::R);
      else
        attributes.push_back(VertexAtrributes::NR);
     }
    else if(attr_token == "green") {
      if(prop_data_type_token == "uchar")
        attributes.push_back(VertexAtrributes::G);
      else
        attributes.push_back(VertexAtrributes::NG);
     }
    else if(attr_token == "blue") {
      if(prop_data_type_token == "uchar")
        attributes.push_back(VertexAtrributes::B);
      else
        attributes.push_back(VertexAtrributes::NB);
     }
    else if (attr_token == "alpha") {
      if(prop_data_type_token == "uchar")
        attributes.push_back(VertexAtrributes::A);
      else
        attributes.push_back(VertexAtrributes::NA);
    }
    else {
      throw std::runtime_error("Error: PLY Unhandled Vertex Attribute");
     }
  }
  return attributes;
}

template<class VertexContainer>
std::tuple<bool, bool> readVertexData(std::istream &istream, VertexContainer& vertices,
                                      const std::size_t num_of_elements,
                                      const std::vector<std::string>& properties) {

  typedef typename VertexContainer::value_type VertexData;


  const std::vector<VertexAtrributes> attributes = getVertexAtrributes(properties);

  const bool has_color =  std::find(attributes.begin(), attributes.end(), VertexAtrributes::R) != attributes.end();
  const bool has_normal =  std::find(attributes.begin(), attributes.end(), VertexAtrributes::NX) != attributes.end();

  //// Debug print the attr list
  //for (const VertexAtrributes attr : attributes)
  //  std::cout << attr << " ";
  //std::cout << std::endl;


  vertices.resize(num_of_elements);
  for (std::size_t i = 0; i < num_of_elements; ++i) {
    std::string line;
    std::getline(istream, line);
    removeCR(line);
    std::istringstream iss(line);


    Eigen::Vector3f position(Eigen::Vector3f::Zero());
    Eigen::Vector3f normal(Eigen::Vector3f::Zero());
    Eigen::Matrix<unsigned char, 4, 1> color(128, 128, 128, 255);

    for (const VertexAtrributes attr : attributes) {
      switch (attr) {
        case VertexAtrributes::X:
          iss >> position.x();
          break;
        case VertexAtrributes::Y:
          iss >> position.y();
          break;
        case VertexAtrributes::Z:
          iss >> position.z();
          break;
        case VertexAtrributes::NX:
          iss >> normal.x();
          break;
        case VertexAtrributes::NY:
          iss >> normal.y();
          break;
        case VertexAtrributes::NZ:
          iss >> normal.z();
          break;
        case VertexAtrributes::R:
        {
          int r;
          iss >> r;
          color.x() = r;
          break;
        }
        case VertexAtrributes::G:
        {
          int b;
          iss >> b;
          color.y() = b;
          break;
        }
        case VertexAtrributes::B:
        {
          int b;
          iss >> b;
          color.z() = b;
          break;
        }
        case VertexAtrributes::A:
        {
          int a;
          iss >> a;
          color.w() = a;
          break;
        }
        case VertexAtrributes::NR:
        {
          float r;
          iss >> r;
          color.x() = (unsigned char)(255 * r);
          break;
        }
        case VertexAtrributes::NG:
        {
          float g;
          iss >> g;
          color.y() = (unsigned char)(255 * g);
          break;
        }
        case VertexAtrributes::NB:
        {
          float b;
          iss >> b;
          color.z() = (unsigned char)(255 * b);
          break;
        }
        case VertexAtrributes::NA:
        {
          float a;
          iss >> a;
          color.w() = (unsigned char)(255 * a);
          break;
        }
        case VertexAtrributes::IGNORE:
          double ignore;
          iss >> ignore;
          break;
      }
    }

    VertexData& vd = vertices[i];

    setVertexPosition(vd, position);

    if(has_normal) {
      setVertexNormal(vd, normal);
    }


    if(has_color) {
      setVertexColor(vd, color);
    }
  }

  return std::make_tuple(has_color, has_normal);
}

template<class VertexContainer>
std::tuple<bool, bool> readVerticesBinary(std::istream &istream,
                        VertexContainer& vertices,
                        const std::size_t num_of_elements,
                        const std::vector<std::string>& properties) {




  bool has_normal = false;
  bool has_color = false;
  bool has_alpha = false;

  std::size_t offset = 0;


  const std::vector<VertexAtrributes> attributes = getVertexAtrributes(properties);

  if(attributes.size() < 3) {
    throw std::runtime_error("Error: Expects atleast 3 attributes");
  }

  if (attributes[offset + 0] != VertexAtrributes::X ||
      attributes[offset + 1] != VertexAtrributes::Y ||
      attributes[offset + 2] != VertexAtrributes::Z) {
    throw std::runtime_error("Error: Expects the first 3 vertex atrributes to be XYZ");
  }
  offset += 3;


  if (attributes.size() >= (offset + 3) &&
      attributes[offset + 0] == VertexAtrributes::NX &&
      attributes[offset + 1] == VertexAtrributes::NY &&
      attributes[offset + 2] == VertexAtrributes::NZ) {
    has_normal = true;
    offset += 3;
  }

  if (attributes.size() >= (offset + 3) &&
      attributes[offset + 0] == VertexAtrributes::R &&
      attributes[offset + 1] == VertexAtrributes::G &&
      attributes[offset + 2] == VertexAtrributes::B) {
    has_color = true;
    offset += 3;
  }

  if (attributes.size() >= (offset + 1) &&
      attributes[offset + 0] == VertexAtrributes::A) {
    has_alpha = true;
    offset += 1;
  }

  if (offset != attributes.size()) {
    throw std::runtime_error("Error: PlY file probably has un-handled vertex attributes");
  }

  vertices.resize(num_of_elements);
  for (std::size_t i = 0; i < num_of_elements; ++i) {
    using VertexData = typename VertexContainer::value_type;
    VertexData& vd = vertices[i];

    Eigen::Vector3f position;
    istream.read((char *) position.data(), 3 * sizeof(float));
    setVertexPosition(vd, position);

    if(has_normal) {
      Eigen::Vector3f normal;
      istream.read((char *)normal.data(), 3 * sizeof(float));
      setVertexNormal(vd, normal);
    }


    if(has_color) {
      Eigen::Matrix<unsigned char, 4, 1> rgba (128, 128, 128, 255);
      istream.read((char *)rgba.data(), (has_alpha ? 4 : 3) * sizeof(unsigned char));
      setVertexColor(vd, rgba);
    }
  }

  return std::make_tuple(has_color, has_normal);
}

template<class Derived>
void readFaceData(std::istream &istream, Eigen::MatrixBase<Derived> const & faces_,
                  const std::size_t num_of_elements,
                  const std::vector<std::string>& properties) {
  using FacesContainer = Eigen::MatrixBase<Derived>;
  FacesContainer& faces = const_cast<FacesContainer& >(faces_);
  faces.derived().resize(num_of_elements, 3);

  for (std::size_t i = 0; i < num_of_elements; ++i) {
    std::string line;
    std::getline(istream, line);

    std::istringstream iss(line);

    int num_of_vertics;
    iss >> num_of_vertics;

    if(num_of_vertics != 3)
      throw std::runtime_error("Error: Only support triangles for face indices. Requested polygon type: " + std::to_string(num_of_vertics));

    iss >> faces(i, 0) >> faces(i, 1) >> faces(i, 2);
  }
}

template<class Derived>
void readFaceDataBinary(std::istream &istream, Eigen::MatrixBase<Derived> const & faces_,
                        const std::size_t num_of_elements,
                        const std::vector<std::string>& properties) {

  using FacesContainer = Eigen::MatrixBase<Derived>;
  FacesContainer& faces = const_cast<FacesContainer& >(faces_);
  faces.derived().resize(num_of_elements, 3);

  for (std::size_t i = 0; i < num_of_elements; ++i) {
    unsigned char num_of_vertics;
    istream.read((char *)&num_of_vertics, sizeof(unsigned char));

    if(num_of_vertics != 3)
      throw std::runtime_error("Error: Only support triangles for face indices. Requested polygon type: " + std::to_string(num_of_vertics));

    Eigen::Matrix<unsigned int, 1, 3> face;
    istream.read((char *)&face[0], 3 * sizeof(unsigned int));

    faces.row(i) = face;
  }
}

MeshData loadMeshFromPLY(const std::string& file_name) {
  MeshData mesh_data;

  std::ifstream file(file_name.c_str(), std::ios::binary | std::ios::in);
  if (!file.is_open()) {
    throw std::runtime_error("loadMeshFromPLY: Cannot open File from " + file_name);
  }

  PLYFormat format;
  std::vector<PlyElement> ply_elements;

  std::tie(format, ply_elements) = parsePLYheader(file);


  bool has_color(true);
  bool has_normal(true);

  if(format == PLYFormat::ASCII) {
    for (const PlyElement& ply_element : ply_elements) {
      if (ply_element.element_type == "vertex") {
        std::tie(has_color, has_normal) = readVertexData(
            file, mesh_data.vertices, ply_element.num_of_elements,
            ply_element.properties);

      }
      else if(ply_element.element_type == "face") {
        readFaceData(file, mesh_data.faces,
                     ply_element.num_of_elements,
                     ply_element.properties);

      }
      else {
        // Unhandled element type
        // just read the lines and ignore
        for( std::size_t i = 0; i < ply_element.num_of_elements; ++i ) {
          std::string line;
          std::getline( file, line );
        }
      }
    }
  }
  else {
    // Binary PLY

    const PlyElement& vertex_element = ply_elements.front();
    if(vertex_element.element_type != "vertex")
      throw std::runtime_error("loadMeshFromPLY: Binary PLY reading only support vertex as 1st element");

    std::tie(has_color, has_normal) = readVerticesBinary(file, mesh_data.vertices, vertex_element.num_of_elements, vertex_element.properties);

    const PlyElement& faces_element = ply_elements[1];
    if(faces_element.element_type != "face")
      throw std::runtime_error("loadMeshFromPLY: Binary PLY reading only support face as 2nd element");

    readFaceDataBinary(file, mesh_data.faces, faces_element.num_of_elements, faces_element.properties);
  }


  if(!has_normal) {
    computeNormals(mesh_data);
  }


  if(!has_color) {
    colorizeMesh(mesh_data, MeshData::ColorType(192, 192, 192, 255));
  }



  file.close();
  return mesh_data;
}

std::vector<VertexXYZRGBA> loadPointCloudFromPLY(const std::string& file_name) {
  std::ifstream file(file_name.c_str(), std::ios::binary | std::ios::in);
  if (!file.is_open()) {
    throw std::runtime_error("loadPointCloudFromPLY: Cannot open File from " + file_name);
  }

  PLYFormat format;
  std::vector<PlyElement> ply_elements;

  std::tie(format, ply_elements) = parsePLYheader(file);

  std::vector<VertexXYZRGBA> vertices;

  if(format == PLYFormat::ASCII) {
    for (const PlyElement& ply_element : ply_elements) {
      if (ply_element.element_type == "vertex") {
        readVertexData(file, vertices, ply_element.num_of_elements, ply_element.properties);

      } else {
        // Unhandled element type
        // just read the lines and ignore
        for (std::size_t i = 0; i < ply_element.num_of_elements; ++i) {
          std::string line;
          std::getline(file, line);
        }
      }
    }
  }
  else {
    // Binary PLY
    const PlyElement& vertex_element = ply_elements.front();
    if(vertex_element.element_type != "vertex")
      throw std::runtime_error("loadPointCloudFromPLY: Only support PLY with first element vertex");

    readVerticesBinary(file, vertices, vertex_element.num_of_elements, vertex_element.properties);
  }

  file.close();
  return vertices;
}

void saveMeshAsASCIIPly(const MeshData& mesh_data, const std::string& filename,
                        const bool save_normal, const bool save_color) {

  std::ofstream ofs(filename.c_str(), std::ios::binary | std::ios::out);
  if (!ofs.is_open())
    throw std::runtime_error("Cannot save at " + filename);

  const Eigen::Index num_of_faces = mesh_data.faces.rows();

  ofs << "ply" << '\n'
      << "format ascii 1.0" << '\n'
      << "element vertex "  << mesh_data.vertices.size() << '\n'
      << "property float x" << '\n'
      << "property float y" << '\n'
      << "property float z" << '\n';

  if(save_normal) {
    ofs << "property float nx" << '\n'
        << "property float ny" << '\n'
        << "property float nz" << '\n';
  }

  if(save_color) {
    ofs << "property uchar red" << '\n'
        << "property uchar green" << '\n'
        << "property uchar blue" << '\n';
  }

  ofs << "element face " << num_of_faces << '\n'
      << "property list uchar uint vertex_indices" << '\n'
      << "end_header" << std::endl;

  for (std::size_t i = 0; i < mesh_data.vertices.size(); ++i) {
    const MeshData::VertexData& vd = mesh_data.vertices[i];
    ofs << vd.position.x() << ' ' << vd.position.y() << ' ' << vd.position.z();

    if(save_normal) {
      ofs << ' ' << vd.normal.x() << ' ' << vd.normal.y() << ' ' << vd.normal.z();
    }

    if(save_color) {
      ofs << ' ' << (int)(vd.color.x())  << ' ' << (int)(vd.color.y()) << ' ' << (int)(vd.color.z());
    }

    ofs << '\n';
  }

  for (Eigen::Index i = 0; i < num_of_faces; ++i) {
    ofs << '3' << ' ' << mesh_data.faces(i, 0) << ' ' << mesh_data.faces(i, 1) << ' '
        << mesh_data.faces(i, 2) << '\n';
  }

  ofs.close();
}

void saveMeshAsBinPly(const MeshData& mesh_data, const std::string& filename,
                      const bool save_normal, const bool save_color) {

  std::ofstream ofs(filename.c_str(), std::ios::binary | std::ios::out);
  if (!ofs.is_open())
    throw std::runtime_error("Cannot save at " + filename);

  const Eigen::Index num_of_faces = mesh_data.faces.rows();

  ofs << "ply" << '\n'
      << "format binary_little_endian 1.0" << '\n'
      << "element vertex "  << mesh_data.vertices.size() << '\n'
      << "property float x" << '\n'
      << "property float y" << '\n'
      << "property float z" << '\n';

  if(save_normal) {
    ofs << "property float nx" << '\n'
        << "property float ny" << '\n'
        << "property float nz" << '\n';
  }

  if(save_color) {
    ofs << "property uchar red" << '\n'
        << "property uchar green" << '\n'
        << "property uchar blue" << '\n';
  }

  ofs << "element face " << num_of_faces << '\n'
      << "property list uchar uint vertex_indices" << '\n'
      << "end_header" << std::endl;

  for (std::size_t i = 0; i < mesh_data.vertices.size(); ++i) {
    const MeshData::VertexData& vd = mesh_data.vertices[i];

    Eigen::Vector3f position = vd.position.cast<float>();
    ofs.write(reinterpret_cast<char*>(position.data()), 3 * sizeof(float));

    if(save_normal) {
      Eigen::Vector3f normal = vd.normal.cast<float>();
      ofs.write(reinterpret_cast<char*>(normal.data()), 3 * sizeof(float));
    }

    if(save_color) {
      Eigen::Matrix<unsigned char, 3, 1> color = vd.color.head<3>();
      ofs.write(reinterpret_cast<char*>(color.data()), 3 * sizeof(unsigned char));
    }

  }

  for (Eigen::Index i = 0; i < num_of_faces; ++i) {
    const unsigned char primitive_vertices = 3;
    ofs.write(reinterpret_cast<const char*>(&primitive_vertices), sizeof(unsigned char));

    Eigen::Matrix<unsigned int, 1, 3> face = mesh_data.faces.row(i);
    ofs.write(reinterpret_cast<const char*>(face.data()), 3 * sizeof(unsigned int));
  }

  ofs.close();
}

void savePointCloudAsASCIIPly(const std::vector<VertexXYZRGBA>& vertices,
                              const std::string& filename) {

  std::ofstream ofs(filename.c_str(), std::ios::binary | std::ios::out);
  if (!ofs.is_open())
    throw std::runtime_error("Cannot save at " + filename);

  ofs << "ply"
      << '\n' << "format ascii 1.0"
      << '\n' << "element vertex " << vertices.size()
      << '\n' << "property float x"
      << '\n' << "property float y"
      << '\n' << "property float z"
      << '\n' << "property uchar red"
      << '\n' << "property uchar green"
      << '\n' << "property uchar blue"
      << '\n' << "end_header" << std::endl;

  for (std::size_t i = 0; i < vertices.size(); ++i) {
    const VertexXYZRGBA::PositionType& pos = vertices[i].position;
    const VertexXYZRGBA::ColorType& color = vertices[i].color;
    ofs << pos.x() << ' ' << pos.y() << ' ' << pos.z() << ' ';
    ofs << (int)color.z() << ' ' << (int)color.y() << ' ' << (int)color.x() << '\n';
  }
  ofs.close();
}

void savePointCloudAsBinPly(const std::vector<VertexXYZRGBA>& vertices,
                            const std::string& filename) {
  std::ofstream ofs(filename.c_str(), std::ios::binary | std::ios::out);
  if (!ofs.is_open())
    throw std::runtime_error("Cannot save at " + filename);

  ofs << "ply"
      << '\n' << "format binary_little_endian 1.0"
      << '\n' << "element vertex " << vertices.size()
      << '\n' << "property float x"
      << '\n' << "property float y"
      << '\n' << "property float z"
      << '\n' << "property uchar red"
      << '\n' << "property uchar green"
      << '\n' << "property uchar blue"
      << '\n' << "end_header" << std::endl;

  for (std::size_t i = 0; i < vertices.size(); ++i) {
    const Eigen::Vector3f& position = vertices[i].position;
    ofs.write(reinterpret_cast<const char*>(position.data()), 3 * sizeof(float));


    using ColorType = Eigen::Matrix<unsigned char, 3, 1>;
    const ColorType& bgr = vertices[i].color.head<3>();
    ColorType rgb(bgr[2], bgr[1], bgr[0]);
    ofs.write(reinterpret_cast<char*>(rgb.data()), 3 * sizeof(unsigned char));
  }
  ofs.close();
}

}  // namespace CuteGL



