/**
 * @file ImportOFF.cpp
 * @brief ImportOFF
 *
 * @author Abhijit Kundu
 */

#include "ImportOFF.h"
#include "CuteGL/Geometry/ComputeNormals.h"

namespace CuteGL {

MeshData loadMeshFromOFF(const std::string& file_name, const MeshData::ColorType& color) {
  MeshData mesh_data;
  importOFF(file_name, mesh_data.vertices, mesh_data.faces);

  for (MeshData::VertexData& vd : mesh_data.vertices)
    vd.color = color;

  // Compute Normals
  computeNormals(mesh_data);

  return mesh_data;
}

}  // namespace CuteGL


