/**
 * @file OffScreenProjection_Tests.cpp
 * @brief OffScreenProjection_Tests
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Surface/OffScreenRenderViewer.h"
#include "CuteGL/Renderer/MultiObjectRenderer.h"

#include <QGuiApplication>
#include <boost/test/unit_test.hpp>
#include <iostream>

using namespace CuteGL;

BOOST_AUTO_TEST_CASE(PerspectiveProjection_with_Points) {
  std::cout << "Running PerspectiveProjection_with_Points" << std::endl;
  srand((unsigned int) 42);
  int fake = 0;
  char **argv = nullptr;
  QGuiApplication app(fake, argv);

  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  QSurfaceFormat format = viewer.format();
  format.setSamples(1);
  viewer.setFormat(format);

  const int W = 1024;
  const int H = 768;
  viewer.resize(W, H);

  const float radius = 5.0f;

  Eigen::Vector3f cam_center;
  cam_center << Eigen::Vector2f::Random(), 0.2f;
  const Eigen::Isometry3f extrinsic = CuteGL::getExtrinsicsFromLookAt(cam_center.normalized() * radius,
                                                                      Eigen::Vector3f::Zero(),
                                                                      Eigen::Vector3f::UnitZ());

  viewer.camera().extrinsics() = extrinsic;


  float fx = W * radius / 2.0f;
  float fy = H * radius / 2.0f;

  Eigen::Vector2f principal_point(W/2.0f, H/2.0f);
  Eigen::Vector2f focal_length(fx, fy);


  Eigen::Matrix3f K;
  K << fx,  0, principal_point.x(),
        0, fy, principal_point.y(),
        0,  0,    1;

  Eigen::Matrix4f persp = CuteGL::getGLPerspectiveProjection(K, W, H, 0.5f, 10.0f);
  viewer.camera().intrinsics() = persp;


  viewer.create();
  viewer.makeCurrent();

  BOOST_CHECK(viewer.camera().intrinsics().matrix() ==  persp);
  BOOST_CHECK(viewer.camera().extrinsics().matrix() ==  extrinsic.matrix());

  using VertexData = PointCloudDrawer::VertexData;
  using PositionType = VertexData::PositionType;
  using ColorType = VertexData::ColorType;

  std::vector<VertexData> particles;
  {
    const std::size_t num_of_points_to_add = 1e3;
    particles.reserve(num_of_points_to_add);
    for (size_t i = 0; i < num_of_points_to_add; ++i) {
      particles.emplace_back(0.5f * PositionType::Random(), ColorType(255, 255, 255, 255));
    }
    renderer->pointCloudDrawers().addItem(Eigen::Affine3f::Identity(), particles);
  }



  viewer.render();

  QImage image = viewer.readColorBuffer();
  image.save(QString("test_color_buffer_persp.png"));

  {

    std::size_t num_of_points_checked = 0;

    for (const VertexData& vert : particles) {
      const VertexData::PositionType world_frame =  Eigen::Affine3f::Identity() * vert.position;
      Eigen::Vector2f img_proj = (K * extrinsic * world_frame).hnormalized();
      BOOST_CHECK(img_proj.x() >=  0);
      BOOST_CHECK(img_proj.y() >=  0);
      BOOST_CHECK(img_proj.x() <  W);
      BOOST_CHECK(img_proj.y() <  H);

      Eigen::Vector2i img_int_coord = img_proj.cast<int>();

      float max_diff = (img_proj - img_int_coord.cast<float>()).maxCoeff();
      float min_diff = (img_proj - img_int_coord.cast<float>()).minCoeff();

      const float tol = 0.002f;

      if(max_diff > (1.f - tol) || min_diff < tol)
        continue;

      const QRgb expected_color = qRgba(255, 255, 255, 255);
      const QRgb rendered_color = image.pixel(img_proj.x(), img_proj.y());

//      Eigen::Vector3i rgb(qRed(rendered_color), qGreen(rendered_color), qBlue(rendered_color));
//      const Eigen::IOFormat fmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
//      std::cout << world_frame.format(fmt)  << " --> " << img_proj.format(fmt)  << "  " << rgb.format(fmt) << "\n";

      BOOST_CHECK_EQUAL(rendered_color, expected_color);
      ++num_of_points_checked;
    }

    std::cout << "num_of_points_checked = " << num_of_points_checked  << "("<< num_of_points_checked * (100.0 / particles.size()) <<"%)\n";
  }

}

BOOST_AUTO_TEST_CASE(PerspectiveShiftedPrincipal_with_Points) {
  std::cout << "Running PerspectiveShiftedPrincipal_with_Points" << std::endl;
  int fake = 0;
  char **argv = nullptr;
  srand((unsigned int) 42);

  QGuiApplication app(fake, argv);

  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  QSurfaceFormat format = viewer.format();
  format.setSamples(1);
  viewer.setFormat(format);

  const int W = 1024;
  const int H = 768;
  viewer.resize(W, H);

  const float radius = 5.0f;

  Eigen::Vector3f cam_center;
  cam_center << Eigen::Vector2f::Random(), 0.2f;
  const Eigen::Isometry3f extrinsic = CuteGL::getExtrinsicsFromLookAt(cam_center.normalized() * radius,
                                                                      Eigen::Vector3f::Zero(),
                                                                      Eigen::Vector3f::UnitZ());

  viewer.camera().extrinsics() = extrinsic;


  float fx = W * radius / 2.0f;
  float fy = H * radius / 2.0f;

  Eigen::Vector2f principal_point(W/2.0f + 20.0, H/2.0f - 10.0f);
  Eigen::Vector2f focal_length(fx, fy);


  Eigen::Matrix3f K;
  K << fx,  0, principal_point.x(),
        0, fy, principal_point.y(),
        0,  0,    1;

  Eigen::Matrix4f persp = CuteGL::getGLPerspectiveProjection(K, W, H, 0.5f, 10.0f);
  viewer.camera().intrinsics() = persp;


  viewer.create();
  viewer.makeCurrent();

  BOOST_CHECK(viewer.camera().intrinsics().matrix() ==  persp);
  BOOST_CHECK(viewer.camera().extrinsics().matrix() ==  extrinsic.matrix());

  using VertexData = PointCloudDrawer::VertexData;
  using PositionType = VertexData::PositionType;
  using ColorType = VertexData::ColorType;

  std::vector<VertexData> particles;
  {
    const std::size_t num_of_points_to_add = 1e3;
    particles.reserve(num_of_points_to_add);
    for (size_t i = 0; i < num_of_points_to_add; ++i) {
      particles.emplace_back(0.5f * PositionType::Random(), ColorType(255, 255, 255, 255));
    }
    renderer->pointCloudDrawers().addItem(Eigen::Affine3f::Identity(), particles);
  }



  viewer.render();

  QImage image = viewer.readColorBuffer();
  image.save(QString("test_color_buffer_persp_shifted.png"));

  {

    std::size_t num_of_points_checked = 0;

    for (const VertexData& vert : particles) {
      const VertexData::PositionType world_frame =  Eigen::Affine3f::Identity() * vert.position;
      Eigen::Vector2f img_proj = (K * extrinsic * world_frame).hnormalized();
      BOOST_CHECK(img_proj.x() >=  0);
      BOOST_CHECK(img_proj.y() >=  0);
      BOOST_CHECK(img_proj.x() <  W);
      BOOST_CHECK(img_proj.y() <  H);

      Eigen::Vector2i img_int_coord = img_proj.cast<int>();

      float max_diff = (img_proj - img_int_coord.cast<float>()).maxCoeff();
      float min_diff = (img_proj - img_int_coord.cast<float>()).minCoeff();

      const float tol = 0.002f;

      if(max_diff > (1.f - tol) || min_diff < tol)
        continue;

      const QRgb expected_color = qRgba(255, 255, 255, 255);
      const QRgb rendered_color = image.pixel(img_proj.x(), img_proj.y());

//      Eigen::Vector3i rgb(qRed(rendered_color), qGreen(rendered_color), qBlue(rendered_color));
//      const Eigen::IOFormat fmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
//      std::cout << world_frame.format(fmt)  << " --> " << img_proj.format(fmt)  << "  " << rgb.format(fmt) << "\n";

      BOOST_CHECK_EQUAL(rendered_color, expected_color);
      ++num_of_points_checked;
    }

    std::cout << "num_of_points_checked = " << num_of_points_checked  << "("<< num_of_points_checked * (100.0 / particles.size()) <<"%)\n";
  }

}

BOOST_AUTO_TEST_CASE(OrthographicProjection_with_Points) {
  std::cout << "Running OrthographicProjection_with_Points" << std::endl;
  srand((unsigned int) 42);
  int fake = 0;
  char **argv = nullptr;
  QGuiApplication app(fake, argv);

  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  QSurfaceFormat format = viewer.format();
  format.setSamples(1);
  viewer.setFormat(format);

  const int W = 1024;
  const int H = 768;
  viewer.resize(W, H);

  const Eigen::Vector2f middle_point(W/2.0f, H/2.0f);
  const float radius = 5.0f;

  Eigen::Vector3f cam_center;
  cam_center << Eigen::Vector2f::Random(), 0.2f;
  const Eigen::Isometry3f extrinsic = CuteGL::getExtrinsicsFromLookAt(cam_center.normalized() * radius,
                                                                      Eigen::Vector3f::Zero(),
                                                                      Eigen::Vector3f::UnitZ());

  viewer.camera().extrinsics() = extrinsic;


  float fx = W * radius / 2.0f;
  float fy = H * radius / 2.0f;

  Eigen::Vector2f principal_point(W/2.0f, H/2.0f);
  Eigen::Vector2f focal_length(fx, fy);


  Eigen::Matrix3f K;
  K << fx,  0, principal_point.x(),
        0, fy, principal_point.y(),
        0,  0,    1;

  const Eigen::Matrix4f ortho = CuteGL::getGLOrthographicProjection(1.0f, 1.0f, 0.5f, 10.0f);
  viewer.camera().intrinsics() = ortho;


  viewer.create();
  viewer.makeCurrent();

  BOOST_CHECK(viewer.camera().intrinsics().matrix() ==  ortho);
  BOOST_CHECK(viewer.camera().extrinsics().matrix() ==  extrinsic.matrix());

  using VertexData = PointCloudDrawer::VertexData;
  using PositionType = VertexData::PositionType;
  using ColorType = VertexData::ColorType;


  const Eigen::Affine3f model_pose = Eigen::Affine3f::Identity();

  std::vector<VertexData> particles;
  {
    const std::size_t num_of_points_to_add = 1e3;
    particles.reserve(num_of_points_to_add);
    for (size_t i = 0; i < num_of_points_to_add; ++i) {
      particles.emplace_back(0.5f * PositionType::Random(), ColorType(255, 255, 255, 255));
    }
    renderer->pointCloudDrawers().addItem(model_pose, particles);
  }



  viewer.render();

  QImage image = viewer.readColorBuffer();
  image.save(QString("test_color_buffer_ortho.png"));

  {

    std::size_t num_of_points_checked = 0;

    for (const VertexData& vert : particles) {
      const Eigen::Vector3f cam_frame =  extrinsic * model_pose * vert.position;
      Eigen::Vector4f clip = ortho * cam_frame.homogeneous();
      Eigen::Vector3f ndc = clip.hnormalized();
      Eigen::Vector2f screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
      Eigen::Vector2f img_proj(screen.x(), H - screen.y());

      BOOST_CHECK(img_proj.x() >=  0);
      BOOST_CHECK(img_proj.y() >=  0);
      BOOST_CHECK(img_proj.x() <  W);
      BOOST_CHECK(img_proj.y() <  H);

      Eigen::Vector2i img_int_coord = img_proj.cast<int>();

      float max_diff = (img_proj - img_int_coord.cast<float>()).maxCoeff();
      float min_diff = (img_proj - img_int_coord.cast<float>()).minCoeff();

      const float tol = 0.002f;

      if(max_diff > (1.f - tol) || min_diff < tol)
        continue;

      const QRgb expected_color = qRgba(255, 255, 255, 255);
      const QRgb rendered_color = image.pixel(img_proj.x(), img_proj.y());

//      Eigen::Vector3i rgb(qRed(rendered_color), qGreen(rendered_color), qBlue(rendered_color));
//      const Eigen::IOFormat fmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
//      std::cout << world_frame.format(fmt)  << " --> " << img_proj.format(fmt)  << "  " << rgb.format(fmt) << "\n";

      BOOST_CHECK_EQUAL(rendered_color, expected_color);
      ++num_of_points_checked;
    }

    std::cout << "num_of_points_checked = " << num_of_points_checked  << "("<< num_of_points_checked * (100.0 / particles.size()) <<"%)\n";
  }

}
