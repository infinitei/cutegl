/**
 * @file ImportPLY_Tests.cpp
 * @brief ImportPLY_Tests
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/IO/ImportPLY.h"
#include "CuteGL/Geometry/ComputeNormals.h"
#include "CuteGL/Core/Config.h"
#include <boost/test/unit_test.hpp>
#include <cstdio>

using namespace CuteGL;

void checkEqualVertices(const std::vector<MeshData::VertexData>& verticesA,
                          const std::vector<MeshData::VertexData>& verticesB) {
  BOOST_REQUIRE_EQUAL(verticesA.size(), verticesB.size());

  const std::size_t num_of_vertices = verticesA.size();
  for (std::size_t i = 0; i < num_of_vertices; ++i) {
    const MeshData::VertexData& vA = verticesA[i];
    const MeshData::VertexData& vB = verticesB[i];

    BOOST_CHECK_EQUAL(vA.position, vB.position);
    BOOST_CHECK_EQUAL(vA.color.cast<int>(), vB.color.cast<int>());
    BOOST_CHECK_EQUAL(vA.normal, vB.normal);
  }
}

void checkEqualMeshes(const MeshData& meshA, const MeshData& meshB) {
  checkEqualVertices(meshA.vertices, meshB.vertices);
  BOOST_REQUIRE_EQUAL(meshA.faces.rows(), meshB.faces.rows());
  BOOST_CHECK_EQUAL(meshA.faces, meshB.faces);
}

BOOST_AUTO_TEST_CASE(ImportPLY_SimpleMesh) {

  MeshData mesh;

  mesh.vertices.resize(4);
  mesh.vertices[0].position = MeshData::PositionType(0.5f, -0.5f, 0.0f);
  mesh.vertices[0].color = MeshData::ColorType(255, 0, 0, 255);

  mesh.vertices[1].position = MeshData::PositionType(-0.5f, -0.5f, 0.0f);
  mesh.vertices[1].color = MeshData::ColorType(0, 255, 0, 255);

  mesh.vertices[2].position = MeshData::PositionType(0.0f,  0.5f, 0.0f);
  mesh.vertices[2].color = MeshData::ColorType(0, 0, 255, 255);

  mesh.vertices[3].position = MeshData::PositionType(-0.5f,  0.5f, 0.0f);
  mesh.vertices[3].color = MeshData::ColorType(0, 0, 255, 255);

  mesh.faces.resize(2, 3);
  mesh.faces << 0, 2, 1,  // 1st Triangle
                0, 3, 1;  // 2nd Triangle

  computeNormals(mesh);

  {
    saveMeshAsBinPly(mesh, "simple_mesh_bin.ply");
    MeshData mesh_in = loadMeshFromPLY("simple_mesh_bin.ply");
    checkEqualMeshes(mesh_in, mesh);
    std::remove("simple_mesh_bin.ply");
  }

  {
    saveMeshAsASCIIPly(mesh, "simple_mesh_txt.ply");
    MeshData mesh_in = loadMeshFromPLY("simple_mesh_txt.ply");
    checkEqualMeshes(mesh_in, mesh);
    std::remove("simple_mesh_txt.ply");
  }

}

BOOST_AUTO_TEST_CASE(ImportPLY_RandomMesh) {
  MeshData mesh;

  const unsigned int num_of_vertices = 1000;

  for (unsigned int i = 0; i < num_of_vertices; ++i) {
    MeshData::VertexData vd;
    vd.position = MeshData::PositionType::Random();
    vd.normal = MeshData::NormalType::Random();
    vd.color = MeshData::ColorType::Random();

  }

  mesh.faces.resize(50, 3);
  // The mesh create may not be a real mesh
  mesh.faces.setRandom();

  {
    saveMeshAsBinPly(mesh, "random_mesh_bin.ply");
    MeshData mesh_in = loadMeshFromPLY("random_mesh_bin.ply");
    checkEqualMeshes(mesh_in, mesh);
    std::remove("random_mesh_bin.ply");
  }

  {
    saveMeshAsASCIIPly(mesh, "random_mesh_txt.ply");
    MeshData mesh_in = loadMeshFromPLY("random_mesh_txt.ply");
    checkEqualMeshes(mesh_in, mesh);
    std::remove("random_mesh_txt.ply");
  }
}

BOOST_AUTO_TEST_CASE(ImportPLY_Axis) {
  MeshData mesh_bin = loadMeshFromPLY(CUTEGL_ASSETS_FOLDER "/Axis_bin.ply");
  MeshData mesh_txt = loadMeshFromPLY(CUTEGL_ASSETS_FOLDER "/Axis_ascii.ply");
  checkEqualVertices(mesh_bin.vertices, mesh_txt.vertices);
}
