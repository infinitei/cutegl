/**
 * @file SMPLRenderer_Tests.cpp
 * @brief SMPLRenderer_Tests
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Core/Config.h"
#include "CuteGL/Renderer/SMPLRenderer.h"
#include "CuteGL/Surface/OffScreenRenderViewer.h"
#include <boost/test/unit_test.hpp>
#include <QGuiApplication>
#include <QDebug>
#include <memory>
#include <iostream>

#include "TestsConfig.h"


template <class Derived>
void compare_with_expected_image(const Eigen::MatrixBase<Derived>& image, const QString& expected_image_path) {
  BOOST_CHECK_EQUAL(image.minCoeff(), 0);
  BOOST_CHECK_EQUAL(image.maxCoeff(), 24);

  QImage expected_image(expected_image_path);
  BOOST_REQUIRE(expected_image.isNull() == false);
  BOOST_CHECK_EQUAL(expected_image.width(), image.cols());
  BOOST_CHECK_EQUAL(expected_image.height(), image.rows());
  BOOST_CHECK_EQUAL(expected_image.format(), QImage::Format_Indexed8);

  for (int y = 0; y < image.rows(); ++y)
    for (int x = 0; x < image.cols(); ++x) {
      int cval = image(y, x);
      BOOST_CHECK_EQUAL(expected_image.pixelIndex(x, y), cval);
    }
}

template <class Derived>
void save_label_image(const Eigen::MatrixBase<Derived>& image, const QString& save_image_path) {

  QImage qimage(image.cols(), image.rows(), QImage::Format_Indexed8);
  QVector<QRgb> color_table;
  color_table << qRgb(0  ,   0,   0)
              << qRgb(45 , 115, 206)
              << qRgb(129, 206, 93 )
              << qRgb(253, 249, 216)
              << qRgb(144, 60 , 92 )
              << qRgb(79 , 114, 90 )
              << qRgb(54 , 162, 56 )
              << qRgb(197, 165, 219)
              << qRgb(185, 76 , 124)
              << qRgb(116, 231, 220)
              << qRgb(113, 182, 85 )
              << qRgb(14 , 59 , 88 )
              << qRgb(17 , 157, 175)
              << qRgb(62 , 254, 20 )
              << qRgb(49 , 91 , 87 )
              << qRgb(80 , 169, 207)
              << qRgb(56 , 168, 213)
              << qRgb(177, 241, 253)
              << qRgb(202, 32 , 111)
              << qRgb(5  , 221, 155)
              << qRgb(140, 49 , 46 )
              << qRgb(135, 185, 199)
              << qRgb(250, 186, 70 )
              << qRgb(105, 167, 64 )
              << qRgb(72 , 72 , 195);
  qimage.setColorTable(color_table);

  for (int y = 0; y < image.rows(); ++y)
    for (int x = 0; x < image.cols(); ++x) {
      uint8_t cval = image(y, x);
      qimage.setPixel(x, y, cval);
    }
  qimage.save(save_image_path);
}


BOOST_AUTO_TEST_CASE(SMPL_offscreen_labelmap_tests) {
  int fake = 0;
  char **argv = nullptr;
  QGuiApplication app(fake, argv);

  using namespace CuteGL;
  using Eigen::Vector3f;

  // Create the Renderer
  std::unique_ptr<SMPLRenderer> renderer(new SMPLRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  const int W = 640;
  const int H = 480;
  viewer.resize(W, H);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                           Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.create();
  viewer.makeCurrent();

  renderer->setSMPLData(CUTEGL_ASSETS_FOLDER"/smpl_neutral_lbs_10_207_0.h5",
                        CUTEGL_ASSETS_FOLDER"/smpl_vertex_segm24_col24_14.h5");



  using Image = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  using SMPLDrawerType = SMPLRenderer::SMPLDrawerType;
  using ShapeParam = Eigen::Matrix<SMPLDrawerType::PositionScalar, 10, 1>;
  using PoseParam = Eigen::Matrix<SMPLDrawerType::PositionScalar, 72, 1>;

  {
    viewer.render();
    Image buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    save_label_image(buffer, "smpl_label_0.png");
    compare_with_expected_image(buffer, TESTS_DATA_FOLDER_PATH"/smpl_label_0.png");
  }

  {
    ShapeParam shape;
    shape.setZero();
    shape[1] = -2.0f;
    shape[6] = -0.5;
    shape[7] = -0.5;
    shape[8] = -1.3;

    renderer->smplDrawer().shape() = shape;
    renderer->smplDrawer().updateShapeAndPose();

    viewer.render();
    Image buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    save_label_image(buffer, "smpl_label_1.png");
    compare_with_expected_image(buffer, TESTS_DATA_FOLDER_PATH"/smpl_label_1.png");
  }

  {
    ShapeParam shape;
    shape.setZero();
    shape[1] = -2.0f;
    shape[6] = -0.5;
    shape[7] = -0.5;
    shape[8] = -1.3;
    renderer->smplDrawer().shape() = shape;

    PoseParam pose;
    pose.setZero();
    pose[6] = 0.4f;
    pose[7] = 0.1f;
    pose[8] = -0.8f;
    pose[50] = -0.5f;
    renderer->smplDrawer().pose() = pose;

    renderer->smplDrawer().updateShapeAndPose();

    viewer.render();
    Image buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    save_label_image(buffer, "smpl_label_2.png");
    compare_with_expected_image(buffer, TESTS_DATA_FOLDER_PATH"/smpl_label_2.png");

  }

  {
    ShapeParam shape;
    shape.setZero();
    shape[1] = -2.0f;
    shape[6] = -0.5;
    shape[7] = -0.5;
    shape[8] = -1.3;
    renderer->smplDrawer().shape() = shape;

    PoseParam pose;
    pose.setZero();
    pose[15] = 0.4f;
    pose[16] = 0.1f;
    pose[17] = -0.8f;
    pose[62] = -0.5f;
    renderer->smplDrawer().pose() = pose;

    renderer->smplDrawer().updateShapeAndPose();

    viewer.render();
    Image buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    save_label_image(buffer, "smpl_label_3.png");
    compare_with_expected_image(buffer, TESTS_DATA_FOLDER_PATH"/smpl_label_3.png");
  }
}

BOOST_AUTO_TEST_CASE(SMPL_offscreen_labelmap_seq_tests) {
  int fake = 0;
  char **argv = nullptr;
  QGuiApplication app(fake, argv);

  using namespace CuteGL;
  using Eigen::Vector3f;

  // Create the Renderer
  std::unique_ptr<SMPLRenderer> renderer(new SMPLRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  const int W = 640;
  const int H = 480;
  viewer.resize(W, H);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                           Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.create();
  viewer.makeCurrent();

  renderer->setSMPLData(CUTEGL_ASSETS_FOLDER"/smpl_neutral_lbs_10_207_0.h5",
                        CUTEGL_ASSETS_FOLDER"/smpl_vertex_segm24_col24_14.h5");



  using Image = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  using SMPLDrawerType = SMPLRenderer::SMPLDrawerType;
  using ShapeParams = Eigen::Matrix<SMPLDrawerType::PositionScalar, 10, Eigen::Dynamic>;
  using PoseParams = Eigen::Matrix<SMPLDrawerType::PositionScalar, 72, Eigen::Dynamic>;

  ShapeParams shape_params(10, 4);
  PoseParams pose_params(72, 4);

  shape_params.setZero();
  pose_params.setZero();

  {
    shape_params(1, 1) = -2.0f;
    shape_params(6, 1) = -0.5f;
    shape_params(7, 1) = -0.5f;
    shape_params(8, 1) = -1.3f;
  }

  {
    shape_params(1, 2) = -2.0f;
    shape_params(6, 2) = -0.5f;
    shape_params(7, 2) = -0.5f;
    shape_params(8, 2) = -1.3f;

    pose_params(6 , 2) = 0.4f;
    pose_params(7 , 2) = 0.1f;
    pose_params(8 , 2) = -0.8f;
    pose_params(50, 2) = -0.5f;
  }

  {
    shape_params(1, 3) = -2.0f;
    shape_params(6, 3) = -0.5f;
    shape_params(7, 3) = -0.5f;
    shape_params(8, 3) = -1.3f;

    pose_params(15, 3) = 0.4f;
    pose_params(16, 3) = 0.1f;
    pose_params(17, 3) = -0.8f;
    pose_params(62, 3) = -0.5f;
  }

  std::vector<Image> rendered_images(4, Image(H, W));

  for (Eigen::Index i = 0; i < 4; ++i) {
    renderer->smplDrawer().shape() = shape_params.col(i);
    renderer->smplDrawer().pose() = pose_params.col(i);
    renderer->smplDrawer().updateShapeAndPose();
    viewer.render();
    viewer.readLabelBuffer(rendered_images[i].data());

  }

  for (Eigen::Index i = 0; i < 4; ++i) {
    rendered_images[i].colwise().reverseInPlace();
    QString image_name = QString("%1/smpl_label_%2.png").arg(TESTS_DATA_FOLDER_PATH, QString::number(i));
    compare_with_expected_image(rendered_images[i], image_name);
  }

  // Only shape update
  {
    renderer->smplDrawer().shape() = shape_params.col(0);
    renderer->smplDrawer().pose() = pose_params.col(0);
    renderer->smplDrawer().updateShapeAndPose();
    viewer.render();
    viewer.readLabelBuffer(rendered_images[0].data());

    renderer->smplDrawer().shape() = shape_params.col(1);
    renderer->smplDrawer().pose() = pose_params.col(1);
    renderer->smplDrawer().updateShape();
    viewer.render();
    viewer.readLabelBuffer(rendered_images[1].data());
  }

  // Only pose update
  {
    renderer->smplDrawer().shape() = shape_params.col(2);
    renderer->smplDrawer().pose() = pose_params.col(2);
    renderer->smplDrawer().updateShapeAndPose();
    viewer.render();
    viewer.readLabelBuffer(rendered_images[2].data());

    renderer->smplDrawer().shape() = shape_params.col(3);
    renderer->smplDrawer().pose() = pose_params.col(3);
    renderer->smplDrawer().updatePose();
    viewer.render();
    viewer.readLabelBuffer(rendered_images[3].data());
  }

  for (Eigen::Index i = 0; i < 4; ++i) {
    rendered_images[i].colwise().reverseInPlace();
    QString image_name = QString("%1/smpl_label_%2.png").arg(TESTS_DATA_FOLDER_PATH, QString::number(i));
    compare_with_expected_image(rendered_images[i], image_name);
  }
}
