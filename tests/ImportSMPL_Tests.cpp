/**
 * @file ImportSMPL_Tests.cpp
 * @brief ImportSMPL_Tests
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/IO/ImportSMPL.h"
#include "CuteGL/Core/Config.h"
#include <boost/test/unit_test.hpp>
#include <Eigen/Sparse>

using namespace CuteGL;



BOOST_AUTO_TEST_CASE(ImportSMPL_loadSMPLDataFromHDF5) {
  SMPLData32F32U smpl_data;
  loadSMPLDataFromHDF5(smpl_data, CUTEGL_ASSETS_FOLDER "/smpl_neutral_lbs_10_207_0.h5");

  int N = smpl_data.template_vertices.rows();
  int K = smpl_data.blend_weights.cols() - 1;

  BOOST_CHECK_EQUAL(N, 6890);
  BOOST_CHECK_EQUAL(K, 23);

  // parents K+1 vector
  BOOST_CHECK_EQUAL(smpl_data.parents.size(), K+1);

  // blend_weights N x K+1
  BOOST_CHECK_EQUAL(smpl_data.blend_weights.rows(), N);
  BOOST_CHECK_EQUAL(smpl_data.blend_weights.cols(), K+1);

  // joint_regressor K+1 x N
  BOOST_CHECK_EQUAL(smpl_data.joint_regressor.rows(), K+1);
  BOOST_CHECK_EQUAL(smpl_data.joint_regressor.cols(), N);

  // shape_displacements 10 x N x 3
  BOOST_CHECK_EQUAL(smpl_data.shape_displacements.dimension(1), N);
  BOOST_CHECK_EQUAL(smpl_data.shape_displacements.dimension(2), 3);

  // pose_displacements N x 3 x 9K
  BOOST_CHECK_EQUAL(smpl_data.pose_displacements.dimension(0), 9*K);
  BOOST_CHECK_EQUAL(smpl_data.pose_displacements.dimension(1), N);
  BOOST_CHECK_EQUAL(smpl_data.pose_displacements.dimension(2), 3);
}

BOOST_AUTO_TEST_CASE(ImportSMPL_check_sparsity) {
  SMPLData32F32U smpl_data;
  loadSMPLDataFromHDF5(smpl_data, CUTEGL_ASSETS_FOLDER "/smpl_neutral_lbs_10_207_0.h5");

  int N = smpl_data.template_vertices.rows();
  int K = smpl_data.blend_weights.cols() - 1;

  BOOST_CHECK_EQUAL(N, 6890);
  BOOST_CHECK_EQUAL(K, 23);

//  Eigen::Index nnz = (smpl_data.joint_regressor.array() != 0).count();
//  std::cout << "nnz = " << nnz << " (" << (nnz * 100.0f)/smpl_data.joint_regressor.size() << "%)\n";
//  Eigen::SparseMatrix<float> jr_sparse = smpl_data.joint_regressor.sparseView();
//  std::cout << "jr_sparse.nonZeros() = " << jr_sparse.nonZeros() << "\n";

  Eigen::VectorXi counts = (smpl_data.joint_regressor.array() != 0).rowwise().count();
  BOOST_CHECK_GT(counts.minCoeff(), 0);
  BOOST_CHECK_LE(counts.maxCoeff(), 32);
}
