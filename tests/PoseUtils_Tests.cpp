/**
 * @file PoseUtils_Tests.cpp
 * @brief PoseUtils_Tests
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Core/PoseUtils.h"
#include "CuteGL/Core/Config.h"
#include <Eigen/EulerAngles>
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(wrapToPi_Tests) {
  using namespace CuteGL;
  double tol = 1e-12;
  BOOST_CHECK_CLOSE(wrapToPi(0.0), 0.0, tol);
  BOOST_CHECK_CLOSE(wrapToPi(2 * M_PI), 0.0, tol);
  BOOST_CHECK_CLOSE(wrapToPi(-2 * M_PI), 0.0, tol);
  BOOST_CHECK_CLOSE(wrapToPi(-0.1), -0.1, tol);
  BOOST_CHECK_CLOSE(wrapToPi(0.1), 0.1, tol);
  BOOST_CHECK_CLOSE(wrapToPi(-1.5), -1.5, tol);
  BOOST_CHECK_CLOSE(wrapToPi(1.5), 1.5, tol);
  BOOST_CHECK_CLOSE(wrapToPi(-1.9), -1.9, tol);
  BOOST_CHECK_CLOSE(wrapToPi(1.9), 1.9, tol);

  BOOST_CHECK_CLOSE(wrapToPi(-M_PI), -M_PI, tol);
  BOOST_CHECK_CLOSE(wrapToPi(M_PI), -M_PI, tol);

  BOOST_CHECK_GE(wrapToPi(-M_PI), -M_PI);
  BOOST_CHECK_LT(wrapToPi(M_PI), M_PI);

  BOOST_CHECK_GT(wrapToPi(M_PI - 1e-14), 0);

  Eigen::Vector3f angles =  Eigen::Vector3f::Random();
  Eigen::Vector3f wrapped = angles.unaryExpr(WrapToPi());
  BOOST_CHECK_EQUAL(wrapped[0], wrapToPi(angles[0]));
  BOOST_CHECK_EQUAL(wrapped[1], wrapToPi(angles[1]));
  BOOST_CHECK_EQUAL(wrapped[2], wrapToPi(angles[2]));
}

template<typename Derived>
Eigen::Matrix<typename Derived::Scalar, 3, 3>
getSkewSymmetric(const Eigen::MatrixBase<Derived>& x) {
  using Scalar = typename Derived::Scalar;
  using Mat3 = Eigen::Matrix<Scalar, 3, 3>;
  Mat3 ss;
  ss <<    0, -x[2],  x[1],
        x[2],     0, -x[0],
       -x[1],  x[0],     0;
  return ss;
}

template<typename DerivedA, typename DerivedB>
Eigen::Matrix<typename DerivedA::Scalar, 3, 3>
rotationFromTwoVectors(const Eigen::MatrixBase<DerivedA>& a, const Eigen::MatrixBase<DerivedB>& b) {
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(DerivedA, 3);
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(DerivedB, 3);
  using Scalar = typename DerivedA::Scalar;
  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;
  using Mat3 = Eigen::Matrix<Scalar, 3, 3>;

  Vector3 au = a.normalized();
  Vector3 bu = b.normalized();
  Mat3 ssv = getSkewSymmetric(au.cross(bu));
  Scalar c = au.dot(bu);
  assert(c != -1.0);
  Mat3 R = Mat3::Identity() + ssv + ssv * ssv / (1 + c);
  return R;
}

BOOST_AUTO_TEST_CASE(rotationFromTwoVectors_impl) {
  std::size_t num_of_trials = 1000;

  for (std::size_t i = 0; i < num_of_trials; ++i) {
    Eigen::Vector3d a = Eigen::Vector3d::Random();
    Eigen::Vector3d b = Eigen::Vector3d::Random();

    Eigen::Quaterniond quat = Eigen::Quaterniond::FromTwoVectors(a, b);
    Eigen::Matrix3d R = rotationFromTwoVectors(a, b);

    BOOST_CHECK(R.isApprox(quat.toRotationMatrix()));
  }
}

BOOST_AUTO_TEST_CASE(euler_system_experiments) {
  using namespace Eigen;
  typedef EulerSystem<EULER_Z, EULER_Y, EULER_X> ViewPointSystem;
  typedef EulerAngles<double, ViewPointSystem> ViewPointd;

  BOOST_CHECK(ViewPointSystem::IsTaitBryan);

  std::size_t num_of_trials = 1000;
  for (std::size_t i = 0; i < num_of_trials; ++i) {
    Vector3d vp_angles = Vector3d::Random().cwiseProduct(Vector3d(M_PI, M_PI/2.0, M_PI));

    ViewPointd vp1(vp_angles);

    Matrix3d R;
    R = AngleAxisd(vp_angles.x(), Vector3d::UnitZ())
      * AngleAxisd(vp_angles.y(),  Vector3d::UnitY())
      * AngleAxisd(vp_angles.z(), Vector3d::UnitX());

    BOOST_CHECK(R.isApprox(vp1.toRotationMatrix()));

    ViewPointd vp2(R);
    BOOST_CHECK(vp2.isApprox(vp1));
  }
}

BOOST_AUTO_TEST_CASE(viewpoint_to_pose) {
  using namespace Eigen;
  std::size_t num_of_trials = 1000;
  for (std::size_t i = 0; i < num_of_trials; ++i) {
    Vector3d vp_angles = Vector3d::Random().cwiseProduct(Vector3d(M_PI, M_PI / 2.0, M_PI));

    const double distance = 3.0;

    Isometry3d vp_extrinsic = CuteGL::getExtrinsicsFromViewPoint(vp_angles.x(), vp_angles.y(), vp_angles.z(), distance);

    {
      // Method A
      const Vector3d eye = CuteGL::spherical2cartesian(vp_angles.x(), vp_angles.y(), distance);

      const Vector3d z = (Vector3d::Zero() - eye).normalized();
      const Vector3d x = ((-Vector3d::UnitZ()).cross(z)).normalized(); // minus upVector since yc is pointing down
      const Vector3d y = z.cross(x);
      Matrix3d rot = (Matrix3d() << x, y, z).finished();

      // Apply tilt
      rot *= Eigen::AngleAxisd(vp_angles.z(), Vector3d::UnitZ()).toRotationMatrix();

      Isometry3d extrinsic = Isometry3d::Identity();
      extrinsic.linear() = rot.transpose();
      extrinsic.translation() = -rot.transpose() * eye;
      BOOST_CHECK(vp_extrinsic.isApprox(extrinsic));
    }

    {
      // Method B
      Isometry3d vp_pose = Isometry3d::Identity();
      typedef EulerSystem<-EULER_Z, EULER_Y, -EULER_Z> ViewPointSystem;
      typedef EulerAngles<double, ViewPointSystem> ViewPointd;

      BOOST_CHECK(ViewPointSystem::IsTaitBryan == false);
      BOOST_CHECK(ViewPointSystem::IsBetaOpposite == false);

      ViewPointd vp(vp_angles.z() + M_PI / 2.0, vp_angles.y() + M_PI / 2.0, vp_angles.x());

      vp_pose.linear() = vp.toRotationMatrix();
      vp_pose.translation().z() = distance;
      BOOST_CHECK(vp_extrinsic.isApprox(vp_pose));
    }

    {
      // Method C

      Isometry3d model_pose = Isometry3d::Identity();
      Matrix3d model_rot;
      model_rot = AngleAxisd(-vp_angles.z() - M_PI / 2.0, Vector3d::UnitZ())
                * AngleAxisd(vp_angles.y() + M_PI / 2.0, Vector3d::UnitY())
                * AngleAxisd(-vp_angles.x(), Vector3d::UnitZ());

      model_pose.linear() = model_rot;
      model_pose.translation().z() = distance;
      BOOST_CHECK(vp_extrinsic.isApprox(model_pose));
    }

    {
      // Method D
      Isometry3d vp_pose = Isometry3d::Identity();
      vp_pose.linear() = CuteGL::getRotationFromViewPoint(vp_angles);
      vp_pose.translation().z() = distance;
      BOOST_CHECK(vp_extrinsic.isApprox(vp_pose));
    }
  }
}

BOOST_AUTO_TEST_CASE(rotation_matrix_to_viewpoint) {
  using namespace Eigen;
  using namespace CuteGL;
  std::size_t num_of_trials = 1000;
  for (std::size_t i = 0; i < num_of_trials; ++i) {
    const Vector3d vp_angles = Vector3d::Random().cwiseProduct(Vector3d(M_PI, M_PI / 2.0, M_PI));
    const Matrix3d rot_matrix = CuteGL::getRotationFromViewPoint(vp_angles);

    {
      Vector3d vp_estimate = getViewPointFromRotation(rot_matrix);
      BOOST_CHECK_GE(vp_estimate.x(), -M_PI);
      BOOST_CHECK_LT(vp_estimate.x(), M_PI);

      BOOST_CHECK_GE(vp_estimate.y(), -M_PI/2);
      BOOST_CHECK_LT(vp_estimate.y(), M_PI/2);

      BOOST_CHECK_GE(vp_estimate.z(), -M_PI);
      BOOST_CHECK_LT(vp_estimate.z(), M_PI);

      BOOST_CHECK(vp_angles.isApprox(vp_estimate));
    }

    {
      typedef EulerSystem<-EULER_Z, EULER_Y, -EULER_Z> ViewPointSystem;
      typedef EulerAngles<double, ViewPointSystem> ViewPointd;
      BOOST_CHECK(ViewPointSystem::IsTaitBryan == false);
      BOOST_CHECK(ViewPointSystem::IsAlphaOpposite == true);
      BOOST_CHECK(ViewPointSystem::IsBetaOpposite == false);
      BOOST_CHECK(ViewPointSystem::IsGammaOpposite == true);
      BOOST_CHECK(ViewPointSystem::IsEven == false);
      BOOST_CHECK(ViewPointSystem::IsOdd == true);

      BOOST_CHECK_EQUAL(ViewPointSystem::AlphaAxisAbs - 1, 2);
      BOOST_CHECK_EQUAL((ViewPointSystem::AlphaAxisAbs - 1 + 1 + ViewPointSystem::IsOdd)%3, 1);
      BOOST_CHECK_EQUAL((ViewPointSystem::AlphaAxisAbs - 1 + 2 - ViewPointSystem::IsOdd)%3, 0);


      ViewPointd vp(rot_matrix);
      BOOST_CHECK(rot_matrix.isApprox(vp.toRotationMatrix()));

      double azimuth = vp.angles().z();
      double elevation = vp.angles().y() - M_PI / 2;
      double tilt = wrapToPi(vp.angles().x() - M_PI / 2);

      BOOST_CHECK_GE(azimuth, -M_PI);
      BOOST_CHECK_LT(azimuth, M_PI);

      BOOST_CHECK_GE(elevation, -M_PI/2);
      BOOST_CHECK_LT(elevation, M_PI/2);

      BOOST_CHECK_GE(tilt, -M_PI);
      BOOST_CHECK_LT(tilt, M_PI);

      BOOST_CHECK(vp_angles.isApprox(Vector3d(azimuth, elevation, tilt)));
    }

    {
      const double plusMinus = -1;
      const double minusPlus =  1;

      Eigen::Index I = 2;
      Eigen::Index J = 1;
      Eigen::Index K = 0;

      Vector3d res;

      const double Rsum = std::sqrt((rot_matrix(I, J) * rot_matrix(I, J) + rot_matrix(I, K) * rot_matrix(I, K) + rot_matrix(J, I) * rot_matrix(J, I) + rot_matrix(K, I) * rot_matrix(K, I)) / 2);
      res[1] = std::atan2(Rsum, rot_matrix(I, I));

      // There is a singularity when sin(beta) == 0
      if(Rsum > 4 * std::numeric_limits<double>::epsilon()) {// sin(beta) != 0
        res[0] = std::atan2(rot_matrix(J, I), minusPlus * rot_matrix(K, I));
        res[2] = std::atan2(rot_matrix(I, J), plusMinus * rot_matrix(I, K));
      }
      else if(rot_matrix(I, I) > 0) {// sin(beta) == 0 and cos(beta) == 1
        double spos = plusMinus * rot_matrix(K, J) + minusPlus * rot_matrix(J, K); // 2*sin(alpha + gamma)
        double cpos = rot_matrix(J, J) + rot_matrix(K, K);                         // 2*cos(alpha + gamma)
        res[0] = std::atan2(spos, cpos);
        res[2] = 0;
      }
      else {// sin(beta) == 0 and cos(beta) == -1
        double sneg = plusMinus * rot_matrix(K, J) + plusMinus * rot_matrix(J, K); // 2*sin(alpha - gamma)
        double cneg = rot_matrix(J, J) - rot_matrix(K, K);                         // 2*cos(alpha - gamma)
        res[0] = std::atan2(sneg, cneg);
        res[2] = 0;
      }

      double azimuth = -res[2];
      double elevation = res[1] - M_PI/2 ;
      double tilt = wrapToPi(-res[0] - M_PI/2);

      BOOST_CHECK_GE(azimuth, -M_PI);
      BOOST_CHECK_LT(azimuth, M_PI);

      BOOST_CHECK_GE(elevation, -M_PI/2);
      BOOST_CHECK_LT(elevation, M_PI/2);

      BOOST_CHECK_GE(tilt, -M_PI);
      BOOST_CHECK_LT(tilt, M_PI);

      BOOST_CHECK(vp_angles.isApprox(Vector3d(azimuth, elevation, tilt)));
    }
  }


}
