/**
 * @file BatchSMPLRenderer_Tests.cpp
 * @brief BatchSMPLRenderer_Tests
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Core/Config.h"
#include "CuteGL/Renderer/BatchSMPLRenderer.h"
#include "CuteGL/Surface/OffScreenRenderViewer.h"
#include <boost/test/unit_test.hpp>
#include <QGuiApplication>
#include <QDebug>
#include <memory>
#include <iostream>

#include "TestsConfig.h"


using Image32FC1 = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
using Image8UC1 = Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

Image8UC1 loadImageViaQt(const QString& expected_image_path) {
  QImage expected_image(expected_image_path);
  BOOST_REQUIRE(expected_image.isNull() == false);
  BOOST_CHECK_EQUAL(expected_image.format(), QImage::Format_Indexed8);

  Image8UC1 image(expected_image.height(), expected_image.width());
  for (int y = 0; y < image.rows(); ++y)
    for (int x = 0; x < image.cols(); ++x) {
      image(y, x) = expected_image.pixelIndex(x, y);
    }
  return image;
}


std::vector<Image8UC1> load_expected_images() {
  std::vector<Image8UC1> expected_images;
  expected_images.reserve(4);
  expected_images.emplace_back(loadImageViaQt(TESTS_DATA_FOLDER_PATH"/smpl_label_0.png"));
  expected_images.emplace_back(loadImageViaQt(TESTS_DATA_FOLDER_PATH"/smpl_label_1.png"));
  expected_images.emplace_back(loadImageViaQt(TESTS_DATA_FOLDER_PATH"/smpl_label_2.png"));
  expected_images.emplace_back(loadImageViaQt(TESTS_DATA_FOLDER_PATH"/smpl_label_3.png"));
  return expected_images;
}




template <class Derived>
void compare_with_expected_image(const Eigen::MatrixBase<Derived>& image, const QString& expected_image_path) {
  BOOST_CHECK_EQUAL(image.minCoeff(), 0);
  BOOST_CHECK_EQUAL(image.maxCoeff(), 24);

  QImage expected_image(expected_image_path);
  BOOST_REQUIRE(expected_image.isNull() == false);
  BOOST_CHECK_EQUAL(expected_image.width(), image.cols());
  BOOST_CHECK_EQUAL(expected_image.height(), image.rows());
  BOOST_CHECK_EQUAL(expected_image.format(), QImage::Format_Indexed8);

  for (int y = 0; y < image.rows(); ++y)
    for (int x = 0; x < image.cols(); ++x) {
      int cval = image(y, x);
      BOOST_REQUIRE(expected_image.pixelIndex(x, y) == cval);
    }
}

template <class Derived>
void save_label_image(const Eigen::MatrixBase<Derived>& image, const QString& save_image_path) {

  QImage qimage(image.cols(), image.rows(), QImage::Format_Indexed8);
  QVector<QRgb> color_table;
  color_table << qRgb(0  ,   0,   0)
              << qRgb(45 , 115, 206)
              << qRgb(129, 206, 93 )
              << qRgb(253, 249, 216)
              << qRgb(144, 60 , 92 )
              << qRgb(79 , 114, 90 )
              << qRgb(54 , 162, 56 )
              << qRgb(197, 165, 219)
              << qRgb(185, 76 , 124)
              << qRgb(116, 231, 220)
              << qRgb(113, 182, 85 )
              << qRgb(14 , 59 , 88 )
              << qRgb(17 , 157, 175)
              << qRgb(62 , 254, 20 )
              << qRgb(49 , 91 , 87 )
              << qRgb(80 , 169, 207)
              << qRgb(56 , 168, 213)
              << qRgb(177, 241, 253)
              << qRgb(202, 32 , 111)
              << qRgb(5  , 221, 155)
              << qRgb(140, 49 , 46 )
              << qRgb(135, 185, 199)
              << qRgb(250, 186, 70 )
              << qRgb(105, 167, 64 )
              << qRgb(72 , 72 , 195);
  qimage.setColorTable(color_table);

  for (int y = 0; y < image.rows(); ++y)
    for (int x = 0; x < image.cols(); ++x) {
      uint8_t cval = image(y, x);
      qimage.setPixel(x, y, cval);
    }
  qimage.save(save_image_path);
}

// Sigleton groundtruth class
class GroundTruthData {
 public:
  std::vector<Image8UC1> expected_images;

  // Return a reference to a static sigleton instance
  static GroundTruthData& Instance() {
    static GroundTruthData instance;
    return instance;
  }

  // delete copy and move constructors and assign operators
  GroundTruthData(GroundTruthData const&) = delete;  // Copy construct
  GroundTruthData(GroundTruthData&&) = delete;       // Move construct
  GroundTruthData& operator=(GroundTruthData const&) = delete;  // Copy assign
  GroundTruthData& operator=(GroundTruthData&&) = delete;       // Move assign

 protected:
  GroundTruthData() {
    BOOST_TEST_MESSAGE("Setting up GroundTruthData");
    expected_images = load_expected_images();
  }

  ~GroundTruthData() { BOOST_TEST_MESSAGE("Tearing down GroundTruthData"); }
};

// BOOST_GLOBAL_FIXTURE(GroundTruthData)

BOOST_AUTO_TEST_CASE(BatchSMPL_offscreen_labelmap_tests) {
  int fake = 0;
  char **argv = nullptr;
  QGuiApplication app(fake, argv);

  using namespace CuteGL;
  using Eigen::Vector3f;

  // Create the Renderer
  std::unique_ptr<BatchSMPLRenderer> renderer(new BatchSMPLRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  const int W = 640;
  const int H = 480;
  viewer.resize(W, H);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                           Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.create();
  viewer.makeCurrent();

  renderer->setSMPLData(1,
                        CUTEGL_ASSETS_FOLDER"/smpl_neutral_lbs_10_207_0.h5",
                        CUTEGL_ASSETS_FOLDER"/smpl_vertex_segm24_col24_14.h5");

  using SMPLDrawerType = BatchSMPLRenderer::SMPLDrawerType;
  using ShapeParam = Eigen::Matrix<SMPLDrawerType::PositionScalar, 10, 1>;
  using PoseParam = Eigen::Matrix<SMPLDrawerType::PositionScalar, 72, 1>;

  const bool remap_vbos = true;

  {
    viewer.render();
    Image32FC1 buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    compare_with_expected_image(buffer, TESTS_DATA_FOLDER_PATH"/smpl_label_0.png");
  }

  {
    ShapeParam shape;
    shape.setZero();
    shape[1] = -2.0f;
    shape[6] = -0.5;
    shape[7] = -0.5;
    shape[8] = -1.3;
    renderer->smplDrawer().shape_params() = shape;

    PoseParam pose;
    pose.setZero();
    pose[15] = 0.4f;
    pose[16] = 0.1f;
    pose[17] = -0.8f;
    pose[62] = -0.5f;
    renderer->smplDrawer().pose_params() = pose;

    renderer->smplDrawer().updateShapeAndPose(remap_vbos);

    viewer.render();
    Image32FC1 buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    compare_with_expected_image(buffer, TESTS_DATA_FOLDER_PATH"/smpl_label_3.png");
  }

  {
    renderer->smplDrawer().shape_params().setZero();
    renderer->smplDrawer().pose_params().setZero();
    renderer->smplDrawer().updateShapeAndPose(remap_vbos);

    viewer.render();
    Image32FC1 buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    compare_with_expected_image(buffer, TESTS_DATA_FOLDER_PATH"/smpl_label_0.png");
  }

  {
    ShapeParam shape;
    shape.setZero();
    shape[1] = -2.0f;
    shape[6] = -0.5;
    shape[7] = -0.5;
    shape[8] = -1.3;
    renderer->smplDrawer().shape_params() = shape;

    renderer->smplDrawer().updateShape(remap_vbos);

    viewer.render();
    Image32FC1 buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    compare_with_expected_image(buffer, TESTS_DATA_FOLDER_PATH"/smpl_label_1.png");
  }

  {
    PoseParam pose;
    pose.setZero();
    pose[6] = 0.4f;
    pose[7] = 0.1f;
    pose[8] = -0.8f;
    pose[50] = -0.5f;
    renderer->smplDrawer().pose_params() = pose;

    renderer->smplDrawer().updatePose(remap_vbos);

    viewer.render();
    Image32FC1 buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    compare_with_expected_image(buffer, TESTS_DATA_FOLDER_PATH"/smpl_label_2.png");

  }

  {
    PoseParam pose;
    pose.setZero();
    pose[15] = 0.4f;
    pose[16] = 0.1f;
    pose[17] = -0.8f;
    pose[62] = -0.5f;
    renderer->smplDrawer().pose_params() = pose;

    renderer->smplDrawer().updatePose(remap_vbos);

    viewer.render();
    Image32FC1 buffer(H, W);
    viewer.readLabelBuffer(buffer.data());
    buffer.colwise().reverseInPlace();

    compare_with_expected_image(buffer, TESTS_DATA_FOLDER_PATH"/smpl_label_3.png");
  }
}

BOOST_AUTO_TEST_CASE(BatchSMPL_offscreen_labelmap_seq_tests) {
  int fake = 0;
  char **argv = nullptr;
  QGuiApplication app(fake, argv);

  using namespace CuteGL;
  using Eigen::Vector3f;

  // Create the Renderer
  std::unique_ptr<BatchSMPLRenderer> renderer(new BatchSMPLRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  const int batch_size = 4;

  const int W = 640;
  const int H = 480;
  viewer.resize(W, H);



  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                           Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.create();
  viewer.makeCurrent();

  renderer->setSMPLData(batch_size,
                        CUTEGL_ASSETS_FOLDER"/smpl_neutral_lbs_10_207_0.h5",
                        CUTEGL_ASSETS_FOLDER"/smpl_vertex_segm24_col24_14.h5");

  const bool remap_vbos = true;

  using SMPLDrawerType = BatchSMPLRenderer::SMPLDrawerType;
  using ShapeParams = Eigen::Matrix<SMPLDrawerType::PositionScalar, 10, Eigen::Dynamic>;
  using PoseParams = Eigen::Matrix<SMPLDrawerType::PositionScalar, 72, Eigen::Dynamic>;

  ShapeParams shape_params(10, batch_size);
  PoseParams pose_params(72, batch_size);

  shape_params.setZero();
  pose_params.setZero();

  {
    shape_params(1, 1) = -2.0f;
    shape_params(6, 1) = -0.5f;
    shape_params(7, 1) = -0.5f;
    shape_params(8, 1) = -1.3f;
  }

  {
    shape_params(1, 2) = -2.0f;
    shape_params(6, 2) = -0.5f;
    shape_params(7, 2) = -0.5f;
    shape_params(8, 2) = -1.3f;

    pose_params(6 , 2) = 0.4f;
    pose_params(7 , 2) = 0.1f;
    pose_params(8 , 2) = -0.8f;
    pose_params(50, 2) = -0.5f;
  }

  {
    shape_params(1, 3) = -2.0f;
    shape_params(6, 3) = -0.5f;
    shape_params(7, 3) = -0.5f;
    shape_params(8, 3) = -1.3f;

    pose_params(15, 3) = 0.4f;
    pose_params(16, 3) = 0.1f;
    pose_params(17, 3) = -0.8f;
    pose_params(62, 3) = -0.5f;
  }

  std::vector<Image8UC1>& expected_images = GroundTruthData::Instance().expected_images;
  BOOST_REQUIRE(expected_images.size() == batch_size);

  for (int j = 0; j < 10; ++j) {
    std::vector<Image32FC1> rendered_images(batch_size, Image32FC1(H, W));

    renderer->smplDrawer().shape_params() = shape_params;
    renderer->smplDrawer().pose_params() = pose_params;
    renderer->smplDrawer().updateShapeAndPose(remap_vbos);

    for (Eigen::Index i = 0; i < batch_size; ++i) {
      renderer->smplDrawer().batchId() = i;
      viewer.render();
      viewer.readLabelBuffer(rendered_images[i].data());
    }

    for (Eigen::Index i = 0; i < batch_size; ++i) {
      rendered_images[i].colwise().reverseInPlace();
      BOOST_CHECK_EQUAL(expected_images[i].rows(), rendered_images[i].rows());
      BOOST_CHECK_EQUAL(expected_images[i].cols(), rendered_images[i].cols());
      BOOST_CHECK_EQUAL(expected_images[i], rendered_images[i].cast<unsigned char>());
    }
  }
}

BOOST_AUTO_TEST_CASE(BatchSMPL_big_batch_readPixels) {
  int fake = 0;
  char **argv = nullptr;
  QGuiApplication app(fake, argv);

  static_assert(std::is_same<Eigen::Index, int>::value, "Need to set EIGEN_DEFAULT_DENSE_INDEX_TYPE to int");

  using namespace CuteGL;
  using Eigen::Vector3f;

  // Create the Renderer
  std::unique_ptr<BatchSMPLRenderer> renderer(new BatchSMPLRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  const int batch_size = 32;

  const int W = 640;
  const int H = 480;
  viewer.resize(W, H);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                           Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.create();
  viewer.makeCurrent();

  renderer->setSMPLData(batch_size,
                        CUTEGL_ASSETS_FOLDER"/smpl_neutral_lbs_10_207_0.h5",
                        CUTEGL_ASSETS_FOLDER"/smpl_vertex_segm24_col24_14.h5");


  std::vector<Image8UC1>& expected_images = GroundTruthData::Instance().expected_images;
  BOOST_REQUIRE(expected_images.size() == 4);

  const bool remap_vbos = true;

  using SMPLDrawerType = BatchSMPLRenderer::SMPLDrawerType;
  using ShapeParams = Eigen::Matrix<SMPLDrawerType::PositionScalar, 10, Eigen::Dynamic>;
  using PoseParams = Eigen::Matrix<SMPLDrawerType::PositionScalar, 72, Eigen::Dynamic>;

  ShapeParams shape_params(10, batch_size);
  PoseParams pose_params(72, batch_size);

  shape_params.setZero();
  pose_params.setZero();

  {
    shape_params(1, 1) = -2.0f;
    shape_params(6, 1) = -0.5f;
    shape_params(7, 1) = -0.5f;
    shape_params(8, 1) = -1.3f;
  }

  {
    shape_params(1, 2) = -2.0f;
    shape_params(6, 2) = -0.5f;
    shape_params(7, 2) = -0.5f;
    shape_params(8, 2) = -1.3f;

    pose_params(6 , 2) = 0.4f;
    pose_params(7 , 2) = 0.1f;
    pose_params(8 , 2) = -0.8f;
    pose_params(50, 2) = -0.5f;
  }

  {
    shape_params(1, 3) = -2.0f;
    shape_params(6, 3) = -0.5f;
    shape_params(7, 3) = -0.5f;
    shape_params(8, 3) = -1.3f;

    pose_params(15, 3) = 0.4f;
    pose_params(16, 3) = 0.1f;
    pose_params(17, 3) = -0.8f;
    pose_params(62, 3) = -0.5f;
  }

  const int num_of_trials = 10;
  for (int j = 0; j < num_of_trials; ++j) {
    std::vector<Image32FC1> rendered_images(batch_size, Image32FC1(H, W));

    renderer->smplDrawer().shape_params() = shape_params;
    renderer->smplDrawer().pose_params() = pose_params;
    renderer->smplDrawer().updateShapeAndPose(remap_vbos);

    for (Eigen::Index i = 0; i < batch_size; ++i) {
      renderer->smplDrawer().batchId() = i;
      viewer.render();
      viewer.readLabelBuffer(rendered_images[i].data());
    }

    for (Eigen::Index i = 0; i < batch_size; ++i) {
      rendered_images[i].colwise().reverseInPlace();
//      save_label_image(rendered_images[i], QString("smpl_batch_label_%1.png").arg(QString::number(i)));
      BOOST_CHECK_EQUAL(expected_images[i < 4 ? i : 0] , rendered_images[i].cast<unsigned char>());
    }
  }
}

BOOST_AUTO_TEST_CASE(BatchSMPL_big_batch_pbo) {
  int fake = 0;
  char **argv = nullptr;
  QGuiApplication app(fake, argv);

  static_assert(std::is_same<Eigen::Index, int>::value, "Need to set EIGEN_DEFAULT_DENSE_INDEX_TYPE to int");

  using namespace CuteGL;
  using Eigen::Vector3f;

  // Create the Renderer
  std::unique_ptr<BatchSMPLRenderer> renderer(new BatchSMPLRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  const int batch_size = 32;

  const int W = 640;
  const int H = 480;
  viewer.resize(W, H);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                           Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.create();
  viewer.makeCurrent();

  renderer->smplDrawer().engine().setCudaDeviceToCurrentGLDevice();

  renderer->setSMPLData(batch_size,
                        CUTEGL_ASSETS_FOLDER"/smpl_neutral_lbs_10_207_0.h5",
                        CUTEGL_ASSETS_FOLDER"/smpl_vertex_segm24_col24_14.h5");


  std::vector<Image8UC1>& expected_images = GroundTruthData::Instance().expected_images;
  BOOST_REQUIRE(expected_images.size() == 4);

  const bool remap_vbos = true;

  using SMPLDrawerType = BatchSMPLRenderer::SMPLDrawerType;
  using ShapeParams = Eigen::Matrix<SMPLDrawerType::PositionScalar, 10, Eigen::Dynamic>;
  using PoseParams = Eigen::Matrix<SMPLDrawerType::PositionScalar, 72, Eigen::Dynamic>;

  ShapeParams shape_params(10, batch_size);
  PoseParams pose_params(72, batch_size);

  shape_params.setZero();
  pose_params.setZero();

  {
    shape_params(1, 1) = -2.0f;
    shape_params(6, 1) = -0.5f;
    shape_params(7, 1) = -0.5f;
    shape_params(8, 1) = -1.3f;
  }

  {
    shape_params(1, 2) = -2.0f;
    shape_params(6, 2) = -0.5f;
    shape_params(7, 2) = -0.5f;
    shape_params(8, 2) = -1.3f;

    pose_params(6 , 2) = 0.4f;
    pose_params(7 , 2) = 0.1f;
    pose_params(8 , 2) = -0.8f;
    pose_params(50, 2) = -0.5f;
  }

  {
    shape_params(1, 3) = -2.0f;
    shape_params(6, 3) = -0.5f;
    shape_params(7, 3) = -0.5f;
    shape_params(8, 3) = -1.3f;

    pose_params(15, 3) = 0.4f;
    pose_params(16, 3) = 0.1f;
    pose_params(17, 3) = -0.8f;
    pose_params(62, 3) = -0.5f;
  }

  const size_t pbo_size = W * H * sizeof(float);

  // Create PBOS
  std::vector<GLuint> pbo_ids = createGLBuffers(viewer.glFuncs(), batch_size, GL_PIXEL_PACK_BUFFER, pbo_size, GL_DYNAMIC_READ);
  // register PBOS with CUDA
  std::vector<cudaGraphicsResource*> cuda_pbo_resources = createCudaGLBufferResources(pbo_ids, cudaGraphicsRegisterFlagsReadOnly);
  // get device pointers to the PBOS
  std::vector<float*> d_pbo_ptrs = createCudaGLBufferPointers<float>(cuda_pbo_resources, pbo_size);

  const int num_of_trials = 10;
  for (int j = 0; j < num_of_trials; ++j) {
    std::vector<Image32FC1> rendered_images(batch_size, Image32FC1(H, W));

    renderer->smplDrawer().shape_params() = shape_params;
    renderer->smplDrawer().pose_params() = pose_params;
    renderer->smplDrawer().updateShapeAndPose(remap_vbos);

    // Render to PBOs
    for (Eigen::Index i = 0; i < batch_size; ++i) {
      renderer->smplDrawer().batchId() = i;
      viewer.render();
      viewer.readLabelBuffer(GL_FLOAT, pbo_ids[i]);
    }
    viewer.glFuncs()->glFinish();

    // Copy from PBOS to CPU data
    {
      std::vector<cudaStream_t> streams(batch_size);
      for (int i = 0; i < batch_size; ++i) {
        CHECK_CUDA(cudaStreamCreate(&streams[i]));
        CHECK_CUDA(cudaMemcpyAsync(rendered_images[i].data(), d_pbo_ptrs[i], pbo_size, cudaMemcpyDeviceToHost, streams[i]));
        CHECK_CUDA(cudaStreamDestroy(streams[i]));
      }
    }

    for (Eigen::Index i = 0; i < batch_size; ++i) {
      rendered_images[i].colwise().reverseInPlace();
//      save_label_image(rendered_images[i], QString("smpl_batch_label_%1.png").arg(QString::number(i)));
      BOOST_CHECK_EQUAL(expected_images[i < 4 ? i : 0] , rendered_images[i].cast<unsigned char>());
    }
  }

  // Free Cuda resources and PBOS
  for (int p = 0; p < batch_size; ++p)
    CHECK_CUDA(cudaGraphicsUnregisterResource(cuda_pbo_resources[p]));
  viewer.glFuncs()->glDeleteBuffers(batch_size, pbo_ids.data());
}


