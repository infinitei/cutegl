/**
 * @file ComputeNormals_Tests.cpp
 * @brief ComputeNormals_Tests
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Geometry/ComputeNormals.h"

#include <boost/test/unit_test.hpp>

using namespace CuteGL;

BOOST_AUTO_TEST_CASE(ComputeNormals_on_Mesh) {

  MeshData md;

  md.vertices.resize(3);
  md.vertices[0].position = MeshData::PositionType(0.5f, -0.5f, 0.0f);
  md.vertices[0].color = MeshData::ColorType(255, 0, 0, 255);

  md.vertices[1].position = MeshData::PositionType(-0.5f, -0.5f, 0.0f);
  md.vertices[1].color = MeshData::ColorType(0, 255, 0, 255);

  md.vertices[2].position = MeshData::PositionType(0.0f,  0.5f, 0.0f);
  md.vertices[2].color = MeshData::ColorType(0, 0, 255, 255);

  md.faces.resize(1, 3);
  md.faces << 0, 2, 1;  // First Triangle

  computeNormals(md);

  for(const MeshData::VertexData& vd : md.vertices) {
    BOOST_CHECK(vd.normal.isApprox(Eigen::Vector3f::UnitZ()));
  }


}

