/**
 * @file CameraIntrinsics_Tests.cpp
 * @brief CameraIntrinsics_Tests
 *
 * @author Abhijit Kundu
 */

#include "CuteGL/Core/PoseUtils.h"

#include <boost/test/unit_test.hpp>

using namespace CuteGL;
using namespace Eigen;

BOOST_AUTO_TEST_CASE(PerspectiveProjection) {

  std::vector<Vector2i> image_sizes = {
      Vector2i(640, 480),
      Vector2i(1024, 768),
      Vector2i(1099, 1035)
  };

  std::vector<Vector2d> focal_lengths = {
      Vector2d(500.0, 500.0),
      Vector2d(500.0, 500.0),
      Vector2d(490.0, 520.0)
  };

  for (const Vector2i& WH : image_sizes) {
    const Vector2d middle_point = WH.cast<double>() / 2.0;

    for (const Vector2d& focal_length : focal_lengths) {
      for (int p = 0; p < 5; ++p) {

        const Vector2d principal_point = middle_point + 5.0 * Vector2d::Random();
        Matrix3d K;
        K << focal_length.x(), 0, principal_point.x(),
             0, focal_length.y(), principal_point.y(),
             0, 0, 1;

        const Matrix4d persp = getGLPerspectiveProjection(K, WH.x(), WH.y(), 0.5, 10.0);

        for (int i = 0; i < 100; ++i) {
          Vector3d cam_frame((Vector3d() << Vector2d::Random(), 5.0).finished());
          Vector4d clip = persp * cam_frame.homogeneous();
          Vector3d ndc = clip.hnormalized();
          Vector2d screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
          Vector2d window_coords(screen.x(), WH.y() - screen.y());

          Vector2d img_coords = (K * cam_frame).hnormalized();

          BOOST_CHECK(img_coords.isApprox(window_coords));
        }
      }
    }
  }
}
